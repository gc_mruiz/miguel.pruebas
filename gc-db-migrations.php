<?php
// Load main class and run
include_once(__DIR__ . '/include/core/GestyMVC.php');
GestyMVC::initialize();

// Initialize session
Session::init(false);

// Avoid unauthenticated access
if (Authentication::get('user', 'level') < 3 && (!get_var('token') || get_var('token') != GestyMVC::config('db_migrations_token'))) {
    header('Location: ./');
    exit();
}

// Initialize db migration model and table
$DbMigration = MySQLModel::getInstance('DbMigration');
$DbMigration->createTableIfNotExists();

// Retrieve all files and directories in directory
$migrationFiles = scandir(ROOT_PATH . '.db');

// Error
$error_found = false;

// Foreach file, process
foreach ($migrationFiles as $file) {
    // Skip default files
    if (!is_file(ROOT_PATH . '.db/' . $file) || in_array($file, array(
            '.',
            '..',
            '.htaccess',
        ))
    ) {
        continue;
    }
    
    // Output current progress
    echo $file . ' - ';
    flush();
    
    if (!$error_found) {
        if (!$DbMigration->count(array('where' => array('file' => $file)))) {
            // Current file error
            $error = false;
            
            // One line per query, add ignore foreign keys
            $queries = explode("\n", 'SET foreign_key_checks = 0;' . "\n" . 'SET sql_mode = \'NO_AUTO_VALUE_ON_ZERO\';' . "\n" . file_get_contents(ROOT_PATH . '.db/' . $file));
            $queries[] = 'DELIMITER ;';
            
            // Begin transaction
            $DbMigration->beginTransaction();
            
            // Query params
            $delimiter_not_default = false;
            $failed_query = null;
            $query = '';
            
            // Cleanup queries
            foreach ($queries as $query_) {
                $query .= ' ' . $query_;
                $query = trim($query);
                
                if (stripos($query, 'DELIMITER') !== false) {
                    $delimiter_query = str_ireplace('DELIMITER ;;', 'DELIMITER $$', $query);
                    if (stripos($delimiter_query, 'DELIMITER $$') !== false) {
                        $delimiter_not_default = true;
                    }
                    if (stripos($delimiter_query, 'DELIMITER ;') !== false) {
                        $delimiter_not_default = false;
                    }
                    unset($delimiter_query);
                }
                
                if (ends_with($query, ';') && !$delimiter_not_default) {
                    $queries2Execute = array();
                    
                    if (stripos($query, 'DELIMITER ;;') !== false) {
                        $query = str_ireplace('DELIMITER ;;', '', $query);
                        $query = str_ireplace('DELIMITER ;', '', $query);
                        
                        $queries2Execute = explode(';;', $query);
                    } else {
                        $query = str_ireplace('DELIMITER ;', '', $query);
                        $queries2Execute[] = $query;
                    }
                    
                    foreach ($queries2Execute as $q_) {
                        $q_ = to_efficient_encoding(trim($q_));
                        
                        if (strlen($q_) < 2) {
                            continue;
                        }
                        
                        if (@$DbMigration->query($q_) === false) {
                            $error = true;
                            $failed_query = $q_;
                            break;
                        }
                    }
                    
                    $query = '';
                    
                    if ($error) {
                        break;
                    }
                }
            }

            // Commit or rollback transaction (if there was an error no changes should be applied)
            if ($error) {
                // Retrieve error
                $mysql_error = $DbMigration->queryError();

                // Rollbakc
                $DbMigration->rollbackTransaction();
            } else {
                // No mysql error
                $mysql_error = null;

                // Commit
                $DbMigration->commitTransaction();
            }
            
            if ($error) {
                $error_found = true;
                echo '<strong style="color: red;">ERROR</strong>' . ($mysql_error ? ' - <span style="color: blue">' . $mysql_error . '</span>' : '') . ($failed_query ? ' - ' . $failed_query : '');
            } else {
                $DbMigration->addNew(array('file' => $file));
                echo '<strong>IMPORTED</strong>';
            }
        } else {
            echo '<strong style="color: yellow;">ALREADY IMPORTED</strong>';
        }
    } else {
        echo '<strong style="color: red;">SKIPPED</strong>';
    }
    
    echo '<br>';
    flush();
}

MySQLModel::closeConnection();