<?php

/**
 * Prints trace data in page
 *
 * @param $data object data to be traced
 * @param $exit object data to be traced
 * @param $production bool whether or not to show in production enviroment
 *
 * @return string text
 */
function gtrace($data = null, $exit = null, $production = null) {
    if (is_null($data)) {
        return false;
    }

    if (is_null($production) && GestyMVC::config('environment') == 'PRO') {
        $ip = getIp();
        $authorizedIps = array('217.126.178.175', '90.94.70.210');

        if (!in_array($ip, $authorizedIps)) {
            return false;
        }
    }

    $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1);

    $informationData = array(
        'Date' => date('d/m/Y H:i:s', strtotime(now())),
        'Called from file' => DIRECTORY_SEPARATOR . str_replace(ROOT_PATH, '', $backtrace[0]['file']) . ' in line ' . $backtrace[0]['line'],
        'Called from IP' => getIp(),
        // 'Called from browser' => getBrowser(),
    );

    if (is_object($data) || is_array($data)) {
        echo '<pre>';
        print_r(array('Information' => $informationData, 'Data' => $data));
        echo '</pre>' . '<br>' . PHP_EOL;
    } else {
        echo '<pre>';
        print_r($informationData);
        echo '</pre>';

        var_dump($data);
        echo '<br>' . PHP_EOL;
    }

    if (!is_null($exit)) {
        echo '<br>Stopping by request .......<hr>';
        exit;
    }

    return false;
}

/**
 * Gets IP client address
 *
 * @return null
 */
function getIp() {
    $ip = '0.0.0.0';
    $ip_keys = array(
        'HTTP_CLIENT_IP',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED',
        'HTTP_X_CLUSTER_CLIENT_IP',
        'HTTP_FORWARDED_FOR',
        'HTTP_FORWARDED',
        'REMOTE_ADDR',
    );

    foreach ($ip_keys as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                // trim for safety measures
                $ip = trim($ip);

                // attempt to validate IP
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
                    $ip_length = strlen($ip);
                    if ($ip_length == 4 || $ip_length == 16) {
                        $ip = inet_ntop(pack('A' . $ip_length, $ip));
                    }

                    break;
                }
            }
        }
    }

    return $ip;
}

function getMyCachedIp() {
    $ip = null;

    if (GestyMVC::config('AWS')) {
        $internal_ip = @file_get_contents('http://169.254.169.254/latest/meta-data/local-ipv4');
        $cache_filename = ROOT_PATH . '.cache/external-ip-' . $internal_ip . '.txt';
    } else {
        $cache_filename = ROOT_PATH . '.cache/external-ip-' . $_SERVER['SERVER_ADDR'] . '.txt';
    }

    // Try to retrieve IP
    if (file_exists($cache_filename)) {
        $ip =  @file_get_contents($cache_filename);
    }

    // If ip is not set, request
    if ($ip === null) {
        $ifconfig_result = @file_get_contents('https://ifconfig.co/ip');

        if (validateIPv4($ifconfig_result)) {
            $ip = trim($ifconfig_result);
            file_put_contents($cache_filename, $ip);
        }
    }

    return $ip;
}

function validateIPv4($IPv4) {
    $parts = explode('.', $IPv4);

    if (count($parts) !== 4) {
        return false;
    }

    foreach ($parts as $part) {
        if ($part > 0 || $part < 255) {
            return false;
        }
    }

    return true;
}

/**
 * Gets client browser information
 *
 * @return array
 */
function getBrowser() {
    $visitor_agent = $_SERVER['HTTP_USER_AGENT'];
    $browser_name = 'Unknown';
    $platform = 'Unknown';

    //First get the platform?
    if (preg_match('/linux/i', $visitor_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $visitor_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $visitor_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $visitor_agent) && !preg_match('/Opera/i', $visitor_agent)) {
        $browser_name = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $visitor_agent)) {
        $browser_name = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/Chrome/i', $visitor_agent)) {
        $browser_name = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $visitor_agent)) {
        $browser_name = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Opera/i', $visitor_agent)) {
        $browser_name = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Netscape/i', $visitor_agent)) {
        $browser_name = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $visitor_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($visitor_agent, "Version") < strripos($visitor_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if (empty(trim($version))) {
        $version = "?";
    }

    return array(
        'userAgent' => $visitor_agent,
        'name' => $browser_name,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern,
    );
}

function array_sort($array, $on, $order = 'SORT_ASC') {
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case 'SORT_ASC':
                asort($sortable_array);
                break;
            case 'SORT_DESC':
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}
