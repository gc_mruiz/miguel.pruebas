<?php

/**
 * App specific model class.
 * Current app models inherit this class instead of the original one. All specific modifications to the model class
 * must be done to this one instead.
 */
class AppMySQLModel extends MySQLModel {
    /**
     * All mysql tables require created and modified fields. If this is set to true, the table also requires
     * created_by_user_id and modified_by_user_id and both area populated automatically.
     *
     * Being set on AppMySQLModel, all models require these fields if not set otherwise on the model itself.
     *
     * @var bool
     */
    protected $user_timestamp_fields = true;
}
