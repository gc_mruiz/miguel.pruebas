<?php

/**
 * Class PicturesController.
 *
 * Handles a picture related HTTP request.
 */
class PicturesController extends AppController {
    /**
     * Controller name
     */
    public $name = 'Pictures';

    /**
     * @see Controller::$layout
     */
    protected $layout = 'popup';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
        'thumb' => -1,
    );

    /**
     * Handles picture listing.
     *
     * @param $picture_category_id int
     */
    public function adminIndex($picture_category_id = null) {
        // If accessed with $_GET instead of path variables, redirect
        if (get_var('picture_category_id')) {
            $this->redirect(array('action' => 'adminIndex', get_var('picture_category_id')));
        }

        // Load PictureCategory model and retrieve list for view filter select
        $PictureCategory = MySQLModel::getInstance('PictureCategory');
        $this->set('listOfPictureCategories', $PictureCategory->getList());

        // If category id was provided
        if ($picture_category_id) {
            // Retrieve category
            $pictureCategory = $PictureCategory->getById($picture_category_id, array(
                'fields' => array('id'),
                'recursive' => -1,
            ));

            // If picture category does not exist, redirect
            if (!$pictureCategory) {
                Session::set('pictures/admin-index/$picture_category_id', null);
                $this->redirect(array('action' => 'adminIndex'));
            }
        } else {
            // Retrieve from session
            $picture_category_id = Session::get('pictures/admin-index/$picture_category_id');

            if ($picture_category_id) {
                // Redirect
                $this->redirect(array('action' => 'adminIndex', $picture_category_id));
            } else {
                // If no id was provided, get the first one
                $pictureCategory = $PictureCategory->get(array('fields' => array('id'), 'recursive' => -1));

                // If there are no categories, add one
                if (!$pictureCategory) {
                    $pictureCategory = $PictureCategory->addNew(array('name' => 'General'));
                }

                // Redirect
                $this->redirect(array('action' => 'adminIndex', $pictureCategory['PictureCategory']['id']));
            }
        }

        // Save category for further use
        Session::set('pictures/admin-index/$picture_category_id', $picture_category_id);

        // Load Picture model
        $Picture = MySQLModel::getInstance('Picture');
        $Picture->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $Picture;
        $this->pagination['where'] = array('picture_category_id' => $picture_category_id);
        $this->pagination['elements_per_page'] = 100;
        $this->pagination['default_order_by_direction'] = 'DESC';

        // Retrieve elements
        $pictures = $this->paginate();

        // Set view vars
        $this->set(compact('pictures', 'picture_category_id'));
    }

    /**
     * Handles picture add request. Data is served via $_POST['picture']['Picture'].
     */
    public function adminAdd() {
        // Load category from session
        $this->set('picture', array('Picture' => array('picture_category_id' => Session::get('pictures/admin-index/$picture_category_id'))));

        // Load $PictureCategory model
        $PictureCategory = MySQLModel::getInstance('PictureCategory');

        // Load Picture model
        $Picture = MySQLModel::getInstance('Picture');

        // If post data was provided
        if (post_var('picture[Picture]')) {
            // Create new picture category if required
            if (post_var('new_picture_category_name')) {
                // Create new category
                $pictureCategory = $PictureCategory->addNew(array('name' => sql_escape($_POST['new_picture_category_name'])));

                if ($pictureCategory) {
                    // If saved, store id
                    $_POST['picture']['Picture']['picture_category_id'] = $pictureCategory['PictureCategory']['id'];
                } else {
                    // Error, set flash message
                    Session::set('flash[title]', __('Picture', true));
                    Session::set('flash[message]', sprintf(__('There was a problem creating %s new picture category. Please try again.', true), post_var('new_picture_category_name')));

                    $_POST['picture']['Picture']['picture_category_id'] = null;
                }
            }

            // Saved picture
            $saved_pictures = 0;

            // Continue only if picture category is set
            if (post_var('picture[Picture][picture_category_id]')) {
                // Add each picture
                foreach (files_var('file') as $file) {
                    // Try to add picture
                    if ($Picture->saveFile(post_var('picture[Picture][alt]'), $file, post_var('picture[Picture][scale]'), post_var('picture[Picture][picture_category_id]'))) {
                        $saved_pictures++;
                    }
                }
            } else {
                // Set category error
                $Picture->errors = array(__('Please select a category.', true));
            }

            // Check result
            if ($saved_pictures) {
                // Success, set flash message and redirect
                Session::set('flash[title]', __('Picture', true));
                Session::set('flash[message]', sprintf(__('%s pictures have been uploaded.', true), $saved_pictures));

                // Redirect
                $this->redirect(array('action' => 'adminIndex', post_var('picture[Picture][picture_category_id]')));
            } else {
                // Set flash message
                Session::set('flash[title]', __('Picture', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($Picture->errors) {
                    // Add all retrieved messages translated
                    foreach ($Picture->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Retrieve list of picture categories for view select
        $listOfPictureCategories = $PictureCategory->getList();
        $listOfPictureCategories[0] = __('+ add new category +', true);

        // Set view vars
        $this->set(compact('listOfPictureCategories'));
    }

    /**
     * Handles picture edit request. Data is served via $_POST['picture']['Picture']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load $PictureCategory model
        $PictureCategory = MySQLModel::getInstance('PictureCategory');

        // Load Picture model
        $Picture = MySQLModel::getInstance('Picture');

        // If post data was provided
        if (post_var('picture[Picture]')) {
            // Create new picture category if required
            if (post_var('new_picture_category_name')) {
                // Create new category
                $pictureCategory = $PictureCategory->addNew(array('name' => sql_escape($_POST['new_picture_category_name'])));

                if ($pictureCategory) {
                    // If saved, store id
                    $_POST['picture']['Picture']['picture_category_id'] = $pictureCategory['PictureCategory']['id'];
                } else {
                    // Error, set flash message
                    Session::set('flash[title]', __('Picture', true));
                    Session::set('flash[message]', sprintf(__('There was a problem creating %s new picture category. Please try again.', true), post_var('new_picture_category_name')));

                    $_POST['picture']['Picture']['picture_category_id'] = null;
                }
            }

            // Src field cannot be edited
            unset($_POST['picture']['Picture']['src']);

            // Saved picture
            $picture = null;

            // Continue only if picture category is set
            if (post_var('picture[Picture][picture_category_id]')) {
                // Try to update picture
                $picture = $Picture->updateFields($id, post_var('picture[Picture]'));
            } else {
                // Set category error
                $Picture->errors = array(__('Please select a category.', true));
            }

            // Check result
            if ($picture) {
                // Success, set flash message and redirect
                Session::set('flash[title]', __('Picture', true));
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex', $picture['Picture']['picture_category_id']));
            } else {
                // Set flash message
                Session::set('flash[title]', __('Picture', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($Picture->errors) {
                    // Add all retrieved messages translated
                    foreach ($Picture->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load picture
        $picture = $Picture->getById($id);

        // Check if there was any result
        if (empty($picture)) {
            $this->notFound();
        }

        // Retrieve list of picture categories for view select
        $listOfPictureCategories = $PictureCategory->getList();
        $listOfPictureCategories[0] = __('+ add new category +', true);

        // Set view vars
        $this->set(compact('picture', 'listOfPictureCategories'));
    }

    /**
     * Handles picture delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load Picture model
        $Picture = MySQLModel::getInstance('Picture');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load picture
        $picture = $Picture->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($picture)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('Picture', true));

        // Try to delete
        if ($Picture->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($Picture->errors) {
                // Add all retrieved messages translated
                foreach ($Picture->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex', $picture['Picture']['picture_category_id']));
        }
    }

    /**
     * Picture thumbs
     *
     * @param $size int
     * @param $id int
     * @param $token string
     */
    public function thumb($size, $id, $token) {
        if (in_array($size, array(
            320,
            640,
        ))) {
            // Extra memory for image processing
            ini_set('memory_limit', '512M');

            // Generate thumb URL
            $Picture = MySQLModel::getInstance('Picture');
            $thumb_url = $Picture->getThumbUrl($id, $size, $token);

            // Redirect to it permanently
            if ($thumb_url) {
                $this->redirect($thumb_url, 301);
            }
        }

        // No output
        $this->notFound(false);
    }
}