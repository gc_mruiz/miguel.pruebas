<?php

/**
 * Class ErrorsController.
 *
 * Displays HTTP errors
 */
class ErrorsController extends AppController {
    /**
     * Controller name
     */
    public $name = 'Errors';

    /**
     * Set the layout to use
     *
     * @var string
     */
    protected $layout = 'public';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => -1,
    );

    /**
     * Not found page.
     */
    public function notFound($redirectToUrl = true) {
        $this->layout = '404';
    }
}