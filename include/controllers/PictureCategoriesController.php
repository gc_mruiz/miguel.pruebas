<?php

/**
 * Class PictureCategoriesController.
 *
 * Handles a picture category related HTTP request.
 */
class PictureCategoriesController extends AppController {
    /**
     * Controller name
     */
    public $name = 'PictureCategories';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles pictureCategory listing.
     */
    public function adminIndex() {
        // Load PictureCategory model
        $PictureCategory = MySQLModel::getInstance('PictureCategory');
        $PictureCategory->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $PictureCategory;

        // Retrieve elements
        $pictureCategories = $this->paginate();

        // Set view vars
        $this->set(compact('pictureCategories'));
    }

    /**
     * Handles pictureCategory add request. Data is served via $_POST['pictureCategory']['PictureCategory'].
     */
    public function adminAdd() {
        // Load PictureCategory model
        $PictureCategory = MySQLModel::getInstance('PictureCategory');

        // If post data was provided
        if (post_var('pictureCategory[PictureCategory]')) {
            // Retrieve data from post
            $pictureCategoryData = post_var('pictureCategory[PictureCategory]');
            
            // Try to add picture category
            $pictureCategory = $PictureCategory->addNew($pictureCategoryData);

            // Check result
            if ($pictureCategory) {
                // Success, set flash message and redirect
                Session::set('flash[title]', __('Picture category', true));
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                Session::set('flash[title]', __('Picture category', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($PictureCategory->errors) {
                    // Add all retrieved messages translated
                    foreach ($PictureCategory->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }
    }

    /**
     * Handles pictureCategory edit request. Data is served via $_POST['pictureCategory']['PictureCategory']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load PictureCategory model
        $PictureCategory = MySQLModel::getInstance('PictureCategory');

        // If post data was provided
        if (post_var('pictureCategory[PictureCategory]')) {
            // Retrieve data from post
            $pictureCategoryData = post_var('pictureCategory[PictureCategory]');

            // Try to update pictureCategory
            $pictureCategory = $PictureCategory->updateFields($id, $pictureCategoryData);

            // Check result
            if ($pictureCategory) {
                // Success, set flash message and redirect
                Session::set('flash[title]', __('Picture category', true));
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                Session::set('flash[title]', __('Picture category', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($PictureCategory->errors) {
                    // Add all retrieved messages translated
                    foreach ($PictureCategory->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load pictureCategory
        $pictureCategory = $PictureCategory->getById($id);

        // Check if there was any result
        if (empty($pictureCategory)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('pictureCategory'));
    }

    /**
     * Handles pictureCategory delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load PictureCategory model
        $PictureCategory = MySQLModel::getInstance('PictureCategory');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load pictureCategory
        $pictureCategory = $PictureCategory->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($pictureCategory)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('Picture category', true));

        // Try to delete
        if ($PictureCategory->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($PictureCategory->errors) {
                // Add all retrieved messages translated
                foreach ($PictureCategory->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }
}