<?php

/**
 * Class FilesController.
 *
 * Handles a file related HTTP request.
 */
class FilesController extends AppController {
    /**
     * Controller name
     */
    public $name = 'Files';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles file listing.
     */
    public function adminIndex() {
        // Load File model
        $File = MySQLModel::getInstance('File');
        $File->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $File;
        $this->pagination['default_order_by_direction'] = 'DESC';

        // Retrieve elements
        $files = $this->paginate();

        // Set view vars
        $this->set(compact('files'));
    }

    /**
     * Handles file add request. Data is served via $_POST['file']['File'].
     */
    public function adminAdd() {
        // Load File model
        $File = MySQLModel::getInstance('File');

        // If post data was provided
        if (post_var('file[File]')) {
            // Saved file
            $saved_files = 0;

            // Add each file
            foreach (files_var('file') as $file) {
                // Try to add file
                if ($File->saveFile(post_var('file[File][alt]'), $file)) {
                    $saved_files++;
                }
            }

            // Check result
            if ($saved_files) {
                // Success, set flash message and redirect
                Session::set('flash[title]', __('File', true));
                Session::set('flash[message]', sprintf(__('%s files have been uploaded.', true), $saved_files));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                Session::set('flash[title]', __('File', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($File->errors) {
                    // Add all retrieved messages translated
                    foreach ($File->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }
    }

    /**
     * Handles file edit request. Data is served via $_POST['file']['File']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load File model
        $File = MySQLModel::getInstance('File');

        // If post data was provided
        if (post_var('file[File]')) {
            // Retrieve data from post
            $fileData = post_var('file[File]');
            
            // Src field cannot be edited
            unset($fileData['src']);

            // Saved file
            $file = null;

            // Try to update file
            $file = $File->updateFields($id, $fileData);

            // Check result
            if ($file) {
                // Success, set flash message and redirect
                Session::set('flash[title]', __('File', true));
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                Session::set('flash[title]', __('File', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($File->errors) {
                    // Add all retrieved messages translated
                    foreach ($File->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load file
        $file = $File->getById($id);

        // Check if there was any result
        if (empty($file)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('file'));
    }

    /**
     * Handles file delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load File model
        $File = MySQLModel::getInstance('File');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load file
        $file = $File->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($file)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('File', true));

        // Try to delete
        if ($File->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($File->errors) {
                // Add all retrieved messages translated
                foreach ($File->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }
}