<?php

/**
 * Class LayoutTextsController.
 *
 * Handles a layout texts related HTTP request.
 */
class LayoutTextsController extends AppController {
    /**
     * Controller name
     */
    public $name = 'LayoutTexts';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles layoutText listing.
     */
    public function adminIndex() {
        // Load LayoutText model
        $LayoutText = MySQLModel::getInstance('LayoutText');
        $LayoutText->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $LayoutText;

        // If excel download was requested, switch view
        if (get_var('excel')) {
            // Change layout and view
            $this->layout = 'excel';
            $this->view = 'admin-index-excel';

            // Set excel file name
            $this->set('filename_for_layout', 'layoutTexts.xls');
            $this->pagination['elements_per_page'] = 100000;

            // Ignore pages
            unset($_GET['p']);
        }

        // Retrieve elements
        $layoutTexts = $this->paginate();

        // Set view vars
        $this->set(compact('layoutTexts'));
    }

    /**
     * Handles layoutText edit request. Data is served via $_POST['layoutText']['layoutText']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load LayoutText model
        $LayoutText = MySQLModel::getInstance('LayoutText');

        // If post data was provided
        if (post_var('layoutText[LayoutText]')) {
            // Retrieve data from post
            $layoutTextData = post_var('layoutText[LayoutText]');

            // Try to update layoutText
            $layoutText = $LayoutText->updateFields($id, $layoutTextData);

            // Set flash title
            Session::set('flash[title]', __('Layout texts', true));

            // Check result
            if ($layoutText) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($LayoutText->errors) {
                    // Add all retrieved messages translated
                    foreach ($LayoutText->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load layoutText
        $layoutText = $LayoutText->getById($id);

        // Check if there was any result
        if (empty($layoutText)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('layoutText'));
    }
}