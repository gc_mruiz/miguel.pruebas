<?php

/**
 * Class PagesController.
 *
 * Handles static pages related HTTP request.
 */
class PagesController extends AppController {
    /**
     * Controller name
     */
    public $name = 'Pages';

    /**
     * Set the layout to use
     *
     * @var string
     */
    protected $layout = 'public';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => -1,
    );

    /**
     * Website home page.
     */
    public function index() {
        // Redirect page's home to user's home
        $this->redirect(array('controller' => 'Users', 'action' => 'index'));
    }

    /**
     * Admin redirection
     */
    public function admin() {
        // Redirect page's home to user's home
        $this->redirect(array('controller' => 'Users', 'action' => 'login'));
    }
}