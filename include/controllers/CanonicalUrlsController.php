<?php

/**
 * Class CanonicalUrlsController.
 *
 * Handles a canonical url related HTTP request.
 */
class CanonicalUrlsController extends AppController {
    /**
     * Controller name
     */
    public $name = 'CanonicalUrls';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles canonicalUrl listing.
     */
    public function adminIndex() {
        // Load CanonicalUrl model
        $CanonicalUrl = MySQLModel::getInstance('CanonicalUrl');
        $CanonicalUrl->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $CanonicalUrl;

        // Retrieve elements
        $canonicalUrls = $this->paginate();

        // Set view vars
        $this->set(compact('canonicalUrls'));
    }

    /**
     * Handles canonicalUrl add request. Data is served via $_POST['canonicalUrl']['CanonicalUrl'].
     */
    public function adminAdd() {
        // Load CanonicalUrl model
        $CanonicalUrl = MySQLModel::getInstance('CanonicalUrl');

        // If post data was provided
        if (post_var('canonicalUrl[CanonicalUrl]')) {
            // Retrieve data from post
            $canonicalUrlData = post_var('canonicalUrl[CanonicalUrl]');

            // Try to add canonicalUrl
            $canonicalUrl = $CanonicalUrl->addNew($canonicalUrlData);

            // Check result
            if ($canonicalUrl) {
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                Session::set('flash[title]', __('Canonical url', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($CanonicalUrl->errors) {
                    // Add all retrieved messages translated
                    foreach ($CanonicalUrl->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }
    }

    /**
     * Handles canonicalUrl edit request. Data is served via $_POST['canonicalUrl']['CanonicalUrl']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load CanonicalUrl model
        $CanonicalUrl = MySQLModel::getInstance('CanonicalUrl');

        // If post data was provided
        if (post_var('canonicalUrl[CanonicalUrl]')) {
            // Retrieve data from post
            $canonicalUrlData = post_var('canonicalUrl[CanonicalUrl]');
            
            // Try to update canonicalUrl
            $canonicalUrl = $CanonicalUrl->updateFields($id, $canonicalUrlData);

            // Check result
            if ($canonicalUrl) {
                // Success, set flash message and redirect
                Session::set('flash[title]', __('Canonical url', true));
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                Session::set('flash[title]', __('Canonical url', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($CanonicalUrl->errors) {
                    // Add all retrieved messages translated
                    foreach ($CanonicalUrl->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load canonicalUrl
        $canonicalUrl = $CanonicalUrl->getById($id);

        // Check if there was any result
        if (empty($canonicalUrl)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('canonicalUrl'));
    }

    /**
     * Handles canonicalUrl delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load CanonicalUrl model
        $CanonicalUrl = MySQLModel::getInstance('CanonicalUrl');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load canonicalUrl
        $canonicalUrl = $CanonicalUrl->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($canonicalUrl)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('Canonical url', true));

        // Try to delete
        if ($CanonicalUrl->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($CanonicalUrl->errors) {
                // Add all retrieved messages translated
                foreach ($CanonicalUrl->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }
}