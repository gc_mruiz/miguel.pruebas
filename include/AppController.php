<?php

/**
 * App specific controller class.
 * Current app controllers inherit this class instead of the original one. All specific modifications to the controller
 * class must be done to this one instead.
 */
class AppController extends Controller {
    /**
     * Before render callback. Called before the action's view is called.
     *
     * @return bool if not true stops execution
     */
    protected function beforeRender() {
        // Load layout-specific data
        if (in_array($this->layout, array(
            'public',
            'json',
            'excel',
            'admin',
        ))) {
            $this->loadUserLevelTranslations();
        }

        // Disable SEO for non-html results
        if (in_array($this->layout, array(
            'json',
            'excel',
        ))) {
            $this->seo = false;
        }

        // Delegate aborting on the parent call
        return parent::beforeRender();
    }

    /**
     * User levels are not on the database. Set the vars so they can be used.
     *
     * @return void
     */
    private function loadUserLevelTranslations() {
        $this->set('userLevels', array(
            __('User', true),
            __('Advanced User', true),
            __('Admin', true),
            __('Super Admin', true),
        ));
    }
}