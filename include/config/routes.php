<?php
/**
 * Route definitions.
 * Attention: parsing is made in the opposite order as listed here.
 */


// Check whether the lib is in a module configuration
$modules = self::config('modules');

if (!in_array('otp', $modules)) {
    // Default
    Router::addRoute(':controller/:action/*', array());

    // Home
    Router::addRoute('', array(
        'controller' => 'Pages',
        'action' => 'index',
    ));

    // Admin
    Router::addRoute('admin', array(
        'controller' => 'Users',
        'action' => 'login',
    ));
}
