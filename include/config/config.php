<?php
// GestyMVC::addModule('module-name');
// GestyMVC::addModule('acl');
GestyMVC::addModule('otp');

GestyMVC::setConfig('debug', array(
    'force_for_all' => false,
    'forceFromIps' => false,
    'onlyFromIps' => false,
    'host_exact_match' => false,
    'host_pattern' => ':81',
));

GestyMVC::setConfig('langs', array('es', 'en'));
GestyMVC::setConfig('default_lang', 'es');
GestyMVC::setConfig('use_prefix_for_default_lang', false);

GestyMVC::setConfig('time_zone', 'Europe/Madrid');

GestyMVC::setConfig('disable_https_on_background_tasks', true);
GestyMVC::setConfig('user_digest_token', '674f857c3c0ddf98b878830ed0853db7');
GestyMVC::setConfig('session_digest_token', '7784fcf7f3db7371491b5905ef516e47');
GestyMVC::setConfig('form_helper_token', '9cbd75dfd575bbac74b24a6a1678a533');
GestyMVC::setConfig('db_migrations_token', 'a6f85c8f41655120bdea6167ad856668');
GestyMVC::setConfig('max_login_failed_attempts', 3);

GestyMVC::setConfig('password_valid_for_minutes', 129600); // 90 days
GestyMVC::setConfig('password_history_days', 90);
GestyMVC::setConfig('password_reset_validity_minutes', 30);
GestyMVC::setConfig('password_min_length', 8);
GestyMVC::setConfig('password_requires_letters', true);
GestyMVC::setConfig('password_requires_numbers', true);
GestyMVC::setConfig('password_requires_both_cases', true);
GestyMVC::setConfig('password_reset_min_interval_minutes', 60);

GestyMVC::setConfig('mysql_log_all_queries_on_debug', true);

GestyMVC::setConfig('production_no_session_host_name', false); // static.mysite.com
GestyMVC::setConfig('development_no_session_host_name', 'static.localhost:81');

GestyMVC::setConfig('x_frame_options', 'SAMEORIGIN');

GestyMVC::setConfig('db', array(
    'dev' => array(
        'host' => 'localhost',
        'user' => 'root',
        'password' => 'root',
        'schema' => 'sareb_otp',
        'table_prefix' => '',
    ),
    'pro' => array(
        'host' => '',
        'user' => '',
        'password' => '',
        'schema' => '',
        'table_prefix' => '',
    ),
));

GestyMVC::setConfig('mail', array(
    'mail_function' => false,
    'host' => 'smtp.gmail.com',
    'user' => 'web@gestycontrol.com',
    'password' => 'N0R3plYW3b',
    'port' => 465,
    'ssl' => true,
    'timeout' => 30,
    'email' => 'web@gestycontrol.com',
    'display' => 'Sareb OTP',
));

GestyMVC::setConfig('dev_js_folder', 'static/js');
GestyMVC::setConfig('pro_js_folder', 'static/js.min');
GestyMVC::setConfig('dev_css_folder', 'static/css');
GestyMVC::setConfig('pro_css_folder', 'static/css.min');

GestyMVC::setConfig('staticRootFolders', array('content', 'static'));

GestyMVC::setConfig('static_cache_ts', floor(time() / 60 / 60 / 24));
GestyMVC::setConfig('cms_tags', '<div><br><b><p><u><ul><li><ol><a><strong><em><i><img><h1><h2><h3><h4><h5><h6><span><table><tr><td><th>');
GestyMVC::setConfig('pagination_accumulate_orders', true);