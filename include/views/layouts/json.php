<?php
if ($content_for_layout) {
    $resultForLayout['response']['html'] = $content_for_layout;
}

echo json_encode_safe($resultForLayout);
