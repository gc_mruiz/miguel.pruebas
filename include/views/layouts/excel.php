<?php

if (mb_detect_encoding($content_for_layout) == 'UTF-8') {
    $content_for_layout = utf8_decode($content_for_layout);
}

ob_start();
?>
<html>
    <body>
        <?php echo $content_for_layout
        ?>
    </body>
</html>
<?php

$content_for_layout = ob_get_clean();

if (true) {
    // Content type header
    header('Content-Type: application/force-download');
    header('Content-Length: ' . strlen($content_for_layout));
    header('Content-Disposition: attachment; filename=' . $filename_for_layout);
}

echo $content_for_layout;
