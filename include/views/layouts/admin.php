<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
        <script>if(window.innerWidth < 800) { document.getElementById('viewport').setAttribute('content', 'width=800'); }</script>

        <?php if(!isset($seo_title)) $seo_title = 'Sareb OTP'; ?><title><?php echo utf8html($seo_title) ?></title>
        <?php if(isset($seo_description) && $seo_description): ?><meta name="description" content="<?php echo utf8html($seo_description) ?>" /><?php endif ?>
        <?php if(isset($seo_title) && $seo_title): ?><meta property="og:title" content="<?php echo utf8html($seo_title) ?>" /><?php endif ?>
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="<?php echo utf8html('Sareb OTP') ?>" />
        <?php if(isset($seo_description) && $seo_description): ?><meta property="og:description" content="<?php echo utf8html($seo_description) ?>" /><?php endif ?>
        <?php if(isset($og_image) && $og_image): ?><meta property="og:image" content="<?php echo utf8html($og_image) ?>" /><?php endif ?>
        <?php if(isset($canonical_url) && $canonical_url): ?><link rel="canonical" href="<?php echo utf8html($canonical_url) ?>" /><?php endif ?>

        <?php echo HtmlHelper::favicon('/favicon.ico') ?>
        <?php echo HtmlHelper::css('css.css') ?>
        <?php echo FormHelper::translationStyles() ?>

        <script type="text/javascript">
            var LANG = <?php echo json_encode_safe(LANG) ?>;
            var ROOT_URL = <?php echo json_encode_safe(ROOT_URL) ?>;
            var LOCALED_ROOT_URL = <?php echo json_encode_safe(LOCALED_ROOT_URL) ?>;
            var JS_FOLDER = <?php echo json_encode_safe(JS_FOLDER) ?>;
        </script>

        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/bootstrap.min.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/jquery-ui-1.12.0/jquery-ui.min.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/jquery-ui-1.12.0/datetimepicker/jquery.datetimepicker.min.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/font-awesome/css/font-awesome.css', array('no_session' => false)) ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/plugins/morris/morris-0.4.3.min.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/js/plugins/gritter/jquery.gritter.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/animate.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/style.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/plugins/summernote/summernote.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/plugins/summernote/summernote-bs3.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/plugins/dataTables/dataTables.bootstrap.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/plugins/dataTables/dataTables.responsive.css') ?>
    </head>
    <body class="pace-done <?php if(!Session::get('layout[nav_deployed]')) echo 'mini-navbar' ?>">
        <div class="pace  pace-inactive">
            <div class="pace-progress" data-progress-text="100%" data-progress="99">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div>
        </div>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <span> <img alt="image" class="img-circle" src="https://secure.gravatar.com/avatar/<?php echo md5(strtolower(trim(Authentication::get('user', 'email')))) ?>?s=60"> </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo Authentication::get('user', 'email') ?></strong> </span> <span class="text-muted text-xs block"><?php echo $userLevels[Authentication::get('user', 'level')] ?><b class="caret"></b></span> </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li>
                                        <?php echo HtmlHelper::link(__('Change password', true), array('controller' => 'Users', 'action' => 'changePassword')) ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?php echo HtmlHelper::link(__('Logout', true), array('controller' => 'Users', 'action' => 'logout')) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                GC
                            </div>
                        </li>
                        <?php
                        echo '<li>' . HtmlHelper::link('<i class="fa fa-home"></i><span class="nav-label">' . __('Home', true) . '</span>', array('controller' => 'Pages', 'action' => 'index'), array('escape' => false)) . '</li>';
                        if (Authentication::get('user', 'level') >= 3) {
                            echo '<li>' . HtmlHelper::link('<i class="fa fa-database"></i><span class="nav-label">' . __('Database', true) . '</span>', '/gc-adminer.php', array('escape' => false, 'target' => '_blank')) . '</li>';
                        }
                        if (Authentication::get('user', 'level') >= 2) {
                            echo '<li>' . HtmlHelper::link('<i class="fa fa-user"></i><span class="nav-label">' . __('Users', true) . '</span>', array('controller' => 'Users', 'action' => 'adminIndex'), array('escape' => false)) . '</li>';
                            echo '<li>' . HtmlHelper::link('<i class="fa fa-users"></i><span class="nav-label">' . __('Users groups', true) . '</span>', array('module' => 'acl', 'controller' => 'UserGroups', 'action' => 'adminIndex'), array('escape' => false)) . '</li>';
                            echo '<li>' . HtmlHelper::link('<i class="fa fa-cubes"></i><span class="nav-label">' . __('Resources', true) . '</span>', array('controller' => 'Resources', 'action' => 'adminIndex'), array('escape' => false)) . '</li>';
                        }
                        echo '<li>' . HtmlHelper::link('<i class="fa fa-google"></i><span class="nav-label">' . __('SEO', true) . '</span>', array('controller' => 'CanonicalUrls', 'action' => 'adminIndex'), array('escape' => false)) . '</li>';
                        echo '<li>' . HtmlHelper::link('<i class="fa fa-file-text"></i><span class="nav-label">' . __('Layout texts', true) . '</span>', array('controller' => 'LayoutTexts', 'action' => 'adminIndex'), array('escape' => false)) . '</li>';
                        echo '<li>' . HtmlHelper::link('<i class="fa fa-picture-o"></i><span class="nav-label">' . __('Picture categories', true) . '</span>', array('controller' => 'PictureCategories', 'action' => 'adminIndex'), array('escape' => false)) . '</li>';
                        echo '<li>' . HtmlHelper::link('<i class="fa fa-picture-o"></i><span class="nav-label">' . __('Pictures', true) . '</span>', 'javascript:void(0)', array('escape' => false, 'onclick' => 'Form.selectPicture()')) . '</li>';
                        echo '<li>' . HtmlHelper::link('<i class="fa fa-file-pdf-o"></i><span class="nav-label">' . __('Files', true) . '</span>', array('controller' => 'Files', 'action' => 'adminIndex'), array('escape' => false)) . '</li>';
                        ?>
                        <!-- generated button goes here -->
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg">
                <div class="row">
                    <nav class="navbar navbar-static-top white-bg" role="navigation">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-deviaje" href="#"><i class="fa fa-bars"></i> </a>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li></li>
                            <li>
                                <?php echo HtmlHelper::link('<i class="fa fa-sign-out"></i>' . __('Logout', true), array('controller' => 'Users', 'action' => 'logout'), array('escape' => false)) ?>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper wrapper-content">
                    <?php echo $content_for_layout ?>
                </div>
                <div class="footer">
                    <div>
                        Sareb OTP
                    </div>
                </div>
            </div>
        </div>

        <!-- Mainly scripts -->
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/jquery-1.11.1.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/jquery-ui-1.12.0/jquery-ui.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/jquery-ui-1.12.0/datetimepicker/jquery.datetimepicker.full.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/bootstrap.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/plugins/metisMenu/jquery.metisMenu.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/plugins/summernote/summernote.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/plugins/slimscroll/jquery.slimscroll.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/inspinia.js') ?>

        <?php echo HtmlHelper::js('Message.js') ?>
        <?php echo HtmlHelper::js('Form.js') ?>
        <?php echo HtmlHelper::js('GCBootstrap.js') ?>

        <?php if(Session::get('flash[message]')): ?>
        <script type="text/javascript">
            $(document).ready(function() {
                Message.showText(<?php echo json_encode_safe(Session::get('flash[message]')) ?>, <?php echo json_encode_safe(Session::get('flash[title]')) ?>);
            });
        </script>
        <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>
    </body>
</html>