<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title></title>
    </head>
    <body>
        <table align="center" width="100%" border="0" bgcolor="white">
            <tr>
                <td align="center" style="padding: 20px;">
                    <table align="center" width="100%" border="0" style="border-collapse: collapse; max-width: 600px;" cellpadding="0" cellspacing="0" bgcolor="white">
                        <tr>
                            <td align="center">
                                <a style="color: black; text-decoration: none;" href="<?php echo LOCALED_ROOT_URL ?>"><img src="<?php echo ROOT_URL ?>static/img/logo.png" border="0" style="display: block"></a>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" height="20">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <?php echo $content_for_layout ?>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="color: black; font-size: 14px">
                                © <?php echo utf8html('Sareb OTP') ?> <?php echo date('Y') ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
     </body>
</html>
