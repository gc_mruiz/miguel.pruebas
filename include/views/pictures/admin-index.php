<div class="popup-header">
    <?php
        echo HtmlHelper::link(__('upload picture', true), array('controller' => 'pictures', 'action' => 'adminAdd', 'picture_category_id' => get_var('picture_category_id')), array('method' => 'post', '+class' => 'btn btn-success'));
        echo FormHelper::open(array('method' => 'get', 'id' => 'category-filter'));
        echo FormHelper::select('picture_category_id', $listOfPictureCategories, false, array('label' => __('Category Filter', true), 'onchange' => 'this.form.submit()'));
        echo FormHelper::close();
    ?>
</div>
<div class="popup-content">
    <?php echo HtmlHelper::flashMessage() ?>
    <div class="pictures">
        <?php foreach($pictures as $picture): ?>
        <div class="picture-container picture picture-<?php echo $picture['Picture']['id'] ?>">
            <div class="image">
                <?php echo ImageHelper::centered($picture, 100, 100, array('link' => array('onclick' => 'window.opener.Form.pictureSelected(window, ' . str_replace(array('\'', '"'), array("\\'", "'"), json_encode_safe($picture)) . ')'))) ?>
            </div>
            <div class="dimensions"><?php echo $picture['Picture']['width'].' x '.$picture['Picture']['height'].' px' ?></div>
            <?php echo FormHelper::button('X', array('+class' => 'btn btn-danger btn-sm', 'div' => array('+class' => 'delete'), 'href' => array('action' => 'adminDelete', $picture['Picture']['id']), 'confirm' => sprintf(__('Are you sure you want to delete #%s?', true), $picture['Picture']['id']))); ?>
            <?php echo HtmlHelper::link('<i class="fa fa-pencil"></i>', array('action' => 'adminEdit', $picture['Picture']['id']), array('div' => false, 'escape' => false, 'class' => 'edit btn btn-primary')); ?>
        </div>
        <?php endforeach; ?>
    </div>
    <?php echo PaginatorHelper::links($pagination) ?>
</div>