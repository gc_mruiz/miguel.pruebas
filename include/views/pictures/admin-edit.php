<div class="popup-header">
    <?php echo HtmlHelper::link(__('< return', true), array('action' => 'adminIndex', $picture['Picture']['picture_category_id']), array('+class' => 'btn btn-success')) ?>
</div>
<div class="popup-content">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><?php __('Edit Picture') ?></h2>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="image" style="margin-bottom: 20px;">
                            <?php echo ImageHelper::centered($picture, 320, 320, array('link' => array('onclick' => 'window.opener.Form.pictureSelected(window, ' . str_replace(array('\'', '"'), array("\\'", "'"), json_encode_safe($picture)) . ')'))) ?>
                        </div>
                        <?php echo HtmlHelper::flashMessage() ?>
                        <?php echo FormHelper::open() ?>
                        <?php echo FormHelper::input('picture[Picture][id]', 'hidden', false) ?>
                        <?php echo FormHelper::input('picture[Picture][alt]', 'text', false, array('label' => __('Display name', true))) ?>
                        <?php echo FormHelper::input('picture[Picture][src]', 'text', false, array('disabled' => 'disabled', 'label' => __('URL', true))) ?>
                        <?php echo FormHelper::select('picture[Picture][picture_category_id]', $listOfPictureCategories, false, array('associative' => true, 'onchange' => "$('#NewPictureCategoryName').parent().toggle($(this).val() == '0')")) ?>
                        <?php echo FormHelper::input('new_picture_category_name', 'text', false, array('div' => array('style' => 'display: none'))) ?>
                        <div>
                            <div class="pull-right">
                                <?php echo FormHelper::button(__('cancel', true), array('div' => false, 'href' => array('action' => 'adminIndex', 'picture_category_id' => $picture['Picture']['picture_category_id']), '+class' => 'btn btn-sm btn-white m-t-n-xs')); ?>
                                <?php echo FormHelper::submit(__('save', true), array('div' => false, 'class' => 'btn btn-sm btn-primary m-t-n-xs')) ?>
                            </div>
                            <span class="clearfix"></span>
                        </div>
                        <?php echo FormHelper::close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>