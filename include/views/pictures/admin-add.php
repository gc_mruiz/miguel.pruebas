<div class="popup-header">
    <?php echo HtmlHelper::link(__('< return', true), array('action' => 'adminIndex', get_var('picture_category_id')), array('method' => 'get', '+class' => 'btn btn-success')) ?>
</div>
<div class="popup-content">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><?php __('Upload Picture') ?></h2>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <?php echo HtmlHelper::flashMessage() ?>
                        <?php echo FormHelper::open(array('type' => 'file')) ?>
                        <fieldset>
                            <legend><?php __('File') ?></legend>
                            <?php echo FormHelper::input('file[]', 'file', false, array('multiple' => 'multiple', 'label' => __('File', true))) ?>
                            <?php echo FormHelper::input('picture[Picture][alt]', 'text', false, array('label' => __('Display name', true))) ?>
                        </fieldset>
                        <br>
                        <fieldset>
                            <legend><?php __('Category') ?></legend>
                            <?php echo FormHelper::select('picture[Picture][picture_category_id]', $listOfPictureCategories, false, array('associative' => true, 'onchange' => "$('#NewPictureCategoryName').parent().toggle($(this).val() == '0')")) ?>
                            <?php echo FormHelper::input('new_picture_category_name', 'text', false, array('div' => array('style' => 'display: none'))) ?>
                        </fieldset>
                        <br>
                        <fieldset>
                            <legend><?php __('Scaling') ?></legend>
                            <?php echo FormHelper::input('picture[Picture][scale]', 'radio', 'none', array('label' => __('Original size', true))) ?>
                            <?php echo FormHelper::input('picture[Picture][scale]', 'radio', '2K', array('label' => __('2K', true))) ?>
                            <?php echo FormHelper::input('picture[Picture][scale]', 'radio', 'FullHD', array('label' => __('FullHD', true))) ?>
                            <?php echo FormHelper::input('picture[Picture][scale]', 'radio', 'HD', array('label' => __('HD', true))) ?>
                            <?php echo FormHelper::input('picture[Picture][scale]', 'radio', 'SD', array('label' => __('SD', true))) ?>
                        </fieldset>
                        <div>
                            <div class="pull-right">
                                <?php echo FormHelper::button(__('cancel', true), array('div' => false, 'href' => array('action' => 'adminIndex'), '+class' => 'btn btn-sm btn-white m-t-n-xs')); ?>
                                <?php echo FormHelper::submit(__('upload', true), array('div' => false, 'class' => 'btn btn-sm btn-primary m-t-n-xs')) ?>
                            </div>
                            <span class="clearfix"></span>
                        </div>
                        <?php echo FormHelper::close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>