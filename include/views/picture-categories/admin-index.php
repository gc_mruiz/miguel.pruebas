<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
    <h2><?php __('Picture categories') ?></h2>
    <ol class="breadcrumb">
            <li>
                <?php echo HtmlHelper::link(__('Home', true), array('controller' => 'Users', 'action' => 'index')) ?>
            </li>
            <li class="active">
                <strong><?php __('Picture categories') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"><?php echo FormHelper::button(__('add new', true), array('div' => false, 'class' => 'pull-right m btn btn-success', 'href' => array('action' => 'adminAdd'))) ?></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <?php if(!empty($pictureCategories)): ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if(Session::get('flash[message]')): ?>
                    <div class="alert alert-info">
                        <?php echo nl2br(utf8html(Session::get('flash[message]'))) ?>
                    </div>
                    <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>

                    <table class="table table-striped table-bordered">
                        <tr>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Id', true), 'id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Name', true), 'name') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Created', true), 'created') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Modified', true), 'modified') ?></th>
                            <th class="actions">&nbsp;</th>
                        </tr>
                        <?php
                        $altrow = false;
                        $count = 0; foreach($pictureCategories as $pictureCategory): ?>
                        <tr class="<?php if($altrow) echo 'alt' ?>">
                            <td><?php echo $pictureCategory['PictureCategory']['id'] ?>&nbsp;</td>
                            <td><?php echo $pictureCategory['PictureCategory']['name'] ?>&nbsp;</td>
                            <td><?php echo ($pictureCategory['PictureCategory']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($pictureCategory['PictureCategory']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($pictureCategory['PictureCategory']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($pictureCategory['PictureCategory']['modified'])) : '') ?>&nbsp;</td>
                            <td>
                                <span class="btn-group">
                                    <?php echo FormHelper::button(__('edit', true), array('div' => false, 'href' => array('action' => 'adminEdit', $pictureCategory['PictureCategory']['id']), '+class' => 'btn btn-sm btn-primary')); ?>
                                    <?php echo FormHelper::button(__('delete', true), array('div' => false, 'href' => array('action' => 'adminDelete', $pictureCategory['PictureCategory']['id']), 'confirm' => sprintf(__('Are you sure you want to delete #%s?', true), $pictureCategory['PictureCategory']['id']), '+class' => 'btn btn-sm btn-danger')); ?>
                                </span>
                            </td>
                        </tr>
                        <?php $altrow = !$altrow; $count++; endforeach ?>
                    </table>
                    <?php echo PaginatorHelper::links($pagination) ?>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="ibox-content m-b-sm border-bottom">
        <?php if(Session::get('flash[message]')): ?>
        <div class="alert alert-info">
            <?php echo nl2br(utf8html(Session::get('flash[message]'))) ?>
        </div>
        <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>

        <div class="text-center p-lg">
            <h2><?php __('There are yet no elements to display.'); ?>
        </div>
    </div>
    <?php endif ?>
</div>
<div class="form-actions">
    <div class="left">
        <?php echo FormHelper::button(__('< return', true), array('div' => false, 'href' => array('controller' => 'Users', 'action' => 'index'), '+class' => 'btn btn-sm')); ?>
    </div>
</div>
