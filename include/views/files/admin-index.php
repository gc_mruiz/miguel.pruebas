<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
    <h2><?php __('Files') ?></h2>
    <ol class="breadcrumb">
            <li>
                <?php echo HtmlHelper::link(__('Home', true), array('controller' => 'Users', 'action' => 'index')) ?>
            </li>
            <li class="active">
                <strong><?php __('Files') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"><?php echo FormHelper::button(__('add new', true), array('div' => false, 'class' => 'pull-right m btn btn-success', 'href' => array('action' => 'adminAdd'))) ?></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <?php if(!empty($files)): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <?php echo HtmlHelper::flashMessage() ?>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th><?php echo PaginatorHelper::reorder($pagination, __('Id', true), 'id') ?></th>
                                <th><?php echo PaginatorHelper::reorder($pagination, __('Name', true), 'alt') ?></th>
                                <th><?php echo PaginatorHelper::reorder($pagination, __('Link', true), 'src') ?></th>
                                <th><?php echo PaginatorHelper::reorder($pagination, __('Uploaded', true), 'created') ?></th>
                                <th class="actions">&nbsp;</th>
                            </tr>
                            <?php
                            $altrow = false;
                            foreach($files as $file): ?>
                            <tr class="<?php if($altrow) echo 'alt' ?>" onclick="if(jQuery(this).hasClass('selected')) jQuery(this).removeClass('selected'); else jQuery(this).addClass('selected');">
                                <td><?php echo $file['File']['id'] ?>&nbsp;</td>
                                <td><?php echo $file['File']['alt'] ?>&nbsp;</td>
                                <td><?php echo $file['File']['src'] ?>&nbsp;</td>
                                <td><?php echo ($file['File']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($file['File']['created'])) : '') ?>&nbsp;</td>
                                <td>
                                    <span class="btn-group">
                                        <?php echo FormHelper::button(__('view', true), array('div' => false, 'href' => $file['File']['src'], '+class' => 'btn btn-sm')); ?>
                                        <?php echo FormHelper::button(__('delete', true), array('div' => false, 'href' => array('action' => 'adminDelete', $file['File']['id']), 'confirm' => sprintf(__('Are you sure you want to delete #%s?', true), $file['File']['id']), '+class' => 'btn btn-sm btn-danger')); ?>
                                    </span>
                                </td>
                            </tr>
                            <?php $altrow = !$altrow; endforeach ?>
                        </table>
                        <?php echo PaginatorHelper::links($pagination) ?>
                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
    <div class="ibox-content m-b-sm border-bottom">
        <?php if(Session::get('flash[message]')): ?>
        <div class="alert alert-info">
            <?php echo nl2br(utf8html(Session::get('flash[message]'))) ?>
        </div>
        <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>

        <div class="text-center p-lg">
            <h2><?php __('There are yet no elements to display.'); ?>
        </div>
    </div>
    <?php endif ?>
</div>
<div class="form-actions">
    <div class="left">
        <?php echo FormHelper::button(__('< return', true), array('div' => false, 'href' => array('controller' => 'Users', 'action' => 'index'), '+class' => 'btn btn-sm')); ?>
    </div>
</div>
