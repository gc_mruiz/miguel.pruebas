<?php __('Hello there.') ?>

<?php echo sprintf(__('We have received a restore password request for your account. To proceed, click on the following link %s .', true),Router::url(array('controller' => 'Users', 'action' => 'reset', $email, $reset_password_token))) ?>

<?php __('If you did not request this, you can ignore this message.') ?>
<?php __('Thank you.') ?>