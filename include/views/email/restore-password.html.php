<p><?php __('Hello there.') ?></p>
<p><?php echo sprintf(__('We have received a restore password request for your account. To proceed, click on the following link %s .', true), '<a href="' . Router::url(array('controller' => 'Users', 'action' => 'reset', $email, $reset_password_token)) . '">' . Router::url(array('controller' => 'Users', 'action' => 'reset', $email, $reset_password_token)) .'</a>') ?></p>
<p><?php __('If you did not request this, you can ignore this message.') ?></p>
<p><?php __('Thank you.') ?></p>