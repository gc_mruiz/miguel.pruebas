<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
    <h2><?php __('Layout texts') ?></h2>
    <ol class="breadcrumb">
            <li>
                <?php echo HtmlHelper::link(__('Home', true), array('controller' => 'Users', 'action' => 'index')) ?>
            </li>
            <li class="active">
                <strong><?php __('Layout texts') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <?php if(!empty($layoutTexts)): ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php echo HtmlHelper::flashMessage() ?>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Id', true), 'id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Language', true), 'locale') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Code', true), 'code') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Text', true), 'text') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Picture', true), 'picture_id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Modified', true), 'modified') ?></th>
                            <th class="actions">&nbsp;</th>
                        </tr>
                        <?php
                        $altrow = false;
                        $count = 0; foreach($layoutTexts as $layoutText): ?>
                        <tr class="<?php if($altrow) echo 'alt' ?>">
                            <td><?php echo $layoutText['LayoutText']['id'] ?>&nbsp;</td>
                            <td><?php echo $layoutText['LayoutText']['locale'] ?>&nbsp;</td>
                            <td><?php echo $layoutText['LayoutText']['code'] ?>&nbsp;</td>
                            <td><?php echo $layoutText['LayoutText']['text'] ?>&nbsp;</td>
                            <td><?php if($layoutText['Picture']['id']) echo ImageHelper::image($layoutText['Picture'], array('styleArray' => array('width' => '150px', 'height' => 'auto'))) ?>&nbsp;</td>
                            <td><?php echo ($layoutText['LayoutText']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($layoutText['LayoutText']['modified'])) : '') ?>&nbsp;</td>
                            <td>
                                <span class="btn-group">
                                    <?php echo FormHelper::button(__('edit', true), array('div' => false, 'href' => array('action' => 'adminEdit', $layoutText['LayoutText']['id']), '+class' => 'btn btn-sm btn-primary')); ?>
                                </span>
                            </td>
                            </tr>
                            <?php $altrow = !$altrow; $count++; endforeach ?>
                        </table>
                    <?php echo PaginatorHelper::links($pagination) ?>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="ibox-content m-b-sm border-bottom">
        <?php if(Session::get('flash[message]')): ?>
        <div class="alert alert-info">
            <?php echo nl2br(utf8html(Session::get('flash[message]'))) ?>
        </div>
        <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>

        <div class="text-center p-lg">
            <h2><?php __('There are yet no elements to display.'); ?>
        </div>
    </div>
    <?php endif ?>
</div>
<div class="form-actions">
    <div class="left">
        <?php echo FormHelper::button(__('< return', true), array('div' => false, 'href' => array('controller' => 'Users', 'action' => 'index'), '+class' => 'btn btn-sm')); ?>
    </div>
</div>
