<div class="middle-box text-center animated fadeInDown">
    <h1>404</h1>
    <h3 class="font-bold"><?php __('Page not found') ?></h3>
    <div class="error-desc">
        <?php __('Sorry, but the page you are looking for has not been found. Try checking the URL for error, then hit the refresh button on your browser or navigate somewhere else.') ?>
        <div>
            <a href="<?php echo LOCALED_ROOT_URL ?>" class="btn btn-primary m text-normalcase"><?php __('Home') ?></a>
        </div>
    </div>
</div>