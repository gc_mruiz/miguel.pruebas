<?php

/**
 * Class MethodCache
 *
 * Caches any object's public method
 */
class MethodCache {
    private static $running_method_for_cache = false;

    public static function caching() {
        return self::$running_method_for_cache;
    }

    public static function executeStatic($class_name, $method, $params = array(), $cacheOptions = array()) {
        $object = null;

        return self::execute($object, $class_name, $method, $params, $cacheOptions);
    }

    public static function execute(&$object, $class_name, $method, $params = array(), $cacheOptions = array()) {
        if ($cacheOptions === true) {
            $cacheOptions = array();
        }

        // Set default options
        $cacheOptions = array_merge(array(
                                        'user_id' => null,
                                        'expires' => null,
                                        'clearers' => array(),
                                    ), $cacheOptions);

        // Md5 params
        $params_md5 = md5(json_encode($params));

        // Retrieve cached result
        $cachedResult = self::getCached($class_name, $method, ($cacheOptions['user_id'] ? $cacheOptions['user_id'] . '-' : '') . $params_md5, $cacheOptions['expires']);

        // If found, return
        if ($cachedResult['success']) {
            return $cachedResult['content'];
        } elseif ($cacheOptions['user_id']) {
            // Retrieve cached result
            $cachedResult = self::getCached($class_name, $method, $params_md5);

            // If found, return
            if ($cachedResult['success']) {
                return $cachedResult['content'];
            }
        }

        // Set caching
        self::$running_method_for_cache = true;

        // Else, execute
        if ($object) {
            $result = call_user_func_array(array($object, $method), $params);
        } else {
            $result = call_user_func_array(array($class_name, $method), $params);
        }

        // Set caching
        self::$running_method_for_cache = false;

        // Save cache
        self::saveCache($class_name, $method, ($cacheOptions['user_id'] ? $cacheOptions['user_id'] . '-' : '') . $params_md5, $cacheOptions, $result);

        // Return calculated result
        return $result;
    }

    /**
     * Retrieves the cached information for the requested method and params
     *
     * @param $class_name
     * @param $method
     * @param $params_md5
     * @param $expires
     * @return array|bool
     */
    private static function getCached($class_name, $method, $params_md5, $expires = null) {
        // Generate filename
        $filename = 'methods/' . str_replace('_', '-', Inflector::underscore($class_name)) . '/' . str_replace('_', '-', Inflector::underscore($method)) . '/' . $params_md5 . '.json';
        $filename = CachedAdapter::getKey() . '/' . $filename;

        // Get cache
        $result = CachedAdapter::get($filename);

        if ($result['success']) {
            return array(
                'success' => true,
                'content' => $result['content'],
            );
        }

        // No cache
        return array(
            'success' => false,
            'content' => null,
        );
    }

    /**
     * Creates a cache for a method and params
     *
     * @param $class_name
     * @param $method
     * @param $params_md5
     * @param $cacheOptions
     * @param $result
     */
    private static function saveCache($class_name, $method, $params_md5, $cacheOptions, $result) {
        // Generate filename
        $filename = 'methods/' . str_replace('_', '-', Inflector::underscore($class_name)) . '/' . str_replace('_', '-', Inflector::underscore($method)) . '/' . $params_md5 . '.json';
        $filename = CachedAdapter::getKey() . '/' . $filename;

        if ($cachedPreviousContent = CachedAdapter::get($filename)) {
            if ($cachedPreviousContent['success']) {
                return;
            }
        }

        // Write cache
        CachedAdapter::set($filename, $result, $cacheOptions['clearers'], $cacheOptions['expires']);
    }
}