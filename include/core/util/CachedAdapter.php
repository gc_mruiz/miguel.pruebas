<?php

class CachedAdapter {
    private static $key = '';

    private static $instance = null;
    public static $disable_cache_clearing = false;
    private $clearers = array();
    private $setClearers = array();

    public static function setKey($key) {
        if (defined('LANG')) {
            self::$key = LANG . ($key ? '/' . clean_for_url($key) : '');
        } else {
            self::$key = clean_for_url($key);
        }
    }

    public static function getKey() {
        return self::$key;
    }

    public static function get($path) {
        $return = array(
            'success' => false,
            'content' => null,
            'expires' => null,
        );

        if (GestyMVC::config('do_not_use_cache')) {
            return $return;
        }

        $MemcachedAdapter = new MemcachedAdapter();

        if (GestyMVC::config('memcached[server]')) {
            $return = $MemcachedAdapter->get($path);
        } else {
            if (file_exists(ROOT_PATH . '.cache/' . $path)) {
                $return['success'] = true;
                $return['content'] = json_decode(file_get_contents(ROOT_PATH . '.cache/' . $path), true);

                if (array_key_exists('gc_cache_expires', $return['content'])) {
                    $return['expires'] = $return['content']['gc_cache_expires'];
                    $return['content'] = $return['content']['content'];
                } else {
                    $return['expires'] = null;
                }
            }
        }

        if ($return['success'] && $return['expires'] && strtotime($return['expires']) < time()) {
            $return = array(
                'success' => false,
                'content' => null,
                'expires' => null,
            );

            self::clear($path);
        }

        return $return;
    }

    public static function set($path, $value, $clearers = array(), $expires = null) {
        if (GestyMVC::config('do_not_use_cache')) {
            return;
        }

        $MemcachedAdapter = new MemcachedAdapter();

        if (GestyMVC::config('memcached[server]')) {
            $MemcachedAdapter->set($path, $value, $expires);
        } else {
            $folder = substr($path, 0, -strlen(basename($path)));

            // Create cache folder if not exists
            if (!file_exists(ROOT_PATH . '.cache')) {
                @mkdir(ROOT_PATH . '.cache', 0755);
                @file_put_contents(ROOT_PATH . '.cache/.htaccess', 'order allow,deny' . PHP_EOL . 'deny from all');
            }

            if (!file_exists(ROOT_PATH . '.cache/' . $folder)) {
                @mkdir(ROOT_PATH . '.cache/' . $folder, 0755, true);
            }

            @file_put_contents(ROOT_PATH . '.cache/' . $path, json_encode(array(
                                                                              'content' => $value,
                                                                              'gc_cache_expires' => $expires,
                                                                          )));
        }

        if ($clearers) {
            self::setClearers($path, $clearers);
        }
    }

    public static function clear($path) {
        if (self::$disable_cache_clearing) {
            return;
        }

        $MemcachedAdapter = new MemcachedAdapter();

        if (GestyMVC::config('memcached[server]')) {
            $MemcachedAdapter->clear($path);
        } else {
            @unlink(ROOT_PATH . '.cache/' . $path);
        }
    }

    public static function clearAll() {
        if (self::$disable_cache_clearing) {
            return;
        }

        $MemcachedAdapter = new MemcachedAdapter();

        if (GestyMVC::config('memcached[server]')) {
            $MemcachedAdapter->clearAll();
        } else {
            // Retrieve all files and directories in cache directory
            $directories = scandir(ROOT_PATH . '.cache');

            // Foreach directory, delete recursively
            foreach ($directories as $dir) {
                // Only child directories
                if (!is_file(ROOT_PATH . '.cache/' . $dir) && !in_array($dir, array(
                        '.',
                        '..',
                    ))
                ) {
                    rrmdir(ROOT_PATH . '.cache/' . $dir, false);
                }
            }
        }
    }

    public static function setClearers($filename, $clearers) {
        self::singleInstance()->_setClearers($filename, $clearers);
    }

    public function _setClearers($filename, $clearers) {
        foreach ($clearers as $model => $ids) {
            if (!is_array($ids)) {
                $ids = array($ids);
            }

            foreach ($ids as $id) {
                if ($id == '*') {
                    $id = null;
                }

                if (!isset($this->setClearers[$model . '-' . $id])) {
                    $this->setClearers[$model . '-' . $id] = array();
                }

                $this->setClearers[$model . '-' . $id][] = $filename;
            }
        }
    }

    public static function clearByClearer($model, $id = null) {
        if (self::$disable_cache_clearing) {
            return;
        }

        clitrace('Queueing clear of related cache ' . $model . '#' . $id);

        self::singleInstance()->_queueClearer($model, $id);
    }

    private function _queueClearer($model, $id) {
        if (!isset($this->clearers[$model])) {
            $this->clearers[$model] = array();
        }

        if (in_array(null, $this->clearers[$model])) {
            return;
        }

        if ($id === null) {
            $this->clearers[$model] = array($id);
        } else {
            $this->clearers[$model][] = $id;
        }
    }

    public function __destruct() {
        // If there are queued clearers, clear them
        self::$disable_cache_clearing = false;

        foreach ($this->setClearers as $key => $filenames) {
            $filenamesToClear = CachedAdapter::get('clearers/' . $key);

            if ($filenamesToClear['success']) {
                $filenamesToClear = $filenamesToClear['content'];

                if (!is_array($filenamesToClear)) {
                    $filenamesToClear = array();
                }
            } else {
                $filenamesToClear = array();
            }

            foreach ($filenames as $filename) {
                $filenamesToClear[] = $filename;
            }

            CachedAdapter::set('clearers/' . $key, $filenamesToClear);
        }

        foreach ($this->clearers as $model => $ids) {
            foreach ($ids as $id) {
                $this->_clearByClearer($model, $id);
            }
        }

        MySQLModel::closeConnection();
    }

    private function _clearByClearer($model, $id = null) {
        if ($id == '*') {
            $id = null;
        }

        if ($id) {
            $filenamesToClear = CachedAdapter::get('clearers/' . $model . '-' . $id);

            if ($filenamesToClear['success']) {
                CachedAdapter::clear('clearers/' . $model . '-' . $id);
            }

            if ($filenamesToClear['success'] && $filenamesToClear['content']) {
                foreach ($filenamesToClear['content'] as $filename) {
                    self::clear($filename);
                }
            }
        }

        $id = null;

        $filenamesToClear = CachedAdapter::get('clearers/' . $model . '-' . $id);

        if ($filenamesToClear['success']) {
            CachedAdapter::clear('clearers/' . $model . '-' . $id);
        }

        if ($filenamesToClear['success'] && $filenamesToClear['content']) {
            foreach ($filenamesToClear['content'] as $filename) {
                self::clear($filename);
            }
        }
    }

    /**
     * @return CachedAdapter
     */
    public static function singleInstance() {
        if (self::$instance === null) {
            self::$instance = new CachedAdapter();
        }

        return self::$instance;
    }

}