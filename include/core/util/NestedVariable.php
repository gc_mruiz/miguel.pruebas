<?php

/**
 * Class NestedVariable
 *
 * Controls setting, getting and unsetting of nested array values by name.
 */
class NestedVariable {
    /**
     * Extracts a variable subvalue by its Form-like name. (Array values can be set using parent[children] syntax)
     *
     * @param $var array nested array
     * @param $name string Form-like name of the index
     *
     * @return mixed value of the subindex or null if not found
     */
    public static function get(&$var, $name) {
        // Allow full var return
        if ($name === null) {
            return $var;
        }

        // By default the value is null
        $value = null;

        // Generate list of indexes based on name
        $indexes = NestedVariable::extractIndexes($name);

        // Navigate to the index position
        if ($name && isset($var[$indexes[0]])) {
            $value = $var;
            foreach ($indexes as $index) {
                if (isset($value[$index])) {
                    $value = $value[$index];
                } else {
                    $value = null;
                    break;
                }
            }
        }

        // Return retrieved value (null for not found)
        return $value;
    }

    /**
     * Sets a variable subvalue by its Form-like name. (Array values can be set using parent[children] syntax)
     *
     * @param $var array nested array
     * @param $name string Form-like name of the index
     * @param $value mixed value to be set
     * @param $unset bool whether or not to unset the var if the value is null
     */
    public static function set(&$var, $name, $value, $unset = false) {
        // Generate list of indexes based on name
        $indexes = NestedVariable::extractIndexes($name);

        // Start at the beginning of the array
        $subVar =& $var;

        // Navigate by reference to the index
        foreach ($indexes as $index) {
            // Initialize nonexistent indexes
            if (!isset($subVar[$index])) {
                $subVar[$index] = array();
            }

            // Store reference
            $subVar =& $subVar[$index];
        }

        // If value is not null or unset is false, set
        if ($value !== null || !$unset) {
            $subVar = $value;
        } else {
            // If asked to unset when null, unset value
            $subVar = '';
            unset($subVar);
        }
    }

    /**
     * Unsets a variable subvalue by its Form-like name. (Array values can be set using parent[children] syntax)
     *
     * @param $var array nested array
     * @param $name string Form-like name of the index
     */
    public static function clear(&$var, $name) {
        NestedVariable::set($var, $name, null, true);
    }

    /**
     * Extracts the part of the Form-like name. (Array values can be set using parent[children] syntax)
     *
     * @param $name
     *
     * @return array list of name parts
     */
    private static function extractIndexes($name) {
        $indexes = null;

        if (strpos($name, '[')) {
            $indexes = str_replace(']', '', $name);
            $indexes = explode('[', $indexes);
        } else {
            $indexes = array($name);
        }

        return $indexes;
    }
}

/**
 * Global shortcut to NestedVariable::get($_POST, ..) XSSINJECTION-safe
 *
 * @param $index string
 * @param $strip_tags boolean whether or not to strip tags from input (default true)
 * @param $allowable_tags string param for strip_tags (default null)
 *
 * @return null
 */
function post_var($index, $strip_tags = true, $allowable_tags = null) {
    $value = NestedVariable::get($_POST, $index);

    if ($strip_tags) {
        strip_tags_recursive($value, $allowable_tags);
    }

    return $value;
}

/**
 * Global shortcut to NestedVariable::get($_GET, ..) XSSINJECTION-safe
 *
 * @param $index string
 * @param $strip_tags boolean whether or not to strip tags from input (default true)
 * @param $allowable_tags string param for strip_tags (default null)
 *
 * @return null
 */
function get_var($index, $strip_tags = true, $allowable_tags = null) {
    $value = NestedVariable::get($_GET, $index);

    if ($strip_tags) {
        strip_tags_recursive($value, $allowable_tags);
    }

    return $value;
}

/**
 * Global shortcut to NestedVariable::get(View::$viewVars, ..) optional XSSINJECTION-safe
 *
 * @param $index
 * @param $strip_tags boolean whether or not to strip tags from input (default false since it is a database value)
 * @param $allowable_tags string param for strip_tags (default null)
 *
 * @return null
 */
function data_var($index, $strip_tags = false, $allowable_tags = null) {
    $value = NestedVariable::get(View::$viewVars, $index);

    if ($strip_tags) {
        strip_tags_recursive($value, $allowable_tags);
    }

    return $value;
}

/**
 * Global shortcut to NestedVariable::get($_FILES, ..) optional XSSINJECTION-safe
 *
 * @param $index
 * @param $strip_tags boolean whether or not to strip tags from input (default true)
 * @param $allowable_tags string param for strip_tags (default null)
 *
 * @return null
 */
function files_var($index, $strip_tags = true, $allowable_tags = null) {
    $value = NestedVariable::get($_FILES, $index);

    if ($strip_tags) {
        strip_tags_recursive($value, $allowable_tags);
    }

    return $value;
}

/**
 * Global shortcut to NestedVariable::get($_POST, ..) || NestedVariable::get(View::$viewVars, ..) XSSINJECTION-safe
 *
 * @param $index string
 * @param $strip_tags boolean whether or not to strip tags from input (default true)
 * @param $allowable_tags string param for strip_tags (default null)
 *
 * @return null
 */
function form_var($index, $strip_tags = true, $allowable_tags = null) {
    // Try to get var from post
    $var = post_var($index, $strip_tags, $allowable_tags);

    if ($var === null) {
        // If not found, try to get it from $data
        $var = data_var($index, $strip_tags, $allowable_tags);
    }

    // Return result
    return $var;
}