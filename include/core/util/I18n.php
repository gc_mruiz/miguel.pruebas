<?php

/**
 * Class I18n
 *
 * Internationalization functions.
 */
class I18n {
    /**
     * Translation strings for current language.
     *
     * @var array
     */
    private static $strings = array();

    /**
     * Translation strings for other languages.
     *
     * @var array
     */
    private static $otherLangStrings = array();

    /**
     * Initializes the translation in the current language.
     */
    public static function init() {
        // Translations array (empty by default)
        $strings =& self::$strings;

        // Load translations
        if (file_exists(INCLUDE_PATH . 'lang/' . LANG . '.php')) {
            include_once(INCLUDE_PATH . 'lang/' . LANG . '.php');
        }
    }

    public static function loadLanguage($lang = null) {
        if (!$lang) {
            $lang = LANG;
        }

        // Do not load twice
        if (!isset(self::$otherLangStrings[$lang])) {
            self::$otherLangStrings[$lang] = array();
        } else {
            return;
        }

        // Translations array (empty by default)
        $strings =& self::$otherLangStrings[$lang];

        // Load translations
        if (file_exists(INCLUDE_PATH . 'lang/' . $lang . '.php')) {
            include(INCLUDE_PATH . 'lang/' . $lang . '.php');
        }
    }

    /**
     * Shortcut to self::translate
     *
     * @param $str string
     * @param bool|false $return
     * @param string $lang
     *
     * @return mixed
     */
    public static function translate($str, $return = false, $lang = null) {
        if (!$lang) {
            $lang = LANG;
        }

        if ($lang == LANG) {
            if (isset(self::$strings[$str])) {
                $result = self::$strings[$str];
            } else {
                $result = $str;
            }

            if ($return) {
                return $result;
            } else {
                echo $result;
            }
        } else {
            return self::translateDifferentLang($str, $return, $lang);
        }

        return null;
    }

    public static function translateDifferentLang($str, $return = false, $lang) {
        self::loadLanguage($lang);

        if (isset(self::$otherLangStrings[$lang][$str])) {
            $result = self::$otherLangStrings[$lang][$str];
        } else {
            $result = $str;
        }

        if ($return) {
            return $result;
        } else {
            echo $result;
        }

        return null;
    }

    /**
     * Shortcut to self::translate
     *
     * @param $str string
     * @param bool|false $return
     *
     * @return mixed
     */
    public static function reverse($str, $return = false) {
        $stringsReverted = array_flip(self::$strings);

        if (isset($stringsReverted[$str])) {
            $result = $stringsReverted[$str];
        } else {
            $result = $str;
        }

        if ($return) {
            return $result;
        } else {
            echo $result;
        }
    }

    /**
     * Updates a translation dynamically
     *
     * @param $str
     * @param $value
     */
    public static function set($str, $value) {
        self::$strings[$str] = $value;
    }
}

/**
 * Shortcut to self::translate
 *
 * @param $str
 * @param bool|false $return
 * @param string $lang
 *
 * @return mixed
 */
function __($str, $return = false, $lang = null) {
    if (!$lang) {
        $lang = LANG;
    }

    return I18n::translate($str, $return, $lang);
}

function __x($str, $return = false) {
    if (LANG == 'en') {
        return I18n::translate($str, $return);
    }

    if ($return) {
        return $str;
    } else {
        echo $str;
    }
}