<?php

class MemcachedAdapter {
    private $memcached = false;
    private $server = null;
    private $port = null;
    private $available = false;

    public function __construct() {
        $this->server = GestyMVC::config('memcached[server]');
        $this->port = GestyMVC::config('memcached[port]');

        if ($this->server && class_exists('Memcached')) {
            /** @var Memcached memcached */
            $this->memcached = new Memcached();
            $this->memcached->addServer($this->server, $this->port);
            $this->isAvailable();
        }
    }

    public function isAvailable() {
        if ($this->memcached) {
            $status = $this->memcached->getStats();
            $this->available = $status[$this->server . ':' . $this->port]['pid'] > 0 ? true : false;
        } else {
            $this->available = false;
        }

        return $this->available;
    }

    public function getStatus() {
        return $this->memcached->getStats();
    }

    public function get($key) {
        $return = array(
            'success' => false,
            'content' => null,
            'expires' => null,
        );

        if ($this->isAvailable()) {
            $cache = $this->memcached->get($key);

            if ($cache) {
                $return['success'] = true;
                $return['content'] = $cache['content'];

                if (isset($cache['gc_cache_expires'])) {
                    $return['expires'] = $cache['gc_cache_expires'];
                } else {
                    $return['expires'] = null;
                }
            }
        }

        return $return;
    }

    public function set($key, $value, $expires = null) {
        if ($this->isAvailable()) {
            $var = array(
                'cached' => true,
                'content' => $value,
                'gc_cache_expires' => $expires,
            );

            $this->memcached->set($key, $var, 0);
        }
    }

    public function clear($key) {
        if ($this->isAvailable()) {
            $this->memcached->delete($key);
        }
    }

    public function clearAll() {
        if ($this->isAvailable()) {
            $this->memcached->flush();
        }
    }
}