<?php

/**
 * Class Email
 *
 * Handles email sending.
 */
class Email {
    /**
     * Generated errors array.
     *
     * @var array
     */
    public static $errors = array();

    /**
     * Sends an email using the default configuration.
     *
     * @param $subject string
     * @param $content string
     * @param $emails array|string
     * @param $attachments array key value pair of name => path
     * @param $is_html bool
     * @param $text_content string|null
     * @param $options array
     *  - reset_errors: if set to false, ::$errors will not be reset at the beginning of execution (for nested calls)
     *  - bcc: emails to add as BCC
     *  - cc: emails to add as CC
     *
     * @return bool success
     */
    public static function send($subject, $content, $emails, $attachments = array(), $is_html = false, $text_content = null, $options = array()) {
        // Load PHPMailser and SMTP classes
        include_once(INCLUDE_PATH . 'vendors/PHPMailer/class.phpmailer.php');
        include_once(INCLUDE_PATH . 'vendors/PHPMailer/class.smtp.php');

        // Clear validation errors
        if (!isset($options['reset_errors']) || $options['reset_errors']) {
            Email::$errors = array();
        }

        // Turn string into array
        if (!is_array($emails)) {
            $emails = array($emails);
        }

        // Initialize PHPMailer
        $mail = new PHPMailer();

        // Retrieve mail configuration
        $mailConfig = GestyMVC::config('mail');

        // Set encoding
        $mail->CharSet = 'UTF-8';

        // Set HTML and alternate content if required
        if ($is_html) {
            $mail->IsHTML();

            if ($text_content) {
                $mail->AltBody = $text_content;
            }
        }

        // SMTP server
        $mail->setFrom($mailConfig['email'], $mailConfig['display']);
        $mail->SMTPSecure = ($mailConfig['ssl'] ? 'ssl' : false);

        $mail->Host = $mailConfig['host'];
        $mail->Port = $mailConfig['port'];

        if (Debug::debugging()) {
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true,
                ),
            );
        }

        // Set email content and recipient
        $mail->Subject = $subject;
        $mail->Body = $content;

        foreach ($emails as $email) {
            $mail->addAddress($email);
        }

        if (!empty($options['cc'])) {
            if (!is_array($options['cc'])) {
                $options['cc'] = array($options['cc']);
            }

            foreach ($options['cc'] as $email) {
                $mail->addCC($email);
            }
        }

        if (!empty($options['bcc'])) {
            if (!is_array($options['bcc'])) {
                $options['bcc'] = array($options['bcc']);
            }

            foreach ($options['bcc'] as $email) {
                $mail->addBCC($email);
            }
        }

        if (!empty($options['reply_to'])) {
            $mail->addReplyTo($options['reply_to']);
        }

        // Add attachments
        foreach ($attachments as $filename => $path) {
            if (is_int($filename)) {
                $filename = basename($path);
            }

            $mail->addAttachment($path, $filename);
        }

        // Switch between the usage of the mail() function and SMTP email
        if ($mailConfig['mail_function'] === false) {
            $mail->isSMTP();

            if ($mailConfig['user']) {
                $mail->SMTPAuth = true;
                $mail->Username = $mailConfig['user'];
                $mail->Password = $mailConfig['password'];
            } else {
                $mail->SMTPAuth = false;
            }
        } else {
            $mail->isMail();
        }

        if (isset($mailConfig['SMTPAutoTLS'])) {
            $mail->SMTPAutoTLS = ($mailConfig['SMTPAutoTLS'] ? $mailConfig['SMTPAutoTLS'] : false);
        }

        // Send email
        try {
            if ($mail->send()) {
                return true;
            } else {
                Email::$errors[] = $mail->ErrorInfo;
                Debug::log(json_encode(array(
                                           'error' => $mail->ErrorInfo,
                                           'params' => compact('subject', 'content', 'email', 'attachments', 'is_html', 'text_content', 'options'),
                                       )), 'email', 'json');

                return false;
            }
        } catch (Exception $e) {
            Email::$errors[] = __('Unexpected error.', true);
            Debug::log(json_encode(array(
                                       'error' => __('Unexpected error.', true),
                                       'params' => compact('subject', 'content', 'email', 'attachments', 'is_html', 'text_content', 'options'),
                                   )), 'email', 'json');

            return false;
        }
    }

    /**
     * Sends and email based on a template.
     *
     * @param $subject string
     * @param $emails array|string
     * @param $attachments array key value pair of name => path
     * @param $template_name string name of the template to include from views/email/. Template must have a .html and a
     *     .txt version. Do not include extension on the call.
     * @param $data array variables to use inside the template view
     * @param $module string name of the module containing the views
     * @param $helpers array custom helpers to load
     * @param $layout string custom layout for email
     * @param $options array
     *  - reset_errors: if set to false, ::$errors will not be reset at the beginning of execution (for nested calls)
     *  - bcc: emails to add as BCC
     *  - cc: emails to add as CC
     *
     * @return bool
     */
    public static function sendTemplate($subject, $emails, $attachments = array(), $template_name, $data = array(), $module = null, $helpers = array(), $layout = 'email', $options = array()) {
        if ($attachments === null) {
            $attachments = array();
        }

        if ($data === null) {
            $data = array();
        }

        // Calculate HTML email version
        $htmlContent = View::render($module, 'email/' . $template_name . '.html', $layout . '.html', $data, $helpers);

        // Calculate TXT email version
        $textContent = View::render($module, 'email/' . $template_name . '.txt', $layout . '.txt', $data, $helpers);

        // Send both
        return Email::send($subject, $htmlContent['full'], $emails, $attachments, true, $textContent['full'], $options);
    }
}