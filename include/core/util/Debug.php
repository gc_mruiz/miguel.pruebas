<?php

/**
 * Controls debugging.
 *
 * Class Debug
 */
class Debug {
    /**
     * Debug mode status.
     *
     * @var bool
     */
    private static $debugging = false;

    /**
     * Initializes the debug
     *
     * @param $debugging
     */
    public static function init($debugging) {
        // Store debug status
        Debug::$debugging = $debugging;

        // Set server debug settings
        Debug::showErrors();
    }

    /**
     * Configure servers to show or hide all errors.
     *
     * @param null $debugging
     */
    public static function showErrors($debugging = null) {
        // If debugging not explicitly set, use general config
        if ($debugging === null) {
            $debugging = Debug::$debugging;
        }

        if (GestyMVC::isCli() || $debugging) {
            // If debugging, show all errors.
            ini_set('display_errors', 1);
            error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT);
        } else {
            // If not debugging, show no errors.
            ini_set('display_errors', 0);
            error_reporting(false);
        }
    }

    /**
     * Checks if debugging or not
     *
     * @return bool debugging
     */
    public static function debugging() {
        return Debug::$debugging;
    }

    /**
     * Crash and log
     *
     * @param $crash_in_production bool whether or not to crash also in production (message will not be shown)
     * @param $message string
     * @param $folder string .logs subfolder
     * @param $type string log extension
     */
    public static function crash($crash_in_production = false, $message = '', $folder = null, $type = null) {
        // Log message if provided
        if ($message) {
            // Add prefix
            $message = 'FATAL ERROR: ' . $message;

            // Log
            Debug::log($message, $folder, $type);

            // If debugging, show message on screen
            if (Debug::$debugging) {
                echo $message;
            }
        }

        // Stop execution
        if ($crash_in_production || Debug::$debugging) {
            exit();
        }
    }

    /**
     * Logs data
     *
     * @param $message string | array
     * @param $folder string .logs subfolder
     * @param $type string log extension
     * @param $microtime_file
     */
    public static function log($message, $folder = null, $type = null, $microtime_file = true) {
        if (is_array($message) || is_object($message)) {
            $message = json_encode($message);

            // Set default type
            if ($type === null) {
                $type = 'json';
            }
        }

        // Set default type
        if ($type === null) {
            $type = 'log';
        }

        if (!GestyMVC::config('save_debug_logs_to_database') || $folder == 'mysql' || $folder == 'mysql-debug') {
            // Set path parts, add base log path
            $pathParts = array('.logs');

            // Add folder if required
            if ($folder) {
                $pathParts[] = $folder;
            }

            // Add current day folders
            $pathParts[] = date('Y');
            $pathParts[] = date('m');
            $pathParts[] = date('d');

            // Generate path
            $path = ROOT_PATH;

            // Append path parts
            foreach ($pathParts as $path_path) {
                $path .= $path_path . '/';

                // Check folder and create if not exists
                if (!file_exists($path)) {
                    @mkdir($path);
                }
            }

            $path .= date('H.i');

            if ($microtime_file) {
                $path .= '-' . ceil(microtime(true) * 1000);
            }

            $path .= '.' . $type;

            // Store
            @file_put_contents($path, $message . PHP_EOL, FILE_APPEND);
        } else {
            /* @var DebugLog $DebugLog */
            $DebugLog = MySQLModel::getInstance('DebugLog');
            $DebugLog->addNew(array(
                                  'message' => $message,
                                  'folder' => $folder,
                                  'type' => $type,
                              ));
        }
    }
}