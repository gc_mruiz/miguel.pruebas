<?php

/**
 * Class Inflector
 *
 * Handles text transformations
 */
class Inflector {
    /**
     * Converts an underscored text into a camelcase.
     *
     * @param $underscored_text
     *
     * @return string
     */
    static function camelcase($underscored_text) {
        // Camel case result
        $camel_text = '';

        // Force lowercase on underscore
        $underscored_text = mb_strtolower($underscored_text, 'UTF-8');

        // Strip words
        $words = explode('_', $underscored_text);

        // Add each word with ucfirst
        foreach ($words as $word) {
            if ($camel_text) {
                $camel_text .= mb_ucfirst($word, 'UTF-8');
            } else {
                $camel_text = $word;
            }
        }

        return $camel_text;
    }

    /**
     * Converts an underscored text into a pascalcase.
     *
     * @param $underscored_text
     *
     * @return string
     */
    static function pascalcase($underscored_text) {
        // Convert to camelcase
        $camel_text = Inflector::camelcase($underscored_text);

        // If not empty, change the first letter to uppercase
        if ($underscored_text) {
            return mb_strtoupper(mb_substr($camel_text, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($camel_text, 1, mb_strlen($camel_text, 'UTF-8') - 1);
        }

        // Return empty string in any other case
        return '';
    }

    /**
     * Converts a camel case or pascal case text into underscore
     *
     * @param $camel_case_text
     *
     * @return mixed|string
     */
    static function underscore($camel_case_text) {
        // Generated underscore text
        $underscored_text = '';

        // For each char
        for ($i = 0; $i < mb_strlen($camel_case_text, 'UTF-8'); $i++) {
            // Extract char
            $c = mb_substr($camel_case_text, $i, 1, 'UTF-8');

            // If char is uppercase and it is not the first one, add an underscore
            if ($underscored_text && $c == mb_strtoupper($c, 'UTF-8')) {
                $underscored_text .= '_';
            }

            // Add char in lowercase
            $underscored_text .= mb_strtolower($c, 'UTF-8');
        }

        // Remove consecutive underscores
        while (mb_strpos($underscored_text, '__', null, 'UTF-8')) {
            $underscored_text = str_replace('__', '_', $underscored_text);
        }

        // Return resultant underscored text
        return $underscored_text;
    }

    /**
     * Singularizes a noun.
     *
     * @param $plural_noun string Noun to singularize.
     *
     * @return string the noun singularized and
     * lowerized
     */
    static function singularize($plural_noun) {
        // Set to lower
        $plural_noun = mb_strtolower($plural_noun, 'UTF-8');

        // If underscored, singularize only last word
        if (strpos($plural_noun, '_') !== false) {
            // Split words
            $words = explode('_', $plural_noun);

            // Singularize last word
            $words[sizeof($words) - 1] = Inflector::singularize($words[sizeof($words) - 1]);

            // Join words
            return implode('_', $words);
        }

        // Check if its the plural of an irregular noun
        $singular_noun = array_search($plural_noun, Inflector::$irregular_nouns);

        if ($singular_noun) {
            // Remove trailing index [2], [3]...
            $singular_noun = explode('[', $singular_noun);
            $singular_noun = trim($singular_noun[0]);
        } else {
            // Singularize the noun manualy
            if (mb_strlen($plural_noun) >= 3 && mb_substr($plural_noun, mb_strlen($plural_noun) - 3, 3, 'UTF-8') == 'ies') {
                // Change the trailing 'ies' for a 'y'
                $singular_noun = mb_substr($plural_noun, 0, mb_strlen($plural_noun, 'UTF-8') - 3, 'UTF-8') . 'y';
            } elseif (mb_strlen($plural_noun) >= 3 && mb_substr($plural_noun, mb_strlen($plural_noun) - 3, 3, 'UTF-8') == 'ses') {
                // Change the trailing 'ses' for a 's'
                $singular_noun = mb_substr($plural_noun, 0, mb_strlen($plural_noun, 'UTF-8') - 3, 'UTF-8') . 's';
            } elseif (mb_strlen($plural_noun) >= 3 && mb_substr($plural_noun, mb_strlen($plural_noun) - 3, 3, 'UTF-8') == 'hes') {
                // Change the trailing 'hes' for a 'h'
                $singular_noun = mb_substr($plural_noun, 0, mb_strlen($plural_noun, 'UTF-8') - 3, 'UTF-8') . 'h';
            } elseif (mb_strlen($plural_noun, 'UTF-8') >= 1 && mb_substr($plural_noun, mb_strlen($plural_noun, 'UTF-8') - 1, 1, 'UTF-8') == 's') {
                // Remove trailing 's'
                $singular_noun = mb_substr($plural_noun, 0, mb_strlen($plural_noun, 'UTF-8') - 1, 'UTF-8');
            } else {
                // Do nothing
                $singular_noun = $plural_noun;
            }
        }

        // Return singular noun
        return $singular_noun;
    }

    /**
     * Pluralizes a noun.
     *
     * @param $singular_noun string Noun to pluralize
     *
     * @return string the noun pluralized and lowerized
     */
    static function pluralize($singular_noun) {
        // Set to lower
        $singular_noun = mb_strtolower($singular_noun, 'UTF-8');

        // If underscored, pluralize only last word
        if (strpos($singular_noun, '_') !== false) {
            // Split words
            $words = explode('_', $singular_noun);

            // Pluralize last word
            $words[sizeof($words) - 1] = Inflector::pluralize($words[sizeof($words) - 1]);

            // Join words
            return implode('_', $words);
        }

        // Check if its an irregular noun
        $plural_noun = (isset(Inflector::$irregular_nouns[$singular_noun]) ? Inflector::$irregular_nouns[$singular_noun] : null);

        if (!$plural_noun) {
            // Pluralize the noun manualy
            if (mb_strlen($singular_noun, 'UTF-8') >= 1 && mb_substr($singular_noun, mb_strlen($singular_noun, 'UTF-8') - 1, 1, 'UTF-8') == 'y') {
                // Change the trailing 't' for 'ies'
                $plural_noun = mb_substr($singular_noun, 0, mb_strlen($singular_noun, 'UTF-8') - 1) . 'ies';
            } elseif (mb_strlen($singular_noun, 'UTF-8') >= 1 && mb_substr($singular_noun, mb_strlen($singular_noun, 'UTF-8') - 1, 1, 'UTF-8') == 's') {
                // Add 'es'
                $plural_noun = $singular_noun . 'es';
            } elseif (mb_strlen($singular_noun, 'UTF-8') >= 1 && mb_substr($singular_noun, mb_strlen($singular_noun, 'UTF-8') - 1, 1, 'UTF-8') == 'h') {
                // Add 'es'
                $plural_noun = $singular_noun . 'es';
            } else {
                // Add an 's'
                $plural_noun = $singular_noun . 's';
            }
        }

        // Return singular noun
        return $plural_noun;
    }

    /**
     * @param $name
     * @return string
     */
    public static function lowercaseName($name) {
        $name = trim(mb_strtolower($name, 'UTF-8'));
        $separators = array(' ', '-', '\'', '´', '.', '>', '<');

        foreach ($separators as $separator) {
            $words = explode($separator, $name);

            foreach ($words as $index => $word) {
                if (!$word) {
                } else {
                    $words[$index] = mb_strtoupper(mb_substr($word, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($word, 1);
                }
            }

            $name = implode($separator, $words);
        }

        return $name;
    }

    /**
     * English irregular nouns
     *
     * @var array
     */
    static $irregular_nouns = array(
        'alumnus' => 'alumni',
        'attendant' => 'attendees',
        'analysis' => 'analyses',
        'antenna' => 'antennae',
        'appendix' => 'appendices',
        'axis' => 'axes',
        'bacterium' => 'bacteria',
        'basis' => 'bases',
        'beau' => 'beaux',
        'bureau' => 'bureaus',
        'cactus' => 'cacti',
        'child' => 'children',
        'corpus' => 'corpora',
        'crisis' => 'crises',
        'criterion' => 'criteria',
        'curriculum' => 'curricula',
        'datum' => 'data',
        'deer' => 'deer',
        'diagnosis' => 'diagnoses',
        'ellipsis' => 'ellipses',
        'fish' => 'fish',
        'focus' => 'foci',
        'focus [2]' => 'focuses',
        'foot' => 'feet',
        'formula' => 'formulae',
        'formula [2]' => 'formulas',
        'fungus' => 'fungi',
        'fungus [2]' => 'funguses',
        'genus' => 'genera',
        'goose' => 'geese',
        'hypothesis' => 'hypotheses',
        'index' => 'indeces',
        'index [2]' => 'indexes',
        'louse' => 'lice',
        'man' => 'men',
        'matrix' => 'matrixes',
        'matrix [2]' => 'matrices',
        'means' => 'means',
        'medium' => 'media',
        'memorandum' => 'memoranda',
        'mouse' => 'mice',
        'nebula' => 'nebulae',
        'news' => 'news',
        'nucleus' => 'nuclei',
        'oasis' => 'oases',
        'offspring' => 'offspring',
        'ox' => 'oxen',
        'paralysis' => 'paralyses',
        'parenthesis' => 'parentheses',
        'phenomenon' => 'phenomena',
        'radius' => 'radii',
        'series' => 'series',
        'sheep' => 'sheep',
        'species' => 'species',
        'stimulus' => 'stimuli',
        'stratum' => 'strata',
        'synopsis' => 'synopses',
        'synthesis' => 'syntheses',
        'tableau' => 'tableaus',
        'tableau [2]' => 'tableaux',
        'thesis' => 'theses',
        'tooth' => 'teeth',
        'vertebra' => 'vertebrae',
        'vita' => 'vitae',
        'woman' => 'women',
        'status' => 'statuses',
    );
}
