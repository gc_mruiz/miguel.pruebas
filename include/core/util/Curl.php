<?php

/**
 * Handles Curl request
 */
class Curl {
    /**
     * Loads url content
     *
     * @param $url string URL to load content from
     * @param $postData array data to POST to URL (array)
     * @param $httpAuth array ('username' =>, 'password' =>)
     * @param $cache_hours float
     * @param $headers array custom headers
     *
     * @return string page content
     */
    static function getContent($url, $postData = array(), $httpAuth = array(), $cache_hours = 0.00, $headers = array()) {
        if ($cache_hours > 0) {
            $cacheOptions = array(
                'user_id' => null,
                'expires' => date('Y-m-d H:i:s', strtotime('+' . $cache_hours . ' hours')),
                'clearers' => array(),
            );

            return MethodCache::executeStatic('Curl', 'getContent', array(
                $url,
                $postData,
                $httpAuth,
                null,
            ), $cacheOptions);
        }

        // Init curl
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Setup headers
        $headers[] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5';
        $headers[] = 'Cache-Control: max-age=0';
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Keep-Alive: 300';
        $headers[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7';
        $headers[] = 'Accept-Language: en-us,en;q=0.5';
        $headers[] = 'Pragma: ';

        // Select a random referer to trick robots
        $referrers = array(
            'google.com',
            'yahoo.com',
            'msn.com',
            'ask.com',
            'live.com',
        );
        $choice = array_rand($referrers);
        $referrer = 'http://' . $referrers[$choice] . '';

        // Select a random browser to trick robots
        $browsers = array(
            'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.3) Gecko/2008092510 Ubuntu/8.04 (hardy) Firefox/3.0.3',
            'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1) Gecko/20060918 Firefox/2.0',
            'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)',
        );
        $choice2 = array_rand($browsers);
        $browser = $browsers[$choice2];

        // Set curl options
        if (!empty($httpAuth)) {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $httpAuth['username'] . ':' . $httpAuth['password']);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, $browser);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_REFERER, $referrer);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 7);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        // If there is data to POST
        if (!empty($postData)) {
            // URL-encode array var1=val1&var2=val2
            $post_query = http_build_query($postData);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_query);
        }

        // Retrieve data
        $data = curl_exec($ch);

        // If there was an error, load id
        if ($data === false) {
            $data = curl_error($ch);
        }

        // Close curl
        curl_close($ch);

        // Return the loaded data
        return $data;
    }

    /**
     * Calls itself without waiting to launch a background task.
     *
     * @param $url
     * @param int $timeout
     */
    static function backgroundTask($url, $timeout = 1) {
        // Disables HTTPS if server does not support calls to itself over the protocol
        if (GestyMVC::config('disable_https_on_background_tasks') && stripos($url, 'https://') === 0) {
            $url = 'http://' . substr($url, 8);
        }

        // Calls the URL without waiting nor processing the server response
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        curl_close($ch);
    }
}
