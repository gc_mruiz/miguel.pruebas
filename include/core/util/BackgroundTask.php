<?php

class BackgroundTask {
    /**
     * Executes an action over CLI in background.
     * Action must be public and protected with a calculated hash to avoid intrusion.
     *
     * @param $controller
     * @param $action
     * @param $params
     */
    public static function exec($controller, $action, $params) {
        $command = self::getBackgroundTaskCommand($controller, $action, $params);

        if ($command) {
            self::run_process($command);
        }
    }

    /**
     * @param $controller
     * @param $action
     * @param $params
     * @return bool|string
     */
    public static function getBackgroundTaskCommand($controller, $action, $params) {
        $php_exec_path = GestyMVC::config('php_exec_path');

        if (!$php_exec_path) {
            Debug::crash(true, 'Please define the PHP path on config.php before calling background tasks. GestyMVC::setConfig(\'php_exec_path\', \'\');');

            return false;
        }

        $url = $params;
        $url['controller'] = $controller;
        $url['action'] = $action;
        $url = str_replace(ROOT_URL, '/', Router::url($url));

        return $php_exec_path . ' "' . ROOT_PATH . 'index.php" --document_root="' . $_SERVER['DOCUMENT_ROOT'] . '" --host="' . $_SERVER['HTTP_HOST'] . '" --url="' . $url . '" --https="' . (Router::isHTTPS() ? 'true' : 'false') . '"';
    }

    /**
     * Runs process
     *
     * @param $cmd
     * @param string $outputFile
     * @param bool $append
     */
    private static function run_process($cmd, $outputFile = '/dev/null', $append = false) {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $WshShell = new COM('WScript.Shell');
            $WshShell->Run($cmd, 0, false);
        } else {
            shell_exec(sprintf('%s %s %s 2>&1 & echo $!', $cmd, ($append) ? '>>' : '>', $outputFile));
        }
    }
}