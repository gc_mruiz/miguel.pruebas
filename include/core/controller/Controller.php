<?php

/**
 * Class Controller.
 *
 * Handles an user request.
 */
class Controller {
    /**
     * Name of the extended controller class.
     *
     * @var null
     */
    public $name = null;

    /**
     * Vars that will be passed to the view.
     *
     * @var array()
     */
    protected $viewVars = array();

    /**
     * Module to be executed.
     *
     * @var string
     */
    protected $module = null;

    /**
     * Action to be executed.
     *
     * @var string
     */
    protected $action = null;

    /**
     * View to be loaded.
     *
     * @var string
     */
    protected $view = null;

    /**
     * Layout to be used.
     *
     * @var string|'admin'
     */
    protected $layout = 'admin';

    /**
     * Whether or not the view needs to be rendered automatically.
     * View can be rendered directy from the action, in this case it must not be rendered automatically;
     *
     * @var bool
     */
    protected $render_view_automatically = true;

    /**
     * Already executed callbacks.
     *
     * @var array
     */
    protected $executedCallbacks = array();

    /**
     * Access level of each action.
     *
     * Structure:
     *  array(
     *      'action_name' => 0, // -1 for public, 0+ for user with level at least equal to
     *      'other_action_name' => 1,
     *      'another_action_name' => 2,
     * );
     *
     * @var null
     */
    protected $actionAccessLevel = array();

    /**
     * $_POST security and validation.
     *
     * Structure:
     *  array(
     *      'action_name' => array('check_session' => true, 'check_fields' => true, 'map_checkboxes' => true)
     * );
     *
     * By default all options are set to true for all actions.
     *
     * @var null
     */
    protected $postValidation = array();

    /**
     * When validating $_POST array, some vars will be removed. The original $_POST array is stored on this variable for
     * other purposes.
     *
     * @var array
     */
    protected $originalPost = array();

    /**
     * Helpers to be loaded in the view
     *
     * @var array
     */
    protected $helpers = array();

    /**
     * Pagination conditions
     *
     * @var array
     */
    protected $pagination = array('fields' => '*');

    /**
     * Current page load parameters. Does not include $_GET nor $_POST parameters, only pathed ones.
     *
     * @var array
     */
    protected $params = array();

    /**
     * Whether or not to generate SEO tags.
     *
     * Structure:
     *  true for all, false for none
     *  array('canonical_url' => true, 'validGetParams' => array(), 'seo_title' => true, 'seo_description' => true,
     * 'og_image' => false,)
     *
     * @var bool|array options
     */
    protected $seo = true;

    /**
     * Caché config.
     *
     * Structure:
     *  false for no caching
     *  array(
     *      'layout' => 'public', // null for layout-caching
     *      'clearers' => array(
     *          'User' => 1,
     *          'Picture' => '*',
     *          'LayoutText' => array(1,2),
     *      )
     *  );
     *
     * Attention: clearer models must implement the CacheClearerBehavior or cache will never be cleared.
     */
    protected $cache = false;

    /**
     * Caché config. Whether or not to check for cache on this action.
     *
     */
    protected $cachedActions = array();

    /**
     * @var array
     */
    protected $cacheIgnoreGetParams = array();

    /**
     * Default response for json requests.
     *
     * @var array
     */
    protected $resultForLayout = array(
        'error' => null,
        'response' => array(
            'succeeded' => true,
        ),
    );

    /**
     * Constructor
     *
     * @param $name string
     *
     * @return Controller
     */
    public function Controller($name) {
        $this->name = $name;
    }

    /**
     * Runs the required action and it's callbacks.
     *
     * @param $module
     * @param $action
     * @param array $params
     */
    public function run($module, $action, $params = array()) {
        // Store parameter values
        $this->module = $module;
        $this->params = $params;
        $this->action = $action;

        // Match action to function
        $valid_function = false;

        // Check that action exists as function exists and it is a public function
        if (method_exists($this, $this->action)) {
            $reflectionMethod = new ReflectionMethod($this, $this->action);

            if ($reflectionMethod->isPublic()) {
                $valid_function = true;
            }
        }

        // If it is not valid, throw 404 error
        if (!$valid_function) {
            $this->notFound();
        }

        // On old servers magic quotes may need to be disabled
        $this->disableMagicQuotes();

        // Clean up post
        $this->cleanUpPostData();

        // Reorder $_FILES
        $this->reorderFiles();

        // Execute beforeFilter callback
        if (!$this->beforeFilter()) {
            exit();
        }

        // Retrieved cached data
        if (!empty($this->cachedActions[$this->action])) {
            $cached = $this->getCached();
        } else {
            $cached = false;
        }

        // Generate data if not cached
        if (!$cached) {
            // Function is still valid if action has not been changed
            $valid_function = ($action == $this->action);

            // IF action has changed, check that action exists as function exists (since it has been
            // changed programmatically, we do not check for public here).
            if (!$valid_function && method_exists($this, $this->action)) {
                $valid_function = true;
            }

            // If it is valid, run
            if ($valid_function) {
                call_user_func_array(array($this, $this->action), $this->params);
            } else {
                // The action name has been overridden by the beforeFilter callback. Therefore if it does not exists it
                // is a programming error and not a request error. Crash with log before sending a 404 header.
                Debug::crash(false, 'Method ' . $this->action . ' is undefined for class ' . $this->name . 'Controller');
                $this->notFound();
            }

            // Call the afterFilter if not already called
            if (!in_array('afterFilter', $this->executedCallbacks)) {
                if (!$this->afterFilter()) {
                    exit();
                }
            }

            // Render the view if required
            if ($this->render_view_automatically) {
                $this->render();
            }
        } else {
            // Render the cached data
            $this->renderCached($cached);
        }
    }

    /**
     * Throws a 404 error.
     *
     * @param $redirectToUrl bool|array|string whether or not to $this->redirect to the 404 page or the URL to redirect to
     */
    public function notFound($redirectToUrl = true) {
        // Set 404 header
        @ob_end_clean();
        header('HTTP/1.0 404 Not Found', true, 404);

        if ($redirectToUrl) {
            // If asked to $this->redirect, generate 404 URL indicating the current URL.
            if ($redirectToUrl === true) {
                $url = array(
                    'controller' => 'Errors',
                    'action' => 'notFound',
                    '?' => array('page' => Router::currentUrl()),
                );
            } else {
                $url = $redirectToUrl;
            }

            // Redirect
            header('Refresh: 0; url=' . Router::url($url));
            include_once(INCLUDE_PATH . 'core/assets/404.html');
        }

        // Stop execution
        exit();
    }

    /**
     * Revert magic quotes (very heavy, but needed if it cannot be disabled on the hosting)
     */
    private function disableMagicQuotes() {
        if (function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
            $process = array(
                &$_GET,
                &$_POST,
                &$_COOKIE,
                &$_REQUEST,
            );
            while (list($key, $val) = each($process)) {
                foreach ($val as $k => $v) {
                    unset($process[$key][$k]);
                    if (is_array($v)) {
                        $process[$key][stripslashes($k)] = $v;
                        $process[] = &$process[$key][stripslashes($k)];
                    } else {
                        $process[$key][stripslashes($k)] = stripslashes($v);
                    }
                }
            }
            unset($process);
        }
    }

    /**
     * Cleans the $_POST array removing unwanted variables and validating form submit.
     */
    protected function cleanUpPostData() {
        if (!empty($_POST)) {
            // Translate inputs
            if (isset($_POST['format'])) {
                $formattedFields = json_decode($_POST['format'], true);

                if ($formattedFields) {
                    foreach ($formattedFields as $field => $format) {
                        if ($field) {
                            $value_modified = false;
                            $value = NestedVariable::get($_POST, $field);

                            if ($value !== null && strlen($value)) {
                                if ($format == 'translated_date') {
                                    $value = date2php($value);
                                    $value_modified = true;
                                } elseif (in_array($format, array(
                                    'translated_euro',
                                    'euro',
                                    'translated_decimal',
                                    'translated_integer',
                                    'translated_percent',
                                ))) {
                                    $value_modified = true;

                                    if (LANG == 'en') {
                                        $value = str_replace(',', '', $value); // Remove thousands separator
                                    } else {
                                        $value = str_replace('.', '', $value); // Remove thousands separator
                                        $value = str_replace(',', '.', $value); // Replace decimal separator
                                    }

                                    if (in_array($format, array(
                                        'translated_euro',
                                        'euro',
                                    ))) {
                                        $value = str_replace(array(' ', '€', '&euro;'), '', $value);
                                    }

                                    if ($format == 'translated_percent') {
                                        $value = floatval(str_replace(array(' ', '%'), '', $value)) / 100;
                                    }
                                }

                                if ($value_modified) {
                                    NestedVariable::set($_POST, $field, $value);
                                }
                            }
                        }
                    }
                }
            }

            // Store original $_POST
            $this->originalPost = $_POST;

            // By default, do everything
            $actionPostValidation = array(
                'check_session' => true,
                'check_fields' => true,
                'map_checkboxes' => true,
            );

            // Override with specific configuration
            if (isset($this->postValidation[$this->action])) {
                // Merge
                if ($this->postValidation[$this->action] === false) {
                    $actionPostValidation = array(
                        'check_session' => false,
                        'check_fields' => false,
                        'map_checkboxes' => false,
                    );
                } elseif ($this->postValidation['*'] === false) {
                    $actionPostValidation = array(
                        'check_session' => false,
                        'check_fields' => false,
                        'map_checkboxes' => false,
                    );
                } else {
                    $actionPostValidation = array_merge($actionPostValidation, $this->postValidation[$this->action]);
                }
            } elseif (isset($this->postValidation['*'])) {
                // Merge
                if ($this->postValidation['*'] === false) {
                    $actionPostValidation = array(
                        'check_session' => false,
                        'check_fields' => false,
                        'map_checkboxes' => false,
                    );
                } elseif ($this->postValidation['*'] === true) {
                    $actionPostValidation = array(
                        'check_session' => true,
                        'check_fields' => true,
                        'map_checkboxes' => true,
                    );
                } else {
                    $actionPostValidation = array_merge($actionPostValidation, $this->postValidation['*']);
                }
            }

            // Check session if required
            if (isset($actionPostValidation['check_session']) && $actionPostValidation['check_session']) {
                // By default, the post is valid
                $post_is_valid = true;

                // Retrieve var values
                $time = NestedVariable::get($_POST, 'validation[time]');
                $token = NestedVariable::get($_POST, 'validation[token]');
                NestedVariable::clear($_POST, 'validation[time]');
                NestedVariable::clear($_POST, 'validation[token]');

                // Decode time
                if ($time) {
                    // Decode base 64
                    $time = @base64_decode($time);

                    // Parse to double
                    if (is_numeric($time)) {
                        $time = doubleval($time);
                    }
                }

                // Check variables
                if (!$token || !$time) {
                    // At least one of the vars is missing
                    $post_is_valid = false;
                } elseif (time() < $time || time() - $time > 60 * 60 * 2) {
                    // Form was printed too long ago
                    $post_is_valid = false;
                } elseif (md5(GestyMVC::config('form_helper_token') . $time . Router::currentUrl(true)) != $token) {
                    // Signature is invalid
                    $post_is_valid = false;
                }

                // Destroy $_POST if it is invalid
                if (!$post_is_valid) {
                    $_POST = array();
                }
            }

            // Map empty checkboxes if required
            if (isset($actionPostValidation['map_checkboxes']) && $actionPostValidation['map_checkboxes']) {
                // Retrieve var values and unset them
                $checkboxes = NestedVariable::get($_POST, 'validation[checkboxes]');
                NestedVariable::clear($_POST, 'validation[checkboxes]');

                // Foreach checkbox if it is not set, set it to empty string
                if ($checkboxes) {
                    // Split checkbox names
                    $checkboxes = explode(';', $checkboxes);

                    // Set vars to empty
                    foreach ($checkboxes as $checkbox_name) {
                        if (NestedVariable::get($_POST, $checkbox_name) === null) {
                            NestedVariable::set($_POST, $checkbox_name, '');
                        }
                    }
                }
            }

            // Check session if required
            if (!empty($_POST) && isset($actionPostValidation['check_fields']) && $actionPostValidation['check_fields']) {
                // By default, the post is valid
                $post_is_valid = true;

                // Retrieve var values and unset them
                $inputs = NestedVariable::get($_POST, 'validation[input]');
                $token = NestedVariable::get($_POST, 'validation[input_token]');
                NestedVariable::clear($_POST, 'validation[input]');
                NestedVariable::clear($_POST, 'validation[input_token]');

                // Check variables
                if (!$token || !$inputs) {
                    // At least one of the vars is missing
                    $post_is_valid = false;
                } elseif (md5(GestyMVC::config('form_helper_token') . $inputs) != $token) {
                    // Signature is invalid
                    $post_is_valid = false;
                }

                // Destroy $_POST if it is invalid
                if (!$post_is_valid) {
                    $_POST = array();
                } else {
                    // Post appears valid, we need now to check if each
                    // of the provided fields appear on the field list
                    $fieldList = explode(';', $inputs);

                    // Received field list
                    $postedFieldList = $this->extractFieldNames($_POST, array('validation'));

                    // Clear post
                    $_POST = array();

                    // Foreach posted field list, check
                    foreach ($postedFieldList as $field_name) {
                        if (in_array($field_name, $fieldList)) {
                            // Set variable in $_POST array if found
                            NestedVariable::set($_POST, $field_name, NestedVariable::get($this->originalPost, $field_name));
                        }
                    }
                }
            }
        }
    }

    /**
     * Extracts the field names of the provided var in an Form-like naming.
     *
     * @param $var array
     * @param $ignoreIndexes array
     * @param $prefix string
     *
     * @return array list of fields
     */
    public function extractFieldNames($var, $ignoreIndexes = array(), $prefix = '') {
        // List of fields
        $fields = array();

        // Extract each field
        foreach ($var as $field_name => $field_value) {
            // Ignore excluded ones
            if (in_array($field_name, $ignoreIndexes)) {
                continue;
            }

            // Var name
            $var_name = $field_name;

            // If prefix, encapsulate
            if ($prefix) {
                $varNameParts = explode('[', str_replace(']', '', $var_name));
                $var_name = $prefix;

                foreach ($varNameParts as $var_name_part) {
                    $var_name .= '[' . $var_name_part . ']';
                }
            }

            if (is_array($field_value)) {
                // Field is an array, there is then more field inside,
                // extract recursively and merge
                $fields = array_merge($fields, $this->extractFieldNames($field_value, array(), $var_name));
            } else {
                // If field value is not an array, add the field itself
                $fields[] = $var_name;
            }
        }

        // Return the generated list of fields
        return $fields;
    }

    /**
     * The $_FILES array is counter-intuitive. Reorder ir as should be.
     */
    private function reorderFiles() {
        // Rearrange file upload array
        if (!empty($_FILES)) {
            $fileUploadIndexes = array_keys($_FILES);

            foreach ($fileUploadIndexes as $file_upload_index) {
                if (isset($_FILES[$file_upload_index]['tmp_name']) && !is_array($_FILES[$file_upload_index]['tmp_name'])) {
                    $_FILES[$file_upload_index] = array($_FILES[$file_upload_index]);
                } elseif (isset($_FILES[$file_upload_index]['tmp_name']) && is_array($_FILES[$file_upload_index]['tmp_name'])) {
                    $files_ = array();

                    for ($i = 0; $i < sizeof($_FILES[$file_upload_index]['tmp_name']); $i++) {
                        $files_[] = array(
                            'name' => $_FILES[$file_upload_index]['name'][$i],
                            'type' => $_FILES[$file_upload_index]['type'][$i],
                            'tmp_name' => $_FILES[$file_upload_index]['tmp_name'][$i],
                            'error' => $_FILES[$file_upload_index]['error'][$i],
                            'size' => $_FILES[$file_upload_index]['size'][$i],
                        );
                    }

                    $_FILES[$file_upload_index] = $files_;
                }
            }
        }
    }

    /**
     * Before filter callback. Called before the action is called.
     * Checks access.
     *
     * @return bool if not true stops execution
     */
    protected function beforeFilter() {
        // Disable framming for this page if required (for security reasons)
        if (GestyMVC::config('x_frame_options')) {
            header('X-Frame-Options: ' . GestyMVC::config('x_frame_options'));
        }

        // Store executed callback to avoid multiple executions when
        // when order changes
        $this->executedCallbacks[] = 'beforeFilter';

        // Check if user has access
        if (!empty($this->actionAccessLevel)) {
            // Current action level
            $action_level = null;

            // Check for specific configuration
            if (isset($this->actionAccessLevel[$this->action])) {
                $action_level = $this->actionAccessLevel[$this->action];
            }

            // Check for wildcard
            if ($action_level === null && isset($this->actionAccessLevel['*'])) {
                $action_level = $this->actionAccessLevel['*'];
            }

            // Validate access
            if ($action_level !== null && $action_level > -1) {
                if (!Authentication::get('user', 'id') || Authentication::get('user', 'level') < $action_level) {
                    // Access denied, $this->redirect to login
                    if (!Authentication::get('user', 'id')) {
                        // If user is not logged in, set the return to parameter
                        $this->redirect(array(
                                            'controller' => 'Users',
                                            'action' => 'login',
                                            'return' => Router::currentUrl(),
                                        ));
                    } else {
                        // If user is logged in, do not set the return to parameter to avoid
                        // infinite $this->redirections
                        $this->redirect(array(
                                            'controller' => 'Users',
                                            'action' => 'login',
                                        ));
                    }

                    // Stop execution of the controller (although it is already stopped by
                    // the $this->redirect command). This line is here just for readability purposes
                    return false;
                }
            }
        }

        // Continue execution
        return true;
    }

    /**
     * Redirects user to an URL.
     *
     * @param $options array|string options to be translated to Router::url
     */
    public function redirect($options) {
        // If action is set and controller is not, set controller to current one
        if (isset($options['action']) && !isset($options['controller'])) {
            $options['controller'] = $this->name;
        }

        // Generate URL with Router
        $url = Router::url($options);

        // Redirect with header
        header('Location: ' . $url);
        exit();
    }

    /**
     * Retrieves the cached information for the requested url.
     *
     * @return bool|array
     */
    protected function getCached() {
        $result = array('success' => false);
        $cacheAuth = GestyMVC::config('cacheAuth');

        $getParams = Dispatcher::originalGet();
        foreach ($this->cacheIgnoreGetParams as $param) {
            unset($getParams[$param]);
        }

        $has_auth = false;

        foreach ($cacheAuth as $authentication_method => $key) {
            if (Authentication::get($authentication_method, 'id')) {
                $has_auth = true;
            }

            if (!$result['success'] && Authentication::get($authentication_method, 'id')) {
                // Generate filename
                $filename = 'views/' . str_replace('_', '-', Inflector::underscore($this->name)) . '/' . str_replace('_', '-', Inflector::underscore($this->action)) . '/' . md5($key . Authentication::get($authentication_method, 'id') . json_encode($this->params) . json_encode($getParams)) . '.json';
                $filename = CachedAdapter::getKey() . '/' . $filename;

                $result = CachedAdapter::get($filename);
            }

            if ($has_auth) {
                break;
            }
        }

        if (!$has_auth && !$result['success']) {
            // Generate filename
            $filename = 'views/' . str_replace('_', '-', Inflector::underscore($this->name)) . '/' . str_replace('_', '-', Inflector::underscore($this->action)) . '/' . md5(json_encode($this->params) . json_encode($getParams)) . '.json';
            $filename = CachedAdapter::getKey() . '/' . $filename;

            $result = CachedAdapter::get($filename);
        }

        if ($result['success']) {
            return array(
                'content' => $result['content']['content'],
                'layout' => $result['content']['layout'],
            );
        }

        // No cache
        return false;
    }

    /**
     * After filter callback. Called after the action is called and before the beforeRender callback.
     *
     * @return bool if not true stops execution
     */
    protected function afterFilter() {
        // Store executed callback to avoid multiple executions when
        // when order changes
        $this->executedCallbacks[] = 'afterFilter';

        return true;
    }

    /**
     * Renders a view.
     *
     * @param $view string|null view to render
     * @param $options array
     *  - force_download
     *  - filename
     */
    protected function render($view = null, $options = array()) {
        // Update view if required
        if ($view !== null) {
            $this->view = $view;
        }

        // Set the view to no auto-rendering (already rendered)
        $this->render_view_automatically = false;

        // Call the afterFilter if not already called
        if (!in_array('afterFilter', $this->executedCallbacks)) {
            if (!$this->afterFilter()) {
                exit();
            }
        }

        // Call the beforeRender if not already called
        if (!in_array('beforeRender', $this->executedCallbacks)) {
            if (!$this->beforeRender()) {
                exit();
            }
        }

        // Generate view if required
        if ($this->view === null) {
            $this->view = str_replace('_', '-', Inflector::underscore($this->action));
        }

        // Assume current controller group views if folder was not specified
        if ($this->view !== false && strpos($this->view, '/') === false) {
            $this->view = str_replace('_', '-', Inflector::underscore($this->name)) . '/' . $this->view;
        }

        // Set view for layout
        $this->set('view_for_layout', $this->view);

        // Generate view content
        $viewContent = View::render($this->module, $this->view, $this->layout, $this->viewVars, $this->helpers);

        // Call the beforeRender if not already called
        if (!in_array('afterRender', $this->executedCallbacks)) {
            if (!$this->afterRender($viewContent['full'], $viewContent['view'])) {
                exit();
            }
        }

        // Force download if required
        if (isset($options['force_download']) && $options['force_download']) {
            // Set default filename
            if (!isset($options['filename']) || !$options['filename']) {
                $options['filename'] = 'index.html';
            }

            // Set headers
            header('Content-Type: application/force-download');
            header('Content-Length: ' . strlen($viewContent['full']));
            header('Content-Disposition: attachment; filename=' . $options['filename']);
        }

        if (ends_with($viewContent['full'], '</html>')) {
            $viewContent['full'] = substr($viewContent['full'], 0, -strlen('</html>')) . '<!-- took ' . number_format(GestyMVC::elapsedMillis(), 0, '.', ' ') . 'ms--></html>';
        }

        // Output view content and stop execution
        echo $viewContent['full'];
        exit();
    }

    /**
     * Before render callback. Called before the action's view is called.
     *
     * @return bool if not true stops execution
     */
    protected function beforeRender() {
        // Store executed callback to avoid multiple executions when
        // when order changes
        $this->executedCallbacks[] = 'beforeRender';

        // Set result for layout
        $this->set('resultForLayout', $this->resultForLayout);

        // Generate seo vars if required
        if ($this->seo !== false) {
            $this->defineSeo();
        }

        return true;
    }

    /**
     * Sets a variable for the view.
     *
     * @param $var string|array name of the var to set
     * @param $value array
     */
    public function set($var, $value = array()) {
        // Set multiple or single variables with the same function
        if (is_array($var)) {
            $this->viewVars = array_merge($this->viewVars, $var);
        } else {
            $this->viewVars[$var] = $value;
        }
    }

    /**
     * Defines seo vars
     */
    private function defineSeo() {
        // Fill seo configuration using conventions
        $this->fillSeoConfiguration();

        // Initialize SEO variables
        $canonical_url = null;
        $seo_title = null;
        $seo_h1 = null;
        $seo_description = null;
        $og_image = null;

        // Calculate canonical URL
        if ($this->seo['canonical_url']) {
            if (NestedVariable::get($this->viewVars, 'canonical_url') && NestedVariable::get($this->viewVars, 'canonical_url') != 'none') {
                $canonical_url = Router::url($this->viewVars['canonical_url']);
            } else {
                // Retrieve current request params from dispatcher
                $urlOptions = array(
                    'controller' => $this->name,
                    'action' => $this->action,
                    '?' => array(),
                );

                // Merge
                foreach ($this->params as $param) {
                    $urlOptions[] = $param;
                }

                // Foreach valid get value, add it if present
                foreach ($this->seo['validGetParams'] as $valid_get_param) {
                    if (isset($_GET[$valid_get_param]) && strlen($_GET[$valid_get_param]) > 0) {
                        $urlOptions['?'][$valid_get_param] = $_GET[$valid_get_param];
                    }
                }

                // Generate URL
                $canonical_url = Router::url($urlOptions);
                unset($urlOptions);
            }
        }

        // Attempt to retrieve seo info from the canonical URL
        if (isset($canonical_url)) {
            // We check whether the url in the database has a full path
            if (GestyMVC::config('full_path_in_canonical') === false) {
                $canonical_url = str_ireplace(ROOT_URL, '', $canonical_url);
            }

            // Initialize CanonicalUrl model
            /* @var CanonicalUrl $CanonicalUrl */
            $CanonicalUrl = MySQLModel::getInstance('CanonicalUrl');
            $canonicalUrl = $CanonicalUrl->getByUrl($canonical_url, array('recursive' => 0));

            // If element was found, override
            if ($canonicalUrl) {
                if (!empty($canonicalUrl['CanonicalUrl']['seo_title']) && $canonicalUrl['CanonicalUrl']['seo_title'] != 'none') {
                    $this->viewVars['seo_title'] = $canonicalUrl['CanonicalUrl']['seo_title'];
                }
                if (!empty($canonicalUrl['CanonicalUrl']['seo_h1']) && $canonicalUrl['CanonicalUrl']['seo_h1'] != 'none') {
                    $this->viewVars['seo_h1'] = $canonicalUrl['CanonicalUrl']['seo_h1'];
                }
                if (!empty($canonicalUrl['CanonicalUrl']['seo_description']) && $canonicalUrl['CanonicalUrl']['seo_description'] != 'none') {
                    $this->viewVars['seo_description'] = $canonicalUrl['CanonicalUrl']['seo_description'];
                }
                if (!empty($canonicalUrl['Picture']['id'])) {
                    $this->viewVars['og_image'] = $canonicalUrl['Picture']['src'];
                }
            }
        }

        // Calculate seo title
        if ($this->seo['seo_title']) {
            if (isset($this->viewVars['seo_title']) && $this->viewVars['seo_title'] != 'none') {
                $seo_title = $this->viewVars['seo_title'];
            } elseif (isset($this->viewVars['seoinfo'])) {
                if (isset($this->viewVars['seoinfo']['seo_title']) && $this->viewVars['seoinfo']['seo_title'] != 'none') {
                    $seo_title = $this->viewVars['seoinfo']['seo_title'];
                } elseif (isset($this->viewVars['seoinfo']['seo_title_if_empty']) && $this->viewVars['seoinfo']['seo_title_if_empty'] != 'none') {
                    $seo_title = $this->viewVars['seoinfo']['seo_title_if_empty'];
                } elseif (isset($this->viewVars['seoinfo']['title']) && $this->viewVars['seoinfo']['title'] != 'none') {
                    $seo_title = $this->viewVars['seoinfo']['title'];
                } elseif (isset($this->viewVars['seoinfo']['name']) && $this->viewVars['seoinfo']['name'] != 'none') {
                    $seo_title = $this->viewVars['seoinfo']['name'];
                }
            }

            // Cleanup title for layout
            if ($seo_title) {
                $seo_title = $this->cleanTextForSeo($seo_title, 65);
            }
        }

        // Calculate seo title
        if ($this->seo['seo_h1']) {
            if (isset($this->viewVars['seo_h1']) && $this->viewVars['seo_h1'] != 'none') {
                $seo_h1 = $this->viewVars['seo_h1'];
            } elseif (isset($this->viewVars['seoinfo'])) {
                if (isset($this->viewVars['seoinfo']['seo_h1']) && $this->viewVars['seoinfo']['seo_h1'] != 'none') {
                    $seo_h1 = $this->viewVars['seoinfo']['seo_h1'];
                } elseif (isset($this->viewVars['seoinfo']['seo_h1_if_empty']) && $this->viewVars['seoinfo']['seo_h1_if_empty'] != 'none') {
                    $seo_h1 = $this->viewVars['seoinfo']['seo_h1_if_empty'];
                } elseif (isset($this->viewVars['seoinfo']['title']) && $this->viewVars['seoinfo']['title'] != 'none') {
                    $seo_h1 = $this->viewVars['seoinfo']['title'];
                } elseif (isset($this->viewVars['seoinfo']['name']) && $this->viewVars['seoinfo']['name'] != 'none') {
                    $seo_h1 = $this->viewVars['seoinfo']['name'];
                }
            }

            // Cleanup title for layout
            if ($seo_h1) {
                $seo_h1 = $this->cleanTextForSeo($seo_h1, 65);
            }
        }

        // Calculate seo description
        if ($this->seo['seo_description']) {
            if (isset($this->viewVars['seo_description']) && $this->viewVars['seo_description'] != 'none') {
                $seo_description = $this->viewVars['seo_description'];
            } elseif (isset($this->viewVars['seoinfo'])) {
                if (isset($this->viewVars['seoinfo']['seo_description']) && $this->viewVars['seoinfo']['seo_description'] != 'none') {
                    $seo_description = $this->viewVars['seoinfo']['seo_description'];
                } elseif (isset($this->viewVars['seoinfo']['seo_description_if_empty']) && $this->viewVars['seoinfo']['seo_description_if_empty'] != 'none') {
                    $seo_description = $this->viewVars['seoinfo']['seo_description_if_empty'];
                } elseif (isset($this->viewVars['seoinfo']['description']) && $this->viewVars['seoinfo']['description'] != 'none') {
                    $seo_description = $this->viewVars['seoinfo']['description'];
                } elseif (isset($this->viewVars['seoinfo']['content']) && $this->viewVars['seoinfo']['name'] != 'content') {
                    $seo_description = $this->viewVars['seoinfo']['content'];
                }
            }

            // Cleanup description
            if ($seo_description) {
                $seo_description = $this->cleanTextForSeo($seo_description, 165);
            }
        }

        // Calculate og image
        if ($this->seo['og_image']) {
            if (NestedVariable::get($this->viewVars, 'og_image') && NestedVariable::get($this->viewVars, 'og_image') != 'none') {
                $og_image = $this->viewVars['og_image'];
            }

            // Cleanup image
            if ($og_image) {
                $og_image = Router::url($og_image);
            }
        }

        // Store final vars
        $this->set(compact('seo_title', 'seo_h1', 'seo_description', 'og_image', 'canonical_url'));
    }

    /**
     * Fill seo configuration using conventions
     */
    private function fillSeoConfiguration() {
        if ($this->seo === true) {
            // True for all true
            $this->seo = array(
                'canonical_url' => true,
                'validGetParams' => array(),
                'seo_title' => true,
                'seo_h1' => true,
                'seo_description' => true,
                'og_image' => true,
            );
        } elseif ($this->seo === false) {
            // False for all false
            $this->seo = array(
                'canonical_url' => false,
                'validGetParams' => array(),
                'seo_title' => false,
                'seo_h1' => false,
                'seo_description' => false,
                'og_image' => false,
            );
        } else {
            // Array for default with overrides
            $this->seo = array_merge(array(
                                         'canonical_url' => true,
                                         'validGetParams' => array(),
                                         'seo_title' => true,
                                         'seo_h1' => true,
                                         'seo_description' => true,
                                         'og_image' => true,
                                     ), $this->seo);
        }
    }

    /**
     * Trims, removes line breaks, tabs, double spaces and html tags from a text for HTML SEO tag use.
     *
     * @param $text
     * @param $max_length
     *
     * @return string
     */
    private function cleanTextForSeo($text, $max_length) {
        // Add spaces to avoid word being joined and remove line breaks
        $text = str_replace(array(
                                ' < br',
                                ' < br',
                                "\n",
                                "\t",
                                "\r",
                            ), array(
                                ' < br',
                                ' < p',
                                ' ',
                                ' ',
                                ' ',
                            ), $text);

        // Trim
        $text = trim($text);

        // Remove HTML tags
        if (strpos($text, ' < ') !== false) {
            $text = trim(strip_tags($text));
        }

        // Remove double spaces
        while (strpos($text, '  ') !== false) {
            $text = str_replace('  ', ' ', $text);
        }

        // Cut down
        if (mb_strlen($text, 'UTF-8') > $max_length) {
            $text = mb_substr($text, 0, $max_length - 3, 'UTF-8') . '...';
        }

        // Return optimized text
        return $text;
    }

    /**
     * After render callback. Called after the action's view is called and before the layout view is called.
     *
     * @param $full_content string generated view content
     * @param $only_view_content string generated view content (without layout)
     *
     * @return bool if not true stops execution
     */
    protected function afterRender(&$full_content, $only_view_content) {
        // Store executed callback to avoid multiple executions when
        // when order changes
        $this->executedCallbacks[] = 'afterRender';

        // Save cache
        $this->saveCache($full_content, $only_view_content);

        // Close connection to database if open
        MySQLModel::closeConnection();

        return true;
    }

    /**
     * Creates a cache for this request.
     *
     * @param $full_content string
     * @param $view_content string
     */
    protected function saveCache($full_content, $view_content) {
        // If cache is enabled and there are no flash messages
        if ($this->cache !== false && !HtmlHelper::flashPainted()) {
            // Set default options
            $this->cache = array_merge(array(
                                           'expires' => null,
                                           'layout' => $this->layout,
                                           'clearers' => array(),
                                       ), $this->cache);

            $cacheAuth = GestyMVC::config('cacheAuth');
            $filename = null;

            $getParams = Dispatcher::originalGet();
            foreach ($this->cacheIgnoreGetParams as $param) {
                unset($getParams[$param]);
            }

            foreach ($cacheAuth as $authentication_method => $key) {
                if (!$filename && Authentication::get($authentication_method, 'id')) {
                    // Generate filename
                    $filename = 'views/' . str_replace('_', '-', Inflector::underscore($this->name)) . '/' . str_replace('_', '-', Inflector::underscore($this->action)) . '/' . md5($key . Authentication::get($authentication_method, 'id') . json_encode($this->params) . json_encode($getParams)) . '.json';
                }
            }

            if (!$filename) {
                // Generate filename
                $filename = 'views/' . str_replace('_', '-', Inflector::underscore($this->name)) . '/' . str_replace('_', '-', Inflector::underscore($this->action)) . '/' . md5(json_encode($this->params) . json_encode($getParams)) . '.json';
            }

            // Shorten url
            $filename = CachedAdapter::getKey() . '/' . $filename;

            if ($cachedPreviousContent = CachedAdapter::get($filename)) {
                if ($cachedPreviousContent['success']) {
                    return;
                }
            }

            // Write cache
            CachedAdapter::set($filename, array(
                'content' => ($this->cache['layout'] ? $view_content : $full_content),
                'layout' => $this->cache['layout'],
            ), $this->cache['clearers']);
        }
    }

    /**
     * Renders a view based on the cache.
     *
     * @param $cached array
     */
    protected function renderCached($cached) {
        // Layout may need to be loaded
        if ($cached['layout']) {
            // Call the beforeRender if not already called
            if (!in_array('beforeRender', $this->executedCallbacks)) {
                if (!$this->beforeRender()) {
                    exit();
                }
            }

            // Generate view content
            $this->viewVars['content_for_layout'] = $cached['content'];
            $cached['content'] = View::render($this->module, 'layouts/' . $cached['layout'], null, $this->viewVars, $this->helpers);
            $cached['content'] = $cached['content']['full'];
        }

        if (ends_with($cached['content'], '</html>')) {
            $cached['content'] = substr($cached['content'], 0, -strlen('</html>')) . '<!-- took ' . number_format(GestyMVC::elapsedMillis(), 0, '.', ' ') . 'ms--></html>';
        }

        // Output view content and stop execution
        echo $cached['content'];
        exit();
    }

    /**
     * Throws a 403 error.
     *
     * @param $redirect bool whether or not to $this->redirect to the 403 page.
     */
    protected function forbidden($redirect = true) {
        // Set 403 header
        header('HTTP/1.0 403 Forbidden');

        if ($redirect) {
            // If asked to $this->redirect, generate 404 URL indicating the current URL.
            $url = array(
                'controller' => 'Errors',
                'action' => 'forbidden',
                '?' => array('page' => Router::currentUrl()),
            );

            // Redirect
            $this->redirect($url);
        } else {
            // Stop execution
            exit();
        }
    }

    /**
     * Paginates based on the $this->pagination configuration.
     *
     * @return array results
     */
    protected function paginate($return_elements = true) {
        // Fill seo configuration using conventions and add
        // 'p' and 'order' as valid params
        $this->fillSeoConfiguration();
        $this->seo['validGetParams'][] = 'p';
        $this->seo['validGetParams'][] = 'order';

        // Fill array with default options
        $this->pagination = array_merge(array(
                                            'model' => null,
                                            'where' => 1,
                                            'default_order_by_expression' => null,
                                            'default_order_by_direction' => 'ASC',
                                            'elements_per_page' => 20,
                                            'propagate_query_string' => true,
                                            'options' => array(),
                                            'count_field' => 'id',
                                            'accumulate_orders' => !!GestyMVC::config('pagination_accumulate_orders')
                                        ), $this->pagination);

        // If model was not set, cannot search, fail
        if (!$this->pagination['model']) {
            Debug::crash(true, 'Attempting to paginate in controller ' . $this->name . ' on action ' . $this->action . ' without setting the model.');
        }

        // Set default order if not set
        if (!$this->pagination['default_order_by_expression']) {
            $this->pagination['default_order_by_expression'] = substr($this->pagination['model']->order, 0, -4);
        }

        // Add paginator helper
        if (!in_array('Paginator', $this->helpers)) {
            $this->helpers[] = 'Paginator';
        }

        // Default query string for pages
        $queryString = null;

        // Propagate vars if required
        if ($this->pagination['propagate_query_string']) {
            $queryString = $_GET;
            unset($queryString['url']);
            unset($queryString['p']);
            unset($queryString['order_by']);
        } else {
            $queryString = array();
        }

        // Set default pagination values
        $pagination['accumulate_orders'] = $this->pagination['accumulate_orders'];
        $pagination['page'] = 1;
        $pagination['count'] = $this->pagination['model']->count(array(
                                                                     'field' => $this->pagination['count_field'],
                                                                     'where' => $this->pagination['where'],
                                                                 ));
        $pagination['elements_per_page'] = $this->pagination['elements_per_page'];
        $pagination['elementsPerPageOptions'] = (isset($this->pagination['elementsPerPageOptions']) && $this->pagination['elementsPerPageOptions'] ? $this->pagination['elementsPerPageOptions'] : array(
            10,
            25,
            50,
            100,
        ));

        if (array_key_exists('per_page', $_GET) && in_array($_GET['per_page'], $pagination['elementsPerPageOptions'])) {
            $pagination['elements_per_page'] = $_GET['per_page'];
            $pagination['elements_per_page_modified'] = true;
        } else {
            $pagination['elements_per_page_modified'] = false;
        }

        $pagination['pages'] = ceil($pagination['count'] / $pagination['elements_per_page']);
        $pagination['default_order'][] = array(
            'expression' => $this->pagination['default_order_by_expression'],
            'direction' => $this->pagination['default_order_by_direction'],
        );
        $pagination['default_order_by_expression'] = $this->pagination['default_order_by_expression'];
        $pagination['default_order_by_direction'] = $this->pagination['default_order_by_direction'];
        $pagination['baseUrl'] = array_merge(array('action' => $this->action, '?' => $queryString), $this->params);

        if (isset($this->pagination['max_pages'])) {
            $pagination['max_pages'] = $this->pagination['max_pages'];
        }

        // Check order by
        // Check if a specific order was requested and if
        // it is valid
        $get_order_by = get_var('order_by');

        if ($get_order_by) {
            $order_by = array();

            if (is_array($get_order_by)) {
                $order_by = $get_order_by;
            } else if (strpos($get_order_by, '[') == 0 && strpos($get_order_by, ']') == strlen($get_order_by) - 1) {
                $order_by = explode(',', substr($get_order_by, 1, -1));
            } else {
                $order_by[] = $get_order_by;
            }

            $pagination['order'] = array();

            foreach ($order_by as $order) {
                if (strpos($order, '+') !== false) {
                    // Retrieve order by expression and direction
                    $requested_order_by = explode('+', $order);
                    $requested_order_by_expression = trim($requested_order_by[0]);
                    $requested_order_by_direction = trim($requested_order_by[1]);

                    // Check if order by part count is valid (==2)
                    if (sizeof($requested_order_by) == 2) {
                        // Check if requested order by expression is valid
                        // and requested direction is valid
                        if (in_array($requested_order_by_expression, $this->pagination['model']->validOrderByExpressions) && in_array($requested_order_by_direction, array(
                                'ASC',
                                'DESC',
                            ))
                        ) {
                            // The requested order is valid
                            $pagination['order'][] = array(
                                'expression' => $requested_order_by_expression,
                                'direction' => $requested_order_by_direction,
                            );
                        } else {
                            // If requested order is invalid. Redirect to index
                            // (302)
                            if ($this->action) {
                                $this->redirect($pagination['baseUrl']);
                                break;
                            }
                        }
                    }
                }
            }

            // Check if requested order and direction are the
            // default ones
            if ($pagination['order'] == $pagination['default_order']) {
                if (!isset($this->pagination['allow_default_order']) || !$this->pagination['allow_default_order']) {
                    // If it is, $this->redirect to index (301) to avoid
                    // duplicate content
                    if ($this->action) {
                        $this->redirect301($pagination['baseUrl']);
                    }
                }
            }
        } else {
            $pagination['order'] = $pagination['default_order'];
        }

        // If order is not the default one, add to query
        // parts
        if ($pagination['order'] != $pagination['default_order']) {
            $pagination['baseUrl']['?']['order_by'] = array();

            foreach ($pagination['order'] as $order) {
                $pagination['baseUrl']['?']['order_by'][] = $order['expression'] . '+' . $order['direction'];
            }
        }

        // Check page
        $provided_valid_page = !isset($_GET['p']);

        // Check if a specific page was requested and if it
        // is numeric
        if (isset($_GET['p']) && $_GET['p'] && is_numeric($_GET['p'])) {
            // Convert requested page to float
            $_GET['p'] = $_GET['p'] * 1;

            // Check if page value is valid (integer, > 1 and
            // <= maximum)
            if (is_int($_GET['p']) && $_GET['p'] && $_GET['p'] > 1 && $_GET['p'] <= $pagination['pages']) {
                // Page is valid, set it
                $pagination['page'] = $_GET['p'];
                $provided_valid_page = true;
            } elseif ($_GET['p'] == 1) {
                if (!isset($this->pagination['allow_default_page']) || !$this->pagination['allow_default_page']) {
                    // Page is 1, $this->redirect to index (301) to avoid
                    // duplicate content
                    $this->redirect301($pagination['baseUrl']);
                }
            }
        }

        // If page is not valid, $this->redirect
        if (!$provided_valid_page) {
            // Requested page is not valid, $this->redirect to index
            // (302)
            if ($this->action) {
                $this->redirect($pagination['baseUrl']);
            }
        }

        // If page is not the default one, add to query
        // parts
        if ($pagination['page'] != 1) {
            $pagination['baseUrl']['?']['p'] = $pagination['page'];
        }

        $sql_order_by_expression = array();
        foreach ($pagination['order'] as $order) {
            $sql_order_by_expression[] = $order['expression'] . ' ' . $order['direction'];
        }

        // Set pagination data for the view
        $this->set('pagination', $pagination);

        // Join old options and new ones
        $options = array_merge($this->pagination['options'], array(
            'fields' => $this->pagination['fields'],
            'where' => $this->pagination['where'],
            'limit' => $pagination['elements_per_page'],
            'offset' => $pagination['elements_per_page'] * ($pagination['page'] - 1),
            'order' => $sql_order_by_expression,
        ));

        // Load elements
        if ($return_elements) {
            return $this->pagination['model']->getAll($options);
        } else {
            return null;
        }
    }

    /**
     * Redirects user to an URL with a 301 header.
     *
     * @param $options array|string options to be translated to Router::url
     */
    public function redirect301($options) {
        header('HTTP/1.1 301 Moved Permanently', true, 301);
        $this->redirect($options);
    }

    /**
     * Retrieves posted json input and decodes it.
     *
     * @param $decoded bool whether or not to decode data
     *
     * @return bool|mixed
     */
    protected function getPostedJson($decoded = true) {
        // Retrieve original post information
        $json = @file_get_contents('php://input');

        // If post is not empty
        if ($json !== null || $json !== false) {
            // Decode if requested
            if ($decoded) {
                // Decode object
                $obj = json_decode($json, true);
                // Check and return object
                if ($obj !== false) {
                    return $obj;
                }
            } else {
                // Return data
                return $json;
            }
        }

        // By default false
        return false;
    }
}