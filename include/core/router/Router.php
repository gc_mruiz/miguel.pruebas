<?php

/**
 * Class Router
 *
 * Manages routes.
 */
class Router {
    /**
     * Currently executing url relative to ROOT_URL.
     *
     * @var string
     */
    private static $current_url = null;

    /**
     * Defined routes.
     *
     * @var array
     */
    private static $routes = array();

    /**
     * Sets the current url
     *
     * @param $current_url string
     */
    public static function setCurrentUrl($current_url) {
        Router::$current_url = $current_url;
    }

    /**
     * Gets the current url
     *
     * @param $absolute bool
     *
     * @return string
     */
    public static function currentUrl($absolute = false) {
        return ($absolute ? ROOT_URL : '') . Router::$current_url;
    }

    /**
     * Generates a routed URL based on the options.
     *
     * @param $options array|string
     *  - controller
     *  - action
     *  - no_session: bool whether or not to use the no-session host instead of the current one
     *  - https: if set, changes the https configuration
     * @param $moreOptions array|null merges with the previous one
     *
     * @return string generated URL
     */
    public static function url($options, $moreOptions = null) {
        $hash = '';

        // Merge $options and $moreOptions
        if ($moreOptions !== null) {
            if (!is_array($options)) {
                $options = array('url' => $options);
            }

            $options = array_merge($options, $moreOptions);
        }

        // Generated url
        $url = null;

        if (!is_array($options)) {
            // If it is not an array, it is an URL
            if (starts_with($options, '/')) {
                // If it starts with slash, assume static resource and prepend with ROOT_URL
                $url = ROOT_URL . mb_substr($options, 1, mb_strlen($options, 'UTF-8') - 1, 'UTF-8');
            } else {
                // Check for protocol
                $protocol_pos = strpos($options, ':');

                if ($protocol_pos === false || $protocol_pos > 10) {
                    // If there is no protocol, it's a relative link
                    $url = LOCALED_ROOT_URL . $options;
                } else {
                    // If there is, it's an absolute link
                    $url = $options;
                }
            }
        } else {
            // Retrieve url params from options, remove special vars
            $urlParams = $options;
            unset($urlParams['controller']);
            unset($urlParams['action']);
            unset($urlParams['https']);
            unset($urlParams['no_session']);
            unset($urlParams['lang']);

            // If option ? is present, join get params and other options
            if (isset($urlParams['?'])) {
                $urlParams = array_merge($urlParams, $urlParams['?']);
                unset($urlParams['?']);
            }

            // If option # is present, extract
            if (array_key_exists('#', $urlParams)) {
                if ($urlParams['#']) {
                    $hash = '#' . $urlParams['#'];
                }

                unset($urlParams['#']);
            }

            // Path vars are the non-named variables, that will be read in a SEO-friendly way
            $params = array();

            // Get vars
            $getVars = array();

            // If there are options to add, process them
            if (sizeof($urlParams)) {
                // Foreach var, process it as a path var or a get var
                foreach ($urlParams as $var_index => $var_value) {
                    if (is_int($var_index)) {
                        // For integer keys, use as path var
                        $params[] = $var_value;
                    } else {
                        // For named keys, use as get var
                        $getVars[$var_index] = $var_value;
                    }
                }
            }

            // Set controller to current if tacit
            if (!isset($options['controller']) && isset($options['action'])) {
                $options['controller'] = Dispatcher::currentControllerName();
            }

            // If action is set but not lang
            if (!isset($options['lang']) && isset($options['action'])) {
                $options['lang'] = LANG;
            }

            // If controller and action are set, generate url
            if (isset($options['controller']) && isset($options['action'])) {
                $url = Router::write($options['controller'], $options['action'], $params, $options['lang']);
            } elseif (isset($options['url'])) {
                $url = Router::url($options['url']);
                unset($getVars['url']);
            } else {
                Debug::crash(true, 'Invalid URL structure: ' . json_encode($options) . ' ' . implode(' > ', involvedFilesDebug(array(
                                                                                                                                  'index.php',
                                                                                                                                  'GestyMVC.php',
                                                                                                                                  'Dispatcher.php',
                                                                                                                                  'Controller.php',
                                                                                                                                  'MySQLModel.php',
                                                                                                                                  'AppMySQLModel.php',
                                                                                                                              ))));
            }

            // Current host with protocol
            $current_protocol = Router::protocol();
            $current_host = $_SERVER['HTTP_HOST'];
            $new_protocol = $current_protocol;
            $new_host = $current_host;

            // Change protocol if required
            if (isset($options['https'])) {
                $new_protocol = Router::protocol($options['https']);
            }

            // Replace host if static was specified
            if ($url && isset($options['no_session']) && $options['no_session']) {
                if (Debug::debugging()) {
                    if (GestyMVC::config('development_no_session_host_name')) {
                        $new_host = GestyMVC::config('development_no_session_host_name');
                    }
                } else {
                    if (GestyMVC::config('production_no_session_host_name')) {
                        $new_host = GestyMVC::config('production_no_session_host_name');
                    }
                }
            }

            // Change host and protocol on URL if do not match
            if ($new_protocol . $new_host != $current_protocol . $current_host) {
                $url = $new_protocol . $new_host . mb_substr($url, mb_strlen($current_protocol . $current_host, 'UTF-8'), mb_strlen($url, 'UTF-8') - mb_strlen($current_protocol . $current_host, 'UTF-8'), 'UTF-8');
            }

            // Add get vars if present
            if (sizeof($getVars)) {
                // Add trailing question mark if not already present
                if (!ends_with($url, '?')) {
                    $url .= '?';
                }

                // Append HTTP query
                $url .= http_build_query($getVars);
            }
        }

        $url .= $hash;

        // Return generated URL
        return $url;
    }

    /**
     * Checks if current request was loaded over https protocol.
     *
     * @retun bool
     */
    public static function isHTTPS() {
        $is_https = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'];
        $is_forwarder_https = isset($_SERVER['HTTP_X_FORWARDER_PROTO']) && $_SERVER['HTTP_X_FORWARDER_PROTO'] === 'https';
        $is_secure = $is_https || $is_forwarder_https;

        return $is_secure;
    }

    /**
     * Returns the protocol.
     *
     * @param $https bool
     *
     * @return string
     */
    public static function protocol($https = null) {
        // If not provided, use current config
        if ($https === null) {
            $https = Router::isHTTPS();
        }

        // Return the corresponding protocol prefix
        return ($https ? 'https://' : 'http://');
    }

    /**
     * Defines the LANG constant based on the URL.
     *
     * @param $url
     *
     * @return bool success
     */
    private static function defineLang($url) {
        // Whether or not the URL has a proper lang definition
        $success = true;

        // Retrieve lang from url
        $lang_prefix = (strlen($url) >= 4 ? substr($url, 0, 4) : '/');
        $lang = null;

        // Check language format
        if ($lang_prefix == '/' || !starts_with($lang_prefix, '/') || !ends_with($lang_prefix, '/')) {
            if (!GestyMVC::config('use_prefix_for_default_lang')) {
                $lang = GestyMVC::config('default_lang');
                $lang_prefix = '/';
            } else {
                $success = false;
            }
        } else {
            // Set url provided url
            $lang = substr($lang_prefix, 1, 2);

            // Extract available langs
            $langs = GestyMVC::config('langs');

            // If language is not a valid language, set to
            // default
            if (!in_array($lang, $langs)) {
                $success = false;
            }

            // Check if prefix was found but not required
            if (!GestyMVC::config('use_prefix_for_default_lang') && $lang == GestyMVC::config('default_lang') && $lang_prefix !== '/') {
                $lang = null;
                $success = false;
            }
        }

        // If lang was not detected, set default for system purposes
        if (!$success) {
            $lang_prefix = '/';
            $lang = GestyMVC::config('default_lang');
        }

        // Define current lang
        define('LANG', $lang);
        CachedAdapter::setKey('');

        // Define root url for the current language
        if (GestyMVC::config('use_prefix_for_default_lang') || $lang != GestyMVC::config('default_lang')) {
            define('LOCALED_ROOT_URL', ROOT_URL . LANG . '/');
        } else {
            define('LOCALED_ROOT_URL', ROOT_URL);
        }

        // Store language on session
        Session::set('lang', LANG);

        // Initialize i18n
        I18n::init(LANG);

        // If false, redirection must be made
        if ($success) {
            // Return URL without lang prefix
            return mb_substr($url, mb_strlen($lang_prefix, 'UTF-8'), mb_strlen($url, 'UTF-8') - mb_strlen($lang_prefix, 'UTF-8'), 'UTF-8');
        } else {
            return false;
        }
    }

    /**
     * Parses an URL into a controller/action/params request.
     *
     * @param $url
     *
     * @return array|bool
     */
    public static function parse($url) {
        // Parse result
        $parsed = array(
            'module' => null,
            'controller' => null,
            'controller_url' => null,
            'action' => null,
            'action_url' => null,
            'params' => array(),
            'force_ending_slash' => true,
        );

        // Define LANG and retrieve URL without lang
        $url_without_lang_prefix = Router::defineLang($url);

        // Check language and redirect if it is not valid
        if ($url_without_lang_prefix === false && $url === '/' && GestyMVC::config('use_prefix_for_default_lang')) {
            // Set alias to the localed page
            $parsed['alias_of'] = LOCALED_ROOT_URL;

            // Stop parsing
            return $parsed;
        } elseif ($url_without_lang_prefix === false) {
            return false;
        }

        // Splitted url
        $urlParts = explode('/', $url_without_lang_prefix);
        $pattern_allows_params = null;

        // Check routes
        foreach (Router::$routes as $pattern => $configuration) {
            // Check lang
            if (isset($configuration['lang']) && $configuration['lang'] !== LANG) {
                continue;
            }

            // Whether or not the pattern is valid
            $pattern_is_valid = true;
            $pattern_allows_params = false;

            // Check if pattern matches exactly
            if ($url === $pattern) {
                $pattern_is_valid = true;
            } else {
                // Generated values
                $controller_url = null;
                $action_url = null;

                // If pattern ends in *, allows params
                if (ends_with($pattern, '/*')) {
                    $pattern_allows_params = true;
                    $pattern = substr($pattern, 0, -2);
                }

                // Split pattern
                $patternSplitted = explode('/', $pattern);

                // If pattern is bigger than url, not valid
                if (sizeof($patternSplitted) > sizeof($urlParts) || (sizeof($patternSplitted) < sizeof($urlParts) && !$pattern_allows_params)) {
                    continue;
                }

                // Foreach part, try to parse
                foreach ($patternSplitted as $index => $pattern_part) {
                    if ($pattern_part === ':controller') {
                        $controller_url = $urlParts[$index];
                    } elseif ($pattern_part === ':action') {
                        $action_url = $urlParts[$index];
                    } elseif ($pattern_part === $urlParts[$index]) {
                    } else {
                        $pattern_is_valid = false;
                        break;
                    }
                }
            }

            // If patter is valid, load configuration
            if ($pattern_is_valid) {
                $parsed['controller_url'] = $controller_url;
                $parsed['action_url'] = $action_url;

                if (isset($configuration['force_ending_slash'])) {
                    $parsed['force_ending_slash'] = $configuration['force_ending_slash'];
                }

                foreach ($configuration as $index => $value) {
                    if (is_int($index)) {
                        $parsed['params'][] = strval($value);
                    } elseif (in_array($index, array('controller', 'action', 'module'))) {
                        $parsed[$index] = $value;
                    }
                }

                for ($i = sizeof($patternSplitted); $i < sizeof($urlParts); $i++) {
                    $parsed['params'][] = $urlParts[$i];
                }

                // If last param is empty, ignore
                if (sizeof($parsed['params']) && $parsed['params'][sizeof($parsed['params']) - 1] == '') {
                    unset($parsed['params'][sizeof($parsed['params']) - 1]);
                }

                // Found, stop searching
                break;
            }
        }

        // Check ending slash
        if ($pattern_allows_params && sizeof($url_without_lang_prefix) && !ends_with($url_without_lang_prefix, '/')) {
            if ($parsed['force_ending_slash']) {
                $parsed['alias_of'] = $_SERVER['REQUEST_URI'];

                if (strpos($parsed['alias_of'], '?') !== false) {
                    $parsed['alias_of'] = substr($parsed['alias_of'], 0, strpos($parsed['alias_of'], '?'));
                }

                $parsed['alias_of'] .= '/';
            }
        }

        // Validate controller part (lowercase with no underscores)
        if (!isset($parsed['controller']) && $parsed['controller_url'] != mb_strtolower($parsed['controller_url'], 'UTF-8') || strpos($parsed['controller_url'], '_') !== false) {
            $parsed['controller_url'] = false;
        }

        // Validate action part (lowercase with no underscores)
        if (!isset($parsed['action']) && $parsed['action_url'] != mb_strtolower($parsed['action_url'], 'UTF-8') || strpos($parsed['action_url'], '_') !== false) {
            $parsed['action_url'] = false;
        }

        // Convert controller and action part into PascalCase and camelCase
        if (!isset($parsed['controller']) && isset($parsed['controller_url']) && $parsed['controller_url']) {
            $parsed['controller'] = Inflector::pascalcase(str_replace('-', '_', $parsed['controller_url']));
        }
        if (!isset($parsed['action']) && isset($parsed['action_url']) && $parsed['action_url']) {
            $parsed['action'] = Inflector::camelcase(str_replace('-', '_', $parsed['action_url']));
        }

        // Cleanup
        unset($parsed['force_ending_slash']);
        unset($parsed['controller_url']);
        unset($parsed['action_url']);

        // Return parsed data
        if ($parsed['controller'] && $parsed['action']) {
            return $parsed;
        } else {
            // If any of it failed, the whole fails
            return false;
        }
    }

    /**
     * Parse's reverse function.
     *
     * @param $controller
     * @param string $action
     * @param array $params
     * @param string $lang
     *
     * @return string url
     */
    private static function write($controller, $action = 'index', $params, $lang = null) {
        // Assume current language if not set
        if ($lang === null) {
            $lang = LANG;
        }

        // Default pattern
        $selected_pattern = '';

        // Matching status
        $lang_matched = false;
        $controller_matched = false;
        $action_matched = false;
        $params_matched = null;

        // Check routes
        foreach (Router::$routes as $pattern => $configuration) {
            // Whether or not to allow params
            $pattern_allows_params = false;

            // If pattern ends in *, allows params
            if (ends_with($pattern, '/*')) {
                $pattern_allows_params = true;
                $pattern = substr($pattern, 0, -1);
            }

            // Fails or not
            $fails = false;

            // Current route matching
            $matches_lang = false;
            $matches_controller = false;
            $matches_action = false;
            $matches_params = 0;

            // Try to match
            foreach ($configuration as $index => $value) {
                if ($index === 'lang') {
                    if ($configuration['lang'] === $lang) {
                        $matches_lang = true;
                    } else {
                        $fails = true;
                        break;
                    }
                } elseif ($index === 'controller') {
                    if ($configuration['controller'] === $controller) {
                        $matches_controller = true;
                    } else {
                        $fails = true;
                        break;
                    }
                } elseif ($index === 'action') {
                    if ($configuration['action'] === $action) {
                        $matches_action = true;
                    } else {
                        $fails = true;
                        break;
                    }
                } elseif (is_numeric($index)) {
                    if (isset($params[$index]) && strval($configuration[$index]) === strval($params[$index])) {
                        $matches_params++;
                    } else {
                        $fails = true;
                        break;
                    }
                }
            }

            // If more params than allowed, abort
            if (!$fails && !$pattern_allows_params && sizeof($params) - $matches_params > (isset($configuration['params']) ? sizeof($configuration['params']) : 0)) {
                $fails = true;
            }

            // Check if any rule faild
            if (!$fails) {
                // Whether or not to override current selected rule
                $override = true;

                // Checks if the current rule is more specific
                // than the selected one
                if ($lang_matched && !$matches_lang) {
                    $override = false;
                } elseif ($controller_matched && !$matches_controller) {
                    $override = false;
                } elseif ($action_matched && !$matches_action) {
                    $override = false;
                } elseif ($params_matched !== null && $params_matched >= $matches_params) {
                    $override = false;
                }

                // Check params
                if (!$pattern_allows_params && $matches_params < sizeof($params)) {
                    // Too many params for this pattern
                    $override = false;
                }

                // Overrides rule if required
                if ($override) {
                    $selected_pattern = $pattern;
                    $lang_matched = $matches_lang;
                    $controller_matched = $matches_controller;
                    $action_matched = $matches_action;
                    $params_matched = $matches_params;
                }
            }
        }

        // Generate base URL
        $url = ROOT_URL . (GestyMVC::config('use_prefix_for_default_lang') || $lang != GestyMVC::config('default_lang') ? $lang . '/' : '') . str_replace(array(
                                                                                                                                                              ':controller',
                                                                                                                                                              ':action',
                                                                                                                                                          ), array(
                                                                                                                                                              str_replace('_', '-', Inflector::underscore($controller)),
                                                                                                                                                              str_replace('_', '-', Inflector::underscore($action)),
                                                                                                                                                          ), $selected_pattern);

        // Remove all matched params
        for ($i = 0; $i < $params_matched; $i++) {
            unset($params[0]);
        }

        // Add path vars if any remains
        if (sizeof($params)) {
            // Add trailing slash if not already present
            if (!ends_with($url, '/')) {
                $url .= '/';
            }

            // Add each var
            foreach ($params as $path_var_index => $value) {
                // Do not append last empty value
                if ($path_var_index < sizeof($params) - 1 || $value) {
                    $url .= $value . '/';
                }
            }
        }

        // Return generated URL
        return $url;
    }

    /**
     * Adds a route to the configuration.
     *
     * @param $pattern string base structure of the URL.
     * @param $configuration array the params to generate/match
     */
    public static function addRoute($pattern, $configuration) {
        Router::$routes = array($pattern => $configuration) + Router::$routes;
    }
}