<?php

/**
 * Class Dispatcher
 *
 * Launchs the execution of the correct controller.
 */
class Dispatcher {
    /**
     * Requested URI without query string.
     *
     * @var string
     */
    private static $request_uri;

    /**
     * Query string part of the URL.
     *
     * @var string
     */
    private static $query_string = null;

    /**
     * Current controller name, action and params
     *
     * @var string
     */
    private static $currentCall = null;

    /**
     * Dispatchs a controller based on the current URL
     */
    public static function dispatch() {
        // Calculate and define the website root url
        Dispatcher::defineRoot();

        // Calculate current URL
        $url = Dispatcher::getCurrentUrl();

        // Stop execution for static files
        if (Dispatcher::isStaticFile($url)) {
            // Module static file
            $modules = GestyMVC::config('modules');
            if ($modules) {
                foreach ($modules as $module) {
                    if (file_exists(INCLUDE_PATH . 'modules/' . $module . $url)) {
                        header('Pragma: public');
                        header('Cache-Control: max-age=86400');
                        header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
                        header('Content-Type: ' . mime_content_type(INCLUDE_PATH . 'modules/' . $module . $url));
                        header('Content-Length: ' . filesize(INCLUDE_PATH . 'modules/' . $module . $url));
                        header('Content-Disposition: filename=' . basename($url));

                        readfile(INCLUDE_PATH . 'modules/' . $module . $url);
                        exit();
                    }
                }
            }

            @ob_end_clean();
            header('HTTP/1.0 404 Not Found', true, 404);
            exit();
        }

        // Parse controller, action and parameters
        if (($call = Router::parse($url)) === false) {
            /* @var AppController $AppController */
            $AppController = new AppController(false);
            $AppController->notFound();
            exit();
        }

        // Check if redirection is needed
        if (isset($call['alias_of']) && $call['alias_of']) {
            header('HTTP/1.1 301 Moved Permanently', true, 301);
            header('Location: ' . $call['alias_of'] . (!empty($_GET) ? '?' . http_build_query($_GET) : ''));
            exit();
        }

        // Generate controller class
        $controller_class_name = $call['controller'] . 'Controller';

        // Include controller class
        $controller_filename = null;

        if (file_exists(INCLUDE_PATH . 'controllers/' . $controller_class_name . '.php')) {
            $controller_filename = INCLUDE_PATH . 'controllers/' . $controller_class_name . '.php';
        }

        // Module controller
        $modules = GestyMVC::config('modules');
        if ($modules) {
            foreach ($modules as $module) {
                if (file_exists(INCLUDE_PATH . 'modules/' . $module . '/controllers/' . $controller_class_name . '.php')) {
                    $call['module'] = $module;
                    $controller_filename = INCLUDE_PATH . 'modules/' . $module . '/controllers/' . $controller_class_name . '.php';
                    break;
                }
            }
        }

        if ($controller_filename && file_exists($controller_filename)) {
            include_once($controller_filename);

            // Store current controller name
            Dispatcher::$currentCall = $call;

            // Initialize controller
            $controller_class_name = basename($controller_class_name);
            /* @var Controller $controller */
            $controller = new $controller_class_name(basename($call['controller']));

            // Run action
            $controller->run($call['module'], $call['action'], $call['params']);
        } else {
            /* @var AppController $AppController */
            $AppController = new AppController(false);
            $AppController->notFound();
            exit();
        }
    }

    /**
     * Defines the ROOT_URL constant comparing the server files. This constant points to the website instalation root
     * HTTP/HTTPS path.
     */
    private static function defineRoot() {
        // Retrieve request URI and query string
        Dispatcher::$request_uri = $_SERVER['REQUEST_URI'];
        Dispatcher::$query_string = $_SERVER['QUERY_STRING'];

        // Remove trailing question mark
        if (ends_with(Dispatcher::$request_uri, '?')) {
            Dispatcher::$request_uri = mb_substr(Dispatcher::$request_uri, 0, -1, 'UTF-8');
        }

        // Remove the first parameter if there are more than one
        if (strpos(Dispatcher::$query_string, '&') !== false) {
            Dispatcher::$query_string = explode('&', Dispatcher::$query_string);
            unset(Dispatcher::$query_string[0]);
            Dispatcher::$query_string = implode('&', Dispatcher::$query_string);
        } else {
            Dispatcher::$query_string = '';
        }

        // If no URL was provided, set as empty string
        if (!isset($_GET['url']) || !$_GET['url']) {
            $_GET['url'] = '';
        }

        // Remove ending '/' of apache DocumentRoot if needed
        if (substr($_SERVER['DOCUMENT_ROOT'], -1, 1) == DIRECTORY_SEPARATOR) {
            $_SERVER['DOCUMENT_ROOT'] = substr($_SERVER['DOCUMENT_ROOT'], 0, -1);
        }

        // Calculate root url based on the ROOT_PATH and the DOCUMENT_ROOT
        $server_subfolder = substr(ROOT_PATH, strpos(ROOT_PATH, $_SERVER['DOCUMENT_ROOT']) + strlen($_SERVER['DOCUMENT_ROOT']));
        $ROOT_URL = Router::protocol() . $_SERVER['HTTP_HOST'] . $server_subfolder;

        // Define constant
        define('ROOT_URL', $ROOT_URL);
        define('ROOT_RELATIVE_URL', $server_subfolder);

        $no_session_host_name = null;

        if (Debug::debugging() && GestyMVC::config('development_no_session_host_name')) {
            $no_session_host_name = GestyMVC::config('development_no_session_host_name');
        } else if (!Debug::debugging() && GestyMVC::config('production_no_session_host_name')) {
            $no_session_host_name = GestyMVC::config('production_no_session_host_name');
        }

        if ($no_session_host_name) {
            define('STATIC_ROOT_URL', str_replace($_SERVER['HTTP_HOST'], $no_session_host_name, ROOT_URL));
        } else {
            define('STATIC_ROOT_URL', ROOT_URL);
        }
    }

    /**
     * Retrieves and stores the current executing URL as seen by the user.
     *
     * @return string
     */
    private static function getCurrentUrl() {
        // Retrieve requested URL from $_GET
        $url = (isset($_GET['url']) ? $_GET['url'] : '');
        unset($_GET['url']);
        $url = '/' . $url;

        // Set full requested url
        Router::setCurrentUrl(substr($url, 1) . (Dispatcher::$query_string ? '?' . Dispatcher::$query_string : ''));

        return $url;
    }

    /**
     * Checks whether or not the requested URL points to a static folder.
     *
     * @param $url
     *
     * @return bool
     */
    private static function isStaticFile($url) {
        // Define static root folders. Execution should not continue if the url begins with a static folder path
        $staticRootFolders = GestyMVC::config('staticRootFolders');

        // For each static file folder, retrieve error if file is not found
        foreach ($staticRootFolders as $static_root_folder) {
            if (strpos($url, '/' . $static_root_folder . '/') === 0 || $url == '/' . $static_root_folder) {
                return true;
            }
        }

        // Not an static file
        return false;
    }

    /**
     * Gets the current controller name.
     */
    public static function currentControllerName() {
        return Dispatcher::$currentCall['controller'];
    }

    /**
     * Gets the current action name.
     */
    public static function currentActionName() {
        return Dispatcher::$currentCall['action'];
    }

    /**
     * Gets the current controller params.
     */
    public static function currentControllerParams() {
        return Dispatcher::$currentCall['params'];
    }

    public static function originalGet() {
        $get = array();
        parse_str(self::$query_string, $get);

        return $get;
    }
}