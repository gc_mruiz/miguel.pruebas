<?php

/**
 * MySQL datatable model. Generates queries from functions, retrieves, updates and deletes information from a table
 */
class MySQLModel {
    /**
     * Connection open or not
     *
     * @var bool
     */
    protected static $conn_open = false;
    private static $transaction_depth = 0;

    /**
     * MySQL connection instance. It is autoopened when required.
     *
     * It is autoclosed by the layout manager class
     * when controller execution ended.
     *
     * @var mysqli
     */
    protected static $conn = null;

    /**
     * Model name. By convention the singularized table name in pascal case
     *
     * @var string
     */
    public $name = null;

    /**
     * Model structure
     *
     * @var array
     */
    protected $structure = null;

    /**
     * MySQL table prefix
     *
     * @var string
     */
    protected $table_prefix = null;

    /**
     * MySQL table name. By convection the pluralized model name in underscore case
     *
     * @var string
     */
    protected $table_name = null;

    /**
     * Table name as it appears on the database (with the prefix)
     *
     * @var string
     */
    protected $full_table_name = null;

    /**
     * Field select expression. On SELECT queries, the part between SELECT and FROM. By default, '*'
     *
     * @var string
     */
    protected $fields_select_expression = null;

    /**
     * The FROM expression for SELECT queries. By default, the table name
     *
     * @var string
     */
    public $from_expression = null;

    /**
     * The ORDER BY expression for SELECT queries. By default, the table name
     *
     * @var string
     */
    public $order_by_expression = null;

    /**
     * Recursivity on get calls.
     *
     * -1: current model only
     * 0: current model and belongsTo models
     * 1: current model, belongsTo models and hasMany/hasAndBelongsToMany models.
     * ..: this property is inherited by related models reduced by 2. Therefor, with a value of 3 we would be loading
     * current, belongsTo with recursive 1 and hasMany/hasAndBelongsToMany with recursive 1
     *
     * @var int
     */
    public $recursive = 0;

    /**
     * Default order for SELECT queries.
     *
     * @var string
     */
    public $order = null;

    /**
     * Belongs To (child->father) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'foreign_key' => 'foreign_key_id',
     * 'options' => array()) By convention ModelName and RelationshipName are equal unless they cannot be. By
     * convention foreign_key will be the id of the related model on the current table. Options are translated to the
     * other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $belongsTo = array();

    /**
     * Has Many (father->children) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'foreign_key' => 'foreign_key_id',
     * 'options' => array())
     * By convention ModelName and RelationshipName are equal unless they cannot be.
     * By convention foreign_key will be the id of the current model on the related table Options are translated to the
     * other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $hasMany = array();

    /**
     * Has And Belongs To Many (many-many) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'association_model' =>
     * 'AssociationModelName', 'foreign_key' => 'foreign_key_id', 'association_foreign_key' =>
     * 'association_foreign_key_id', 'options' => array())
     * By convention ModelName and RelationshipName are equal unless they cannot be.
     * By convention AssociationModelName will be a model representing only the ids of both related models
     * By convention foreign_key will be the id of the current model on the association table
     * By convention association_foreign_key will be the id of the related model on the association table
     * Options are translated to the other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $habtm = array();

    /**
     * List of fields that are allowed to be updated from the updateFieldValue call.
     *
     * @var array
     */
    protected $singleUpdateFields = array();

    /**
     * Check locale field when selecting.
     * Localization means there is a 'locale' varchar field on the table that is null or matches one of the sites
     * languages. If it is set to true, the get methods will always add a condition (`locale` IS NULL OR `locale` =
     * LANG) to every SELECT query generated if option['ignore_locale'] is not set.
     *
     * @var bool
     */
    protected $check_locale = false;

    /**
     * Check publish field when selecting.
     * Publishing means there is a 'publish' tinyint field on the table that is equal to 1 or 0. If it is set to true,
     * the get methods will always add a condition (`publish` = 1) to every SELECT query generated IF NO USER IS
     * LOGGED and option['ignore_publish'] is not set. We are aware this breaks the model/view/controller convention.
     *
     * @var bool
     */
    protected $check_publish = false;

    /**
     * Check hidden field when selecting.
     * Hiding means there is a 'hidden' datetime field on the table that is null or the date in which the row has
     * hidden. If it is set to true, the get methods will always add a condition (`hidden` IS NULL ) to every SELECT
     * query generated if option['ignore_hidden'] is not set.
     * When using this option rows can be hidden from the system instead of deleted.
     *
     * @var bool
     */
    protected $check_hidden = false;

    /**
     * All mysql tables require created and modified fields. If this is set to true, the table also requires
     * created_by_user_id and modified_by_user_id and both area populated automatically.
     *
     * @var bool
     */
    protected $user_timestamp_fields = false;

    /**
     * Whether or not to launch a sitemap generation on updates.
     *
     * @var bool
     */
    protected $update_sitemap = false;

    /**
     * Errors generated while saving, deleting, etc..
     * When methods return true or false, if false the array may be filled with the error messages generated. Errors
     * are user-readable.
     *
     * @var array
     */
    public $errors = array();

    /**
     * Errors generated while saving, deleting, etc.. (by field)
     * When methods return true or false, if false the array may be filled with the error messages generated. Errors
     * are user-readable.
     *
     * @var array
     */
    public $errorsByField = array();

    /**
     * Errors generated while saving, deleting, etc.. (by field)
     * When methods return true or false, if false the array may be filled with the error messages generated. Errors
     * are codes.
     *
     * @var array
     */
    public $errorCodesByField = array();

    /**
     * Stores callback error
     *
     * @var string
     */
    public $callbackException = null;

    /**
     * Default display fields for the model. When more than one, they are concatenated with an space.
     *
     * @var array
     */
    public $displayFields = array('id');

    /**
     * Validation array for fields.
     * Structure: array('field_name' => array('validation_method' => 'Validation error message.')
     * Validation methods available:
     *  - notempty: if present, value cannot be empty
     *  - required: value must be present on creation. An empty value validates.
     *  - email: value must have an email format
     *  - numeric: value must be numeric
     *  - int: value must be the representation of an integer
     *
     * @var array
     */
    public $validation = array();

    /**
     * Valid order by expressions. List of expression that can be used to order. Expression can be a field or a complex
     * expression. Do not include direction of order.
     *
     * @var array
     */
    public $validOrderByExpressions = array();

    /**
     * Whether or not log the updates and inserts into the Log model.
     *
     * @var bool
     */
    protected $logged = false;

    /**
     * Hash of behaviors supported by the model with their options.
     * Behaviors add funcionalities overriding the callbacks. The callbacks of each behavior will be executed in the
     * same order as listed here.
     * Structure: array('Behavior1' => array(), 'Behavior2' => array('option1' => 'value1')); The key must be the
     * classname of the behavior.
     *
     * @var array
     */
    protected $behaviors = array();

    /**
     * List of instances of the behavior classes named on the $behavior array.
     *
     * @var array
     */
    protected $Behaviors = array();

    /**
     * Whether or not to cache
     *
     * @var bool
     */
    public $cache = false;

    /**
     * Base get options
     *
     * @var array
     */
    public $defaultGetOptions = array();

    /**
     * Opens the MySQL connection to the database based on the $config (dispatcher.php)
     *
     * @return void
     */
    public static function openConnection() {
        // Proceed only if connection is closed
        if (!self::$conn_open) {
            // Set connection as open
            self::$conn_open = true;

            // Retrieve database configuration
            $dbConfig = GestyMVC::dbConfig();

            // Initialize mysql connection and select schema
            self::$conn = mysqli_connect($dbConfig['host'], $dbConfig['user'], $dbConfig['password']);
            mysqli_select_db(self::$conn, $dbConfig['schema']);

            // Set encoding and time zone
            mysqli_set_charset(self::$conn, 'utf8mb4');
            self::query('SET time_zone = \'' . self::realEscapeString(GestyMVC::config('time_zone')) . '\'');
        }
    }

    public function addValidationQueryErrors() {
        $error = self::queryError();

        if ($error && strpos($error, 'a foreign key constraint fails') !== false && strpos($error, 'FOREIGN KEY') !== false) {
            $field = substr($error, strpos($error, 'FOREIGN KEY (`') + strlen('FOREIGN KEY (`'));
            $field = substr($field, 0, strpos($field, '`'));

            return $this->addValidationQueryErrorsForField($field, array('id'));
        } elseif ($error && strpos($error, 'for column \'')) {
            $field = substr($error, strpos($error, 'for column \'') + strlen('for column \''));
            $field = substr($field, 0, strpos($field, '\''));

            return $this->addValidationQueryErrorsForField($field);
        } elseif ($error && strpos($error, 'Field \'') !== false) {
            $field = substr($error, strpos($error, 'Field \'') + strlen('Field \''));
            $field = substr($field, 0, strpos($field, '\''));

            return $this->addValidationQueryErrorsForField($field);
        }

        return false;
    }

    public function addValidationQueryErrorsForField($field, $matchRule = array()) {
        foreach ($matchRule as $rule) {
            if (isset($this->validation[$field][$rule])) {
                $this->errors[] = $this->errorsByField[$field] = __($this->validation[$field][$rule], true);
                $this->errorCodesByField[$field] = $rule;

                return true;
            }
        }

        $this->errors[] = $this->errorsByField[$field] = sprintf(__('Invalid value for field %s.', true), __(str_replace('_', ' ', (ends_with($field, '_id') ? substr($field, 0, -3) : $field)), true));
        $this->errorCodesByField[$field] = 'invalid';

        return false;
    }

    public static function queryError() {
        return mysqli_error(self::$conn);
    }

    /**
     * Execute query and logs if on debug mode.
     *
     * @param $query string
     *
     * @return mysqli_result mysql result
     */
    public static function query($query) {
        // Open connection if not already open
        self::openConnection();

        // Get current microtime (float)
        $init_time = microtime(true);

        // Execute query
        $result = mysqli_query(self::$conn, $query);

        $end_time = microtime(true);

        // Substract old microtime to current
        $milliseconds = ceil(($end_time - $init_time) * 1000);

        // Retrieve config
        $mysql_log_queries_slower_than_seconds = GestyMVC::config('mysql_log_queries_slower_than_seconds');
        $mysql_log_all_queries_on_debug = GestyMVC::config('mysql_log_all_queries_on_debug');

        // If debugging or slow query, log
        if ($mysql_log_all_queries_on_debug && Debug::debugging()) {
            Debug::log($query . '; #took ' . $milliseconds . 'ms ' . implode(' > ', involvedFilesDebug(array(
                                                                                                          'index.php',
                                                                                                          'GestyMVC.php',
                                                                                                          'Dispatcher.php',
                                                                                                          'Controller.php',
                                                                                                          'MySQLModel.php',
                                                                                                      ))), 'mysql-debug', 'sql', false);
        }

        // If debugging or slow query, log
        if ($mysql_log_queries_slower_than_seconds && $milliseconds > $mysql_log_queries_slower_than_seconds * 1000) {
            Debug::log($query . '; #took ' . $milliseconds . 'ms ' . implode(' > ', involvedFilesDebug(array(
                                                                                                          'index.php',
                                                                                                          'GestyMVC.php',
                                                                                                          'Dispatcher.php',
                                                                                                          'Controller.php',
                                                                                                          'MySQLModel.php',
                                                                                                      ))), 'mysql', 'sql', false);
        }

        // Return mysql result
        return $result;
    }

    /**
     * Public access to escape the string with the current connection.
     *
     * @param $value string original string
     *
     * @return string escaped string
     */
    public static function realEscapeString($value) {
        // Open connection if not already open
        self::openConnection();

        // Return escaped value
        return mysqli_real_escape_string(self::$conn, $value);
    }

    /**
     * Closes the MySQL connection if open
     *
     * @return void
     */
    public static function closeConnection() {
        // Proceed only if connection is open
        if (self::$conn_open) {
            // Set connection as closed
            self::$conn_open = false;

            // Close connection
            mysqli_close(self::$conn);
        }
    }

    /**
     * Model constructor. Calls the initialize method.
     *
     * @return MySQLModel
     */
    public function MySQLModel() {
        $this->initialize();
    }

    /**
     * Initializes the model.
     * Sets the model variables if not already set.
     *
     * @return void
     */
    public function initialize() {
        // Clear structure
        $this->structure = array();

        if ($this->cache) {
            $this->behaviors[] = 'CacheClearer';
        }

        // Load structure if exists
        if (file_exists(INCLUDE_PATH . 'models/structure/' . $this->name . '.php')) {
            $this->structure = (include INCLUDE_PATH . 'models/structure/' . $this->name . '.php');
        }

        // Calculates the table name based on the model name
        if (!$this->table_name && $this->name) {
            $this->table_name = Inflector::pluralize(Inflector::underscore($this->name));
        }

        // Sets the table prefix if not set
        if ($this->table_prefix === null) {
            // Retrieve database configuration
            $dbConfig = GestyMVC::dbConfig();

            // Configure the prefix if required or set to empty string
            $this->table_prefix = (isset($dbConfig['table_prefix']) ? $dbConfig['table_prefix'] : '');
        }

        // Calculates the full table name (prefix + table_name) is not set
        if (!$this->full_table_name && $this->table_name) {
            $this->full_table_name = '`' . $this->table_prefix . $this->table_name . '`';
        }

        // Sets the SELECT FROM expression if not set
        if (!$this->from_expression && $this->full_table_name) {
            $this->from_expression = $this->full_table_name;
        }

        // Sets the SELECT fields expression if not set
        if (!$this->fields_select_expression && $this->full_table_name) {
            $this->fields_select_expression = $this->full_table_name . '.*';
        }

        // Sets the order field if not set
        if (!$this->order && $this->full_table_name) {
            $this->order = $this->full_table_name . '.`id` ASC';
        }

        // Initialize each required behavior
        foreach ($this->behaviors as $behavior_name => $behaviorOptions) {
            // Allow list instead of hash also
            if (is_int($behavior_name)) {
                $behavior_name = $behaviorOptions;
                $behaviorOptions = array();
            }

            // Load and initialize behavior with options
            $this->Behaviors[$behavior_name] = Behavior::getInstance($behavior_name, $this, $behaviorOptions);
        }
    }

    /**
     * Overridable method to return the default record if no record is found.
     * By default returns null.
     *
     * @return null
     */
    protected function getDummyRecord() {
        return null;
    }

    /**
     * Converts array-based conditions into SQL.
     *
     * @param $conditions mixed
     * @param $join_with string
     *
     * @return string
     */
    public function generateSqlConditions($conditions, $join_with = 'AND') {
        // If it is not an array, return value
        if (!is_array($conditions)) {
            return $conditions;
        }

        // Valid operators (long ones first)
        $validOperators = array('NOT LIKE', 'LIKE', '<>', '!=', '>=', '<=', '>', '<', '=',);

        // No-field chars (to detect if it is an expression)
        $expressionChars = array(' ', '(', ')', '.', '-', '+', '<', '>', '`', '*', '/');

        // Generated SQL conditions array
        $sqlConditions = array();

        // Add each condition
        foreach ($conditions as $index => $condition) {
            if ($index === 'OR') {
                $sqlConditions[] = $this->generateSqlConditions($condition, 'OR');
            } elseif ($index === 'AND' || is_int($index)) {
                $sqlConditions[] = $this->generateSqlConditions($condition, 'AND');
            } else {
                // Field conditions
                $field_name = $index;

                if (ends_with($field_name, ' ')) {
                    Debug::crash(true, 'Field name in array conditions cannot end on space. Found: ' . json_encode($field_name));
                }

                // Operator to use between field name and value
                $operator = '=';

                // Try to parse a different operator
                foreach ($validOperators as $valid_operator) {
                    if (ends_with($field_name, ' ' . $valid_operator)) {
                        $operator = $valid_operator;
                        $field_name = substr($field_name, 0, -(strlen($operator) + 1));
                        break;
                    }
                }

                // Whether or not it is a simple field
                $is_simple_field = true;

                // Check if it is an expression
                foreach ($expressionChars as $expression_char) {
                    if (strpos($field_name, $expression_char) !== false) {
                        $is_simple_field = false;
                        break;
                    }
                }

                // Field is numeric
                $field_is_numeric = false;

                if ($is_simple_field) {
                    // If field is of type int, double, float, long or decimal or id
                    // it is a numeric value and must not be put into quotes
                    if (!empty($this->validation[$field_name]['id']) || !empty($this->validation[$field_name]['int']) || !empty($this->validation[$field_name]['long']) || !empty($this->validation[$field_name]['float']) || !empty($this->validation[$field_name]['double']) || !empty($this->validation[$field_name]['decimal'])) {
                        $field_is_numeric = true;
                    }

                    // Repeat check based on table structure
                    if (isset($this->structure[$field_name]) && in_array($this->structure[$field_name]['DATA_TYPE'], array(
                            'integer',
                            'int',
                            'smallint',
                            'tinyint',
                            'mediumint',
                            'bigint',
                            'decimal',
                            'numeric',
                            'float',
                            'double',
                            'bit',
                        ))
                    ) {
                        $field_is_numeric = true;
                    }

                    // Encapsulate simple field
                    $field_name = $this->full_table_name . '.`' . $field_name . '`';
                }

                // Check IN and NOT IN operations
                if (is_array($condition)) {
                    // (,,,,) expression of escaped values
                    $in_expression = '';

                    if (!empty($condition)) {
                        // Escape values
                        foreach ($condition as $value) {
                            if (!$field_is_numeric) {
                                $in_expression .= (strlen($in_expression) ? ',' : '') . '\'' . self::realEscapeString($value) . '\'';
                            } else {
                                if (strlen($value) && $this->validateValue(array(), '', $value, 'decimal')) {
                                    $in_expression .= (strlen($in_expression) ? ',' : '') . self::realEscapeString($value);
                                } else {
                                    // Skip value since it is not valid
                                }
                            }
                        }
                    }

                    if (!empty($in_expression)) {
                        // Override field condition value
                        $condition = '(' . $in_expression . ')';

                        // Override operator
                        if ($operator === '=') {
                            $operator = 'IN';
                        } else {
                            $operator = 'NOT IN';
                        }
                    } else {
                        if ($operator === '=') {
                            // IN with empty list is the same than false
                            $sqlConditions[] = '0';
                        } else {
                            // NOT IN with empty list is the same than true
                            $sqlConditions[] = '1';
                        }

                        // Do nothing more
                        continue;
                    }
                } elseif ($condition === null) {
                    // Check IS NULL
                    $condition = 'NULL';

                    // Switch to IS, OR IS NOT
                    if ($operator === '=') {
                        $operator = 'IS';
                    } else {
                        $operator = 'IS NOT';
                    }
                } else {
                    // Other cases
                    if (!$field_is_numeric) {
                        $condition = '\'' . self::realEscapeString($condition) . '\'';
                    } else {
                        if (($operator == 'LIKE' || $operator == 'NOT LIKE') && strlen($condition) && $this->validateValue(array(), '', str_replace('%', '', $condition), 'decimal')) {
                            $condition = '\'' . self::realEscapeString($condition) . '\'';
                        } else if (strlen($condition) && $this->validateValue(array(), '', $condition, 'decimal')) {
                            $condition = self::realEscapeString($condition);
                        } else {
                            if (in_array($operator, array('NOT LIKE', '<>', '!=', 'NOT IN'))) {
                                // Value is invalid, thereof not record can have it (all records are valid)
                                $sqlConditions[] = '1';
                            } else {
                                // Value is invalid, thereof not record can have it nor can we compare (no record is valid)
                                $sqlConditions[] = '0';
                            }

                            // Do nothing more
                            continue;
                        }
                    }
                }

                // Add condition
                $sqlConditions[] = $field_name . ' ' . $operator . ' ' . $condition;
            }
        }

        foreach ($sqlConditions as $index => $sqlCondition) {
            if (is_null($sqlCondition) || (is_array($sqlCondition) && empty($sqlCondition))) {
                $sqlConditions[$index] = 1;

                Debug::log('Invalid conditions. ' . json_encode($conditions) . ' generated empty condition and was ignored. ' . implode(' > ', involvedFilesDebug(array(
                                                                                                                                                                     'index.php',
                                                                                                                                                                     'GestyMVC.php',
                                                                                                                                                                     'Dispatcher.php',
                                                                                                                                                                     'Controller.php',
                                                                                                                                                                     'MySQLModel.php',
                                                                                                                                                                     'AppMySQLModel.php',
                                                                                                                                                                 ))), 'model');
            }
        }

        // Return joined conditions
        if (sizeof($sqlConditions) == 0) {
            return 1;
        } elseif (sizeof($sqlConditions) == 1) {
            return $sqlConditions[0];
        } else {
            return '(' . implode(') ' . $join_with . ' (', $sqlConditions) . ')';
        }
    }

    /**
     * Returns the count of records matching options.
     *
     * @param array $options
     *  - where: SQL conditions
     *  - field: field to count
     *  - group_by
     *
     * @return null
     */
    public function count($options = array()) {
        if (!$options) {
            $options = array();
        }

        if (!isset($options['field'])) {
            $options['field'] = $this->full_table_name . '.`id`';
        }

        $options['fields'] = 'COUNT(' . $options['field'] . ') as `count`';
        $options['ignore_translation'] = true;

        return $this->field('count', $options);
    }

    /**
     * Returns the sum of records matching options.
     *
     * @param $field
     * @param array $options
     *  - where: SQL conditions
     *  - field: field to count
     *  - group_by
     *
     * @return null
     */
    public function sum($field = null, $options = array()) {
        if (!$options) {
            $options = array();
        }

        if (!$field) {
            Debug::crash(true, 'Call to MySQLModel::sum without indicating the field.');
        }

        // No-field chars (to detect if it is an expression)
        $expressionChars = array(' ', '(', ')', '.', '-', '+', '<', '>', '`', '*', '/');

        // Whether or not it is a simple field
        $is_simple_field = true;

        // Check if it is an expression
        foreach ($expressionChars as $expression_char) {
            if (strpos($field, $expression_char) !== false) {
                $is_simple_field = false;
                break;
            }
        }

        // Encapsulate if required
        if ($is_simple_field) {
            $field = '`' . $field . '`';
        }

        $options['fields'] = 'COALESCE(SUM(' . $field . '), 0) as `sum`';

        // Retrieve the field value
        return $this->field('sum', $options);
    }

    /**
     * Retrieves the list of elements matching the options.
     *
     * @param array $options
     *  - where: SQL conditions
     *  - order: order of the results (SQL expression with direction)
     *  - limit: max results (default 1000)
     *  - offset: results offset, for pagination
     *  - ignore_locale: if set to true the query will not include by default the locale conditions
     *  - ignore_hidden: if set to true the query will not include by default the hidden conditions
     *  - ignore_publish: if set to true the query will not include by default the publish conditions
     *  - recursive: recursivity of get, check self::$recursive
     *  - hash_by: if set, hashes the resultant array by the specified field
     *  - list_by: if set, generates a hash of sublists hashed by the specified field
     *  - fields: fields to retrieve from the model, default '*'
     *  - group_by: group by expression, default false
     *  - RelationshipName: options specific for subqueries of the relationship
     *
     * @return array
     */
    public function getAll($options = array()) {
        // If current app does not have any mysql server, return empty array
        if (GestyMVC::config('db') === false) {
            return array();
        }

        // Override default options by provided ones
        $options = array_merge(array(
                                   'where' => 1,
                                   'order' => $this->order,
                                   'limit' => 1000000,
                                   'offset' => 0,
                                   'ignore_locale' => false,
                                   'ignore_hidden' => false,
                                   'ignore_publish' => false,
                                   'recursive' => $this->recursive,
                                   'hash_by' => null,
                                   'list_by' => null,
                                   'fields' => $this->fields_select_expression,
                                   'group_by' => false,
                               ), $this->defaultGetOptions, $options);

        // Set conditions as array
        if (!is_array($options['where'])) {
            $options['where'] = array($options['where']);
        }

        // Execute beforeGet callback
        $this->beforeGet($options);

        // Return cached element
        if ($this->cache && !MethodCache::caching()) {
            $cacheOptions = $this->cache;

            if ($cacheOptions === true) {
                $cacheOptions = array(
                    'user_id' => null,
                    'clearers' => array($this->name => '*'),
                );
            }

            return MethodCache::execute($this, $this->name, 'getAll', array($options), $cacheOptions);
        }

        // Set locale
        if ($this->check_locale && !$options['ignore_locale']) {
            $options['where'][] = array('OR' => array(array('locale' => LANG), array('locale' => null)));
        }

        // Load only published one
        if ($this->check_publish && !Authentication::get('user', 'id') && !$options['ignore_publish']) {
            $options['where'][] = array('publish' => 1);
        }

        // Load only not hidden ones
        if ($this->check_hidden && !$options['ignore_hidden']) {
            $options['where'][] = array('hidden' => null);
        }

        // Translate conditions
        $options['where'] = $this->generateSqlConditions($options['where']);

        // If fields option is empty, use default one
        if (!$options['fields']) {
            $options['fields'] = $this->fields_select_expression;
        }

        // If fields option is an array, convert to string
        if (is_array($options['fields'])) {
            $fields_str = '';

            foreach ($options['fields'] as $field) {
                if (strpos($field, '`') === false && strpos($field, '.') === false) {
                    $field = $this->full_table_name . '.`' . $field . '`';
                }

                if ($fields_str) {
                    $fields_str .= ',';
                }

                $fields_str .= $field;
            }

            $options['fields'] = $fields_str;
        }

        // Result array
        $all = array();

        // Check empty where
        if ($options['where'] == '0') {
            return $all;
        }

        if(is_array($options['order'])) {
            $this->order_by_expression = join(', ', $options['order']);
        } else {
            $this->order_by_expression = $options['order'];
        }

        // Offset
        if ($options['offset']) {
            $options['limit'] = $options['offset'] . ', ' . $options['limit'];
            unset($options['offset']);
        }

        // Load all matching where query
        $query = 'SELECT ' . $options['fields'] . ' FROM ' . $this->from_expression . ' WHERE ' . $options['where'] . ($options['group_by'] ? ' GROUP BY ' . $options['group_by'] : '') . ' ORDER BY ' . $this->order_by_expression . ' LIMIT ' . $options['limit'];
        $result = self::query($query);

        // If query fails and in debug mode, output the failing query and stop execution
        if ($result === false) {
            Debug::crash(false, $query . ' #' . self::queryError() . ' ' . implode(' > ', involvedFilesDebug(array(
                                                                                                                'index.php',
                                                                                                                'GestyMVC.php',
                                                                                                                'Dispatcher.php',
                                                                                                                'Controller.php',
                                                                                                                'MySQLModel.php',
                                                                                                                'AppMySQLModel.php',
                                                                                                            ))), 'mysql', 'sql');
        }

        // Array to store retrieved element ids
        $elementIds = array();

        // Array to store related elements ids
        $belongsToIds = array();

        // Foreach result
        while (($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) != false) {
            // Add class prefix
            $element = array($this->name => $line);

            // Execute afterGet callback
            $this->afterGet($element, $options);

            // Store belongs to id
            foreach ($this->belongsTo as $belongsToRelationShipName => $belongsToRelationShip) {
                if (!isset($belongsToIds[$belongsToRelationShipName])) {
                    $belongsToIds[$belongsToRelationShipName] = array();
                }

                if (isset($element[$this->name][$belongsToRelationShip['foreign_key']])) {
                    $belongsToIds[$belongsToRelationShipName][] = $element[$this->name][$belongsToRelationShip['foreign_key']];
                }
            }

            // Store id
            if (isset($line['id'])) {
                $elementIds[] = $line['id'];
            }

            // Store result element in array
            if ($options['hash_by']) {
                $all[$element[$this->name][$options['hash_by']]] = $element;
            } else {
                $all[] = $element;
            }
        }

        // Recursivity
        if ($options['recursive'] > -1 && !empty($elementIds)) {
            // Load all belongs to related items
            foreach ($this->belongsTo as $belongsToRelationShipName => $belongsToRelationShip) {
                if (isset($options[$belongsToRelationShipName]) && $options[$belongsToRelationShipName] === false) {
                    // User requested this relationship not been loaded
                    continue;
                }

                // Initialize relationship model
                $MySQLModel = self::getRecycledInstance($belongsToRelationShip['model'], (isset($belongsToRelationShip['relationships'])) ? $belongsToRelationShip['relationships'] : array(), $this, 'MySQLModel');

                if (isset($belongsToIds[$belongsToRelationShipName])) {
                    // Initialize relationship conditions
                    $belongsToRelationShipOptions = array(
                        'recursive' => -1,
                        'hash_by' => 'id',
                    );

                    // Translate general relationship specific options
                    // to the new model
                    if (isset($belongsToRelationShip['options'])) {
                        $belongsToRelationShipOptions = array_merge($belongsToRelationShipOptions, $belongsToRelationShip['options']);
                    }

                    // Translate current request relationship specific
                    // options to the new model
                    if (isset($options[$belongsToRelationShipName])) {
                        $belongsToRelationShipOptions = array_merge($belongsToRelationShipOptions, $options[$belongsToRelationShipName]);
                    }

                    // Remove duplicates
                    $belongsToIds[$belongsToRelationShipName] = array_unique($belongsToIds[$belongsToRelationShipName]);

                    $relatedElementsHash = $MySQLModel->getAnyById($belongsToIds[$belongsToRelationShipName], $belongsToRelationShipOptions);
                } else {
                    $relatedElementsHash = array();
                }

                foreach ($all as $element_index => $element) {
                    // User removed the belongs to field, relationship will not be loaded
                    if (!array_key_exists($belongsToRelationShip['foreign_key'], $element[$this->name])) {
                        continue;
                    }

                    if (isset($relatedElementsHash[$element[$this->name][$belongsToRelationShip['foreign_key']]])) {
                        $all[$element_index][$belongsToRelationShipName] = $relatedElementsHash[$element[$this->name][$belongsToRelationShip['foreign_key']]];
                    } else {
                        $all[$element_index][$belongsToRelationShipName] = null;
                    }

                    if (!$all[$element_index][$belongsToRelationShipName]) {
                        $all[$element_index][$belongsToRelationShipName] = $MySQLModel->getDummyRecord();
                    }

                    if ($all[$element_index][$belongsToRelationShipName]) {
                        $all[$element_index][$belongsToRelationShipName] = array_merge($all[$element_index][$belongsToRelationShipName][$belongsToRelationShip['model']], $all[$element_index][$belongsToRelationShipName]);
                        unset($all[$element_index][$belongsToRelationShipName][$belongsToRelationShip['model']]);
                    }
                }
            }
        }

        if ($options['recursive'] > 0 && !empty($elementIds)) {

            // Load all has many to related items
            foreach ($this->hasMany as $hasManyRelationShipName => $hasManyRelationShip) {
                if (isset($options[$hasManyRelationShipName]) && $options[$hasManyRelationShipName] === false) {
                    // User requested this relationship not been loaded
                    continue;
                }

                // Load hasmany model
                $MySQLModel = self::getRecycledInstance($hasManyRelationShip['model'], (isset($hasManyRelationShip['relationships'])) ? $hasManyRelationShip['relationships'] : array(), $this, 'MySQLModel');

                // Initialize relationship conditions
                $hasManyRelationShipOptions = array(
                    'recursive' => $options['recursive'] - 2,
                    'list_by' => $hasManyRelationShip['foreign_key'],
                    'where' => array(),
                );

                // Translate general relationship specific options
                // to the new model
                if (isset($hasManyRelationShip['options'])) {
                    $hasManyRelationShipOptions = array_merge($hasManyRelationShipOptions, $hasManyRelationShip['options']);
                }

                // Translate current request relationship specific
                // options to the new model
                if (isset($options[$hasManyRelationShipName])) {
                    $hasManyRelationShipOptions = array_merge($hasManyRelationShipOptions, $options[$hasManyRelationShipName]);
                }

                // Invalid options
                if (array_key_exists('list_by', $hasManyRelationShipOptions) && $hasManyRelationShipOptions['list_by'] != $hasManyRelationShip['foreign_key']) {
                    Debug::crash(false, 'Invalid option list_by for hasMany relationship.');
                } elseif (array_key_exists('hash_by', $hasManyRelationShipOptions)) {
                    Debug::crash(false, 'Invalid option hash_by for hasMany relationship.');
                } elseif (array_key_exists('group_by', $hasManyRelationShipOptions)) {
                    Debug::crash(false, 'Invalid option group_by for hasMany relationship.');
                } elseif (array_key_exists('limit', $hasManyRelationShipOptions)) {
                    Debug::crash(false, 'Invalid option limit for hasMany relationship.');
                }

                // Add conditions
                if (!is_array($hasManyRelationShipOptions['where'])) {
                    $hasManyRelationShipOptions['where'] = array($hasManyRelationShipOptions['where']);
                }

                $elementIds = array_unique($elementIds);

                $hasManyRelationShipOptions['where'][] = array($hasManyRelationShip['foreign_key'] => $elementIds);

                // Load all children
                $relatedElementsHash = $MySQLModel->getAll($hasManyRelationShipOptions);

                foreach ($all as $element_index => $element) {
                    if (isset($element[$this->name]['id'])) {
                        if (isset($relatedElementsHash[$element[$this->name]['id']])) {
                            foreach ($relatedElementsHash[$element[$this->name]['id']] as $relatedElement) {
                                $relatedElement = array_merge($relatedElement[$hasManyRelationShip['model']], $relatedElement);
                                unset($relatedElement[$hasManyRelationShip['model']]);
                                $all[$element_index][$hasManyRelationShipName][] = $relatedElement;
                            }
                        } else {
                            $all[$element_index][$hasManyRelationShipName] = array();
                        }
                    }
                }
            }
        }

        if ($options['recursive'] > 0 && !empty($elementIds)) {
            // Load all has and belongs to many to habtm items
            foreach ($this->habtm as $habtmRelationShipName => $habtmRelationShip) {
                if (isset($options[$habtmRelationShipName]) && $options[$habtmRelationShipName] === false) {
                    // User requested this relationship not been loaded
                    continue;
                }

                // Load HABTM model
                $AssociationMySQLModel = self::getRecycledInstance($habtmRelationShip['association_model'], array(), $this, 'MySQLModel');
                $MySQLModel = self::getRecycledInstance($habtmRelationShip['model'], (isset($belongsToRelationShip['relationships'])) ? $belongsToRelationShip['relationships'] : array(), $this, 'MySQLModel');

                // Load all associations
                $habtmElementsHash = $AssociationMySQLModel->getAll(array(
                                                                        'where' => array($habtmRelationShip['foreign_key'] => $elementIds),
                                                                        'recursive' => -1,
                                                                        'list_by' => $habtmRelationShip['foreign_key'],
                                                                        'fields' => array(
                                                                            $habtmRelationShip['foreign_key'],
                                                                            $habtmRelationShip['association_foreign_key'],
                                                                        ),
                                                                    ));

                $habtmElementIds = array();
                $habtmElementIdsHash = array();

                foreach ($habtmElementsHash as $element_id => $habtmElements) {
                    $habtmElementIdsHash[$element_id] = array();

                    foreach ($habtmElements as $habtmElement) {
                        $habtm_element_id = $habtmElement[$habtmRelationShip['association_model']][$habtmRelationShip['association_foreign_key']];

                        if (!in_array($habtm_element_id, $habtmElementIds)) {
                            $habtmElementIds[] = $habtm_element_id;
                        }

                        $habtmElementIdsHash[$element_id][] = $habtm_element_id;
                    }
                }

                // Initialize relationship conditions
                $habtmRelationShipOptions = array(
                    'hash_by' => 'id',
                    'recursive' => $options['recursive'] - 2,
                );

                // Translate general relationship specific options
                // to the new model
                if (isset($habtmRelationShip['options'])) {
                    $habtmRelationShipOptions = array_merge($habtmRelationShipOptions, $habtmRelationShip['options']);
                }

                // Translate current request relationship specific
                // options to the new model
                if (isset($options[$habtmRelationShipName])) {
                    $habtmRelationShipOptions = array_merge($habtmRelationShipOptions, $options[$habtmRelationShipName]);
                }

                // Invalid options
                if (array_key_exists('hash_by', $habtmRelationShipOptions) && $habtmRelationShipOptions['hash_by'] != 'id') {
                    Debug::crash(false, 'Invalid option hash_by for habtm relationship.');
                } elseif (array_key_exists('list_by', $habtmRelationShipOptions)) {
                    Debug::crash(false, 'Invalid option list_by for habtm relationship.');
                } elseif (array_key_exists('group_by', $habtmRelationShipOptions)) {
                    Debug::crash(false, 'Invalid option group_by for habtm relationship.');
                } elseif (array_key_exists('limit', $habtmRelationShipOptions)) {
                    Debug::crash(false, 'Invalid option limit for habtm relationship.');
                }

                // Remove duplicates
                $habtmElementIds = array_unique($habtmElementIds);

                $habtmElements = $MySQLModel->getAnyById($habtmElementIds, $habtmRelationShipOptions);

                foreach ($all as $element_index => $element) {
                    if (isset($all[$element_index][$this->name]['id'])) {
                        if (isset($habtmElementIdsHash[$all[$element_index][$this->name]['id']])) {
                            $all[$element_index][$habtmRelationShipName] = array();

                            foreach ($habtmElementIdsHash[$all[$element_index][$this->name]['id']] as $habtm_element_id) {
                                $habtmElement = $habtmElements[$habtm_element_id];
                                $habtmElement = array_merge($habtmElement[$habtmRelationShip['model']], $habtmElement);
                                unset($habtmElement[$habtmRelationShip['model']]);
                                $all[$element_index][$habtmRelationShipName][] = $habtmElement;

                                unset($habtmElement);
                            }
                            unset($habtm_element_id);
                        } else {
                            $all[$element_index][$habtmRelationShipName] = array();
                        }
                    }
                }
            }
        }

        // Retrieve keys
        $allKeys = array_keys($all);

        // Execute afterGetWithRelationships callback for each element
        foreach ($allKeys as $element_index) {
            $this->afterGetWithRelationships($all[$element_index], $options);
        }

        // Reorder all if asked
        if ($options['list_by']) {
            $newAll = array();

            foreach ($allKeys as $element_index) {
                if (!isset($all[$element_index][$this->name][$options['list_by']])) {
                    Debug::crash(false, 'list_by field value is not set for all records on getAll call. Model: ' . $this->name . ' fields: ' . json_encode($options['fields']));
                }

                if (!isset($newAll[$all[$element_index][$this->name][$options['list_by']]])) {
                    $newAll[$all[$element_index][$this->name][$options['list_by']]] = array();
                }

                $newAll[$all[$element_index][$this->name][$options['list_by']]][] = $all[$element_index];
            }

            $all = $newAll;
        }

        // Devolver el total de los resultados procesados y
        // saneados
        return $all;
    }

    /**
     * Returns the first record matching options.
     * Shortcut to getAll with limit 1 and extracting the resultant record.
     *
     * @param $options array
     *  - where: SQL conditions
     *  - order: order of the results (SQL expression with direction)
     *  - limit: max results (default 1000)
     *  - offset: results offset, for pagination
     *  - ignore_locale: if set to true the query will not include by default the locale conditions
     *  - ignore_hidden: if set to true the query will not include by default the hidden conditions
     *  - ignore_publish: if set to true the query will not include by default the publish conditions
     *  - recursive: recursivity of get, check self::$recursive
     *  - fields: fields to retrieve from the model, default '*'
     *  - RelationshipName: options specific for subqueries of the relationship
     *
     * @return array|null element
     */
    public function get($options = array()) {
        // Override default options by provided ones
        $options = array_merge(array(
                                   'where' => 1,
                                   'order' => $this->order,
                                   'ignore_locale' => false,
                                   'recursive' => $this->recursive,
                               ), $this->defaultGetOptions, $options);

        // For single record get, limit = 1 and also hash_by and list_by must be disabled
        $options['hash_by'] = null;
        $options['list_by'] = null;
        $options['limit'] = 1;

        // Get matching records
        $all = $this->getAll($options);

        // If there is a record, return it
        if (sizeof($all) >= 1) {
            return $all[0];
        } else {
            // If there is no record, return the dummy record (usually null)
            return $this->getDummyRecord();
        }
    }

    /**
     * Returns the content of the field on the first record matching options.
     * Shortcut to get extracting the field.
     *
     * @param $field string
     * @param $options array
     *  - where: SQL conditions
     *  - order: order of the results (SQL expression with direction)
     *  - limit: max results (default 1000)
     *  - offset: results offset, for pagination
     *  - ignore_locale: if set to true the query will not include by default the locale conditions
     *  - ignore_hidden: if set to true the query will not include by default the hidden conditions
     *  - ignore_publish: if set to true the query will not include by default the publish conditions
     *  - recursive: recursivity of get, check self::$recursive
     *  - fields: fields to retrieve from the model, default '*'
     *  - RelationshipName: options specific for subqueries of the relationship
     *
     * @return string|int|null element
     */
    public function field($field, $options = array()) {
        if (!isset($options['fields'])) {
            $options['fields'] = array($field);
        }

        $options['recursive'] = -1;

        $element = $this->get($options);

        if (isset($element[$this->name][$field])) {
            return $element[$this->name][$field];
        }

        return null;
    }

    /**
     * Returns a key=>value array using the specified fields as key or value
     *
     * @param string $key_field
     * @param array|null $valueFields
     * @param array $options
     *  - where: SQL conditions
     *  - order: order of the results (SQL expression with direction)
     *  - limit: max results (default 1000)
     *  - offset: results offset, for pagination
     *  - ignore_locale: if set to true the query will not include by default the locale conditions
     *  - ignore_hidden: if set to true the query will not include by default the hidden conditions
     *  - ignore_publish: if set to true the query will not include by default the publish conditions
     * @param string $join
     *
     * @return array
     */
    public function getList($key_field = 'id', $valueFields = null, $options = array(), $join = ' ') {
        // Retrieve default value fields from the model configuration if not provided
        if (!$valueFields) {
            $valueFields = $this->displayFields;
        }

        // Initialize $options array if not set
        if (!is_array($options)) {
            $options = array();
        }

        // Override provided options. Retrieve only the key field and the required value fields.
        // Recursive is -1 because all other fields will be ignored. Hash the results by the key field.
        $options['fields'] = $valueFields;
        $options['fields'][] = $key_field;
        $options['recursive'] = -1;
        $options['hash_by'] = $key_field;

        // Retrieve records
        $all = $this->getAll($options);

        // Override retrieved elements by the concatenation of value fields
        foreach ($all as &$element) {
            // List of element's values for the $valueFields
            $elementDisplayValues = array();

            // Push every value if not empty
            foreach ($valueFields as $value_field) {
                if (isset($element[$this->name][$value_field]) && $element[$this->name][$value_field]) {
                    $elementDisplayValues[] = $element[$this->name][$value_field];
                }
            }

            // Join values by ' ' and override element
            $element = implode($join, $elementDisplayValues);
        }

        // Return hash
        return $all;
    }

    /**
     * Hides an element.
     * Updates the hidden value of the element to the current date.
     * Calls beforeHide
     * Calls afterHide
     * Fills ::$errors
     *
     * @param $id int id to hide
     * @param $options array list of options
     *  - reset_errors: if set to false, ::$errors will not be reset at the beginning of execution (for nested calls)
     *
     * @return bool success
     */
    public function hideById($id, $options = array()) {
        // Clear validation errors
        if (!isset($options['reset_errors']) || $options['reset_errors']) {
            $this->errors = array();
            $this->errorsByField = array();
            $this->errorCodesByField = array();
            $this->callbackException = null;
        }

        // Retrieve element to be hidden ignoring default filters
        $element = $this->getById($id, array(
            'recursive' => -1,
            'ignore_hidden' => true,
            'ignore_locale' => true,
            'ignore_publish' => true,
        ));

        // If no element was found, return false.
        if (!$element) {
            $this->errors[] = __('Element not found.', true);

            return false;
        }

        // Execute the beforeHide callback. If false, return false (details will be set by beforeHide on the ::$error variable)
        if (!$this->beforeHide($element, $options)) {
            return false;
        }

        // Attempt to update the element
        $element = $this->updateFields($id, array('hidden' => now()), array('reset_errors' => false));

        // If update was successful
        if ($element) {
            // Execute afterHide callback
            $this->afterHide($element, $options);

            // Success
            return true;
        } else {
            // Error (details will be set by updateFields on the ::$error variable)
            return false;
        }
    }

    /**
     * Before hide callback.
     *
     * @param $element array element to be hidden.
     * @param $options array
     *
     * @return bool if not true, stops hiding
     */
    protected function beforeHide($element, $options) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            // If any of them fails, the whole fails
            if (!$Behavior->beforeHide($element, $options)) {
                return false;
            }
        }

        // If no previous errors, success
        return true;
    }

    /**
     * After hide callback.
     *
     * @param $element mixed
     */
    protected function afterHide($element, $options) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            $Behavior->afterHide($element, $options);
        }

        // If the model is set to update the sitemap, launch the update
        if ($this->update_sitemap) {
            $Sitemap = self::getRecycledInstance('Sitemap', array(), $this, 'MySQLModel');
            $Sitemap->updateSitemaps();
        }
    }

    /**
     * Deletes an element.
     * Calls beforeDelete
     * Calls afterDelete
     * Fills ::$errors
     *
     * @param $id int id to delete
     * @param $options array list of options
     *  - reset_errors: if set to false, ::$errors will not be reseted at the begining of execution (for nested calls)
     *
     * @return bool success
     */
    public function deleteById($id, $options = array()) {
        // If current app does not have any mysql server, return false
        if (GestyMVC::config('db') === false) {
            return false;
        }

        // If id is invalid, fail
        if (!is_numeric($id)) {
            return false;
        }

        // Clear validation errors
        if (!isset($options['reset_errors']) || $options['reset_errors']) {
            $this->errors = array();
        }

        if (!isset($options['where'])) {
            $options['where'] = 1;
        }

        // Retrieve element to be hidden ignoring default filters
        $element = $this->getById($id, array(
            'recursive' => -1,
            'ignore_hidden' => true,
            'ignore_locale' => true,
            'ignore_publish' => true,
            'for_delete' => true,
            'where' => $options['where'],
        ));

        // If no element was found, return false.
        if (!$element) {
            $this->errors[] = __('Element not found.', true);

            return false;
        }

        // Execute the beforeDelete callback. If false, return false (details will be set by beforeDelete on the ::$error variable)
        if (!$this->beforeDelete($element, $options)) {
            return false;
        }

        try {
            // Generate delete query
            $query = 'DELETE FROM ' . $this->from_expression . ' WHERE ' . $this->full_table_name . '.`id` = \'' . self::realEscapeString($id) . '\'';

            // Process query
            $result = self::query($query);

            // If query fails (it shouldn't) and on debug mode, stop execution and show the failing query
            if ($result === false) {
                Debug::crash(false, $query . ' #' . self::queryError() . ' ' . implode(' > ', involvedFilesDebug(array(
                                                                                                                    'index.php',
                                                                                                                    'GestyMVC.php',
                                                                                                                    'Dispatcher.php',
                                                                                                                    'Controller.php',
                                                                                                                    'MySQLModel.php',
                                                                                                                ))), 'mysql', 'sql');
            }

            // Validate result
            if ($result) {
                // Execute afterDelete callback
                $this->afterDelete($element, $options);

                return true;
            }
        } catch (Exception $e) {
        }

        // Fail by default
        return false;
    }

    /**
     * Before delete callback.
     *
     * @param $element array element to be deleted.
     * @param $options array
     *
     * @return bool if not true, stops deleting
     */
    protected function beforeDelete($element, $options) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            // If any of them fails, the whole fails
            if (!$Behavior->beforeDelete($element, $options)) {
                return false;
            }
        }

        // If no previous errors, success
        return true;
    }

    /**
     * After delete callback.
     *
     * @param $element array
     * @param $options array
     *
     * @return void
     */
    protected function afterDelete($element, $options) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            $Behavior->afterDelete($element, $options);
        }

        // If the model is set to update the sitemap, launch the update
        if ($this->update_sitemap) {
            $Sitemap = self::getRecycledInstance('Sitemap', array(), $this, 'MySQLModel');
            $Sitemap->updateSitemaps();
        }
    }

    /**
     * Single field update.
     *
     * @param $id int
     * @param $field
     * @param $value
     * @param $options array list of options
     *  - reset_errors: if set to false, ::$errors will not be reseted at the begining of execution (for nested calls)
     *
     * @return boolean result
     */
    public function updateFieldValue($id, $field, $value, $options = array()) {
        // If current app does not have any mysql server, return false
        if (GestyMVC::config('db') === false) {
            return false;
        }

        // Clear validation errors
        if (!isset($options['reset_errors']) || $options['reset_errors']) {
            $this->errors = array();
        }

        // If field is empty, fail
        if (!$field) {
            $this->errors[] = __('Invalid field.', true);

            return false;
        }

        // If field is not listed as single update field, fail
        if (!in_array($field, $this->singleUpdateFields)) {
            $this->errors[] = __('Invalid field.', true);

            return false;
        }

        // Errors may be already set
        $options['reset_errors'] = false;

        // Update required field
        return $this->updateFields($id, array($field => $value), $options);
    }

    /**
     * Multiple field update.
     *
     * @param $id int
     * @param $values array hashmap of fields and
     * values to update
     * @param $options array
     *  - reset_errors: if set to false, ::$errors will not be reseted at the begining of execution (for nested calls)
     *
     * @return boolean result
     */
    public function updateFields($id, $values, $options = array()) {
        // Validation option
        $options['skip_callbacks'] = (!isset($options['skip_callbacks']) || !$options['skip_callbacks'] ? false : true);

        // If current app does not have any mysql server, return false
        if (GestyMVC::config('db') === false) {
            return false;
        }

        // If id is invalid or no values were provided, fail
        if (!is_numeric($id) || empty($values)) {
            return false;
        }

        // Clear validation errors
        if (!isset($options['reset_errors']) || $options['reset_errors']) {
            $this->errors = array();
            $this->errorsByField = array();
            $this->errorCodesByField = array();
            $this->callbackException = null;
        }

        // Retrieve full element
        $element = $this->getById($id, array(
            'ignore_hidden' => true,
            'ignore_locale' => true,
            'ignore_publish' => true,
            'ignore_translation' => true,
            'recursive' => -1,
        ));

        // If element does not exist, abort
        if (!$element) {
            $this->errors[] = __('Record not found.', true);

            return false;
        }

        // Callback
        if (!$options['skip_callbacks'] && !$this->beforeValidate($element, $values, $options)) {
            return false;
        }

        // Callback
        if (!$options['skip_callbacks'] && !$this->validates($element, $values, $options)) {
            return false;
        }

        // Callback
        if (!$options['skip_callbacks'] && !$this->beforeSave($element, $values, $options)) {
            return false;
        }

        if (!isset($options['add_timestamps']) || $options['add_timestamps']) {
            $values['modified'] = now();
        }
        if ($this->user_timestamp_fields && (!isset($options['add_timestamps']) || $options['add_timestamps'])) {
            $values['modified_by_user_id'] = (Authentication::get('user', 'id') ? Authentication::get('user', 'id') : null);
        }

        // Escape values
        $querySets = array();
        foreach ($values as $field => $value) {
            if ($value === null) {
                $value = 'NULL';
            } else {
                $value = '\'' . self::realEscapeString($value) . '\'';
            }

            // Generate SQL update
            $querySets[] = $this->full_table_name . '.`' . $field . '` = ' . $value;
        }

        try {
            // Create query
            $query = 'UPDATE ' . $this->full_table_name . ' SET ' . implode(', ', $querySets) . ' WHERE ' . $this->full_table_name . '.`id` = ' . $id . ' LIMIT 1';

            // Process query
            $result = self::query($query);

            // If query fails (it shouldn't) and on debug mode, stop execution and show the failing query
            if ($result === false && !$this->addValidationQueryErrors()) {
                Debug::crash(false, $query . ' #' . self::queryError() . ' ' . implode(' > ', involvedFilesDebug(array(
                                                                                                                    'index.php',
                                                                                                                    'GestyMVC.php',
                                                                                                                    'Dispatcher.php',
                                                                                                                    'Controller.php',
                                                                                                                    'MySQLModel.php',
                                                                                                                ))), 'mysql', 'sql');
            }

            // Validate result
            if ($result) {
                // If no callbacks, return true directly (no element information)
                if ($options['skip_callbacks']) {
                    return true;
                }

                // Set original element
                $originalElement = $element;

                // Retrieve updated element
                $element = $this->getById($id, array(
                    'recursive' => -1,
                    'ignore_locale' => true,
                    'ignore_publish' => true,
                    'ignore_hidden' => true,
                ));

                if (!$element) {
                    return false;
                }

                // Execute after save callback
                try {
                    $this->afterSave(false, $element, $values, $originalElement, $options);
                } catch (Exception $e) {
                    $this->callbackException = $e;
                }

                // Return updated element
                return $element;
            }
        } catch (Exception $e) {
        }

        // If error, return false
        return false;
    }

    /**
     * Before get callback.
     * ATTENTION: this is the only 'before' callback that cannot break the execution of the call.
     *
     * @param $options array
     *
     * @return void
     */
    protected function beforeGet(&$options) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            $Behavior->beforeGet($options);
        }
    }

    /**
     * After get callback.
     *
     * @param $element array the element retrieved
     * @param $options
     *
     * @return void
     */
    protected function afterGet(&$element, $options = array()) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            $Behavior->afterGet($element, $options);
        }
    }

    /**
     * After get with relationshipis callback.
     *
     * @param $element array the element retrieved
     * @param $options
     *
     * @return void
     */
    protected function afterGetWithRelationships(&$element, $options = array()) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            $Behavior->afterGetWithRelationships($element, $options);
        }
    }

    /**
     * Validates a value with a validation rule
     *
     * @param $element
     * @param $field
     * @param $value
     * @param $rule
     *
     * @return bool if not true, execution will be stoped
     */
    protected function validateValue($element, $field, $value, $rule) {
        // By default, validates
        $validates = true;

        // For each possible rule, check validation
        switch ($rule) {
            case 'required':
                break;

            case 'notempty':
                $validates = strlen(strval($value)) > 0;
                break;

            case 'email':
                $validates = $value === null || $value === '' || filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
                break;

            case 'float':
            case 'single':
            case 'double':
            case 'decimal':
                $validates = $value === null || $value === '' || (is_numeric($value) && stripos($value, 'x') === false && stripos($value, 'e') === false);
                break;

            case 'numeric':
                $validates = $value === null || $value === '' || is_numeric($value);
                break;

            case 'int':
                $validates = $value === null || filter_var($value, FILTER_VALIDATE_INT) !== false;

                if ($validates && (intval($value) > 2147483646 || intval($value) < -2147483646)) {
                    $validates = false;
                }

                break;

            case 'long':
                $validates = $value === null || (is_numeric($value) && strpos(strval($value), '.') === false);
                break;

            case 'date':
            case 'datetime':
                if ($value !== null) {
                    $str_to_time = strtotime($value);
                    $validates = $str_to_time !== -1 && $str_to_time !== false;

                    if ($validates) {
                        $splittedDate = explode(' ', $value);
                        $splittedDate = explode('-', $splittedDate[0]);
                        $validates = sizeof($splittedDate) == 3 && checkdate(intval($splittedDate[1]), intval($splittedDate[2]), intval($splittedDate[0])) && intval($splittedDate[0]) === intval(date('Y', $str_to_time)) && intval($splittedDate[1]) === intval(date('m', $str_to_time)) && intval($splittedDate[2]) === intval(date('d', $str_to_time));
                    }
                } else {
                    $validates = true;
                }

                break;

            case 'spanish_telephone':
                $validates = true;

                if ($value !== null && $value !== '') {
                    $validates = is_numeric($value) && strlen($value) == 9 && (starts_with($value, 6) || starts_with($value, 7) || starts_with($value, 9)) && strpos(strval($value), '.') === false;
                }

                break;

            case 'spanish_mobile_phone':
                $validates = true;

                if ($value !== null && $value !== '') {
                    $validates = is_numeric($value) && strlen($value) == 9 && (starts_with($value, 6) || starts_with($value, 7)) && strpos(strval($value), '.') === false;
                }

                break;

            case 'international_telephone':
                $temp_value = trim($value);
                $validates = true;

                if (!empty($temp_value)) {
                    if (substr($temp_value, 0, 1) == '+'){
                        $temp_value = preg_replace('/\+/', '00', $temp_value, 1);
                    }

                    $temp_value = str_replace(array(' ', '(', ')', '+', '-'), '', $temp_value);

                    if (strlen($temp_value) > 15) {
                        $validates = false;
                    } else {
                        if (!preg_match('/^[0-9\+\(\)\- ]+$/', $temp_value)) {
                            $validates = false;
                        }

                        if ($validates) {
                            if (strpos($temp_value, '  ') !== false) {
                                $validates = false;
                            } elseif (substr_count($temp_value, '+') > 2) {
                                $validates = false;
                            } elseif (substr_count($temp_value, '(') > 2) {
                                $validates = false;
                            } elseif (substr_count($temp_value, ')') > 2) {
                                $validates = false;
                            }
                        }
                    }

                    if (strlen($temp_value) > 9 && substr($temp_value, 0, 2) != '00') {
                        return false;
                    }
                }

                unset($temp_value);
                break;

            case 'unique':
                if ($value !== null && (!$element || is_null($element[$this->name][$field]) || $element[$this->name][$field] != $value)) {
                    // Check for duplicates
                    $duplicatedConditions = array('`' . $field . '` LIKE' => $value);

                    // If user is being edit, remove itself from duplicate query
                    if ($element) {
                        $duplicatedConditions['id <>'] = $element[$this->name]['id'];
                    }

                    // Count duplicates
                    if ($this->count(array('where' => $duplicatedConditions)) > 0) {
                        $validates = false;
                    } else {
                        $validates = true;
                    }
                } else {
                    $validates = true;
                }

                break;

            default:
                // Check for custom validation
                if (method_exists($this, $rule)) {
                    $validates = call_user_func_array(array($this, $rule), array($element, $field, $value, $rule));
                }
        }

        // Return validation status
        return $validates;
    }

    /**
     * Before validate callback.
     * Executed before validation on updateFields and addNew calls.
     *
     * @param $element array element being updated
     * @param $values array values being updated
     * @param $options array options
     *
     * @return bool
     */
    public function beforeValidate($element, &$values, $options) {
        if (array_key_exists('id', $values)) {
            if (!$values['id']) {
                unset($values['id']);
            }
        }

        // Replace empty fields by null
        foreach ($values as $field => &$value) {
            // Field type
            $field_is_date = false;
            $field_is_numeric = false;
            $field_is_nullable = true;
            $field_default_value = null;

            if (isset($this->structure[$field])) {
                // Check if datetime based on table structure
                if (in_array($this->structure[$field]['DATA_TYPE'], array(
                    'date',
                    'datetime',
                ))) {
                    $field_is_date = true;
                }

                if (in_array($this->structure[$field]['DATA_TYPE'], array(
                    'integer',
                    'int',
                    'smallint',
                    'tinyint',
                    'mediumint',
                    'bigint',
                    'decimal',
                    'numeric',
                    'float',
                    'double',
                    'bit',
                ))) {
                    $field_is_numeric = true;
                }

                if ($this->structure[$field]['IS_NULLABLE'] === 'NO') {
                    $field_is_nullable = false;
                }

                if ($this->structure[$field]['COLUMN_DEFAULT'] !== null) {
                    $field_default_value = $this->structure[$field]['COLUMN_DEFAULT'];
                }
            }

            if (is_array($value) && isset($this->validation[$field]) && isset($this->validation[$field]['tinyint'])) {
                if (isset($value[0]) && $value[0]) {
                    $value = 0;
                } elseif (isset($value[1]) && $value[1]) {
                    $value = 1;
                } else {
                    $value = null;
                }
            } elseif (is_array($value)) {
                // Ignore
            } elseif (strlen($field) > 3 && ends_with($field, '_id') && !$value) {
                // Remove 0 and empty strings from foreign key
                // fields
                $value = null;
            } elseif ((($value !== null && strlen($value) == 0) || starts_with($value, '0000')) && ($field_is_date || (isset($this->validation[$field]) && (isset($this->validation[$field]['datetime']) || isset($this->validation[$field]['date']))))) {
                // Remove empty dates
                $value = null;
            } elseif (!$value && isset($this->validation[$field]) && isset($this->validation[$field]['tinyint']) && isset($this->validation[$field]['notempty'])) {
                // Set empty tinyint to 0
                $value = 0;
            } elseif ($value !== null && strlen($value) == 0 && ($field_is_numeric || (isset($this->validation[$field]) && (isset($this->validation[$field]['int']) || isset($this->validation[$field]['float']) || isset($this->validation[$field]['long']) || isset($this->validation[$field]['double']) || isset($this->validation[$field]['decimal']))))) {
                // Remove empty numbers
                $value = null;
            } elseif (($field === 'seo_title' || $field === 'seo_description') && !$value) {
                // Set default seo fields value
                $value = 'none';
            }

            // Override null by default value if not nullable
            if ($value === null && !$field_is_nullable && $field_default_value !== null) {
                $value = $field_default_value;
            }
        }

        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            if (!$Behavior->beforeValidate($element, $values, $options)) {
                return false;
            }
        }

        // If no previous errors, return true
        return true;
    }

    /**
     * Validates provided values.
     *
     * @param $element array element being updated
     * @param $values array values being updated
     *
     * @return bool validates or not
     */
    public function validates($element, &$values, $options) {
        // By default, validated
        $validated = true;

        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            if (!$Behavior->validates($element, $values, $options)) {
                return false;
            }
        }

        // Check for required fields on create
        if (empty($element)) {
            foreach ($this->validation as $field => $fieldValidation) {
                if (isset($fieldValidation['required']) && $fieldValidation['required'] && !isset($values[$field])) {
                    if ($validated) {
                        $validated = false;
                        $this->errors[] = __($fieldValidation['required'], true);
                    }

                    if (!isset($this->errorsByField[$field])) {
                        $this->errorsByField[$field] = __($fieldValidation['required'], true);
                        $this->errorCodesByField[$field] = 'required';
                    }
                }
            }
        }

        // Check for validation
        foreach ($values as $field => $value) {
            if (isset($this->validation[$field])) {
                $fieldValidation = $this->validation[$field];

                foreach ($fieldValidation as $rule => $errorMessage) {
                    if (!$this->validateValue($element, $field, $value, $rule)) {
                        if ($validated) {
                            $validated = false;
                            $this->errors[] = __($errorMessage, true);
                        }

                        if (!isset($this->errorsByField[$field])) {
                            $this->errorsByField[$field] = __($errorMessage, true);
                            $this->errorCodesByField[$field] = $rule;
                        }
                    }
                }
            }
        }

        // If no errors, success
        return $validated;
    }

    /**
     * Before save callback
     *
     * @param $element array. Element that's being
     * updated/created.
     * @param $values array. Values to be updated.
     *
     * @return bool continue or not
     */
    protected function beforeSave($element, &$values, $options) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            if (!$Behavior->beforeSave($element, $values, $options)) {
                return false;
            }
        }

        // Return true by default
        return true;
    }

    /**
     * After save callback.
     *
     * @param $created bool whether or not the record was just created
     * @param $element array updated record
     * @param $values array updated values
     * @param $originalElement array element before update
     * @param $options array
     *
     * @return void
     */
    protected function afterSave($created, $element, $values, $originalElement, $options) {
        // Translate the callback to each of the behaviors present
        foreach ($this->Behaviors as &$Behavior) {
            $Behavior->afterSave($created, $element, $values, $originalElement, $options);
        }

        // Log if this is not the log model
        if ($this->name != 'Log' && $this->logged) {
            // Loag every field change
            foreach ($values as $field => $value) {
                // Create log
                $log = array();
                $log['email'] = (isset($loggedUser['email']) ? $loggedUser['email'] : '');
                $log['model'] = $this->name;
                $log['model_id'] = $element[$this->name]['id'];
                $log['field'] = $field;
                $log['value'] = $value;

                // Insert
                self::getRecycledInstance('Log', array(), $this, 'MySQLModel')->addNew($log);
            }
        }

        // If this model updates the sitemap and is now
        // publish, update the sitemaps
        if (isset($values['publish']) && (($created && $values['publish']) || (!$created)) && $this->update_sitemap) {
            $Sitemap = self::getRecycledInstance('Sitemap', array(), $this, 'MySQLModel');
            $Sitemap->updateSitemaps();
        }
    }

    /**
     * Handles custom method calls, like getBy<field>, getAnyBy<field>, getAllBy<field>,
     * deleteBy<field>, deleteAnyBy<field>, deleteAllBy<field>, hideBy<field>, hideAnyBy<field>, hideAllBy<field>,
     * updateBy<field>, updateAnyBy<field>, updateAllBy<field>,
     *
     * @param $method string name of method to call.
     * @param $params array parameters for the method.
     *
     * @return mixed Whatever is returned by called
     * method
     */
    public function __call($method, $params) {
        // Separate the words from the request
        $methodWords = array('');

        // Separate the words based on casing
        for ($i = 0; $i < mb_strlen($method, 'UTF-8'); $i++) {
            $char = mb_substr($method, $i, 1, 'UTF-8');

            if ($i == 0 || $char != mb_strtoupper($char, 'UTF-8')) {
                $methodWords[sizeof($methodWords) - 1] .= $char;
            } else {
                $methodWords[] .= $char;
            }
        }

        // Allowed first words are 'get', 'delete', 'hide'. Method call must have more than 2 words
        if (sizeof($methodWords) > 2 && in_array($methodWords[0], array('get', 'delete', 'hide', 'update'))) {
            // Extract action (first word)
            $method_action = $methodWords[0];

            // The method call type can be all, any or first
            if ($methodWords[1] == 'All') {
                $type = 'all';

                // Exclude words 1 and 2 (type and -assumed- 'by')
                unset($methodWords[1]);
                unset($methodWords[2]);
            } elseif ($methodWords[1] == 'Any') {
                $type = 'any';

                // Exclude words 1 and 2 (type and -assumed- 'by')
                unset($methodWords[1]);
                unset($methodWords[2]);
            } else {
                $type = 'first';

                // Exclude word 1 (type)
                unset($methodWords[1]);
            }

            // Remove method word 0 (method action)
            unset($methodWords[0]);

            // The field name is the underscored version of all the remaining word
            $field_name = mb_strtolower(implode('_', $methodWords));

            // Retrieve options param
            if (isset($params[1])) {
                $options = $params[1];
            } else {
                $options = array();
            }

            // If where is not set or is not an array, turn it into it
            if (!isset($options['where'])) {
                $options['where'] = array();
            } elseif (!is_array($options['where'])) {
                $options['where'] = array($options['where']);
            }

            if ($type == 'any') {
                // On type any, generate a IN (,) condition
                if (!is_array($params[0])) {
                    // If it is not an array, fatal error.
                    Debug::crash(true, 'Invalid arguments for xAnyBy call. ' . implode(' > ', involvedFilesDebug(array(
                                                                                                                    'index.php',
                                                                                                                    'GestyMVC.php',
                                                                                                                    'Dispatcher.php',
                                                                                                                    'Controller.php',
                                                                                                                    'MySQLModel.php',
                                                                                                                ))));
                }

                // Set conditions
                $options['where'][] = array($field_name => $params[0]);
            } else {
                if (is_array($params[0])) {
                    // If it is an array, fatal error.
                    Debug::crash(true, 'Invalid arguments for xBy call. ' . implode(' > ', involvedFilesDebug(array(
                                                                                                                 'index.php',
                                                                                                                 'GestyMVC.php',
                                                                                                                 'Dispatcher.php',
                                                                                                                 'Controller.php',
                                                                                                                 'MySQLModel.php',
                                                                                                             ))));
                }

                // Set conditions
                $options['where'][] = array($field_name => $params[0]);
            }

            // Limit records if 'first'
            if ($type == 'first') {
                $options['limit'] = 1;
            }

            // If delete or hide, add options
            if (in_array($method_action, array('delete', 'hide'))) {
                $options['ignore_locale'] = true;
                $options['ignore_publish'] = true;
                $options['ignore_hidden'] = true;
                $options['recursive'] = -1;
                $options['fields'] = array('id');
            }

            // Retrieve elements
            $elements = $this->getAll($options);

            // If deleting, hiding or updating, proceed using the id. If get, return values directly
            if ($method_action == 'delete' || $method_action == 'hide' || $method_action == 'update') {
                // For nested calls transactions can be disabled
                $options['transactional'] = (!isset($options['transactional']) || $options['transactional']);

                // Begin transaction if required
                if ($options['transactional']) {
                    $this->beginTransaction();
                }

                $deleteOptions = isset($params[2]) ? $params[2] : array();

                if ($method_action == 'delete') {
                    $deleteOptions['for_delete'] = true;
                }

                // No errors so far
                $errors = false;

                // Foreach element, try to delete, hide or update
                foreach ($elements as $element) {
                    if ($method_action == 'delete' && !$this->deleteById($element[$this->name]['id'], $deleteOptions)) {
                        $errors = true;
                    } elseif ($method_action == 'hide' && !$this->hideById($element[$this->name]['id'], $deleteOptions)) {
                        $errors = true;
                    } elseif ($method_action == 'update' && !$this->updateFields($element[$this->name]['id'], isset($params[2]) ? $params[2] : array(), isset($params[3]) ? $params[3] : array())) {
                        $errors = true;
                    }

                    // Stop if error found
                    if ($errors) {
                        break;
                    }
                }

                // If transactional, commit if no errors or rollback
                if ($options['transactional']) {
                    if (!$errors) {
                        $this->commitTransaction();
                    } else {
                        $this->rollbackTransaction();
                    }
                }

                // Return true (success) if no errors or false
                return !$errors;
            } elseif ($method_action == 'get') {
                if ($type == 'first') {
                    // If type first, return the first element if present or the last
                    return (sizeof($elements) ? $elements[0] : null);
                } else {
                    // If type all, return all elements
                    return $elements;
                }
            }
        }

        // For inherited behavior methods, call the first one found
        foreach ($this->Behaviors as &$Behavior) {
            if (method_exists($Behavior, $method)) {
                return call_user_func_array(array($Behavior, $method), $params);
            }
        }

        // Fatal error, method not found nor recognized.
        Debug::crash(true, 'Call to undefined method ' . $method . ' on Class ' . $this->name);
    }

    /**
     * Adds a new record to the table.
     *
     * @param $values array values to be inserted
     * @param array $options
     *  - reset_errors: if set to false, ::$errors will not be reseted at the begining of execution (for nested calls)
     *
     * @return array|bool
     */
    public function addNew($values, $options = array()) {
        // Validation option
        $options['skip_callbacks'] = (!isset($options['skip_callbacks']) || !$options['skip_callbacks'] ? false : true);

        // If current app does not have any mysql server, return false
        if (GestyMVC::config('db') === false) {
            return false;
        }

        // Clear validation errors
        if (!isset($options['reset_errors']) || $options['reset_errors']) {
            $this->errors = array();
            $this->errorsByField = array();
            $this->errorCodesByField = array();
            $this->callbackException = null;
        }

        // Callback
        if (!$options['skip_callbacks'] && !$this->beforeValidate(array(), $values, $options)) {
            return false;
        }

        // Check if data validates (errors will be filled by the ::validates function)
        if (!$options['skip_callbacks'] && !$this->validates(array(), $values, $options)) {
            return false;
        }

        // Check if allowed to save (errors will be filled by the ::validates function)
        if (!$options['skip_callbacks'] && !$this->beforeSave(array(), $values, $options)) {
            return false;
        }

        // Add default fields
        if (!isset($values['created']) || (!isset($options['add_timestamps']) || $options['add_timestamps'])) {
            $values['created'] = now();
        }
        if (!isset($values['modified']) || (!isset($options['add_timestamps']) || $options['add_timestamps'])) {
            $values['modified'] = now();
        }
        if ($this->user_timestamp_fields && (!isset($values['created_by_user_id']) || (!isset($options['add_timestamps']) || $options['add_timestamps']))) {
            $values['created_by_user_id'] = (Authentication::get('user', 'id') ? Authentication::get('user', 'id') : null);
        }
        if ($this->user_timestamp_fields && (!isset($values['modified_by_user_id']) || (!isset($options['add_timestamps']) || $options['add_timestamps']))) {
            $values['modified_by_user_id'] = (Authentication::get('user', 'id') ? Authentication::get('user', 'id') : null);
        }

        // Escape values
        $SQLvalues = array();
        foreach ($values as $field => $value) {
            $field = $this->full_table_name . '.`' . $field . '`';

            if ($value !== null) {
                $SQLvalues[$field] = '\'' . self::realEscapeString($value) . '\'';
            } else {
                $SQLvalues[$field] = 'NULL';
            }
        }

        try {
            // Generate query
            $query = 'INSERT INTO ' . $this->full_table_name . ' (' . implode(',', array_keys($SQLvalues)) . ') VALUES (' . implode(',', array_values($SQLvalues)) . ')';

            // Process query
            $result = self::query($query);

            // If query fails (it shouldn't) and on debug mode, stop execution and show the failing query
            if ($result === false && !$this->addValidationQueryErrors()) {
                Debug::crash(false, $query . ' #' . self::queryError() . ' ' . implode(' > ', involvedFilesDebug(array(
                                                                                                                    'index.php',
                                                                                                                    'GestyMVC.php',
                                                                                                                    'Dispatcher.php',
                                                                                                                    'Controller.php',
                                                                                                                    'MySQLModel.php',
                                                                                                                ))), 'mysql', 'sql');
            }

            // Validate result
            if ($result) {
                // If no callbacks, return true directly (no element information)
                if ($options['skip_callbacks']) {
                    return true;
                }

                // Retrieve element
                $id = mysqli_insert_id(self::$conn);
                $element = $this->getById($id, array(
                    'ignore_locale' => true,
                    'ignore_publish' => true,
                    'ignore_hidden' => true,
                    'recursive' => -1,
                ));

                // Check the element has been successfully inserted
                if ($element) {
                    // Callback
                    try {
                        $this->afterSave(true, $element, $values, array(), $options);
                    } catch (Exception $e) {
                        $this->callbackException = $e;
                    }

                    // Return resultant element
                    return $element;
                }
            }
        } catch (Exception $e) {
        }

        // By default return false
        return false;
    }

    /**
     * Starts a MySQL transaction.
     *
     * @return void
     */
    public function beginTransaction() {
        if (self::$transaction_depth === 0) {
            self::query('BEGIN');
        }

        self::$transaction_depth++;
    }

    /**
     * Commits a MySQL transaction
     *
     * @return void
     */
    public function commitTransaction() {
        if (self::$transaction_depth === 1) {
            self::query('COMMIT');
        }

        self::$transaction_depth--;
    }

    /**
     * Rollsback a MySQL transaction
     *
     * @return void
     */
    public function rollbackTransaction() {
        if (self::$transaction_depth === 1) {
            self::query('ROLLBACK');
        }

        self::$transaction_depth--;
    }

    /**
     * Shortcut to model initialization (reclycled)
     *
     * @param $model_name
     * @param $relationships array
     * @param $owner object recycled models cannot be shared
     * @param $recycling_key string
     *
     * @return MySQLModel
     */
    public static function getRecycledInstance($model_name, $relationships = array(), &$owner, $recycling_key = null) {
        $full_key = $model_name . '-' . $recycling_key . '-' . md5(json_encode($relationships));

        if (!isset($owner->reclicledModels[$full_key])) {
            if (!isset($owner->reclicledModels)) {
                $owner->reclicledModels = array();
            }

            $owner->reclicledModels[$full_key] = self::getInstance($model_name, $relationships);
        }

        return $owner->reclicledModels[$full_key];
    }

    /**
     * Shortcut to model initialization.
     *
     * @param $model_name
     * @param $relationships array
     *
     * @return MySQLModel
     */
    public static function getInstance($model_name, $relationships = array()) {
        if (Debug::debugging() && $model_name != 'DebugLog') {
            Debug::log(date('H:i:s') . ' ' . $model_name . '::new() ' . implode(' > ', involvedFilesDebug(array(
                                                                                                             'index.php',
                                                                                                             'GestyMVC.php',
                                                                                                             'Dispatcher.php',
                                                                                                             'Controller.php',
                                                                                                             'MySQLModel.php',
                                                                                                         ))), 'model-instances', 'txt', false);
        }

        // Generated instance
        $instance = null;

        // Model file name
        $model_filename = null;

        // Main app model
        if (file_exists(INCLUDE_PATH . 'models/' . $model_name . '.php')) {
            $model_filename = INCLUDE_PATH . 'models/' . $model_name . '.php';
        }

        // Module model
        if (!$model_filename) {
            $modules = GestyMVC::config('modules');
            if ($modules) {
                foreach ($modules as $module) {
                    if (file_exists(INCLUDE_PATH . 'modules/' . $module . '/models/' . $model_name . '.php')) {
                        $model_filename = INCLUDE_PATH . 'modules/' . $module . '/models/' . $model_name . '.php';
                        break;
                    }
                }
            }
        }

        // Core model
        if (!$model_filename) {
            $model_filename = CORE_PATH . 'model/default/' . $model_name . '.php';
        }

        // Check if file exist
        if (file_exists($model_filename)) {
            // Include
            include_once($model_filename);

            // Check class
            if (class_exists($model_name)) {
                // Initialize class
                $instance = new $model_name();
            } else {
                // Class not found, crash
                Debug::crash(true, 'File ' . $model_filename . ' does not define model ' . $model_name . ' class.');
            }
        } else {
            // If file is not found, generate simple model instance on the fly
            $instance = new AppMySQLModel();
            $instance->name = $model_name;
            $instance->initialize();
        }

        // Add belongsTo relationships if required
        if (isset($relationships['belongsTo']) && $relationships['belongsTo']) {
            foreach ($relationships['belongsTo'] as $relationship_name => $relationshipData) {
                $instance->belongsTo[$relationship_name] = $relationshipData;
            }
        }

        // Add hasMany relationships if required
        if (isset($relationships['hasMany']) && $relationships['hasMany']) {
            foreach ($relationships['hasMany'] as $relationship_name => $relationshipData) {
                $instance->hasMany[$relationship_name] = $relationshipData;
            }
        }

        // Add habtm relationships if required
        if (isset($relationships['habtm']) && $relationships['habtm']) {
            foreach ($relationships['habtm'] as $relationship_name => $relationshipData) {
                $instance->habtm[$relationship_name] = $relationshipData;
            }
        }

        // Return generated instance
        return $instance;
    }

    /**
     * Retrieves the full generated table name of this model.
     *
     * @return string
     */
    public function fullTableName() {
        return $this->full_table_name;
    }

    protected function valueHasBeenSet($field, &$element, &$values) {
        if (is_array($field)) {
            foreach ($field as $field_) {
                if ($this->valueHasBeenSet($field_, $element, $values)) {
                    return true;
                }
            }

            return false;
        }

        return array_key_exists($field, $values) && (!$element || !$element[$this->name][$field]) && $values[$field];
    }

    protected function valueHasChanged($field, &$element, &$values, $ignoreFields = array()) {
        if (is_array($field)) {
            foreach ($field as $field_) {
                if (in_array($field_, $ignoreFields)) {
                    continue;
                }

                if ($this->valueHasChanged($field_, $element, $values)) {
                    return true;
                }
            }

            return false;
        }

        $current_value = (isset($values[$field]) ? $values[$field] : null);
        $previous_value = (isset($element[$this->name][$field]) ? $element[$this->name][$field] : null);

        if (is_array($current_value)) {
            $current_value = json_encode($current_value);
        }

        if (is_array($previous_value)) {
            $previous_value = json_encode($previous_value);
        }

        return array_key_exists($field, $values) && strval($current_value) != strval($previous_value);
    }

    protected function currentValue($field, &$element, &$values) {
        return array_key_exists($field, $values) ? $values[$field] : ($element && isset($element[$this->name][$field]) ? $element[$this->name][$field] : null);
    }

    protected function transformDates($dates, &$values) {
        if (!is_array($dates)) {
            $dates = array($dates);
        }

        foreach ($dates as $date) {
            if (array_key_exists($date, $values)) {
                $values[$date] = date2php($values[$date]);
            }
        }
    }

    public function deleteAll($options) {
        // Retrieve elements
        $elements = $this->getAll($options);

        // For nested calls transactions can be disabled
        $options['transactional'] = (!isset($options['transactional']) || $options['transactional']);

        // Begin transaction if required
        if ($options['transactional']) {
            $this->beginTransaction();
        }

        // No errors so far
        $errors = false;

        // Foreach element, try to delete
        foreach ($elements as $element) {
            if (!$this->deleteById($element[$this->name]['id'])) {
                $errors = true;
            }

            // Stop if error found
            if ($errors) {
                break;
            }
        }

        // If transactional, commit if no errors or rollback
        if ($options['transactional']) {
            if (!$errors) {
                $this->commitTransaction();
            } else {
                $this->rollbackTransaction();
            }
        }

        // Return true (success) if no errors or false
        return !$errors;
    }

    public function hideAll($options) {
        // Retrieve elements
        $elements = $this->getAll($options);

        // For nested calls transactions can be disabled
        $options['transactional'] = (!isset($options['transactional']) || $options['transactional']);

        // Begin transaction if required
        if ($options['transactional']) {
            $this->beginTransaction();
        }

        // No errors so far
        $errors = false;

        // Foreach element, try to hide
        foreach ($elements as $element) {
            if (!$this->hideById($element[$this->name]['id'])) {
                $errors = true;
            }

            // Stop if error found
            if ($errors) {
                break;
            }
        }

        // If transactional, commit if no errors or rollback
        if ($options['transactional']) {
            if (!$errors) {
                $this->commitTransaction();
            } else {
                $this->rollbackTransaction();
            }
        }

        // Return true (success) if no errors or false
        return !$errors;
    }

    /**
     * Gets and updates all records.
     *
     * @param $getOptions array options for search
     * @param $values array values to update
     * @param $options array options for save
     *
     * @return bool success
     */
    public function updateAll($getOptions, $values, $options = array()) {
        unset($getOptions['hash_by']);
        unset($getOptions['list_by']);
        $getOptions['recursive'] = -1;
        $getOptions['fields'] = array('id');

        // Retrieve elements
        $elements = $this->getAll($getOptions);

        // For nested calls transactions can be disabled
        $options['transactional'] = (!isset($options['transactional']) || $options['transactional']);

        // Begin transaction if required
        if ($options['transactional']) {
            $this->beginTransaction();
        }

        // No errors so far
        $errors = false;

        // Foreach element, try to update
        foreach ($elements as $element) {
            if (!$this->updateFields($element[$this->name]['id'], $values, $options)) {
                $errors = true;
            }

            // Stop if error found
            if ($errors && (empty($options['continue_on_error']) || $options['transactional'])) {
                break;
            }
        }

        // If transactional, commit if no errors or rollback
        if ($options['transactional']) {
            if (!$errors) {
                $this->commitTransaction();
            } else {
                $this->rollbackTransaction();
            }
        }

        // Return true (success) if no errors or false
        return !$errors;
    }
}