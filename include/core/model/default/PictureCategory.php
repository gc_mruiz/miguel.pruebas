<?php

/**
 * Class PictureCategory
 *
 * Model class for MySQL table picture_categories
 */
class PictureCategory extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'PictureCategory';

    /**
     * @see MySQLModel::$displayFields
     * @var array
     */
    public $displayFields = array('name',);

    /**
     * @see MySQLModel::$singleUpdateFields
     * @var array
     */
    public $singleUpdateFields = array('name',);

    /**
     * @see MySQLModel::$validOrderByExpressions
     * @var array
     */
    public $validOrderByExpressions = array(
        'id',
        'name',
        'created',
        'modified',
    );

    /**
     * @see MySQLModel::$validation
     * @var array
     */
    public $validation = array(
        'name' => array(
            'required' => 'Name is required.',
            'notempty' => 'Name cannot be left empty.',
        ),
    );

    /**
     * Add new override. Looks for a equivalent before adding.
     *
     * @param array $values
     *
     * @return bool|array
     */
    public function addNew($values) {
        if (isset($values['name'])) {
            $element = $this->get(array('where' => array('name LIKE' => $values['name'])));

            if ($element) {
                return $element;
            }
        }

        return parent::addNew($values);
    }

    /**
     * Before delete callback.
     *
     * @param $element array element to be deleted.
     *
     * @return bool if not true, stops deleting
     */
    protected function beforeDelete($element, $options) {
        // Check if there is at least one image in the category
        /* @var Picture $Picture */
        $Picture = MySQLModel::getInstance('Picture');
        $category_has_images = $Picture->count(array('where' => array('picture_category_id' => $element[$this->name]['id']))) > 0;

        // Abort deletion if there are images
        if ($category_has_images) {
            $this->errors[] = __('You cannot delete a non-empty category.', true);

            return false;
        }

        // Continue
        return parent::beforeDelete($element, $options);
    }
}
