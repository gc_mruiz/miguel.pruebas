<?php

/**
 * Class Picture
 *
 * Model for table pictures.
 */
class Picture extends AppMySQLModel {
    /**
     * Model name.
     *
     * @var string
     */
    public $name = 'Picture';

    /**
     * @see MySQLModel::$displayFields
     * @var array
     */
    public $displayFields = array('src');

    /**
     * @see MySQLModel::$belongsTo
     * @var array
     */
    public $belongsTo = array(
        'PictureCategory' => array(
            'model' => 'PictureCategory',
            'foreign_key' => 'picture_category_id',
        ),
    );

    /**
     * @see MySQLModel::initialize
     */
    function initialize() {
        // Call parent
        parent::initialize();

        // Default order `created` DESC
        $this->order = $this->full_table_name . '.`created` DESC';
    }

    /**
     * Before validate callback. Does some data
     * generation:
     *
     * - thumb tokens
     *
     * @param $element array original element
     * @param $values array new values
     * @param $options
     *
     * @return boolean continue with validation
     */
    function beforeValidate($element, &$values, $options) {
        if (!parent::beforeValidate($element, $values, $options)) {
            return false;
        }

        if (!$element) {
            $values['320_token'] = random_token(false);
            $values['640_token'] = random_token(false);
        }

        return true;
    }

    /**
     * @see MySQLModel::getDummyRecord
     * @return array
     */
    function getDummyRecord() {
        return array(
            'Picture' => array(
                'id' => 0,
                'width' => 100,
                'height' => 100,
                'src' => Router::url('/static/img/dummy.png', array('no_session' => true)),
                'alt' => '',
                'title' => '',
                'picture_category_id' => '1',
                '640_token' => null,
                '320_token' => null,
            ),
        );
    }

    /**
     * Mime types recognized as images
     *
     * @var array
     */
    public $imageMimeTypes = array(
        'image/bmp',
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/x-png',
        'image/x-citrix-png',
        'image/x-citrix-jpeg',
        'image/pjpeg',
    );

    /**
     * Mime types - extensions pairs
     *
     * @var array
     */
    public $mimeTypesByExtension = array(
        '323' => 'text/h323',
        'acx' => 'application/internet-property-stream',
        'ai' => 'application/postscript',
        'aif' => 'audio/x-aiff',
        'aifc' => 'audio/x-aiff',
        'aiff' => 'audio/x-aiff',
        'asf' => 'video/x-ms-asf',
        'asr' => 'video/x-ms-asf',
        'asx' => 'video/x-ms-asf',
        'au' => 'audio/basic',
        'avi' => 'video/x-msvideo',
        'axs' => 'application/olescript',
        'bas' => 'text/plain',
        'bcpio' => 'application/x-bcpio',
        'bin' => 'application/octet-stream',
        'bmp' => 'image/bmp',
        'c' => 'text/plain',
        'cat' => 'application/vnd.ms-pkiseccat',
        'cdf' => 'application/x-cdf',
        'cer' => 'application/x-x509-ca-cert',
        'class' => 'application/octet-stream',
        'clp' => 'application/x-msclip',
        'cmx' => 'image/x-cmx',
        'cod' => 'image/cis-cod',
        'cpio' => 'application/x-cpio',
        'crd' => 'application/x-mscardfile',
        'crl' => 'application/pkix-crl',
        'crt' => 'application/x-x509-ca-cert',
        'csh' => 'application/x-csh',
        'css' => 'text/css',
        'dcr' => 'application/x-director',
        'der' => 'application/x-x509-ca-cert',
        'dir' => 'application/x-director',
        'dll' => 'application/x-msdownload',
        'dms' => 'application/octet-stream',
        'doc' => 'application/msword',
        'dot' => 'application/msword',
        'dvi' => 'application/x-dvi',
        'dxr' => 'application/x-director',
        'eps' => 'application/postscript',
        'etx' => 'text/x-setext',
        'evy' => 'application/envoy',
        'exe' => 'application/octet-stream',
        'fif' => 'application/fractals',
        'flr' => 'x-world/x-vrml',
        'gif' => 'image/gif',
        'gtar' => 'application/x-gtar',
        'gz' => 'application/x-gzip',
        'h' => 'text/plain',
        'hdf' => 'application/x-hdf',
        'hlp' => 'application/winhlp',
        'hqx' => 'application/mac-binhex40',
        'hta' => 'application/hta',
        'htc' => 'text/x-component',
        'htm' => 'text/html',
        'html' => 'text/html',
        'htt' => 'text/webviewhtml',
        'ico' => 'image/x-icon',
        'ief' => 'image/ief',
        'iii' => 'application/x-iphone',
        'ins' => 'application/x-internet-signup',
        'isp' => 'application/x-internet-signup',
        'jfif' => 'image/pipeg',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpe' => 'image/jpeg',
        'js' => 'application/x-javascript',
        'latex' => 'application/x-latex',
        'lha' => 'application/octet-stream',
        'lsf' => 'video/x-la-asf',
        'lsx' => 'video/x-la-asf',
        'lzh' => 'application/octet-stream',
        'm13' => 'application/x-msmediaview',
        'm14' => 'application/x-msmediaview',
        'm3u' => 'audio/x-mpegurl',
        'man' => 'application/x-troff-man',
        'mdb' => 'application/x-msaccess',
        'me' => 'application/x-troff-me',
        'mht' => 'message/rfc822',
        'mhtml' => 'message/rfc822',
        'mid' => 'audio/mid',
        'mny' => 'application/x-msmoney',
        'mov' => 'video/quicktime',
        'movie' => 'video/x-sgi-movie',
        'mp2' => 'video/mpeg',
        'mp3' => 'audio/mpeg',
        'mpa' => 'video/mpeg',
        'mpe' => 'video/mpeg',
        'mpeg' => 'video/mpeg',
        'mpg' => 'video/mpeg',
        'mpp' => 'application/vnd.ms-gestymvcProject',
        'mpv2' => 'video/mpeg',
        'ms' => 'application/x-troff-ms',
        'mvb' => 'application/x-msmediaview',
        'nws' => 'message/rfc822',
        'oda' => 'application/oda',
        'p10' => 'application/pkcs10',
        'p12' => 'application/x-pkcs12',
        'p7b' => 'application/x-pkcs7-certificates',
        'p7c' => 'application/x-pkcs7-mime',
        'p7m' => 'application/x-pkcs7-mime',
        'p7r' => 'application/x-pkcs7-certreqresp',
        'p7s' => 'application/x-pkcs7-signature',
        'pbm' => 'image/x-portable-bitmap',
        'pdf' => 'application/pdf',
        'pfx' => 'application/x-pkcs12',
        'pgm' => 'image/x-portable-graymap',
        'pko' => 'application/ynd.ms-pkipko',
        'pma' => 'application/x-perfmon',
        'pmc' => 'application/x-perfmon',
        'pml' => 'application/x-perfmon',
        'pmr' => 'application/x-perfmon',
        'pmw' => 'application/x-perfmon',
        'png' => 'image/png',
        'pnm' => 'image/x-portable-anymap',
        'pot' => 'application/vnd.ms-powerpoint',
        'ppm' => 'image/x-portable-pixmap',
        'pps' => 'application/vnd.ms-powerpoint',
        'ppt' => 'application/vnd.ms-powerpoint',
        'prf' => 'application/pics-rules',
        'ps' => 'application/postscript',
        'pub' => 'application/x-mspublisher',
        'qt' => 'video/quicktime',
        'ra' => 'audio/x-pn-realaudio',
        'ram' => 'audio/x-pn-realaudio',
        'rar' => 'application/x-rar-compressed',
        'ras' => 'image/x-cmu-raster',
        'rgb' => 'image/x-rgb',
        'rmi' => 'audio/mid',
        'roff' => 'application/x-troff',
        'rtf' => 'application/rtf',
        'rtx' => 'text/richtext',
        'scd' => 'application/x-msschedule',
        'sct' => 'text/scriptlet',
        'setpay' => 'application/set-payment-initiation',
        'setreg' => 'application/set-registration-initiation',
        'sh' => 'application/x-sh',
        'shar' => 'application/x-shar',
        'sit' => 'application/x-stuffit',
        'snd' => 'audio/basic',
        'spc' => 'application/x-pkcs7-certificates',
        'spl' => 'application/futuresplash',
        'src' => 'application/x-wais-source',
        'sst' => 'application/vnd.ms-pkicertstore',
        'stl' => 'application/vnd.ms-pkistl',
        'stm' => 'text/html',
        'svg' => 'image/svg+xml',
        'sv4cpio' => 'application/x-sv4cpio',
        'sv4crc' => 'application/x-sv4crc',
        't' => 'application/x-troff',
        'tar' => 'application/x-tar',
        'tcl' => 'application/x-tcl',
        'tex' => 'application/x-tex',
        'texi' => 'application/x-texinfo',
        'texinfo' => 'application/x-texinfo',
        'tgz' => 'application/x-compressed',
        'tif' => 'image/tiff',
        'tiff' => 'image/tiff',
        'tr' => 'application/x-troff',
        'trm' => 'application/x-msterminal',
        'tsv' => 'text/tab-separated-values',
        'txt' => 'text/plain',
        'uls' => 'text/iuls',
        'ustar' => 'application/x-ustar',
        'vcf' => 'text/x-vcard',
        'vrml' => 'x-world/x-vrml',
        'wav' => 'audio/x-wav',
        'wcm' => 'application/vnd.ms-works',
        'wdb' => 'application/vnd.ms-works',
        'wks' => 'application/vnd.ms-works',
        'wmf' => 'application/x-msmetafile',
        'wps' => 'application/vnd.ms-works',
        'wri' => 'application/x-mswrite',
        'wrl' => 'x-world/x-vrml',
        'wrz' => 'x-world/x-vrml',
        'xaf' => 'x-world/x-vrml',
        'xbm' => 'image/x-xbitmap',
        'xla' => 'application/vnd.ms-excel',
        'xlc' => 'application/vnd.ms-excel',
        'xlm' => 'application/vnd.ms-excel',
        'xls' => 'application/vnd.ms-excel',
        'xlt' => 'application/vnd.ms-excel',
        'xlw' => 'application/vnd.ms-excel',
        'xof' => 'x-world/x-vrml',
        'xpm' => 'image/x-xpixmap',
        'xwd' => 'image/x-xwindowdump',
        'z' => 'application/x-compress',
        'zip' => 'application/zip',
    );

    /**
     * Cleans extension.
     * Lo lower case, without URL parameters
     * and trimmed
     *
     * @param $extension to clean
     *
     * @return string cleaned extension
     */
    function cleanExtension($extension) {
        // Change to lower case
        $extension = mb_strtolower($extension);

        // If there are any url parameters
        // take them out
        $extension = explode('?', $extension);
        $extension = $extension[0];

        // Trim spaces and tabs
        $extension = trim($extension);

        // Return resultant extension string
        return $extension;
    }

    /**
     * Retrieves the related mime type of an extension
     *
     * @param $extension string to search
     *
     * @return string mime type
     */
    function getFileMimeTypeByExtension($extension) {
        // Clean extension
        $extension = $this->cleanExtension($extension);

        // Checks if the extension is in the mime type by
        // extension array
        if (isset($this->mimeTypesByExtension[$extension])) {
            // If it is, return the related mime type
            return $this->mimeTypesByExtension[$extension];
        }

        // In any other case return empty string
        return '';
    }

    /**
     * Retrieves the related extension of a mime type
     *
     * @param $mime_type string mime type to search
     *
     * @return string $extension
     */
    function getFileExtensionByMimeType($mime_type) {
        if ($mime_type == 'image/x-png' || $mime_type == 'image/x-citrix-png') {
            $mime_type = 'image/png';
        }
        if ($mime_type == 'image/x-citrix-jpeg' || $mime_type == 'image/pjpeg') {
            $mime_type = 'image/jpeg';
        }

        // Search for key in the mime type by
        // extension array
        $key = array_search($mime_type, $this->mimeTypesByExtension);

        // Check if the $key exists
        if ($key !== false) {
            // If it does, then the extension is the key
            $extension = $key;

            // Return found extension
            return $extension;
        }

        // If the key does not exist, return
        // empty string
        return '';
    }

    /**
     * Save picture file.
     *
     * @param $alt
     * @param $fileStream
     * @param $scale
     * @param $picture_category_id
     * @param $options
     *
     * @return bool success
     */
    function saveFile($alt, $fileStream, $scale = null, $picture_category_id = 1, $options = array()) {
        // If not picture_category_id provided, set default
        if (!$picture_category_id) {
            $picture_category_id = 1;
        }

        // Extend memory limit
        ini_set('memory_limit', '256M');

        // Check that file has been successfully uploaded
        if (is_uploaded_file($fileStream['tmp_name']) || (file_exists($fileStream['tmp_name']) && substr($fileStream['tmp_name'], 0, 2) == '..')) {
            // Store file values
            $file_location = $fileStream['tmp_name'];

            // Generate extension from mime type or mime
            // type from extension in case one of them does not
            // exit
            if (isset($fileStream['type']) && $fileStream['type'] != 'application/octet-stream') {
                $file_stream_type = $fileStream['type'];
                $extension = $this->getFileExtensionByMimeType($file_stream_type);
            } else {
                $extension = $fileStream['extension'];
                $file_stream_type = $this->getFileMimeTypeByExtension($extension);
            }

            // Check if current file is an image
            $file_stream_is_image = (in_array($file_stream_type, $this->imageMimeTypes));

            if (!$file_stream_is_image || !$extension) {
                @unlink($file_location);

                return false;
            }

            // Generate filename
            $new_temp_base_name = clean_for_url(date('YmdHis') . '-' . $fileStream['name']);
            $new_temp_base_name = explode('.', $new_temp_base_name);
            $new_temp_base_name = $new_temp_base_name[0];
            $new_temp_file_name = ROOT_PATH . 'content/img/temp/' . $new_temp_base_name . '.' . $extension;

            // Try to copy content to new destination
            if (copy($file_location, $new_temp_file_name)) {
                // If succeeded replace filename destroy previous
                // file path and replace it
                @unlink($file_location);
                $file_location = $new_temp_file_name;
            } else {
                // If failed, return false
                @unlink($file_location);

                return false;
            }

            // Resize
            $resize = null;
            $max_height = null;
            $max_width = null;
            $fill_square = null;

            if (isset($scale) && $scale) {
                switch ($scale) {
                    case '2K':
                        $resize = true;
                        $max_width = 3840;
                        $max_height = 2160;
                        $fill_square = false;

                        break;
                    case 'FullHD':
                        $resize = true;
                        $max_width = 1920;
                        $max_height = 1080;
                        $fill_square = false;

                        break;
                    case 'HD':
                        $resize = true;
                        $max_width = 1280;
                        $max_height = 720;
                        $fill_square = false;

                        break;
                    case 'SD':
                        $resize = true;
                        $max_width = 860;
                        $max_height = 480;
                        $fill_square = false;

                        break;
                    default:
                        $resize = null;
                        $fill_square = false;

                        break;
                }

                $resized_file_location = str_replace(ROOT_PATH . 'content/img/temp/', ROOT_PATH . 'content/img/', $file_location);

                if ($resize) {
                    $imageSize = getimagesize($file_location);
                    $image_width = $imageSize[0];
                    $image_height = $imageSize[1];

                    $new_image_height = null;
                    $new_image_width = null;

                    if ($image_width / $image_height * $max_height > $max_width) {
                        $new_image_width = $max_width;
                        $new_image_height = $image_height / $image_width * $new_image_width;
                    } else {
                        $new_image_height = $max_height;
                        $new_image_width = $image_width / $image_height * $new_image_height;
                    }

                    if ($this->imageResize($file_location, $resized_file_location, $new_image_width, $new_image_height, 100, $fill_square)) {
                        unlink($file_location);
                        $file_location = $resized_file_location;
                    } else {
                        // If failed, return false
                        @unlink($file_location);

                        return false;
                    }
                } else {
                    // Try to copy content to new destination
                    if (copy($file_location, $resized_file_location)) {
                        // If succeeded replace filename destroy previous
                        // file path and replace it
                        @unlink($file_location);
                        $file_location = $resized_file_location;
                    } else {
                        // If failed, return false
                        @unlink($file_location);

                        return false;
                    }
                }
            }

            // Generate file URL
            $file_url = Router::url('/' . substr($file_location, strlen(ROOT_PATH)), array('no_session' => true));

            // Get width and height
            $imageSize = getimagesize($file_location);

            $addPictureResult = $this->addNew(array(
                                                  'alt' => $alt,
                                                  'path' => $file_location,
                                                  'src' => $file_url,
                                                  'width' => $imageSize[0],
                                                  'height' => $imageSize[1],
                                                  'mime_type' => $file_stream_type,
                                                  'picture_category_id' => $picture_category_id,
                                              ), $options);

            if (!$addPictureResult) {
                @unlink($file_location);

                return false;
            }

            return $addPictureResult;
        }

        return false;
    }

    /**
     * Resizes an image and saves it to a file
     *
     * @param $original_filename
     * @param $resized_filename
     * @param $new_width
     * @param $new_height
     * @param $quality
     * @param $fill_square_width
     *
     * @return boolean success
     */
    function imageResize($original_filename, $resized_filename, $new_width = 0, $new_height = 0, $quality = 100, $fill_square_width = null) {
        $this->errors = array();

        // If no width or not height was set, return false
        if (!$new_width || !$new_height) {
            return false;
        }

        // Check if source file is a valid image file
        if (!($imageInfo = getimagesize($original_filename))) {
            // If not, add error and return false
            $this->errors[] = sprintf(__('Original file is not a valid image: %s', true), $original_filename);

            return false;
        }

        // Store dimensions
        $width = $imageInfo[0];
        $height = $imageInfo[1];

        // Resize image based on image type
        $image_type = $imageInfo[2];

        // Retrieve image from file
        $gif = $png = $jpeg = $bmp = false;
        if ($image_type == IMAGETYPE_GIF) {
            $gif = true;
            $originalImageResource = imagecreatefromgif($original_filename);
        } elseif ($image_type == IMAGETYPE_PNG) {
            $png = true;
            $originalImageResource = imagecreatefrompng($original_filename);
        } elseif ($image_type == IMAGETYPE_JPEG) {
            $jpeg = true;
            $originalImageResource = imagecreatefromjpeg($original_filename);
        } elseif ($image_type == IMAGETYPE_BMP) {
            $bmp = true;
            $originalImageResource = imagecreatefrombmp($original_filename);
        }

        // Check for valid resource
        if (!$originalImageResource) {
            $this->errors[] = __('There was an error loading the image.', true);
        }

        // Generate new resource for resized image
        if (!($resizedImageResource = imagecreatetruecolor(($fill_square_width ? $fill_square_width : $new_width), ($fill_square_width ? $fill_square_width : $new_height)))) {
            $this->errors[] = __('There was an error creating true color image.', true);
        }

        // Alpha values for PNG
        if ($png) {
            imagealphablending($resizedImageResource, false);
        }

        // Resample image in new size
        if (!imagecopyresampled($resizedImageResource, $originalImageResource, ($fill_square_width ? ($fill_square_width + $new_width) / 2 - $new_width : 0), ($fill_square_width ? ($fill_square_width + $new_height) / 2 - $new_height : 0), 0, 0, $new_width, $new_height, $width, $height)) {
            $this->errors[] = __('There was an error resampling image.', true);
        }

        // Alpha values for PNG
        imagesavealpha($resizedImageResource, true);

        // If asked to filled
        if ($fill_square_width) {
            // Retrieve white color instance
            $white = imagecolorallocate($resizedImageResource, 255, 255, 255);

            // Fill with white color
            if ($new_width > $new_height) {
                imagefilledrectangle($resizedImageResource, 0, 0, $fill_square_width, ($fill_square_width + $new_height) / 2 - $new_height, $white);
                imagefilledrectangle($resizedImageResource, 0, ($fill_square_width + $new_height) / 2, $fill_square_width, $fill_square_width, $white);
            } else {
                imagefilledrectangle($resizedImageResource, 0, 0, ($fill_square_width + $new_width) / 2 - $new_width, $fill_square_width, $white);
                imagefilledrectangle($resizedImageResource, ($fill_square_width + $new_width) / 2, 0, $fill_square_width, $fill_square_width, $white);
            }
        }

        if ($png) {
            // For PNG type, revert quality value
            $quality = ceil($quality / 10);

            if ($quality == 0) {
                $quality = 9;
            } else {
                $quality = ($quality - 1) % 9;
            }
        }

        // Save image into file
        if ($gif) {
            $newImage = imagegif($resizedImageResource, $resized_filename);
        } elseif ($png) {
            $newImage = imagepng($resizedImageResource, $resized_filename, $quality);
        } elseif ($jpeg) {
            $newImage = imagejpeg($resizedImageResource, $resized_filename, $quality);
        } elseif ($bmp) {
            $newImage = imagejpeg($resizedImageResource, $resized_filename, $quality);
        }

        if (!$newImage) {
            // If the new image was not saved into file, add
            // error
            $this->errors[] = __('There was an error writing resized image to file.', true);
        }

        // Destroy $resizedImageResource image
        imagedestroy($resizedImageResource);

        // If no erros where found, return true
        return count($this->errors) == 0;
    }

    /**
     * @see MySQLModel::afterDelete
     */
    function afterDelete($element, $options) {
        // Call parent
        parent::afterDelete($element, $options);

        // Delete file
        @unlink($element[$this->name]['path']);
    }

    /**
     * Retrieves the thumb static url for the picture. Generates the thumb if it does not exist
     *
     * @param $id int of the picture
     * @param $thumb_width int thumb size
     * @param $token string the security token
     *
     * @return bool|string false if failed, string if url
     */
    function getThumbUrl($id, $thumb_width, $token) {
        // Retrieve the picture information
        $picture = $this->getById($id, array(
            'recursive' => -1,
            'where' => array($thumb_width . '_token' => $token),
        ));

        // Check result
        if ($picture) {
            // Extract basename and extension from the source field
            $basename = basename($picture['Picture']['src']);
            $extension = explode('.', $basename);
            $extension = strtolower($extension[sizeof($extension) - 1]);

            // Tiff files require special attention
            $is_tiff = ($extension == 'tif' || $extension == 'tiff');

            // Tiff thumbs will be returned as jpg
            if ($is_tiff) {
                $extension = 'jpg';
            }

            // Generate a cryptographic name for the thumb
            $thumb_name = sha1($picture['Picture']['id'] . '/' . $thumb_width . $basename) . '.' . $extension;
            $thumb_temp_name = sha1($picture['Picture']['id'] . '/' . $thumb_width . $basename) . '.temp.' . $extension;

            // Check if file already exists
            if (!file_exists(ROOT_PATH . 'content/img/' . $thumb_width . '/' . $thumb_name)) {
                if (!file_exists($picture['Picture']['path'])) {
                    $picture['Picture']['path'] = $picture['Picture']['src'];
                }

                // If not, thumb need to be generated. Extract the picture file
                // physical path
                $original_file_path = $picture['Picture']['path'];
                $original_image_valid = false;

                // Tiff files need to be transformed into jpg before starting
                if ($is_tiff) {
                    // This can only be done with imagick
                    if (class_exists('Imagick')) {
                        // Transform image
                        $Imagick = new Imagick($original_file_path);
                        $Imagick->setImageFormat('jpeg');

                        // Store transformed image
                        if ($Imagick->writeImage($thumb_temp_name)) {
                            // Switch paths
                            $original_file_path = $thumb_temp_name;
                            $original_image_valid = true;
                        }
                    }

                    // If file could not be processed, try to extract the thumb from the headers
                    if (!$original_image_valid) {
                        if (($jpeg_string_image = exif_thumbnail($original_file_path)) === false) {
                            // No thumbnail!
                        } else {
                            // Extract the thumb
                            $jpeg_string_image = imagecreatefromstring($jpeg_string_image);

                            // Use header's thumb as original image, store
                            if ($jpeg_string_image) {
                                $original_image_valid = imagejpeg($jpeg_string_image, $thumb_temp_name, 100);
                            }

                            // Switch paths
                            $original_file_path = $thumb_temp_name;
                        }
                    }
                } else {
                    // Other file types are accepted directly
                    $original_image_valid = true;
                }

                // Resize image if valid and store it on the thumb path
                if ($original_image_valid) {
                    $this->imageResize($original_file_path, ROOT_PATH . 'content/img/' . $thumb_width . '/' . $thumb_name, $thumb_width, $picture['Picture']['height'] / $picture['Picture']['width'] * $thumb_width, 100, false);
                }

                // Clean temp files
                if (file_exists($thumb_temp_name)) {
                    @unlink($thumb_temp_name);
                }
            }

            // If file exists, return the absolute URL to the file
            if (file_exists(ROOT_PATH . 'content/img/' . $thumb_width . '/' . $thumb_name)) {
                return Router::url('/content/img/' . $thumb_width . '/' . $thumb_name, array('no_session' => true));
            }
        }

        // No file, error
        return false;
    }

}
