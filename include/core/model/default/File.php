<?php

/**
 * Class File
 *
 * Uploaded files model.
 */
class File extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'File';

    /**
     * Mime types recognized as images
     *
     * @var array
     */
    public $validMimeTypes = array(
        'application/pdf',
        'application/msword',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
        'application/vnd.openxmlformats-officedocument.presentationml.template',
        'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
        'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'image/bmp',
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/x-png',
        'image/x-citrix-png',
        'image/x-citrix-jpeg',
        'image/pjpeg',
    );

    /**
     * Mime types - extensions pairs
     *
     * @var array
     */
    public $mimeTypesByExtension = array(
        '323' => 'text/h323',
        'acx' => 'application/internet-property-stream',
        'ai' => 'application/postscript',
        'aif' => 'audio/x-aiff',
        'aifc' => 'audio/x-aiff',
        'aiff' => 'audio/x-aiff',
        'asf' => 'video/x-ms-asf',
        'asr' => 'video/x-ms-asf',
        'asx' => 'video/x-ms-asf',
        'au' => 'audio/basic',
        'avi' => 'video/x-msvideo',
        'axs' => 'application/olescript',
        'bas' => 'text/plain',
        'bcpio' => 'application/x-bcpio',
        'bin' => 'application/octet-stream',
        'bmp' => 'image/bmp',
        'c' => 'text/plain',
        'cat' => 'application/vnd.ms-pkiseccat',
        'cdf' => 'application/x-cdf',
        'cer' => 'application/x-x509-ca-cert',
        'class' => 'application/octet-stream',
        'clp' => 'application/x-msclip',
        'cmx' => 'image/x-cmx',
        'cod' => 'image/cis-cod',
        'cpio' => 'application/x-cpio',
        'crd' => 'application/x-mscardfile',
        'crl' => 'application/pkix-crl',
        'crt' => 'application/x-x509-ca-cert',
        'csh' => 'application/x-csh',
        'css' => 'text/css',
        'dcr' => 'application/x-director',
        'der' => 'application/x-x509-ca-cert',
        'dir' => 'application/x-director',
        'dll' => 'application/x-msdownload',
        'dms' => 'application/octet-stream',
        'doc' => 'application/msword',
        'dot' => 'application/msword',
        'dvi' => 'application/x-dvi',
        'dxr' => 'application/x-director',
        'eps' => 'application/postscript',
        'etx' => 'text/x-setext',
        'evy' => 'application/envoy',
        'exe' => 'application/octet-stream',
        'fif' => 'application/fractals',
        'flr' => 'x-world/x-vrml',
        'gif' => 'image/gif',
        'gtar' => 'application/x-gtar',
        'gz' => 'application/x-gzip',
        'h' => 'text/plain',
        'hdf' => 'application/x-hdf',
        'hlp' => 'application/winhlp',
        'hqx' => 'application/mac-binhex40',
        'hta' => 'application/hta',
        'htc' => 'text/x-component',
        'htm' => 'text/html',
        'html' => 'text/html',
        'htt' => 'text/webviewhtml',
        'ico' => 'image/x-icon',
        'ief' => 'image/ief',
        'iii' => 'application/x-iphone',
        'ins' => 'application/x-internet-signup',
        'isp' => 'application/x-internet-signup',
        'jfif' => 'image/pipeg',
        'jpg' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpe' => 'image/jpeg',
        'js' => 'application/x-javascript',
        'latex' => 'application/x-latex',
        'lha' => 'application/octet-stream',
        'lsf' => 'video/x-la-asf',
        'lsx' => 'video/x-la-asf',
        'lzh' => 'application/octet-stream',
        'm13' => 'application/x-msmediaview',
        'm14' => 'application/x-msmediaview',
        'm3u' => 'audio/x-mpegurl',
        'man' => 'application/x-troff-man',
        'mdb' => 'application/x-msaccess',
        'me' => 'application/x-troff-me',
        'mht' => 'message/rfc822',
        'mhtml' => 'message/rfc822',
        'mid' => 'audio/mid',
        'mny' => 'application/x-msmoney',
        'mov' => 'video/quicktime',
        'movie' => 'video/x-sgi-movie',
        'mp2' => 'video/mpeg',
        'mp3' => 'audio/mpeg',
        'mpa' => 'video/mpeg',
        'mpe' => 'video/mpeg',
        'mpeg' => 'video/mpeg',
        'mpg' => 'video/mpeg',
        'mpp' => 'application/vnd.ms-project',
        'mpv2' => 'video/mpeg',
        'ms' => 'application/x-troff-ms',
        'mvb' => 'application/x-msmediaview',
        'nws' => 'message/rfc822',
        'oda' => 'application/oda',
        'p10' => 'application/pkcs10',
        'p12' => 'application/x-pkcs12',
        'p7b' => 'application/x-pkcs7-certificates',
        'p7c' => 'application/x-pkcs7-mime',
        'p7m' => 'application/x-pkcs7-mime',
        'p7r' => 'application/x-pkcs7-certreqresp',
        'p7s' => 'application/x-pkcs7-signature',
        'pbm' => 'image/x-portable-bitmap',
        'pdf' => 'application/pdf',
        'pfx' => 'application/x-pkcs12',
        'pgm' => 'image/x-portable-graymap',
        'pko' => 'application/ynd.ms-pkipko',
        'pma' => 'application/x-perfmon',
        'pmc' => 'application/x-perfmon',
        'pml' => 'application/x-perfmon',
        'pmr' => 'application/x-perfmon',
        'pmw' => 'application/x-perfmon',
        'png' => 'image/png',
        'pnm' => 'image/x-portable-anymap',
        'pot' => 'application/vnd.ms-powerpoint',
        'ppm' => 'image/x-portable-pixmap',
        'pps' => 'application/vnd.ms-powerpoint',
        'ppt' => 'application/vnd.ms-powerpoint',
        'prf' => 'application/pics-rules',
        'ps' => 'application/postscript',
        'pub' => 'application/x-mspublisher',
        'qt' => 'video/quicktime',
        'ra' => 'audio/x-pn-realaudio',
        'ram' => 'audio/x-pn-realaudio',
        'rar' => 'application/x-rar-compressed',
        'ras' => 'image/x-cmu-raster',
        'rgb' => 'image/x-rgb',
        'rmi' => 'audio/mid',
        'roff' => 'application/x-troff',
        'rtf' => 'application/rtf',
        'rtx' => 'text/richtext',
        'scd' => 'application/x-msschedule',
        'sct' => 'text/scriptlet',
        'setpay' => 'application/set-payment-initiation',
        'setreg' => 'application/set-registration-initiation',
        'sh' => 'application/x-sh',
        'shar' => 'application/x-shar',
        'sit' => 'application/x-stuffit',
        'snd' => 'audio/basic',
        'spc' => 'application/x-pkcs7-certificates',
        'spl' => 'application/futuresplash',
        'src' => 'application/x-wais-source',
        'sst' => 'application/vnd.ms-pkicertstore',
        'stl' => 'application/vnd.ms-pkistl',
        'stm' => 'text/html',
        'svg' => 'image/svg+xml',
        'sv4cpio' => 'application/x-sv4cpio',
        'sv4crc' => 'application/x-sv4crc',
        't' => 'application/x-troff',
        'tar' => 'application/x-tar',
        'tcl' => 'application/x-tcl',
        'tex' => 'application/x-tex',
        'texi' => 'application/x-texinfo',
        'texinfo' => 'application/x-texinfo',
        'tgz' => 'application/x-compressed',
        'tif' => 'image/tiff',
        'tiff' => 'image/tiff',
        'tr' => 'application/x-troff',
        'trm' => 'application/x-msterminal',
        'tsv' => 'text/tab-separated-values',
        'txt' => 'text/plain',
        'uls' => 'text/iuls',
        'ustar' => 'application/x-ustar',
        'vcf' => 'text/x-vcard',
        'vrml' => 'x-world/x-vrml',
        'wav' => 'audio/x-wav',
        'wcm' => 'application/vnd.ms-works',
        'wdb' => 'application/vnd.ms-works',
        'wks' => 'application/vnd.ms-works',
        'wmf' => 'application/x-msmetafile',
        'wps' => 'application/vnd.ms-works',
        'wri' => 'application/x-mswrite',
        'wrl' => 'x-world/x-vrml',
        'wrz' => 'x-world/x-vrml',
        'xaf' => 'x-world/x-vrml',
        'xbm' => 'image/x-xbitmap',
        'xla' => 'application/vnd.ms-excel',
        'xlc' => 'application/vnd.ms-excel',
        'xlm' => 'application/vnd.ms-excel',
        'xls' => 'application/vnd.ms-excel',
        'xlt' => 'application/vnd.ms-excel',
        'xlw' => 'application/vnd.ms-excel',
        'xof' => 'x-world/x-vrml',
        'xpm' => 'image/x-xpixmap',
        'xwd' => 'image/x-xwindowdump',
        'z' => 'application/x-compress',
        'zip' => 'application/zip',
        'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'xltx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
        'potx' => 'application/vnd.openxmlformats-officedocument.presentationml.template',
        'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
        'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
        'sldx' => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'dotx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
        'xlam' => 'application/vnd.ms-excel.addin.macroEnabled.12',
        'xlsb' => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
    );

    /**
     * Cleans extension.
     * Lo lower case, without URL parameters
     * and trimmed
     *
     * @param $extension string to clean
     *
     * @return string cleaned extension
     */
    function cleanExtension($extension) {
        // Change to lower case
        $extension = mb_strtolower($extension);

        // If there are any url parameters
        // take them out
        $extension = explode('?', $extension);
        $extension = $extension[0];

        // Trim spaces and tabs
        $extension = trim($extension);

        // Return resultant extension string
        return $extension;
    }

    /**
     * Retrieves the related mime type of an extension
     *
     * @param $extension string to search
     *
     * @return string mime type
     */
    function getFileMimeTypeByExtension($extension) {
        // Clean extension
        $extension = $this->cleanExtension($extension);

        // Checks if the extension is in the mime type by
        // extension array
        if (isset($this->mimeTypesByExtension[$extension])) {
            // If it is, return the related mime type
            return $this->mimeTypesByExtension[$extension];
        }

        // In any other case return empty string
        return '';
    }

    /**
     * Retrieves the related extension of a mime type
     *
     * @param $mimeType string to search
     *
     * @return string $extension
     */
    function getFileExtensionByMimeType($mimeType) {
        // Search for key in the mime type by
        // extension array
        $key = array_search($mimeType, $this->mimeTypesByExtension);

        // Check if the $key exists
        if ($key !== false) {
            // If it does, then the extension is the key
            $extension = $key;

            // Return found extension
            return $extension;
        }

        // If the key does not exist, return
        // empty string
        return '';
    }

    /**
     * Saves a file
     *
     * @param $alt
     * @param $fileStream
     *
     * @return bool
     */
    function saveFile($alt, $fileStream) {
        // Extend memory limit
        ini_set('memory_limit', '256M');

        // Check that file has been successfully uploaded
        if (is_uploaded_file($fileStream['tmp_name']) || (file_exists($fileStream['tmp_name']) && substr($fileStream['tmp_name'], 0, 2) == '..')) {
            // Store file values
            $file_location = $fileStream['tmp_name'];

            // Generate extension from mime type or mime
            // type from extension in case one of them does not
            // exit
            if (isset($fileStream['type']) && $fileStream['type'] != 'application/octet-stream') {
                $file_stream_type = $fileStream['type'];
                $extension = $this->getFileExtensionByMimeType($file_stream_type);
            } else {
                if (!isset($fileStream['extension'])) {
                    $extension = explode('.', $fileStream['name']);

                    if (sizeof($extension) < 2) {
                        return false;
                    }

                    $extension = $extension[sizeof($extension) - 1];
                } else {
                    $extension = $fileStream['extension'];
                }

                $file_stream_type = $this->getFileMimeTypeByExtension($extension);
            }

            // Check if current file is an image
            $file_stream_is_valid = (in_array($file_stream_type, $this->validMimeTypes));

            if (!$file_stream_is_valid || !$extension) {
                @unlink($file_location);

                return false;
            }

            // Generate filename
            $new_temp_base_name = explode('.', $fileStream['name']);
            $new_temp_base_name = $new_temp_base_name[0];
            $new_temp_base_name = clean_for_url(date('YmdHis') . '-' . $new_temp_base_name);
            $new_temp_file_name = ROOT_PATH . 'content/files/' . $new_temp_base_name . '.' . $extension;

            // Try to copy content to new destination
            if (copy($file_location, $new_temp_file_name)) {
                // If succeeded replace filename destroy previous
                // file path and replace it
                @unlink($file_location);
                $file_location = $new_temp_file_name;
            } else {
                // If failed, return false
                @unlink($file_location);

                return false;
            }

            // Generate file URL
            $file_url = Router::url('/' . substr($file_location, strlen(ROOT_PATH)), array('no_session' => true));

            // Attempt to save
            $saveResult = $this->addNew(array(
                                            'alt' => $alt,
                                            'path' => $file_location,
                                            'src' => $file_url,
                                            'mime_type' => $file_stream_type,
                                        ));

            // If saving failed, destroy file before returning
            if (!$saveResult) {
                @unlink($file_location);

                return false;
            }

            // Return saving result
            return $saveResult;
        }

        // Saving failed, error
        return false;
    }

    /**
     * @see MySQLModel::afterDelete
     */
    function afterDelete($element, $options) {
        // Call parent
        parent::afterDelete($element, $options);

        // Delete file
        @unlink($element[$this->name]['path']);
    }
}
