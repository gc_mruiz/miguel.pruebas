<?php

/**
 * Class LayoutText
 *
 * Model class for table layout_texts
 *
 */
class LayoutText extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'LayoutText';

    /**
     * Default display fields for the model. When more than one, they are concatenated with an space.
     *
     * @var array
     */
    public $displayFields = array('id',);

    /**
     * Valid order by expressions. List of expression that can be used to order. Expression can be a field or a complex
     * expression. Do not include direction of order.
     *
     * @var array
     */
    public $validOrderByExpressions = array('id', 'locale', 'code', 'text', 'picture_id', 'created', 'modified',);

    /**
     * Belongs To (child->father) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'foreign_key' => 'foreign_key_id',
     * 'options' => array()) By convention ModelName and RelationshipName are equal unless they cannot be. By
     * convention foreign_key will be the id of the related model on the current table. Options are translated to the
     * other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $belongsTo = array(
        'Picture' => array(
            'model' => 'Picture',
            'foreign_key' => 'picture_id',
            'options' => array(),
        ),
    );

    /**
     * Check locale field when selecting.
     * Localization means there is a 'locale' varchar field on the table that is null or matches one of the sites
     * languages. If it is set to true, the get methods will always add a condition (`locale` IS NULL OR `locale` =
     * LANG) to every SELECT query generated if option['ignore_locale'] is not set.
     *
     * @var bool
     */
    protected $check_locale = true;

    /**
     * Validation array for fields.
     * Structure: array('field_name' => array('validation_method' => 'Validation error message.')
     * Validation methods available:
     *  - notempty: if present, value cannot be empty
     *  - required: value must be present on creation. An empty value validates.
     *  - email: value must have an email format
     *  - numeric: value must be numeric
     *  - int: value must be the representation of an integer
     *
     * @var array
     */
    public $validation = array(
        'code' => array(
            'required' => 'Code is required.',
            'notempty' => 'Code cannot be left empty.',
        ),
        /*'picture_id' => array(
            'id' => 'Picture id must be a valid id value.',
        ),*/
    );

}
