<?php

/**
 * Class DbMigration
 *
 * Model class for table db_migrations
 *
 */
class DbMigration extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'DbMigration';

    /**
     * Default display fields for the model. When more than one, they are concatenated with an space.
     *
     * @var array
     */
    public $displayFields = array('id',);

    /**
     * Valid order by expressions. List of expression that can be used to order. Expression can be a field or a complex
     * expression. Do not include direction of order.
     *
     * @var array
     */
    public $validOrderByExpressions = array('id', 'file', 'created', 'modified',);

    /**
     * Validation array for fields.
     * Structure: array('field_name' => array('validation_method' => 'Validation error message.')
     * Validation methods available:
     *  - notempty: if present, value cannot be empty
     *  - required: value must be present on creation. An empty value validates.
     *  - email: value must have an email format
     *  - numeric: value must be numeric
     *  - int: value must be the representation of an integer
     *
     * @var array
     */
    public $validation = array(
        'file' => array(
            'required' => 'File is required.',
            'notempty' => 'File cannot be left empty.',
        ),
    );

    /**
     * All mysql tables require created and modified fields. If this is set to true, the table also requires
     * created_by_user_id and modified_by_user_id and both area populated automatically.
     *
     * @var bool
     */
    protected $user_timestamp_fields = true;

    public function tableExists() {
        return mysqli_num_rows(MySQLModel::query('SHOW TABLES LIKE \'' . sql_escape($this->table_prefix . $this->table_name) . '\'')) == 1;
    }

    public function createTableIfNotExists() {
        if ($this->tableExists()) {
            return true;
        }

        if (MySQLModel::query('CREATE TABLE `db_migrations` (
                  `id` INT(11) NOT NULL AUTO_INCREMENT,
                  `file` VARCHAR(255) NOT NULL,
                  `created` DATETIME NOT NULL,
                  `created_by_user_id` INT(11) DEFAULT NULL,
                  `modified` DATETIME NOT NULL,
                  `modified_by_user_id` INT(11) DEFAULT NULL,
                  PRIMARY KEY (`id`),
                  KEY `created_by_user_id` (`created_by_user_id`),
                  KEY `modified_by_user_id` (`modified_by_user_id`),
                  CONSTRAINT `db_migrations_ibfk_1` FOREIGN KEY (`created_by_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL,
                  CONSTRAINT `db_migrations_ibfk_2` FOREIGN KEY (`modified_by_user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;') !== false
        ) {
            // Add base schema file
            $this->addNew(array('file' => '0000-00-00 base-schema.sql'));

            return true;
        }

        return false;
    }
}
