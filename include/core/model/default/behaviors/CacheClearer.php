<?php

/**
 * Class CacheClearerBehavior
 *
 * Allows to clear cache on update/delete/creation.
 */
class CacheClearerBehavior extends Behavior {
    /**
     * After save callback.
     *
     * @param $created bool whether or not the record whas just created
     * @param $element array updated record
     * @param $values array updated values
     * @param $originalElement array element before update
     * @param $options array
     *
     * @return void
     */
    public function afterSave($created, $element, $values, $originalElement, $options) {
        // Call parent
        parent::afterSave($created, $element, $values, $originalElement, $options);

        // Check for non ignored
        $found_non_ignored_fields = false;

        if (!isset($this->options['ignoredFields'])) {
            $this->options['ignoredFields'] = array();
        }

        if (!in_array('modified', $this->options['ignoredFields'])) {
            $this->options['ignoredFields'][] = 'modified';
        }

        if (!in_array('modified_by_user_id', $this->options['ignoredFields'])) {
            $this->options['ignoredFields'][] = 'modified_by_user_id';
        }

        // If created or option has not been set, as if found
        if ($created || empty($this->options['ignoredFields'])) {
            $found_non_ignored_fields = true;
        } else {
            // Check modified fields
            foreach ($values as $field => $value) {
                if (!in_array($field, $this->options['ignoredFields'])) {
                    $current_value = (isset($element[$this->model->name][$field]) ? $element[$this->model->name][$field] : null);
                    $previous_value = (isset($originalElement[$this->model->name][$field]) ? $originalElement[$this->model->name][$field] : null);

                    if (is_array($current_value)) {
                        $current_value = json_encode($current_value);
                    }

                    if (is_array($previous_value)) {
                        $previous_value = json_encode($previous_value);
                    }

                    if (array_key_exists($field, $element[$this->model->name]) && strval($current_value) != strval($previous_value)) {
                        $found_non_ignored_fields = true;
                        break;
                    }
                }
            }
        }

        if ($found_non_ignored_fields) {
            // Delete related cache
            $this->deleteRelatedCache($element);
        }
    }

    /**
     * After delete callback.
     *
     * @param $element array
     * @param $options array
     *
     * @return void
     */
    public function afterDelete($element, $options) {
        // Call parent
        parent::afterDelete($element, $options);

        // Delete related cache
        $this->deleteRelatedCache($element);
    }

    /**
     * Clears any related cache records.
     *
     * @param $element
     */
    private function deleteRelatedCache($element) {
        CachedAdapter::clearByClearer($this->model->name, $element[$this->model->name]['id']);
    }
}