<?php

/**
 * Class CleanFieldBehavior
 *
 * Allows to generate cleaned specialchar-less fields based on other fields.
 *
 * Options:
 *  - fields: array('clean_field_name' => array('field_to_be_contact_and_cleaned1', 'field_to_be_contact_and_cleaned2',
 * ..), ..)
 */
class CleanFieldBehavior extends Behavior {
    /**
     * Initializes the behavior with a reference to the extended model.
     *
     * @param $model MySQLModel reference to extended model
     * @param $options array initialization options
     *
     * @return void
     */
    public function initialize(&$model, $options = array()) {
        // Set default options
        if (!isset($options['fields'])) {
            $options['fields'] = array();
        }

        // Initialize parent
        parent::initialize($model, $options);
    }

    /**
     * Before save callback
     *
     * @param $element array. Element that's being
     * updated/created.
     * @param $values array. Values to be updated.
     *
     * @return bool continue or not
     */
    public function beforeSave($element, &$values, $options) {
        foreach ($this->options['fields'] as $clean_field => $originalFields) {
            // Whether or not the clean field needs to be udpated
            $update_clean_field = false;

            // Allow single-field definition
            if (!is_array($originalFields)) {
                $originalFields = array($originalFields);
            }

            // Check if any of the original fields are present
            foreach ($originalFields as $original_field) {
                if (array_key_exists($original_field, $values)) {
                    $update_clean_field = true;
                    break;
                }
            }

            // Generate new clean field if required
            if ($update_clean_field) {
                // Original values from fields (retrieved from $values or $element)
                $originalValues = array();

                // Check if any of the original fields are present
                foreach ($originalFields as $original_field) {
                    if (array_key_exists($original_field, $values)) {
                        // Retrieve original field value from $values if specified
                        $originalValues[$original_field] = $values[$original_field];
                    } elseif ($element) {
                        // Retrieve original field value from $element if not creating
                        $originalValues[$original_field] = $element[$this->model->name][$original_field];
                    } else {
                        // Original field value is null, do nothing
                    }
                }

                // Generate clean field value
                $values[$clean_field] = clean_for_search(implode(' ', $originalValues));
            }
        }

        // Do not break execution
        return true;
    }
}