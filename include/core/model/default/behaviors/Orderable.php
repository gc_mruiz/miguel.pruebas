<?php

/**
 * Class OrderableBehavior
 *
 * Allows records to be ordered.
 *
 * Options:
 *  - order_field: default order
 *  - override_default_order: default true
 *  - grouping_field: group by this field when ordering. Use false (default) to ignore.
 */
class OrderableBehavior extends Behavior {
    /**
     * Initializes the behavior with a reference to the extended model.
     *
     * @param $model MySQLModel reference to extended model
     * @param $options array initialization options
     *
     * @return void
     */
    public function initialize(&$model, $options = array()) {
        // Set default options
        if (!isset($options['order_field'])) {
            $options['order_field'] = 'order';
        }
        if (!isset($options['override_default_order'])) {
            $options['override_default_order'] = true;
        }
        if (!isset($options['grouping_field'])) {
            $options['grouping_field'] = false;
        }

        // Initialize parent
        parent::initialize($model, $options);

        // Sets order for the model if not set
        if ($this->options['override_default_order']) {
            $this->model->order = $this->model->fullTableName() . '.`' . $this->options['order_field'] . '` ASC';
        }
    }

    /**
     * After save callback.
     *
     * @param $created bool whether or not the record whas just created
     * @param $element array updated record
     * @param $values array updated values
     * @param $originalElement array element before updating
     * @param $options
     *
     * @return void
     */
    public function afterSave($created, $element, $values, $originalElement, $options) {
        // On creation only
        if ($created && $element[$this->model->name][$this->options['order_field']] === null) {
            // Set order to id
            $this->model->updateFields($element[$this->model->name]['id'], array(
                $this->options['order_field'] => $element[$this->model->name]['id'],
            ));
        }
    }

    /**
     * For ordered models, reorders the records using an ordered list of ids.
     *
     * @param $idByOrder array ordered list of ids to reorder
     *
     * @return bool success
     */
    public function reorder($idByOrder) {
        // Exit if provided data is invalid
        if (!$idByOrder || !is_array($idByOrder)) {
            return false;
        }

        // Begin transaction (multiple updates will be generated)
        $this->model->beginTransaction();

        // No errors so far
        $errors = false;

        // Retrieve all elements to be updated.
        $elements = $this->model->getAnyById(array_values($idByOrder), array(
            'recursive' => -1,
            'fields' => array(
                'id',
                'order',
            ),
            'order_by' => '`' . $this->options['order_field'] . '` ASC',
        ));

        // List all ids and orders retrieved
        $ids = array();
        $orders = array();
        foreach ($elements as $element) {
            $ids[] = $element[$this->model->name]['id'];
            $orders[] = $element[$this->model->name][$this->options['order_field']];
        }
        unset($element);
        unset($elements);

        // Store each id that was successfully saved.
        $savedIds = array();

        // Store original validation and remove it from the model to prevent innecesary errors
        $validationCache = $this->model->validation;
        $this->model->validation = array();

        // Foreach provided id, attempt to reorder
        foreach ($idByOrder as $order_ => $id) {
            // If id could not be retrieved from the database, ignore
            if (!in_array($id, $ids)) {
                continue;
            }

            // If id was already stored, ignore (avoid duplicated ids this way)
            if (in_array($id, $savedIds)) {
                continue;
            }

            // Get the following order
            $order = $orders[sizeof($savedIds)];

            // Update the order and stop if it fails
            if (!$this->model->updateFields($id, array($this->options['order_field'] => $order))) {
                $errors = true;
                break;
            }

            // Store saved id
            $savedIds[] = $id;
        }

        // Commit transaction if no errors were found. Rollback in any other case.
        if ($errors) {
            $this->model->rollbackTransaction();
        } else {
            $this->model->commitTransaction();
        }

        // Restore validation rules
        $this->model->validation = $validationCache;

        // Return true (success) if no errors were found, false (error) in the other case
        return !$errors;
    }

    /**
     * Alternates the order of the specified record with the previous one.
     *
     * @param $id int record id to be moved up
     *
     * @return bool success
     */
    public function moveUpById($id) {
        // Required fields to moving and grouping
        $fields = array('id', 'order');

        // Add grouping field if required
        if ($this->options['grouping_field']) {
            $fields[] = $this->options['grouping_field'];
        }

        // Retrieve element
        $element = $this->model->getById($id, array('recursive' => -1, 'fields' => $fields));

        if ($element) {
            // Set sibling condition
            $conditions = array($this->options['order_field'] . ' <' => $element[$this->model->name][$this->options['order_field']]);

            // Add grouping field if required
            if ($this->options['grouping_field']) {
                $conditions[$this->options['grouping_field']] = $element[$this->model->name][$this->options['grouping_field']];
            }

            // Retrieve previous element
            $previousElement = $this->model->get(array(
                                                     'where' => $conditions,
                                                     'recursive' => -1,
                                                     'fields' => $fields,
                                                     $this->options['order_field'] => '`' . $this->options['order_field'] . '` DESC',
                                                 ));

            // If a previous element was found, continue with update
            if ($previousElement) {
                // Begin transaction to avoid data corruption
                $this->model->beginTransaction();

                // Double update to switch orders
                if ($this->model->updateFields($element[$this->model->name]['id'], array(
                        $this->options['order_field'] => $previousElement[$this->model->name]['order'],
                    )) && $this->model->updateFields($previousElement[$this->model->name]['id'], array(
                        $this->options['order_field'] => $element[$this->model->name][$this->options['order_field']],
                    ))
                ) {
                    // If both queries succeed, commit
                    $this->model->commitTransaction();

                    // Success!
                    return true;
                } else {
                    // If any of the queries fails, rollback
                    $this->model->rollbackTransaction();
                }
            }
        }

        // Error!
        return false;
    }
}