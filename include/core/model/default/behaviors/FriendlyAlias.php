<?php

/**
 * Class FriendlyAliasBehavior
 *
 * FriendlyAliasBehavior allows for the generation of a URL-friendly string based on the record fields.
 *
 * Options:
 *  - field: name of the friendly alias field, default 'friendly'
 *  - originFields: list of fields to be contactenated in order to generate the friendly alias
 *  - suffix: suffix to be added to the friendly alias, default ''
 *  - max_length: max length for the friendly alias, default 40
 *  - ignoredWords: words to be ignored if friendly alias is too long
 */
class FriendlyAliasBehavior extends Behavior {
    /**
     * Initializes the behavior with a reference to the extended model.
     *
     * @param $model MySQLModel reference to extended model
     * @param $options array initialization options
     *
     * @return void
     */
    public function initialize(&$model, $options = array()) {
        // Set default options
        $options = array_merge(array(
                                   'field' => 'friendly',
                                   'originFields' => array(),
                                   'suffix' => '',
                                   'max_length' => 40,
                                   'ignoredWords' => array(
                                       'in',
                                       'y',
                                       'and',
                                       'en',
                                       'on',
                                       'de',
                                       'para',
                                       'for',
                                   ),
                               ), $options);

        // Call parent initialization
        parent::initialize($model, $options);
    }

    /**
     * Before save callback
     *
     * @param $element array. Element that's being
     * updated/created.
     * @param $values array. Values to be updated.
     * @param $options
     *
     * @return bool continue or not
     */
    public function beforeSave($element, &$values, $options) {
        // Check if any of the friendly fields are present
        foreach ($this->options['originFields'] as $field) {
            if (array_key_exists($field, $values)) {
                // If any of the specified fields is being updated, generate new friendly
                $values[$this->options['field']] = $this->generateFriendly($element, $values);
                break;
            }
        }

        // Do not block execution
        return true;
    }

    /**
     * Generates an URL-friendly string based on the required fields for SEO purposes.
     *
     * @param $element array original element
     * @param $values array values being updated
     *
     * @return string calculated friendly value
     */
    public function generateFriendly($element, $values) {
        // String to generate friendly from
        $base_string = '';

        // Concat all required field values
        foreach ($this->options['originFields'] as $field) {
            // Extract value from $values or $element
            $value = (array_key_exists($field, $values) ? $values[$field] : ($element ? $element[$this->model->name][$field] : null));

            // If value is present and not empty, contact
            if ($value) {
                $base_string .= ($base_string ? ' ' : '') . $value;
            }
        }

        // Clean base string special chars, spaces, etc.
        $friendly = clean_for_url($base_string);

        // If the calculated friendly is lenght is too big, add word by word ignoring the invalid ones
        if (strlen($friendly) + strlen($this->options['suffix']) > $this->options['max_length']) {
            // Store validated words
            $validWords = array();

            // Strip the words of the base string
            $words = explode(' ', $base_string);

            // For each word check validity and add
            foreach ($words as $word) {
                // Clean spaces and tabs
                $word = trim(clean_for_url($word));

                // Check if word should be ignored or not
                if ($word && !in_array($word, $this->options['ignoredWords'])) {
                    // If word does not fit, stop
                    if (strlen(implode(' ', $validWords)) + strlen($word) + 1 + strlen($this->options['suffix']) >= $this->options['max_length']) {
                        break;
                    }

                    // If world is not ignored and fits, add it
                    $validWords[] = $word;
                }
            }

            // Regenerate friendly value joinning the valid words and cleaning special chars and spaces
            $friendly = clean_for_url(implode(' ', $validWords));
        }

        // If friendly could not be generated, then it's the index
        if (!$friendly) {
            $friendly = 'index';
        }

        // Add suffix if required
        if ($this->options['suffix']) {
            $friendly = $friendly . $this->options['suffix'];
        }

        // Return generated friendly value
        return $friendly;
    }
}