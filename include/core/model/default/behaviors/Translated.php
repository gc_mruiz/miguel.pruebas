<?php

/**
 * Class TranslatedBehavior
 *
 * Manages traslanted information.
 *
 * Options:
 *  - fields: fields to be translated
 */
class TranslatedBehavior extends Behavior {
    /**
     * Translated version of the values being saved.
     *
     * @var array
     */
    private $translationValues = array();

    private static $instances = 0;

    public function __construct() {
        self::$instances++;
    }

    /**
     * Initializes the behavior with a reference to the extended model.
     *
     * @param $model MySQLModel reference to extended model
     * @param $options array initialization options
     *
     * @return void
     */
    public function initialize(&$model, $options = array()) {
        // Set default options
        if (!isset($options['fields'])) {
            $options['fields'] = array();
        }

        // Initialize parent
        parent::initialize($model, $options);
    }

    /**
     * Before get callback.
     * ATTENTION: this is the only 'before' callback that cannot break the execution of the call.
     *
     * @param $options array
     *
     * @return void
     */
    public function beforeGet(&$options) {
        if (GestyMVC::config('ignore_translated_behaviour_on_read')) {
            return;
        }

        if (!empty($options['for_delete'])) {
            $options['ignore_translation'] = true;
        }

        if (!isset($options['ignore_translation']) || !$options['ignore_translation']) {
            if (isset($options['fields']) && is_array($options['fields'])) {
                $translated_field_requested = false;

                foreach ($this->options['fields'] as $field) {
                    if (in_array($field, $options['fields'])) {
                        $translated_field_requested = true;
                        break;
                    }
                }

                if (!$translated_field_requested) {
                    $options['ignore_translation'] = true;
                }
            }
        }

        if (!isset($options['ignore_translation']) || !$options['ignore_translation']) {
            $translatedBelongsTo = $this->model->belongsTo;

            foreach ($translatedBelongsTo as $relationship_name => $relationship) {
                if (!in_array($relationship['foreign_key'], $this->options['fields'])) {
                    unset($translatedBelongsTo[$relationship_name]);
                }
            }

            $this->model->hasMany['Translation'] = array(
                'model' => $this->model->name . 'Lang',
                'foreign_key' => Inflector::underscore($this->model->name) . '_id',
                'options' => array(
                    'where' => array(
                        'locale' => LANG,
                    ),
                    'recursive' => 0,
                ),
                'relationships' => array(
                    'belongsTo' => $translatedBelongsTo,
                ),
            );

            if (GestyMVC::config('default_lang') != LANG) {
                $this->model->hasMany['TranslationDefault'] = array(
                    'model' => $this->model->name . 'Lang',
                    'foreign_key' => Inflector::underscore($this->model->name) . '_id',
                    'options' => array(
                        'where' => array(
                            'locale' => GestyMVC::config('default_lang'),
                        ),
                        'recursive' => ($options['recursive'] > -1 ? 0 : -1),
                    ),
                    'relationships' => array(
                        'belongsTo' => $translatedBelongsTo,
                    ),
                );
            }

            if (isset($options['all_translations']) && $options['all_translations']) {
                $this->model->hasMany['TranslationOther'] = array(
                    'model' => $this->model->name . 'Lang',
                    'foreign_key' => Inflector::underscore($this->model->name) . '_id',
                    'options' => array(
                        'where' => array(
                            'locale <>' => array(
                                GestyMVC::config('default_lang'),
                                LANG,
                            ),
                        ),
                        'recursive' => ($options['recursive'] > -1 ? 0 : -1),
                    ),
                    'relationships' => array(
                        'belongsTo' => $translatedBelongsTo,
                    ),
                );
            }

            // Recursive needs to be 1 at least. If it is lower than 0 we must cancel all belongsTo relationships to avoid loading unnecesary data.
            if ($options['recursive'] < 0) {
                foreach ($this->model->belongsTo as $relationship_name => $relationshipConfig) {
                    $options[$relationship_name] = false;
                }
            }

            // Recursive needs to be 1 at least. If it is lower than 1 we must cancel all hasMany and habtm relationships to avoid loading unnecesary data.
            if ($options['recursive'] < 1) {
                foreach ($this->model->hasMany as $relationship_name => $relationshipConfig) {
                    $options[$relationship_name] = false;
                }
                foreach ($this->model->habtm as $relationship_name => $relationshipConfig) {
                    $options[$relationship_name] = false;
                }
            }

            // Set recursive to at least 1
            if ($options['recursive'] < 1) {
                $options['recursive'] = 1;
            }

            // Remove translation relationship override
            unset($options['Translation']);
            unset($options['TranslationDefault']);
            unset($options['TranslationOther']);

            if (isset($options['fields']) && is_array($options['fields']) && !in_array('id', $options['fields'])) {
                $options['fields'][] = 'id';
            }
        }
    }

    /**
     * After get callback.
     *
     * @param $element array the element retrieved
     * @param $options
     *
     * @return void
     */
    public function afterGetWithRelationships(&$element, $options = array()) {
        if (isset($element['Translation'])) {
            // Add field to check if translation is complete
            $element[$this->model->name]['is_translation_complete'] = true;

            // Replace main values
            foreach ($element[$this->model->name] as $var => $value) {
                if (!in_array($var, array('id', 'created', 'modified')) && in_array($var, $this->options['fields'])) {
                    if (sizeof($element['Translation']) && array_key_exists($var, $element['Translation'][0]) && $element['Translation'][0][$var]) {
                        $element[$this->model->name][$var] = $element['Translation'][0][$var];
                    } elseif (GestyMVC::config('default_lang') != LANG && sizeof($element['TranslationDefault']) && array_key_exists($var, $element['TranslationDefault'][0]) && $element['TranslationDefault'][0][$var]) {
                        $element[$this->model->name][$var] = $element['TranslationDefault'][0][$var];
                        $element[$this->model->name]['is_translation_complete'] = false;
                    } elseif ($value) {
                        $element[$this->model->name]['is_translation_complete'] = false;
                    }
                }
            }

            // Replace belongsTo relationships
            foreach ($this->model->belongsTo as $relationship_name => $relationshipData) {
                if (array_key_exists($relationship_name, $element) && $element[$relationship_name]) {
                    foreach ($element[$relationship_name] as $var => $value) {
                        if (!in_array($var, array(
                                'id',
                                'created',
                                'modified',
                            )) && in_array($var, $this->options['fields'])
                        ) {
                            if (sizeof($element['Translation']) && array_key_exists($relationship_name, $element['Translation'][0]) && $element['Translation'][0][$relationship_name] && array_key_exists($var, $element['Translation'][0][$relationship_name]) && $element['Translation'][0][$relationship_name][$var]) {
                                $element[$relationship_name][$var] = $element['Translation'][0][$relationship_name][$var];
                            } elseif (GestyMVC::config('default_lang') != LANG && sizeof($element['TranslationDefault']) && array_key_exists($relationship_name, $element['TranslationDefault'][0]) && $element['TranslationDefault'][0][$relationship_name] && array_key_exists($var, $element['TranslationDefault'][0][$relationship_name]) && $element['TranslationDefault'][0][$relationship_name][$var]) {
                                $element[$relationship_name][$var] = $element['TranslationDefault'][0][$relationship_name][$var];
                            }
                        }
                    }
                }
            }

            // Add translation sub-index
            if (isset($element['Translation']) && sizeof($element['Translation'])) {
                if (!isset($element[$this->model->name]['Translation'])) {
                    $element[$this->model->name]['Translation'] = array();
                }

                // Remove unnecessary fields
                unset($element['Translation'][0]['id']);
                unset($element['Translation'][0][Inflector::underscore($this->model->name) . '_id']);
                unset($element['Translation'][0]['created']);
                unset($element['Translation'][0]['modified']);

                // Add translation
                $element[$this->model->name]['Translation'][$element['Translation'][0]['locale']] = $element['Translation'][0];
            }

            // Add default translation
            if (isset($element['TranslationDefault']) && sizeof($element['TranslationDefault'])) {
                if (!isset($element[$this->model->name]['Translation'])) {
                    $element[$this->model->name]['Translation'] = array();
                }

                // Remove unnecessary fields
                unset($element['TranslationDefault'][0]['id']);
                unset($element['TranslationDefault'][0][Inflector::underscore($this->model->name) . '_id']);
                unset($element['TranslationDefault'][0]['created']);
                unset($element['TranslationDefault'][0]['modified']);

                // Add translation
                $element[$this->model->name]['Translation'][$element['TranslationDefault'][0]['locale']] = $element['TranslationDefault'][0];
            }

            // Add other translations
            if (isset($element['TranslationOther']) && sizeof($element['TranslationOther'])) {
                if (!isset($element[$this->model->name]['Translation'])) {
                    $element[$this->model->name]['Translation'] = array();
                }

                for ($i = 0; $i < sizeof($element['TranslationOther']); $i++) {
                    // Remove unnecessary fields
                    unset($element['TranslationOther'][$i]['id']);
                    unset($element['TranslationOther'][$i][Inflector::underscore($this->model->name) . '_id']);
                    unset($element['TranslationOther'][$i]['created']);
                    unset($element['TranslationOther'][$i]['modified']);

                    // Add translation
                    $element[$this->model->name]['Translation'][$element['TranslationOther'][$i]['locale']] = $element['TranslationOther'][$i];
                }
            }

            // Clear original translations
            unset($element['Translation']);
            unset($element['TranslationDefault']);
            unset($element['TranslationOther']);

            if (!isset($element[$this->model->name]['Translation'][GestyMVC::config('default_lang')])) {
                $element[$this->model->name]['Translation'][GestyMVC::config('default_lang')] = array();

                foreach ($element[$this->model->name] as $var => $value) {
                    if (in_array($var, $this->options['fields'])) {
                        $element[$this->model->name]['Translation'][GestyMVC::config('default_lang')][$var] = $value;
                    }
                }
            }
        }
    }

    /**
     * Before validate callback.
     * Executed before validation on updateFields and addNew calls.
     *
     * @param $element array element being updated
     * @param $values array values being updated
     *
     * @return bool
     */
    public function beforeValidate($element, &$values, $options) {
        // Avoid saving of auto-value
        unset($values['is_translation_complete']);

        // Store translated values
        if (isset($values['Translation'])) {
            $this->translationValues = $values['Translation'];
        } else {
            $this->translationValues = array();
        }

        // Clear translated values from the saving array
        unset($values['Translation']);

        // If the default language was modified and not the row itself, update row
        if (isset($this->translationValues[GestyMVC::config('default_lang')])) {
            foreach ($this->translationValues[GestyMVC::config('default_lang')] as $var => $value) {
                if (in_array($var, $this->options['fields'])) {
                    if ($value && (!isset($values[$var]) || !$values[$var])) {
                        $values[$var] = $value;
                    }
                }
            }
        } else {
            $this->translationValues[GestyMVC::config('default_lang')] = array();

            foreach ($values as $var => $value) {
                if (in_array($var, $this->options['fields'])) {
                    $this->translationValues[GestyMVC::config('default_lang')][$var] = $value;
                }
            }
        }

        return true;
    }

    /**
     * Before save callback
     *
     * @param $element array. Element that's being
     * updated/created.
     * @param $values array. Values to be updated.
     * @param $options
     *
     * @return bool continue or not
     */
    public function beforeSave($element, &$values, $options) {
        // Initialize translation model for validation
        $TranslationModel = MySQLModel::getRecycledInstance($this->model->name . 'Lang', array(), $this->model, 'translation');

        // Copy validation from current model
        $TranslationModel->validation = $this->model->validation;

        // Remove required and non-empty validations
        foreach ($TranslationModel->validation as $field => $rules) {
            unset($TranslationModel->validation[$field]['notempty']);
            unset($TranslationModel->validation[$field]['required']);
        }

        // Foreach translation, validate
        foreach ($this->translationValues as $lang => $translationValues) {
            if (!$TranslationModel->beforeValidate(array(), $translationValues, $options) || !$TranslationModel->validates(array(), $translationValues, $options)) {
                $this->model->errors = $TranslationModel->errors;

                return false;
            }
        }

        // Return true by default;
        return true;
    }

    /**
     * After save callback.
     *
     * @param $created bool whether or not the record was just created
     * @param $element array updated record
     * @param $values array updated values
     * @param $originalElement array element before update
     * @param $options
     *
     * @return void
     */
    public function afterSave($created, $element, $values, $originalElement, $options) {
        if ($this->translationValues) {
            // Initialize translation model for validation
            $TranslationModel = MySQLModel::getRecycledInstance($this->model->name . 'Lang', array(), $this->model, 'translation');

            // Foreach translation, save
            foreach ($this->translationValues as $lang => $translationValues) {
                // Retrieve
                $translationElement = $TranslationModel->get(array(
                                                                 'where' => array(
                                                                     Inflector::underscore($this->model->name) . '_id' => $element[$this->model->name]['id'],
                                                                     'locale' => $lang,
                                                                 ),
                                                                 'fields' => array('id'),
                                                             ));

                // Replace empty by null
                foreach ($translationValues as $var => $value) {
                    if ($value == '') {
                        $translationValues[$var] = null;
                    }
                }

                // Update or insert
                if ($translationElement) {
                    $TranslationModel->updateFields($translationElement[$TranslationModel->name]['id'], $translationValues);
                } else {
                    $translationValues[Inflector::underscore($this->model->name) . '_id'] = $element[$this->model->name]['id'];
                    $translationValues['locale'] = $lang;
                    $TranslationModel->addNew($translationValues);
                }
            }
        }
    }

    protected function valueHasChanged($field, &$element, &$values, $ignoreFields = array()) {
        if (is_array($field)) {
            foreach ($field as $field_) {
                if (in_array($field_, $ignoreFields)) {
                    continue;
                }

                if ($this->valueHasChanged($field_, $element, $values)) {
                    return true;
                }
            }

            return false;
        }

        $current_value = (isset($values[$field]) ? $values[$field] : null);
        $previous_value = (isset($element[$this->model->name][$field]) ? $element[$this->model->name][$field] : null);

        if (is_array($current_value)) {
            $current_value = json_encode($current_value);
        }

        if (is_array($previous_value)) {
            $previous_value = json_encode($previous_value);
        }

        return array_key_exists($field, $values) && strval($current_value) != strval($previous_value);
    }
}