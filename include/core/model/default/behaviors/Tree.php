<?php

/**
 * Class TreeBehavior
 *
 * Adds a Parent belongsTo, a Child hasMany and generates the full name if required.
 *
 * Options:
 *  - parent_relationship: if not false, a Parent belongsTo relationship will be added
 *  - child_relationship: if not false, a Child hasMany relationship will be added
 *  - foreign_key: by default, 'parent_' . Inflector::underscore($this->model->name) . '_id'
 *  - name_full_field: by default, 'name_full'. Use false to ignore.
 *  - name_full_separator: required if name_full_field is not false. Defaults to ' > '
 *  - name_field: required if name_full_field is not false. Defaults to 'name'
 *  - dependent: whether or not to delete children after deletion. If false, set null.
 */
class TreeBehavior extends Behavior {
    /**
     * Child ids
     */
    private $childIds = array();

    /**
     * Initializes the behavior with a reference to the extended model.
     *
     * @param $model MySQLModel reference to extended model
     * @param $options array initialization options
     *
     * @return void
     */
    public function initialize(&$model, $options = array()) {
        // Set default options
        $options = array_merge(array(
                                   'parent_relationship' => true,
                                   'child_relationship' => false,
                                   'name_full_field' => 'name_full',
                                   'name_field' => 'name',
                                   'name_full_separator' => ' > ',
                                   'foreign_key' => 'parent_' . Inflector::underscore($model->name) . '_id',
                                   'dependent' => false,
                               ), $options);

        // Initialize parent
        parent::initialize($model, $options);

        // Set parent relationship if required
        if ($this->options['parent_relationship']) {
            $this->model->belongsTo['Parent'] = array(
                'model' => $this->model->name,
                'foreign_key' => $this->options['foreign_key'],
            );
        }

        // Set parent relationship if required
        if ($this->options['child_relationship']) {
            $this->model->hasMany['Child'] = array(
                'model' => $this->model->name,
                'foreign_key' => $this->options['foreign_key'],
            );
        }
    }

    /**
     * Before save callback. Generate the full name based on the origin field.
     *
     * @param $element array. Element that's being
     * updated/created.
     * @param $values array. Values to be updated.
     * @param $options
     *
     * @return bool continue or not
     */
    public function beforeSave($element, &$values, $options) {
        // If full name is required
        if ($this->options['name_full_field'] !== false) {
            // Must update full name or not
            $update_full_name = false;

            // Check if foreign key or name fields are present on the updated values
            if (array_key_exists($this->options['foreign_key'], $values) || array_key_exists($this->options['name_field'], $values)) {
                $update_full_name = true;
            }

            // Proceed with update if required
            if ($update_full_name) {
                // Parent row id
                $parent_id = null;

                if (array_key_exists($this->options['foreign_key'], $values)) {
                    // Retrieve foreign key value from $values if specified
                    $parent_id = $values[$this->options['foreign_key']];
                } elseif ($element) {
                    // Retrieve foreign key value from $element if not creating
                    $parent_id = $element[$this->model->name][$this->options['foreign_key']];
                } else {
                    // Foreign key value is null, do nothing
                }

                // Parent row id
                $name = null;

                if (array_key_exists($this->options['name_field'], $values)) {
                    // Retrieve name value from $values if specified
                    $name = $values[$this->options['name_field']];
                } elseif ($element) {
                    // Retrieve name value from $element if not creating
                    $name = $element[$this->model->name][$this->options['name_field']];
                } else {
                    // Name value is null, do nothing
                }

                // To generate the full name we need a name and a parent_id
                if ($name) {
                    if ($parent_id) {
                        // Retrieve parent record
                        $parent = $this->model->getById($parent_id, array(
                            'fields' => array(
                                $this->options['name_full_field'],
                            ),
                            'recursive' => -1,
                            'ignore_hidden' => false,
                            'ignore_locale' => true,
                            'ignore_publish' => true,
                        ));

                        if ($parent) {
                            // Concat name to parent's full name
                            $values[$this->options['name_full_field']] = $parent[$this->model->name][$this->options['name_full_field']] . $this->options['name_full_separator'] . $name;
                        } else {
                            // Parent does not exist, clear reference
                            $values[$this->options['foreign_key']] = null;
                            $parent_id = null;
                        }

                        // Clear memory
                        unset($parent);
                    }

                    // If no parent, the full name is the name
                    if (!$parent_id) {
                        $values[$this->options['name_full_field']] = $name;
                    }
                }
            }
        }

        // Do not break execution
        return true;
    }

    /**
     * After save callback.
     * Update children's full name.
     *
     * @param $created bool whether or not the record whas just created
     * @param $element array updated record
     * @param $values array updated values
     * @param $originalElement array element before update
     * @param $options
     *
     * @return void
     */
    public function afterSave($created, $element, $values, $originalElement, $options) {
        // If the record was updated (cannot have children if just created)
        if (!$created) {
            // if the full name of the current record has changed, we need to change also its children's full names
            if ($element[$this->model->name][$this->options['name_full_field']] != $originalElement[$this->model->name][$this->options['name_full_field']]) {
                // Retrieve children records
                $children = $this->model->getAll(array(
                                                     'where' => array($this->options['foreign_key'] => $element[$this->model->name]['id']),
                                                     'fields' => array(
                                                         'id',
                                                     ),
                                                     'recursive' => -1,
                                                     'ignore_hidden' => false,
                                                     'ignore_locale' => true,
                                                     'ignore_publish' => true,
                                                 ));

                // Foreach children generate and update with the same value
                foreach ($children as $child) {
                    $this->model->updateFields($child[$this->model->name]['id'], array($this->options['foreign_key'] => $element[$this->model->name]['id']));
                }
            }
        }
    }

    /**
     * Before delete callback.
     *
     * @param $element array element to be deleted.
     * @param $options
     *
     * @return bool if not true, stops deleting
     */
    public function beforeDelete($element, $options) {
        // Store child ids
        $this->childIds = array_keys($this->model->getAll(array(
                                                              'where' => array($this->options['foreign_key'] => $element[$this->model->name]['id']),
                                                              'fields' => array('id'),
                                                              'hash_by' => 'id',
                                                              'recursive' => -1,
                                                              'ignore_hidden' => false,
                                                              'ignore_locale' => true,
                                                              'ignore_publish' => true,
                                                          )));

        // Return true by default
        return true;
    }

    /**
     * After delete callback.
     *
     * @param $element array
     * @param $options
     *
     * @return void
     */
    public function afterDelete($element, $options) {
        if ($this->options['dependent']) {
            // If children depend on parent, delete them
            foreach ($this->childIds as $id) {
                $this->model->deleteById($id);
            }
        } else {
            // If children do not depend on parent, set them to no-parent
            foreach ($this->childIds as $id) {
                $this->updateFields($id, array($this->model->options['foreign_key'] => null));
            }
        }
    }
}