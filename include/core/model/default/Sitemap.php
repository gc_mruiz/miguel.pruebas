<?php

/**
 * Class Sitemap
 *
 * Handles sitemap generation and publication.
 */
class Sitemap {
    /**
     * Creates Google Sitemap text
     *
     * @return string
     */
    private function getGoogleSitemap() {
        // Extract available langs
        $langs = GestyMVC::config('langs');

        // XML header for sitemap
        $sitemap_content = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.google.com/schemas/sitemap/0.84" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">';

        // Non-friendly urls to add to sitemap
        $urls = array(
            array('controller' => 'Pages', 'action' => 'index'),
        );

        // Add each url in each language
        foreach ($urls as $url) {
            foreach ($langs as $lang) {
                // Set URL lang
                $url['lang'] = $lang;

                // Add URL routed
                $this->addURL($sitemap_content, Router::url($url), 1);
            }
        }

        // Add generated pages
        // $this->addProjecs($sitemap_content, 0.5);

        // Close sitemap
        $sitemap_content .= '</urlset>';

        // Return complete sitemap
        return $sitemap_content;
    }

    /**
     * Appends given URL to the sitemap XML content.
     *
     * @param $sitemap_content
     * @param $url
     * @param $priority
     *
     * @return void
     */
    private function addURL(&$sitemap_content, $url, $priority) {
        $sitemap_content .= '<url><loc>' . utf8html($url) . '</loc><priority>' . utf8html($priority) . '</priority></url>';
    }

    /**
     * List all projects and add them to the sitemap.
     *
     * @param $sitemap_content string
     * @param $priority float
     *
     * @return void
     */
    private function addProjecs(&$sitemap_content, $priority) {
        // Load all projects
        /* @var Project $Project */
        $Project = MySQLModel::getInstance('Project');
        $projects = $Project->getAll(array(
                                         'ignore_locale' => true,
                                         'where' => array('publish' => 1),
                                         'fields' => array('id', 'friendly'),
                                         'recursive' => -1,
                                     ));

        // Add each project URL to the sitemap
        foreach ($projects as $project) {
            $this->addURL($sitemap_content, Router::url(array(
                                                            'controller' => 'Project',
                                                            'action' => 'view',
                                                            'lang' => $project[$Project->name]['locale'],
                                                            $project[$Project->name]['id'],
                                                            $project[$Project->name]['friendly'],
                                                        )), $priority);
        }
    }

    /**
     * Stores the google sitemap plain and compressed. Pushs the sitemap to Google.
     *
     * @return void
     */
    private function updateGoogleSitemap() {
        // Retrieve google sitemap content
        $sitemap_content = $this->getGoogleSitemap();

        // Open stream writer
        $fp = fopen(ROOT_PATH . 'sitemap.xml', 'w');

        // Stream them to file
        fwrite($fp, $sitemap_content);

        // Close stream writer
        fclose($fp);

        // Generate new filename
        $gzipped_file_location = ROOT_PATH . 'sitemap.xml.gz';

        // Compress data with gz
        $data = implode("", file(ROOT_PATH . 'sitemap.xml'));
        $gzdata = gzencode($data, 9);

        // Open stream writer to save data
        $fp = fopen($gzipped_file_location, 'w');

        // Save the compressed data
        fwrite($fp, $gzdata);

        // Close stream writer
        fclose($fp);

        // Ping sitemaps
        $this->pingGoogleSitemaps(ROOT_URL . 'sitemap.xml.gz');
        $this->pingGoogleSitemaps(ROOT_URL . 'sitemap.xml');
    }

    /**
     * Function to ping Google GoogleSitemaps.
     *
     * Function to ping Google GoogleSitemaps. Returns
     * an integer, e.g. 200 or 404,
     * 0 on error.
     *
     * @author     J de Silva
     * <giddomains@gmail.com>
     * @copyright  Copyright &copy; 2005, J de Silva
     * @link       http://www.gidnetwork.com/b-54.html
     * PHP function to ping Google
     * GoogleSitemaps
     *
     * @param      string $url_xml The sitemap url,
     * e.g.
     * http://www.example.com/google-sitemap-index.xml
     *
     * @return     integer            Status code, e.g.
     * 200|404|302 or 0 on error
     */
    private function pingGoogleSitemaps($url_xml) {
        $status = 0;
        $google = 'www.google.com';

        if ($fp = @fsockopen($google, 80)) {
            $req = 'GET /webmasters/sitemaps/ping?sitemap=' . urlencode($url_xml) . " HTTP/1.1\r\n" . "Host: $google\r\n" . "User-Agent: Mozilla/5.0 (compatible; " . PHP_OS . ") PHP/" . PHP_VERSION . "\r\n" . "Connection: Close\r\n\r\n";
            fwrite($fp, $req);

            while (!feof($fp)) {
                if (@preg_match('~^HTTP/\d\.\d (\d+)~i', fgets($fp, 128), $m)) {
                    $status = intval($m[1]);
                    break;
                }
            }

            fclose($fp);
        }

        return ($status);
    }

    /**
     * Updates all available sitemaps.
     *
     * @return void
     */
    public function updateSitemaps() {
        $this->updateGoogleSitemap();
    }
}
