<?php

/**
 * Class CanonicalUrl
 *
 * Model for table canonical_urls.
 */
class CanonicalUrl extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'CanonicalUrl';
    public $cache = true;

    /**
     * @see MySQLModel::$validOrderByExpressions
     * @var array
     */
    public $validOrderByExpressions = array(
        'id',
        'url',
        'seo_title',
        'seo_description',
        'created',
        'modified',
    );

    /**
     * @see MySQLModel::$validation
     * @var array
     */
    public $validation = array(
        'url' => array(
            'required' => 'URL is required.',
            'notempty' => 'URL cannot be left empty.',
            'unique' => 'There is already an record with that URL.',
        ),
    );

    /**
     * @see MySQLModel::$belongsTo
     * @var array
     */
    public $belongsTo = array(
        'Picture' => array(
            'model' => 'Picture',
            'foreign_key' => 'picture_id',
        ),
    );
}