<?php

/**
 * Class Behavior
 *
 * Behavior extend MySQLModel public functionalities.
 */
class Behavior {
    /**
     * Reference to the extended model
     *
     * @var MySQLModel
     */
    public $model = null;

    /**
     * Initialization options
     *
     * @var array
     */
    public $options = null;

    /**
     * Initializes the behavior with a reference to the extended model.
     *
     * @param $model MySQLModel reference to extended model
     * @param $options array initialization options
     *
     * @return void
     */
    public function initialize(&$model, $options = array()) {
        // Store params
        $this->model = $model;
        $this->options = $options;
    }

    /**
     * Before validate callback.
     * Executed before validation on updateFields and addNew calls.
     *
     * @param $element array element being updated
     * @param $values array values being updated
     *
     * @return bool
     */
    public function beforeValidate($element, &$values, $options) {
        return true;
    }

    /**
     * Validates provided values.
     *
     * @param $element array element being updated
     * @param $values array values being updated
     *
     * @return bool validates or not
     */
    public function validates($element, &$values, $options) {
        return true;
    }

    /**
     * Before save callback
     *
     * @param $element array. Element that's being
     * updated/created.
     * @param $values array. Values to be updated.
     *
     * @return bool continue or not
     */
    public function beforeSave($element, &$values, $options) {
        return true;
    }

    /**
     * After save callback.
     *
     * @param $created bool whether or not the record whas just created
     * @param $element array updated record
     * @param $values array updated values
     * @param $originalElement array element before upadte
     *
     * @return void
     */
    public function afterSave($created, $element, $values, $originalElement, $options) {
    }

    /**
     * Before hide callback.
     *
     * @param $element array element to be hiden.
     *
     * @return bool if not true, stops hidding
     */
    public function beforeHide($element, $options) {
        return true;
    }

    /**
     * After hide callback.
     *
     * @param $element array
     *
     * @return void
     */
    public function afterHide($element, $options) {
    }

    /**
     * Before delete callback.
     *
     * @param $element array element to be deleted.
     *
     * @return bool if not true, stops deleting
     */
    public function beforeDelete($element, $options) {
        return true;
    }

    /**
     * After delete callback.
     *
     * @param $element array
     *
     * @return void
     */
    public function afterDelete($element, $options) {
    }

    /**
     * Before get callback.
     * ATTENTION: this is the only 'before' callback that cannot break the execution of the call.
     *
     * @param $options array
     *
     * @return void
     */
    public function beforeGet(&$options) {
    }

    /**
     * After get callback.
     *
     * @param $element array the element retrieved
     * @param $options
     *
     * @return void
     */
    public function afterGet(&$element, $options = array()) {
    }

    /**
     * After get callback.
     *
     * @param $element array the element retrieved
     * @param $options
     *
     * @return void
     */
    public function afterGetWithRelationships(&$element, $options = array()) {
    }

    /**
     * Shortcut to behavior initialization
     *
     * @param $behavior_name
     * @param $model
     * @param $options
     *
     * @return mixed
     */
    public static function getInstance($behavior_name, &$model, $options) {
        // Model file name
        $behavior_filename = null;

        // Main app behavior
        if (file_exists(INCLUDE_PATH . 'models/behaviors/' . $behavior_name . '.php')) {
            $behavior_filename = INCLUDE_PATH . 'models/behaviors/' . $behavior_name . '.php';
        }

        // Module behavior
        $modules = GestyMVC::config('modules');
        if ($modules) {
            foreach ($modules as $module) {
                if (file_exists(INCLUDE_PATH . 'modules/' . $module . '/models/behaviors/' . $behavior_name . '.php')) {
                    $behavior_filename = INCLUDE_PATH . 'modules/' . $module . '/models/behaviors/' . $behavior_name . '.php';
                    break;
                }
            }
        }

        // Core behavior
        if (!$behavior_filename) {
            $behavior_filename = CORE_PATH . 'model/default/behaviors/' . $behavior_name . '.php';
        }

        // Check if file exist
        if (file_exists($behavior_filename)) {
            // Include
            include_once($behavior_filename);

            // Check class
            if (class_exists($behavior_name . 'Behavior')) {
                // Set class name
                $class_name = $behavior_name . 'Behavior';

                // Initialize class
                $instance = new $class_name();
                $instance->initialize($model, $options);

                // Return generated instance
                return $instance;
            } else {
                // Class not found, crash
                Debug::crash(true, 'File ' . $behavior_filename . ' does not define behavior ' . $behavior_name . 'Behavior class.');
            }
        }

        // File not found, crash
        Debug::crash(true, 'File ' . $behavior_filename . ' does not exist.');
    }
}