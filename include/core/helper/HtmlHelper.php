<?php

/**
 * Helper for creating generic HTML elements.
 *
 * Generates HTMl for views.
 */
class HtmlHelper {
    /**
     * Whether or not a flash message has ben painted on screen.
     */
    private static $flash_painted = false;

    /**
     * Generates a list of HTML attributes for a tag
     * based on an array of options
     *
     * @param $options array list of options (it may be
     * a nested array)
     * @param $ignoreOptions array one-dimention array
     * of attributes to ignore from
     * the $options array
     *
     * @return string
     */
    static function generateAttr($options, $ignoreOptions = array()) {
        // Check provided data
        if (!$options || !is_array($options)) {
            // If empty, no attributes to generate
            return '';
        }

        // Check for options existing on the ignore options
        // array
        foreach ($options as $name => $value) {
            if (in_array($name, $ignoreOptions)) {
                unset($options[$name]);
            }
        }

        // Sanitize known attributes
        if (isset($options['styleArray'])) {
            // New string styles
            $style = '';

            // Concat
            foreach ($options['styleArray'] as $property => $value) {
                $style .= $property . ': ' . $value . '; ';
            }

            // Prepend to string style option
            $options['style'] = $style . (isset($options['style']) ? $options['style'] : '');

            // Remove styleArray from options
            unset($options['styleArray']);
        }

        // Flattern $options array
        $options = flattern_nested_array($options, '-');

        // Attributes array
        $attrs = array();

        // Check for options existing on the ignore options
        // array
        foreach ($options as $name => $value) {
            if (in_array($name, array('disabled', 'readonly', 'checked')) && $value === false) {
                continue;
            }

            if (!in_array($name, $ignoreOptions)) {
                // Add to attributes
                if ($value === null) {
                    $attrs[] = $name;
                } else {
                    $attrs[] = $name . "=\"" . utf8html($value) . "\"";
                }
            }
        }

        // Return attributes HTML
        return implode(' ', $attrs);
    }

    /**
     * Generates the code for an HTML anchorlink <a>
     *
     * @param $content string HTML content for the <a>
     * tag
     * @param $href string|array href attribute
     * @param $options array list of selected options
     *
     * @return string html
     */
    static function link($content, $href, $options = array()) {
        // Retrieve href from options if not set directly
        if (!$href && isset($options['href'])) {
            $href = $options['href'];
        }

        // Add javascript:void(0) as href if href is empty
        // and onclick is not
        if (!$href && isset($options['onclick'])) {
            $href = 'javascript:void(0)';
        }

        // Convert relative link into full absolute link
        $href = Router::url($href);

        // If +class is set, add classes to existing ones
        if (isset($options['+class'])) {
            // Append to class and trim
            $options['class'] = trim((isset($options['class']) ? $options['class'] . ' ' : '') . $options['+class']);

            // Remove +class option
            unset($options['+class']);
        }

        // The confirm message option adds a javascript
        // confirmation on click. They may
        // be incopatibilities between the confirm_message
        // option and the onclick option
        if (isset($options['confirm_message']) && $options['confirm_message']) {
            // Set onclick options if it does not exists
            if (!isset($options['onclick'])) {
                $options['onclick'] = '';
            }

            // Trim onclick contents
            $options['onclick'] = trim($options['onclick']);

            // If javascript onclick contents do not end on
            // semi colon, add it
            if ($options['onclick'] && substr($options['onclick'], -1, 1) != ';') {
                $options['onclick'] .= ';';
            }

            // Add onclick confirm message
            $options['onclick'] .= 'return confirm(\'' . str_replace(array(
                                                                         "'",
                                                                         "\"",
                                                                     ), array(
                                                                         "\\'",
                                                                         "\\\"",
                                                                     ), $options['confirm_message']) . '\')';

            // Unset option
            unset($options['confirm_message']);
        }

        // If link is to be represented as a button instead
        // of a single link (avoids
        // crawlers from excecuting then).
        if ((isset($options['method']) && $options['method'] == 'post') || (isset($options['type']) && $options['type'] == 'button')) {
            // Clear method option
            unset($options['method']);
            unset($options['type']);

            // Generate minimal form
            return '<form class="inline" method="post" action="' . $href . '">' . FormHelper::submit($content, $options) . '</form>';
        }

        // Set href parameter as option
        $options['href'] = $href;

        // Generate HTML attributes from options
        $attrs = HtmlHelper::generateAttr($options, array(
            'content',
            'escape',
        ));

        // If escape is set to true (default behaviour)
        // html encode the contents
        if (!isset($options['escape']) || $options['escape']) {
            $content = utf8html($content);
        }

        // Return HTML anchor link tag
        return "<a {$attrs}>{$content}</a>";
    }

    /**
     * Generates the code for an HTML div
     *
     * @param $content string HTML content for the
     * div tag
     * @param $options array list of selected options
     * and HTML attributes
     *
     * @return string HTML
     */
    static function div($content, $options = array()) {
        if (!isset($options['escape'])) {
            $options['escape'] = true;
        }

        return HtmlHelper::text('div', $content, $options);
    }

    /**
     * Generates the code for an HTML text tag
     *
     * @param $tag_name string HTML tag name
     * @param $content string HTML content for the tag
     * @param $options array list of selected options
     * and HTML attributes
     *
     * @return string HTML
     */
    static function text($tag_name, $content, $options = array()) {
        // If the option +class is present, it will be
        // appended to the default class to allow further
        // css customization
        if (isset($options['+class'])) {
            // Append +class to class
            if (!isset($options['class']) || !$options['class']) {
                $options['class'] = $options['+class'];
            } else {
                $options['class'] .= ' ' . $options['+class'];
            }

            // Clear extra spaces
            $options['class'] = trim($options['class']);

            // Remove +class attribute (already proccessed)
            unset($options['+class']);
        }

        // Generate HTML tag attributes, ignore content and
        // escape
        $attrs = HtmlHelper::generateAttr($options, array(
            'content',
            'escape',
            'self_closing',
        ));

        // If escape is not set or true, transform special
        // chars to HTML
        if (!isset($options['escape']) || $options['escape']) {
            $content = utf8html($content);
        }

        // Generate HTML and return it
        if (!isset($options['self_closing']) || !$options['self_closing'] || $content) {
            return '<' . $tag_name . ' ' . $attrs . '>' . $content . '</' . $tag_name . '>';
        } else {
            return '<' . $tag_name . ' ' . $attrs . ' />';
        }
    }

    /**
     * Prints the flash message.
     *
     * @param array $options
     *
     * @return string html
     */
    static function flashMessage($options = array()) {
        // Avoid param type errors
        if (!is_array($options)) {
            $options = array();
        }

        // Retrieve content from session
        $content = Session::get('flash[message]');

        // Clear session flash
        Session::set('flash', null);

        if ($content) {
            // Set flash paited
            HtmlHelper::$flash_painted = true;

            // Default container configuration
            if (!isset($options['container'])) {
                $options['container'] = array();
            }

            if ($options['container'] !== false) {
                if (!isset($options['container']['tag'])) {
                    $options['container']['tag'] = 'div';
                }
                if (!isset($options['container']['class'])) {
                    $options['container']['class'] = 'alert alert-info';
                }

                // For convention purposes, accept 'div' option
                if (isset($options['div'])) {
                    if ($options['div'] !== false) {
                        $options['container'] = array_merge($options['container'], $options['div']);
                        $options['container']['tag'] = 'div';

                        unset($options['div']);
                    } else {
                        $options['container'] = false;
                    }
                }
            }

            // Wrap in container if required
            if ($options['container'] !== false) {
                // Escape by default
                if (!isset($options['container']['escape'])) {
                    $options['container']['escape'] = (!isset($options['escape']) || $options['escape']);
                }

                return HtmlHelper::text($options['container']['tag'], $content, $options['container']);
            } else {
                if (!isset($options['escape']) || $options['escape']) {
                    $content = utf8html($content);
                }

                return $content;
            }
        } else {
            // If message is empty, do not return anything
            return '';
        }
    }

    /**
     * Generates a link to a css file.
     *
     * @param $url string
     * @param $options array
     *  - cache_timestamp: add a timestamp to the URL to avoid caching
     *
     * @return string
     */
    public static function css($url, $options = array()) {
        // Convert URL
        if (!is_array($url)) {
            $url = array('url' => $url);
        }

        if (!starts_with($url['url'], '/')) {
            // Check for protocol
            $protocol_pos = strpos($url['url'], ':');

            if ($protocol_pos === false || $protocol_pos > 10) {
                // If there is no protocol, it's a relative link
                $url['url'] = '/' . CSS_FOLDER . '/' . $url['url'];

                // Set cache as default
                if (!isset($options['cache_timestamp'])) {
                    $options['cache_timestamp'] = true;
                }
            } else {
                // If there is, it's an absolute link
                if (!isset($url['no_session'])) {
                    $url['no_session'] = false;
                }
            }
        }

        // Set no session
        if (isset($options['no_session'])) {
            $url['no_session'] = $options['no_session'];
        } elseif (!isset($url['no_session'])) {
            $url['no_session'] = true;
        }

        // If requested add cache timestamp
        if (isset($options['cache_timestamp']) && $options['cache_timestamp']) {
            if (!isset($url['?'])) {
                $url['?'] = array();
            }
            $url['?']['ts'] = GestyMVC::config('static_cache_ts');
        }

        // Generate URL
        $url = Router::url($url);

        // Generate tag
        return HtmlHelper::text('link', '', array('href' => $url, 'rel' => 'stylesheet', 'self_closing' => true));
    }

    /**
     * Generates a external script inclusion tag.
     *
     * @param $url string
     * @param $options array
     *  - cache_timestamp: add a timestamp to the URL to avoid caching
     *
     * @return string
     */
    public static function js($url, $options = array()) {
        $options = array_merge(array('src' => null, 'type' => 'text/javascript'), $options);

        // Convert URL
        if (!is_array($url)) {
            $url = array('url' => $url);
        }

        if (!starts_with($url['url'], '/')) {
            // Check for protocol
            $protocol_pos = strpos($url['url'], ':');

            if ($protocol_pos === false || $protocol_pos > 10) {
                // If there is no protocol, it's a relative link
                $url['url'] = '/' . JS_FOLDER . '/' . $url['url'];

                // Set cache as default
                if (!isset($options['cache_timestamp'])) {
                    $options['cache_timestamp'] = true;
                }
            } else {
                // If there is, it's an absolute link
                if (!isset($url['no_session'])) {
                    $url['no_session'] = false;
                }
            }
        }

        // Set no session
        if (isset($options['no_session'])) {
            $url['no_session'] = $options['no_session'];
        } elseif (!isset($url['no_session'])) {
            $url['no_session'] = true;
        }

        // If requested add cache timestamp
        if (isset($options['cache_timestamp']) && $options['cache_timestamp']) {
            if (!isset($url['?'])) {
                $url['?'] = array();
            }

            $url['?']['ts'] = GestyMVC::config('static_cache_ts');
        }

        // Generate URL
        $url = Router::url($url);
        $options['src'] = $url;

        // Clean non-attr options
        unset($options['cache_timestamp']);
        unset($options['no_session']);

        // Generate tag
        return HtmlHelper::text('script', '', $options);
    }

    /**
     * Generates a link to a favicon file.
     *
     * @param $url string
     * @param $options array
     *  - cache_timestamp: add a timestamp to the URL to avoid caching
     *
     * @return string
     */
    public static function favicon($url, $options = array()) {
        // Convert URL
        if (!is_array($url)) {
            $url = array('url' => $url);
        }

        if (!starts_with($url['url'], '/')) {
            // Check for protocol
            $protocol_pos = strpos($url['url'], ':');

            if ($protocol_pos === false || $protocol_pos > 10) {
                // If there is no protocol, it's a relative link
                $url['url'] = '/' . $url['url'];
            } else {
                // If there is, it's an absolute link
                if (!isset($url['no_session'])) {
                    $url['no_session'] = false;
                }
            }
        }

        // Set no session
        if (isset($options['no_session'])) {
            $url['no_session'] = $options['no_session'];
        } elseif (!isset($url['no_session'])) {
            $url['no_session'] = true;
        }

        // If requested add cache timestamp
        if (isset($options['cache_timestamp']) && $options['cache_timestamp']) {
            if (!isset($url['?'])) {
                $url['?'] = array();
            }
            $url['?']['ts'] = GestyMVC::config('static_cache_ts');
        }

        // Generate URL
        $url = Router::url($url);

        // Generate tag
        return HtmlHelper::text('link', '', array(
            'href' => $url,
            'rel' => 'shortcut icon',
            'type' => 'image/x-icon',
            'self_closing' => true,
        ));
    }

    /**
     * HtmlHelper::$flash_painted getter.
     *
     * @return bool
     */
    public static function flashPainted() {
        return HtmlHelper::$flash_painted;
    }
}
