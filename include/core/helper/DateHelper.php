<?php

/**
 * Helper for displaying readable dates.
 */
class DateHelper {
    /**
     * Returns a readable date relative to today if the difference is one day or less.
     * - Today at 10:30
     * - Tomorrow at 10:30
     * - Yesterday at 10:30
     * - Saturday August 17th 2016 at 10:30
     * - Saturday Aug 17th 2016 at 10:30
     * - Today
     * - Tomorrow
     * - Yesterday
     * - Saturday August 2016 17th
     * - Saturday Aug 2016 17th
     *
     * @param $date_str string datetime in mysql format
     * @param $time bool whether or not to include the time (default true)
     * @param $short_month bool whether or not to shorten the month (default true)
     *
     * @return string
     */
    public static function simpleDate($date_str, $time = true, $short_month = true) {
        // Convert to linux time
        $date_t = strtotime($date_str);

        // Check proximity
        $is_today = date('Y-m-d') == date('Y-m-d', $date_t);
        $is_yesterday = date('Y-m-d', strtotime('yesterday')) == date('Y-m-d', $date_t);
        $is_tomorrow = date('Y-m-d', strtotime('tomorrow')) == date('Y-m-d', $date_t);

        // Format date
        if ($is_today) {
            $date_formatted = __('today', true);
        } elseif ($is_yesterday) {
            $date_formatted = __('yesterday', true);
        } elseif ($is_tomorrow) {
            $date_formatted = __('tomorrow', true);
        } else {
            $inlineMonths = array(
                0 => __('January {inline}', true),
                1 => __('February {inline}', true),
                2 => __('March {inline}', true),
                3 => __('April {inline}', true),
                4 => __('May {inline}', true),
                5 => __('June {inline}', true),
                6 => __('July {inline}', true),
                7 => __('August {inline}', true),
                8 => __('September {inline}', true),
                9 => __('October {inline}', true),
                10 => __('November {inline}', true),
                11 => __('December {inline}', true),
            );

            $month = $inlineMonths[intval(date('m', $date_t)) - 1];

            if ($short_month && mb_strlen($month, 'UTF-8') > 3) {
                $month = mb_substr($month, 0, 3, 'UTF-8');
            }

            $inlineDaysOfWeek = array(
                0 => __('Monday {inline}', true),
                1 => __('Tuesday {inline}', true),
                2 => __('Wednesday {inline}', true),
                3 => __('Thursday {inline}', true),
                4 => __('Friday {inline}', true),
                5 => __('Saturday {inline}', true),
                6 => __('Sunday {inline}', true),
            );

            $dow = $inlineDaysOfWeek[intval(date('N', $date_t)) - 1];
            $day_number = date('j', $date_t);

            if ($day_number == 1) {
                $day_of_month = __('%month% %day%st', true);
            } elseif ($day_number == 2) {
                $day_of_month = __('%month% %day%nd', true);
            } elseif ($day_number == 3) {
                $day_of_month = __('%month% %day%rd', true);
            } else {
                $day_of_month = __('%month% %day%th', true);
            }

            $month_of_year = __('%s of %s {month of year}', true);

            $day_of_month = str_replace(array('%month%', '%day%'), array($month, $day_number), $day_of_month);
            $date_formatted = $dow . ' ' . sprintf($month_of_year, $day_of_month, date('Y', $date_t));
        }

        // Return read date
        if ($time) {
            // Format time
            $time_formatted = date('H:i', $date_t);

            return sprintf(__('%s at {time} %s', true), $date_formatted, $time_formatted);
        } else {
            return $date_formatted;
        }
    }
}
