<?php

/**
 * Generates HTML code for pagination
 */
class PaginatorHelper {
    /**
     * Generates reordering URL by a field.
     *
     * @param $pagination
     * @param $expression string expression to order by
     * @param $direction string direction to order by
     * (ASC/DESC)
     *
     * @return string
     */
    static function getReorderUrl($pagination, $expression, $direction) {
        // Remove page. If field ordering is changed, page
        // have to be reset to first.
        unset($pagination['baseUrl']['?']['p']);

        // Reset order in query string vars
        $pagination['baseUrl']['?']['order_by'] = array();

        if ($pagination['accumulate_orders']) {
            // Change order in query string vars
            if ($direction) {
                $pagination['baseUrl']['?']['order_by'][] = $expression . '+' . $direction;
            }

            foreach ($pagination['order'] as $order) {
                if ($expression != $order['expression']) {
                    $pagination['baseUrl']['?']['order_by'][] = $order['expression'] . '+' . $order['direction'];
                }
            }
        } else {
            $pagination['baseUrl']['?']['order_by'][] = $expression . '+' . $direction;
        }

        // Return full URL
        return Router::url($pagination['baseUrl']);
    }

    /**
     * Generates reordering data by an AJAX field.
     *
     * @param $pagination
     * @param $expression string expression to order by
     * @param $direction string direction to order by
     * (ASC/DESC)
     *
     * @return string
     */
    static function getReorderAjaxData($pagination, $expression, $direction) {
        // Reset order in query string vars
        $orderBy = array();

        if ($pagination['accumulate_orders']) {
            // Change order in query string vars
            if ($direction) {
                $orderBy[] = $expression . '+' . $direction;
            }

            foreach ($pagination['order'] as $order) {
                if ($expression != $order['expression']) {
                    $orderBy[] = $order['expression'] . '+' . $order['direction'];
                }
            }
        } else {
            $orderBy[] = $expression . '+' . $direction;
        }

        // Return full URL
        return '[' . join(',', $orderBy) . ']';
    }

    /**
     * Generates HTML anchor link to reorder the list.
     * Reverses order if the list if already ordered by
     * that field.
     *
     * Commonly used as header for table.
     *
     * @param $pagination
     *
     * @param $text string display text for the anchor
     * (usually)
     * @param $expression
     * @param bool $generate_url
     * @return string
     */
    static function reorder($pagination, $text, $expression, $generate_url = true) {
        // Default direction is ASC
        $new_direction = 'ASC';

        $ordering_class = '';

        foreach ($pagination['order'] as $order) {
            // Check if we are already ordering by this
            // expression
            // Set ordering

            if ($order['expression'] == $expression) {
                // Ordering by this field

                $order['direction'] = strtoupper($order['direction']);

                // Reverse direction
                if ($order['direction'] == 'ASC') {
                    $ordering_class = 'asc';
                    if ($pagination['default_order_by_expression'] == $order['expression'] && $pagination['default_order_by_direction'] == 'DESC') {
                        $new_direction = false;
                    } else {
                        $new_direction = 'DESC';
                    }
                }

                // Reverse direction
                if ($order['direction'] == 'DESC') {
                    $ordering_class = 'desc';
                    if ($pagination['default_order_by_expression'] == $order['expression'] && $pagination['default_order_by_direction'] == 'DESC') {
                        $new_direction = 'ASC';
                    } else {
                        $new_direction = false;
                    }
                }
            }
        }

        // Get URL
        $url = ($generate_url ? PaginatorHelper::getReorderUrl($pagination, $expression, $new_direction) : 'javascript:void(0)');
        $data = PaginatorHelper::getReorderAjaxData($pagination, $expression, $new_direction);

        // Generate link
        return HtmlHelper::link($text, $url, array(
            'class' => 'reorder',
            '+class' => $ordering_class,
            'data' => array('order_by' => $data),
        ));
    }

    /**
     * Generates a dictionary with the page number as
     * key and the URL as value based on
     * $data['pagination']
     *
     * @param $pagination
     * @param $propagate_get
     *
     * @return array Page2URL
     */
    static function getPageUrls($pagination, $propagate_get = true) {
        // Page2URL dictionary for URLs (page => URL)
        $urls = array();

        // Add URL for each page
        for ($p = 1; $p <= $pagination['pages']; $p++) {
            // Retrieve query string vars
            $pagination_ = $pagination;

            if (!$propagate_get) {
                $pagination_['baseUrl']['?'] = array();
            }

            // Remore page if page is 1, replace with page
            // number if bigger
            if ($p == 1) {
                unset($pagination_['baseUrl']['?']['p']);
            } else {
                $pagination_['baseUrl']['?']['p'] = $p;
            }

            // Add to dictionary
            $urls[$p] = Router::url($pagination_['baseUrl']);
        }

        // Return dictionary
        return $urls;
    }

    /**
     * Generates headers
     *
     * @param $pagination
     * @param $propagate_get
     *
     * @return string HTML headers
     */
    static function getHeaders($pagination, $propagate_get = true) {
        $urls = self::getPageUrls($pagination, $propagate_get);
        $html = '';

        if (isset($urls[$pagination['page'] - 1])) {
            $html .= '<link rel="prev" href="' . utf8html($urls[$pagination['page'] - 1]) . '" />';
        }

        if (isset($urls[$pagination['page'] + 1])) {
            $html .= '<link rel="next" href="' . utf8html($urls[$pagination['page'] + 1]) . '" />';
        }

        return $html;
    }

    /**
     * Generates HTML for pagination based on
     * $data['pagination'] values.
     *
     * @param $pagination
     *
     * @return string HTML
     */
    static function links($pagination) {
        // Navigation links html
        $links_html = '';

        // Generate HTML only if there is more than one
        // page
        if ($pagination['pages'] > 1) {
            // Retrieve URLs
            $page2URL = PaginatorHelper::getPageUrls($pagination);

            // Initialize HTML tags
            $links_html .= '<div class="row pagination-container">';
            $links_html .= "\t" . '<div class="col-sm-6">';
            $links_html .= "\t\t" . '<div class="dataTables_info">' . sprintf(__('Page %s of %s', true), $pagination['page'], $pagination['pages']) . '</div>';
            $links_html .= "\t" . '</div>';
            $links_html .= "\t" . '<div class="col-sm-6">';
            $links_html .= "\t\t" . '<div class="dataTables_paginate paging_simple_numbers">';
            $links_html .= "\t\t\t" . '<ul class="pagination">';

            // Add first links to pagination menu
            if ($pagination['page'] == 1) {
                // If the first page is currently displaying, show
                // spans, not links
                $links_html .= "\t\t\t\t" . '<li class="paginate_button previous disabled" tabindex="0"><a href="javascript:void(0)">' . __('Previous {pagination}', true) . '</a></li>';
            } else {
                // Add link to previous page
                $links_html .= "\t\t\t\t" . '<li class="paginate_button previous" tabindex="0"><a href="' . $page2URL[$pagination['page'] - 1] . '">' . __('Previous {pagination}', true) . '</a></li>';
            }

            // Add up to 10 links, starting -5 from current or
            // 1, whichever is greater
            $starting_page = ($pagination['page'] > 6 ? $pagination['page'] - 5 : 1);
            $ending_page = ($pagination['pages'] > $starting_page + 9 ? $starting_page + 9 : $pagination['pages']);

            // For each page, add link
            for ($p = $starting_page; $p <= $ending_page; $p++) {
                if ($p == $pagination['page']) {
                    $links_html .= "\t\t\t\t" . '<li class="paginate_button" tabindex="0"><a href="javascript:void(0)">' . $p . '</a></li>';
                } else {
                    $links_html .= "\t\t\t\t" . '<li class="paginate_button active" tabindex="0"><a href="' . $page2URL[$p] . '">' . $p . '</a></li>';
                }
            }

            // Add last links to pagination menu
            if ($pagination['page'] == $pagination['pages']) {
                // If the last page is currently displaying, show
                // spans, not links
                $links_html .= "\t\t\t\t" . '<li class="paginate_button next disabled" tabindex="0"><a href="javascript:void(0)">' . __('Next {pagination}', true) . '</a></li>';
            } else {
                // Add link to next page
                $links_html .= "\t\t\t\t" . '<li class="paginate_button next" tabindex="0"><a href="' . $page2URL[$pagination['page'] + 1] . '">' . __('Next {pagination}', true) . '</a></li>';
            }

            // Finalize HTML tags
            $links_html .= "\t\t\t" . '</ul>';
            $links_html .= "\t\t" . '</div>';
            $links_html .= "\t" . '</div>';
            $links_html .= '</div>';
        }

        // Return generated HTML
        return $links_html;
    }

}
