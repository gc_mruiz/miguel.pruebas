<?php

/**
 * Helper for creating HTML image elements and
 * handling them.
 */
class ImageHelper {
    /**
     * Generates the HTML code for displaying an image
     *
     * @param $image array('src' =>, 'alt' =>, 'width'
     * =>, 'height' =>) or string image URL (absolute
     * or relative to /img/)
     * @param $options array
     *  'edit*' => options for inline editing,
     *  '*' => HTML attributes
     *
     * @return string html
     */
    static function image($image, $options = array()) {
        // If image is not an array, we asume it's an URL
        if (!is_array($image)) {
            // Generate dummy array with image as src
            $image = array(
                'src' => $image,
                'alt' => '',
                'width' => 1,
                'height' => 1,
            );
        } else {
            // Remove index if it comes straight from database
            if (isset($image['Picture'])) {
                $image = $image['Picture'];
            }

            // Fill options with image properties
            $options['alt'] = (isset($options['alt']) ? $options['alt'] : $image['alt']);
            $options['width'] = (isset($options['width']) ? $options['width'] : $image['width']);
            $options['height'] = (isset($options['height']) ? $options['height'] : $image['height']);
        }

        // Set src option
        $options['src'] = (isset($options['src']) ? $options['src'] : $image['src']);

        // Convert src URL in a full url (non-relative)
        $protocol_pos = strpos($image['src'], '://');
        if ($protocol_pos === false || $protocol_pos > 10) {
            // If src has no protocol, it's not an absolute
            // link
            if (strpos($image['src'], '/') === 0) {
                // If src is set to the root of the server, add
                // server root
                $image['src'] = ROOT_URL . substr($image['src'], 1);
            } else {
                if (strpos($image['src'], 'img/') === 0) {
                    // If src includes img folder, remove it
                    $image['src'] = substr($image['src'], 4);
                }

                // We asume the src is set relative to the img
                // folder contents, add full img
                // folder URL
                $image['src'] = ROOT_URL . 'img/' . $image['src'];
            }
        }

        // If edit options are set, add rel values for CMS
        if (isset($options['edit']) && $options['edit']) {
            // Set rel and class
            $options['rel'] = $options['edit_model'] . '/' . $options['edit_id'] . '/' . $options['edit_field'] . '/picture/' . $options['edit_picture_mode'];
            $options['class'] = (isset($options['class']) ? $options['class'] . ' editable' : 'editable');

            // Unset edit fields
            unset($options['edit']);
            unset($options['edit_model']);
            unset($options['edit_id']);
            unset($options['edit_field']);
            unset($options['edit_picture_mode']);
        }

        // Return img HTML
        return '<img ' . HtmlHelper::generateAttr($options) . '>';
    }

    /**
     * Generates HTML code to display an image scaled
     * and centered or cropped inside
     * a div
     *
     * @param $image array image data
     * @param $container_width int width of container
     * to fit in
     * @param $container_height int height of container
     * to fit in
     * @param $crop boolean whether or not to crop
     * @param $options array selected options
     *
     * @return string html
     */
    static function contained($image, $container_width, $container_height, $crop = false, $options = array()) {
        // If image is not an array, we asume it's an URL
        if (!is_array($image)) {
            $image = array(
                'src' => $image,
                'alt' => '',
                'width' => 1,
                'height' => 1,
            );
        } elseif (isset($image['Picture'])) {
            $image = $image['Picture'];
        }

        // Calculate container ratio vs image ratio
        $container_ratio = $container_width / $container_height;
        $image_ratio = $image['width'] / $image['height'];

        // Check if its the with the constraining dimention
        // for scaling
        $constrain_to_width = $image['width'] > $image['height'] * $container_ratio;

        // If image is to be cropped, reverse the
        // constraining dimention
        if ($crop) {
            $constrain_to_width = !$constrain_to_width;
        }

        // Extract div options from option array
        $divOptions = (isset($options['div']) ? $options['div'] : array());
        unset($options['div']);

        // Extract link options from option array
        $linkOptions = (isset($options['link']) ? $options['link'] : array());
        unset($options['link']);

        // Set image style array if not set
        if (!isset($options['styleArray'])) {
            $options['styleArray'] = array();
        }

        // Calculate new width and height based on
        // canstraints
        if ($constrain_to_width) {
            $options['styleArray']['width'] = round($container_width);
            $options['styleArray']['height'] = round($options['styleArray']['width'] / $image_ratio);
        } else {
            $options['styleArray']['height'] = round($container_height);
            $options['styleArray']['width'] = round($options['styleArray']['height'] * $image_ratio);
        }

        // Calculate required margin top and height
        $options['styleArray']['margin-left'] = ($container_width - $options['styleArray']['width']) / 2;
        $options['styleArray']['margin-top'] = ($container_height - $options['styleArray']['height']) / 2;

        // Add unit to style properties
        $options['styleArray']['width'] = (($options['styleArray']['width'] / $container_width) * 100) . '%';
        $options['styleArray']['height'] = (($options['styleArray']['height'] / $container_height) * 100) . '%';
        $options['styleArray']['margin-left'] = (($options['styleArray']['margin-left'] / $container_width) * 100) . '%';
        $options['styleArray']['margin-top'] = (($options['styleArray']['margin-top'] / $container_width) * 100) . '%';

        // Replace image URL by thumb if required
        if ($container_width < 320 && $container_height < 320 && (!isset($options['thumb']) || $options['thumb'] == true) && isset($image['id']) && isset($image['320_token'])) {
            $image['src'] = Router::url(array(
                                            'controller' => 'Pictures',
                                            'action' => 'thumb',
                                            320,
                                            $image['id'],
                                            $image['320_token'],
                                            'no_session' => true,
                                        ));
        } elseif ($container_width < 640 && $container_height < 640 && $options['styleArray']['height'] < 640 && (!isset($options['thumb']) || $options['thumb'] == true) && isset($image['id']) && isset($image['640_token'])) {
            $image['src'] = Router::url(array(
                                            'controller' => 'Pictures',
                                            'action' => 'thumb',
                                            640,
                                            $image['id'],
                                            $image['640_token'],
                                            'no_session' => true,
                                        ));
        }

        // If image is on edition mode, add deafult
        // required data
        if (isset($options['edit']) && $options['edit']) {
            // Add default field 'picture_id'
            if (!isset($options['edit_field'])) {
                $options['edit_field'] = 'picture_id';
            }

            // Add default mode to cropped or centered
            if (!isset($options['edit_picture_mode'])) {
                $options['edit_picture_mode'] = ($crop ? 'cropped' : 'centered');
            }
        }

        // Generate image HTML
        $html = self::image($image, $options);

        // Encapsulate on div if requested
        if ($linkOptions) {
            // Add no-escape option to link
            $linkOptions['escape'] = false;

            // Encapsulate HTML on a link
            $html = HtmlHelper::link($html, 'javascript:void(0)', $linkOptions);
        }

        // Encapsulate on div if requested (default
        // behaviour)
        if (!isset($options['div']) || $options['div'] !== false) {
            // Add no-escape option to link
            $divOptions['escape'] = false;

            // Set div style options if not set
            if (!isset($divOptions['styleArray'])) {
                $divOptions['styleArray'] = array();
            }

            // Add style options to DIV
            $divOptions['styleArray']['position'] = 'relative';
            $divOptions['styleArray']['width'] = $container_width . 'px';
            $divOptions['styleArray']['height'] = $container_height . 'px';
            $divOptions['styleArray']['overflow'] = 'hidden';

            // Encapsulate HTML on a div
            $html = HtmlHelper::div($html, $divOptions);
        }

        // Return resultant HTML
        return $html;
    }

    /**
     * Generates HTML code to display an image scaled
     * and centered inside a div. Shortcut to ::contained() with mode 'centered'.
     *
     * @param $image array image data
     * @param $container_width int width of container
     * to fit in
     * @param $container_height int height of container
     * to fit in
     * @param $options array selected options
     *
     * @return string html
     */
    static function centered($image, $container_width, $container_height, $options = array()) {
        return self::contained($image, $container_width, $container_height, false, $options);
    }

    /**
     * Generates HTML code to display an image scaled
     * and cropped inside a div.  Shortcut to ::contained() with mode 'cropped'.
     *
     * @param $image array image data
     * @param $container_width int width of container
     * to fit in
     * @param $container_height int height of container
     * to fit in
     * @param $options array selected options
     *
     * @return string html
     */
    static function cropped($image, $container_width, $container_height, $options = array()) {
        return self::contained($image, $container_width, $container_height, true, $options);
    }

}
