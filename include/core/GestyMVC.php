<?php

/**
 * Configure class.
 *
 * Makes configuration info accessible for all the
 * application with static variables.
 */
class GestyMVC {
    /**
     * Application config variables. These vars are
     * overriden with /displatcher.php $config
     *
     * @type array
     */
    private static $configuration = array(
        'debug' => array(
            'force_for_all' => false,
            'forceFromIps' => false,
            'onlyFromIps' => false,
            'host_exact_match' => false,
            'host_pattern' => ':81',
            'check_session_digest' => true,
            'check_session_ip' => true,
            'check_session_user_agent' => true,
        ),
        'mysql_log_queries_slower_than_seconds' => 0.1,
        'cms_tags' => '<div><br><b><p><u><ul><li><ol><a><strong><em><i><img><h1><h2><h3><h4><h5><h6><span><table><tr><td><th>',
        'cacheAuth' => array('user' => 'u'),
    );

    private static $is_cli = null;

    public static function isCli() {
        if (self::$is_cli === null) {
            self::$is_cli = (php_sapi_name() == 'cli');
        }

        return self::$is_cli;
    }

    private static function loadCliVars() {
        if (self::isCli()) {
            $options = getopt('', array(
                'document_root:',
                'server_addr:',
                'host:',
                'url:',
                'https::',
                'post::',
            ));

            $_SERVER['REMOTE_ADDR'] = '127.0.0.1';

            if (isset($options['host'])) {
                $_SERVER['HTTP_HOST'] = $options['host'];
            }

            if (isset($options['document_root'])) {
                $_SERVER['DOCUMENT_ROOT'] = $options['document_root'];
            }

            if (isset($options['server_addr'])) {
                $_SERVER['SERVER_ADDR'] = $options['server_addr'];
            }

            if (isset($options['url'])) {
                $_SERVER['REQUEST_URI'] = $options['url'];

                if (strpos($_SERVER['REQUEST_URI'], '?') !== false) {
                    $_SERVER['QUERY_STRING'] = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?') + 1);
                } else {
                    $_SERVER['QUERY_STRING'] = '';
                }

                $_SERVER['QUERY_STRING'] = 'url=' . urlencode(substr($_SERVER['REQUEST_URI'], 1, strlen($_SERVER['REQUEST_URI']) - 1 - strlen($_SERVER['QUERY_STRING']) - (strlen($_SERVER['QUERY_STRING']) > 0 ? 1 : 0))) . ($_SERVER['QUERY_STRING'] ? '&' : '') . $_SERVER['QUERY_STRING'];
                parse_str($_SERVER['QUERY_STRING'], $_GET);
            }

            if (isset($options['https']) && $options['https'] == 'true') {
                $_SERVER['HTTPS'] = 'on';
            }

            if (isset($options['post'])) {
                $_POST = json_decode($options['post']);
            }
        }
    }

    /**
     * Stores configuration.
     *
     * @param $var string
     * @param $value mixed
     */
    public static function setConfig($var, $value) {
        return NestedVariable::set(self::$configuration, $var, $value);
    }

    /**
     * Retrieves configuration.
     *
     * @param $var string
     *
     * @return mixed
     */
    public static function config($var) {
        return NestedVariable::get(self::$configuration, $var);
    }

    /**
     * Retrieves the database configuration (dev or pro) based on Debug::debugging()
     *
     * @return array database config
     */
    public static function dbConfig() {
        // Retrieve database configuration
        $dbConfig = self::config('db');

        // If the website is running on debug mode, use
        // dev database configuration
        if (Debug::debugging()) {
            return $dbConfig['dev'];
        } else {
            return $dbConfig['pro'];
        }
    }

    /**
     * Detect and defines the path to the app's root folder, include and libs
     */
    public static function definePaths() {
        // Define ROOT_PATH
        $ROOT_PATH = substr(str_replace('\\', '/', __DIR__ . DIRECTORY_SEPARATOR), 0, -strlen('include/core/'));

        define('ROOT_PATH', $ROOT_PATH);
        unset($ROOT_PATH);

        // Define paths
        define('INCLUDE_PATH', ROOT_PATH . 'include/');
        define('CORE_PATH', INCLUDE_PATH . 'core/');
    }

    /**
     * Includes all required classes
     */
    public static function includeLibs() {
        // Load framework
        include_once(CORE_PATH . 'functions.php');
        include_once_full_directory(CORE_PATH . 'model');
        include_once_full_directory(CORE_PATH . 'view');
        include_once_full_directory(CORE_PATH . 'controller');
        include_once_full_directory(CORE_PATH . 'router');
        include_once_full_directory(CORE_PATH . 'session');
        include_once_full_directory(CORE_PATH . 'util');

        // Load app-specific extensions
        include_once(INCLUDE_PATH . 'AppController.php');
        include_once(INCLUDE_PATH . 'AppMySQLModel.php');
    }

    /**
     * Initialize method. Loads configuration.
     */
    public static function initialize() {
        // Load paths
        self::definePaths();

        // Include all libs
        self::includeLibs();

        // Initialize cli vars
        self::loadCliVars();

        // Initialize debugging
        $debugging = false;

        if (GestyMVC::config('debug[force_for_all]') || (GestyMVC::config('debug[forceFromIps]') !== false && in_array($_SERVER['REMOTE_ADDR'], GestyMVC::config('debug[forceFromIps]')))) {
            $debugging = true;
        } else {
            if (GestyMVC::config('debug[onlyFromIps]') === false || in_array($_SERVER['REMOTE_ADDR'], GestyMVC::config('debug[onlyFromIps]'))) {
                if (GestyMVC::config('debug[host_pattern]') !== false) {
                    if (GestyMVC::config('debug[host_exact_match]')) {
                        if ($_SERVER['HTTP_HOST'] === GestyMVC::config('debug[host_pattern]')) {
                            $debugging = true;
                        }
                    } else {
                        if (strpos($_SERVER['HTTP_HOST'], GestyMVC::config('debug[host_pattern]')) !== false) {
                            $debugging = true;
                        }
                    }
                }
            }
        }

        Debug::init($debugging);

        // Load config and module classes
        include_once(INCLUDE_PATH . 'config/config.php');
        include_once(INCLUDE_PATH . 'config/routes.php');
        self::loadModuleClassesAndConfig();

        // Set php time zone
        date_default_timezone_set(self::config('time_zone'));
    }

    private static function loadModuleClassesAndConfig() {
        $modules = self::config('modules');

        if ($modules) {
            foreach ($modules as $module) {
                $module_pascal = Inflector::pascalCase(str_replace('-', '_', $module));

                if (file_exists(INCLUDE_PATH . 'modules/' . $module . '/config/routes.php')) {
                    include_once(INCLUDE_PATH . 'modules/' . $module . '/config/routes.php');
                }
                if (file_exists(INCLUDE_PATH . 'modules/' . $module . '/' . $module_pascal . 'Controller.php')) {
                    include_once(INCLUDE_PATH . 'modules/' . $module . '/' . $module_pascal . 'Controller.php');
                }
                if (file_exists(INCLUDE_PATH . 'modules/' . $module . '/' . $module_pascal . 'MySQLModel.php')) {
                    include_once(INCLUDE_PATH . 'modules/' . $module . '/' . $module_pascal . 'MySQLModel.php');
                }
            }
        }
    }

    /** @var int */
    private static $starting_microtime = null;

    public static function elapsedMillis() {
        return ceil((microtime(true) - self::$starting_microtime) * 1000);
    }

    /**
     * Run. Startup method.
     *
     * Only method called from outside a class.
     */
    public static function run() {
        // Get current microtime (float)
        self::$starting_microtime = microtime(true);

        // Load configuration
        self::initialize();

        // Initialize session if not loaded from a session-less host name
        if (isset($_SERVER['HTTP_HOST']) && !in_array($_SERVER['HTTP_HOST'], array(
                self::config('production_no_session_host_name'),
                self::config('development_no_session_host_name'),
            ))
        ) {
            Session::init();
        }

        // Dispatch
        Dispatcher::dispatch();
    }

    public static function addModule($module_name) {
        if (file_exists(INCLUDE_PATH . 'modules/' . $module_name)) {
            $modules = self::config('modules');

            if (!$modules) {
                $modules = array();
            }

            $modules[] = $module_name;
            self::setConfig('modules', $modules);

            if (file_exists(INCLUDE_PATH . 'modules/' . $module_name . '/config/config.php')) {
                include_once(INCLUDE_PATH . 'modules/' . $module_name . '/config/config.php');
            }
        } else {
            Debug::crash(true, 'Module ' . $module_name . ' not found.');
        }
    }

    public static function addLibrary($library_name) {
        if (file_exists(INCLUDE_PATH . 'libs/' . $library_name . '.php')) {
            include_once(INCLUDE_PATH . 'libs/' . $library_name . '.php');
        } else {
            // Check whether the lib is in a module configuration
            $modules = self::config('modules');
            $module_is_found = false;

            if ($modules) {
                foreach ($modules as $module) {
                    if (file_exists(INCLUDE_PATH . 'modules/' . $module . '/libs/' . $library_name . '.php')) {
                        include_once(INCLUDE_PATH . 'modules/' . $module . '/libs/' . $library_name . '.php');

                        $module_is_found = true;
                        break;
                    }
                }
            }

            if (!$module_is_found) {
                Debug::crash(true, 'Library ' . $library_name . ' not found.');
            }
        }
    }
}