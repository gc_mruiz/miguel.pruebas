<?php

/**
 * Class Authentication
 *
 * Handles current request authentication.
 */
class Authentication {
    /**
     * Stores authentication related user data.
     *
     * @var array
     */
    private static $profiles = array();

    /**
     * Retrieves an authentication variable value by its name.
     *
     * @param $profile string
     * @param $var string
     *
     * @return mixed
     */
    public static function get($profile, $var = null) {
        // Load profile data if not already loaded
        if (!array_key_exists($profile, Authentication::$profiles)) {
            // Encapsulate profile
            $session_var_name = 'authentication[' . $profile . ']';

            // Retrieve session data first
            Authentication::$profiles[$profile] = Session::get($session_var_name);
        }

        // Return content
        return NestedVariable::get(Authentication::$profiles[$profile], $var);
    }

    /**
     * Sets an authentication variable value by its name.
     *
     * @param $profile string
     * @param $var mixed
     * @param $value mixed
     *
     * @return mixed
     */
    public static function set($profile, $var, $value = null) {
        // Generate profile session var name
        $profile_session_var_name = 'authentication[' . $profile . ']';

        // Check varname type
        if (!is_array($var) && !is_bool($var)) {
            // Encapsulate varname into session.
            $varNameParts = explode('[', str_replace(']', '', $var));
            $session_var_name = 'authentication[' . $profile . ']';

            foreach ($varNameParts as $session_var_name_part) {
                $session_var_name .= '[' . $session_var_name_part . ']';
            }

            // Set session
            Session::set($session_var_name, $value);

            // Retrieve full session data
            Authentication::$profiles[$profile] = Session::get($profile_session_var_name);
        } else {
            // If $var is an array or boolean, assume it is not the name of the var
            // but the complete value of the profile authentication
            Session::set($profile_session_var_name, $var);

            // Store it locally also
            Authentication::$profiles[$profile] = $var;
        }

        // Regenerate session id for security purposes
        Session::regenerate();
    }
}