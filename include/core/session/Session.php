<?php

/**
 * Class Session
 *
 * Handles session storage and validation.
 */
class Session {
    /**
     * Current session id.
     *
     * @var string
     */
    private static $session_id = null;

    /**
     * Current app session variables.
     *
     * @var array
     */
    private static $vars = null;

    /**
     * Current app session variable digest. Used to verify if vars have been updated by a third-party application
     * running in the same server.
     *
     * @var array
     */
    private static $digest = null;

    /**
     * Initializes session. The application user data is always encapsulated to avoid mixin with other app
     *
     * @param $check_security bool|true Whether or not to check for stored user info
     *
     * @return void
     */
    public static function init($check_security = true) {
        // Secure the session cookie configuration
        self::secureSession();

        // Retrieve current session id
        self::$session_id = session_id();

        // If session wasn't provided
        if (!self::$session_id) {
            if (GestyMVC::config('session_name')) {
                session_name(GestyMVC::config('session_name'));
            }

            // Start session
            session_start();

            // Store new session id
            self::$session_id = session_id();
        }

        // If session var reference is not set and session has been initialized, store
        if (self::$vars === null && isset($_SESSION)) {
            self::setVars();
        }

        // Store validation values into session if not already set
        self::saveSessionSecurityInfo();

        // Unless otherwise specified, check the session to validate
        if ($check_security && !self::checkSessionSecurityInfo()) {
            // If it is invalid, destroy it
            self::destroy();

            // Reload session without checking this time (to avoid infinite loop if session is broken)
            self::init(false);
        }

        // If the current language has been set, store it into session for further access
        if (defined('LANG')) {
            self::set('lang', LANG);
        }
    }

    /**
     * Links class vars to session.
     *
     * @param $force_digest_update bool whether or not to force digest update.
     *
     * @return void
     */
    private static function setVars($force_digest_update = false) {
        // Generate session tokens
        $base_hash_str = strtoupper(GestyMVC::config('session_digest_token') . ROOT_PATH);
        $session_index = 'gc' . crc32($base_hash_str);
        $digest_index = 'gc' . crc32($base_hash_str . '_digest');
        unset($base_hash_str);

        // Check if first call
        $first_call = (!isset($_SESSION[$session_index]) || !isset($_SESSION[$digest_index]) || !is_array($_SESSION[$session_index]));

        // Initialize token positions if not set
        if ($force_digest_update || $first_call) {
            if ($first_call) {
                // Empty array by default
                $_SESSION[$session_index] = array();
            }

            // Save digest
            $_SESSION[$digest_index] = self::sessionDigest();
        }

        // Store references
        self::$vars =& $_SESSION[$session_index];
        self::$digest =& $_SESSION[$digest_index];
    }

    /**
     * Calculates the digest validation string
     *
     * @return string
     */
    private static function sessionDigest() {
        // Non-cryptographic but fast hash for validation
        return strval(crc32(GestyMVC::config('session_digest_token') . self::$session_id . json_encode(self::$vars)));
    }

    /**
     * Destroys session
     *
     * @return void
     */
    public static function destroy() {
        // Clear current app session data
        self::$vars = array();
        self::$vars = null;
        self::$session_id = null;

        // Destroy PHP session
        session_destroy();
    }

    /**
     * Regenerates session
     *
     * @return void
     */
    public static function regenerate() {
        // Regenerate session id
        session_regenerate_id();

        // Update session id reference
        self::$session_id = session_id();

        // Avoid third-party session modification
        self::$digest = self::sessionDigest();
    }

    /**
     * Reloads id
     *
     * @return void
     */
    public static function reloadId() {
        if (function_exists('session_status') && session_status() == PHP_SESSION_NONE) {
            // Re-start session
            session_start();

            // Update session id reference
            self::$session_id = session_id();

            // Re-set vars
            self::setVars(true);
        }
    }

    /**
     * Stores user info on session to avoid illegal access
     *
     * @return void
     */
    private static function saveSessionSecurityInfo() {
        // If not already set
        if (!self::get('security')) {
            // Store IP and user agent
            self::set('security', array(
                'ip' => (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null),
                'user_agent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null,
            ));
        }
    }

    /**
     * Checks session user data for validation
     *
     * @return bool valid session
     */
    private static function checkSessionSecurityInfo() {
        // If the session has been modified outsite the app, session is invalid
        if (GestyMVC::config('check_session_digest') && self::$digest != self::sessionDigest()) {
            return false;
        }

        // If the session ip has changed, session is invalid
        if (GestyMVC::config('check_session_ip') && self::get('security[ip]') != (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null)) {
            return false;
        }

        // If the user agent has changed, session is invalid
        if (GestyMVC::config('check_session_user_agent') && self::get('security[user_agent]') != (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null)) {
            return false;
        }

        // If no error was found, success
        return true;
    }

    /**
     * Changes PHP configuration to HTTP Only cookies and HTTPS only if accessed from HTTPS
     *
     * @return void
     */
    private static function secureSession() {
        // Retrieve current session cookie params (server configuration)
        $currentCookieParams = session_get_cookie_params();

        // Domain for cookies
        $domain_for_cookies = (GestyMVC::config('domain_for_cookies') ? GestyMVC::config('domain_for_cookies') : $currentCookieParams['domain']);

        // Reset configuration to HTTP Only and HTTPS Only (if required)
        session_set_cookie_params($currentCookieParams['lifetime'], $currentCookieParams['path'], $domain_for_cookies, Router::isHTTPS(), true);
    }

    /**
     * Gets the value of a var
     *
     * @param $name string varname to extract. Array values can be extracted using parent[children] syntax
     *
     * @return bool|int|array|null
     */
    public static function get($name) {
        // If session is empty, do nothing
        if (!self::$session_id) {
            return null;
        }

        // Search for the var value
        return NestedVariable::get(self::$vars, $name);
    }

    /**
     * Sets the value of a var
     *
     * @param $name string Form-like name of the index (Array values can be set using parent[children] syntax)
     * @param $value mixed value to be set
     * @param $unset bool whether or not to unset the var if the value is null
     *
     * @return bool success
     */
    public static function set($name, $value, $unset = false) {
        // If session is empty, do nothing
        if (!self::$session_id) {
            return false;
        }

        // If no-empty
        if ($name && self::$vars !== null) {
            // Set value
            NestedVariable::set(self::$vars, $name, $value, $unset);

            // Avoid third-party session modification
            self::$digest = self::sessionDigest();

            // Var was set
            return true;
        }

        // Var could not be set
        return false;
    }

    /**
     * Clear all session variables that do not belong to the provided key
     *
     * @param $key
     */
    public static function clearAllBut($key) {
        $varsKeys = array_keys(self::$vars);

        foreach ($varsKeys as $current_key) {
            if ($current_key != $key) {
                unset(self::$vars[$current_key]);
            }
        }

        self::init(false);
    }
}