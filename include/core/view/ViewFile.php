<?php

/**
 * Class ViewFile
 *
 * Scope por view file importing
 */
class ViewFile {
    /**
     * Defines all vars and loads the view file
     *
     * @param $filename______
     * @param $vars______
     *
     * @return string
     */
    public function executeView($filename______, $vars______) {
        // Attempt to load view
        if (file_exists($filename______)) {
            // Set all variables
            foreach ($vars______ as $var______ => $value______) {
                $$var______ = $value______;
            }

            // Disable buffer output
            ob_start();

            // Include file
            include($filename______);

            // Return buffer content
            return ob_get_clean();
        } else {
            Debug::log('View file ' . $filename______ . ' not found.');
        }
    }
}