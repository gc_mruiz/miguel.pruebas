<?php

/**
 * Class View
 *
 * Handles view generation with layout.
 */
class View {
    /**
     * Vars that will be passed to the view.
     *
     * @var array()
     */
    public static $viewVars = null;

    /**
     * Module from which to load the views.
     *
     * @var string
     */
    protected static $module = null;

    /**
     * View to be loaded.
     *
     * @var string
     */
    protected static $view = null;

    /**
     * Layout to be used.
     *
     * @var string|'admin'
     */
    protected static $layout = 'admin';

    /**
     * Helpers to be loaded in the view
     *
     * @var array
     */
    protected static $helpers = array();

    /**
     * Renders the view
     *
     * @param $module
     * @param $view
     * @param $layout
     * @param $viewVars
     * @param $helpers
     *
     * @return string
     */
    public static function render($module, $view, $layout = null, $viewVars = array(), $helpers = array()) {
        // Store vars
        View::$module = $module;
        View::$view = $view;
        View::$layout = $layout;
        View::$viewVars = $viewVars;
        View::$helpers = $helpers;

        // Initialize helpers
        View::includeHelpers();

        // Switch between static folders base on the debugging status
        if (!defined('JS_FOLDER')) {
            if (Debug::debugging()) {
                define('JS_FOLDER', GestyMVC::config('dev_js_folder'));
                define('CSS_FOLDER', GestyMVC::config('dev_css_folder'));
            } else {
                define('JS_FOLDER', GestyMVC::config('pro_js_folder'));
                define('CSS_FOLDER', GestyMVC::config('pro_css_folder'));
            }
        }

        // View compiled content
        $view_content = null;
        $full_content = isset(View::$viewVars['content_for_layout']) ? View::$viewVars['content_for_layout'] : null;

        // Load view
        if ($view) {
            $view_path = null;

            if ($module) {
                if (file_exists(INCLUDE_PATH . 'views/' . $view . '.php')) {
                    $view_path = INCLUDE_PATH . 'views/' . $view . '.php';
                } else {
                    $view_path = INCLUDE_PATH . 'modules/' . $module . '/views/' . $view . '.php';
                }
            } else {
                $view_path = INCLUDE_PATH . 'views/' . $view . '.php';
            }

            $ViewFile = new ViewFile();
            $view_content = $ViewFile->executeView($view_path, View::$viewVars);
            $full_content = $view_content;
        }

        // Attempt to load layout
        if ($layout) {
            // Load layout content
            View::$viewVars['content_for_layout'] = $full_content;

            $view_path = null;

            if ($module) {
                if (file_exists(INCLUDE_PATH . 'views/layouts/' . $layout . '.php')) {
                    $view_path = INCLUDE_PATH . 'views/layouts/' . $layout . '.php';
                } else {
                    $view_path = INCLUDE_PATH . 'modules/' . $module . '/views/layouts/' . $layout . '.php';
                }
            } else {
                $view_path = INCLUDE_PATH . 'views/layouts/' . $layout . '.php';
            }

            $ViewFile = new ViewFile();
            $full_content = $ViewFile->executeView($view_path, View::$viewVars);
        }

        // Return compiled content
        return array('full' => $full_content, 'view' => $view_content);
    }

    /**
     * Includes all required helpers for the view.
     */
    private static function includeHelpers() {
        // If a single helper was set, it may have been not set
        // as an array
        if (!is_array(View::$helpers)) {
            View::$helpers = array(View::$helpers);
        }

        // Add default helpers
        View::$helpers = array_merge(View::$helpers, array(
            'Html',
            'Form',
            'Image',
        ));

        // Load helpers' classes
        foreach (View::$helpers as $helper) {
            self::includeHelper($helper);
        }
    }

    /**
     * Includes a helper by name
     *
     * @param $helper
     */
    public static function includeHelper($helper) {
        // Check if helper file exists in app
        if (file_exists(INCLUDE_PATH . 'helpers/' . $helper . 'Helper.php')) {
            include_once(INCLUDE_PATH . 'helpers/' . $helper . 'Helper.php');
        } elseif (View::$module && file_exists(INCLUDE_PATH . 'modules/' . View::$module . '/helpers/' . $helper . 'Helper.php')) {
            include_once(INCLUDE_PATH . 'modules/' . View::$module . '/helpers/' . $helper . 'Helper.php');
        } else if (file_exists(CORE_PATH . 'helper/' . $helper . 'Helper.php')) {
            include_once(CORE_PATH . 'helper/' . $helper . 'Helper.php');
        } else {
            Debug::crash(true, 'Helper file for ' . $helper . 'Helper was not found.');
        }
    }
}