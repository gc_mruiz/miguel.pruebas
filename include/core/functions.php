<?php
// Include project functions if exists.
if (file_exists(INCLUDE_PATH . 'AppFunctions.php')) {
    include_once(INCLUDE_PATH . 'AppFunctions.php');
}

// Core functions
function now() {
    return date('Y-m-d H:i:s');
}

function clean_chars($string) {
    $charsToReplace = str_split(utf8_decode('áéíóúýäëïöüÿàèìòùâêîôûñ\'"+-?'));
    $charsToReplaceWith = str_split('aeiouyaeiouyaeiouaeioun     ');

    return str_replace($charsToReplace, $charsToReplaceWith, mb_strtolower(utf8_decode($string)));
}

function remove_accents($string) {
    $charsToReplace = str_split(utf8_decode('áéíóúýäëïöüÿàèìòùâêîôûñ'));
    $charsToReplaceWith = str_split('aeiouyaeiouyaeiouaeioun');

    foreach ($charsToReplace as $index => $char) {
        $string = str_replace(array(
                                  utf8_encode($char),
                                  utf8_encode(strtoupper($char)),
                              ), array(
                                  $charsToReplaceWith[$index],
                                  strtoupper($charsToReplaceWith[$index]),
                              ), $string);
    }

    return $string;
}

function clean_for_search($string) {
    return str_replace(' ', '', clean_chars($string));
}

function clean_for_url($basename) {
    $basename = str_replace(" ", "-", clean_chars($basename));

    $cleanname = '';

    for ($i = 0; $i < strlen($basename); $i++) {
        $char = substr($basename, $i, 1);
        $charCode = ord($char);

        if (($charCode >= 48 && $charCode < 58) || ($charCode >= 65 && $charCode < 91) || ($charCode >= 97 && $charCode < 123) || $char == '-') {
            $cleanname .= $char;
        }
    }

    while (strpos($cleanname, '--') !== false) {
        $cleanname = str_replace('--', '-', $cleanname);
    }

    if (strlen($cleanname) != 0 && substr($cleanname, 0, 1) == '-') {
        $cleanname = substr($cleanname, 1);
    }

    if (strlen($cleanname) != 0 && substr($cleanname, strlen($cleanname) - 1, 1) == '-') {
        $cleanname = substr($cleanname, 0, strlen($cleanname) - 1);
    }

    if (!$cleanname) {
        $cleanname = 'index';
    }

    return $cleanname;
}

/**
 * Returns text in the most efficient
 * encoding
 *
 * @param $text
 * @param $imap bool whether or not to check of imap encoding
 *
 * @return string text encoded
 */
function to_efficient_encoding($text, $imap = false) {
    // Do nothing if empty
    if (!$text) {
        return $text;
    }

    // Check html encoding
    if (strpos($text, '&') !== false) {
        $htmldecoded_text = html_entity_decode($text);

        if (strlen($htmldecoded_text) != 0) {
            $text = $htmldecoded_text;
        }
    }

    // Check imap encoding
    if ($imap && defined('imap_utf8')) {
        try {
            $text_utf8_encoded_imap = imap_utf8($text);

            if ($text_utf8_encoded_imap && strlen($text_utf8_encoded_imap) < strlen($text) && substr_count($text_utf8_encoded_imap, '?') <= substr_count($text, '?')) {
                $text = $text_utf8_encoded_imap;
            }
        } catch (Exception $e) {
        }
    }

    // Decode until decoding loses information
    while (mb_detect_encoding($text) == 'UTF-8') {
        $new_text = utf8_decode($text);

        if (substr_count($new_text, '?') <= substr_count($text, '?')) {
            $text = utf8_decode($text);
        } else {
            break;
        }
    }

    // Re-encode to utf8
    $text = utf8_encode($text);

    // Return transformed string
    return $text;
}

function include_once_full_directory($directory, $recursive = false) {
    // If $directory does not end on trailing slash,
    // add it
    if (strlen($directory) > 1 && substr($directory, -1, 1) != '/') {
        $directory .= '/';
    }

    // Retrieve all files and directories in directory
    $filesAndDirs = scandir($directory);

    // For each file or directory, include_once
    foreach ($filesAndDirs as $file_or_dir) {
        // Ignore navigation dirs
        if (in_array($file_or_dir, array(
            '.',
            '..',
        ))) {
            continue;
        }

        if (!is_dir($directory . $file_or_dir)) {
            // If its not a directory, then its a file
            include_once($directory . $file_or_dir);
        } elseif ($recursive) {
            // If its a directory and the function was called
            // recursively, include all its
            // files
            include_once_full_directory($directory . $file_or_dir, $recursive);
        }
    }
}

function flattern_nested_array($array, $separator = '-') {
    // If it is not, generate a new array
    $flatArray = array();

    // Flattern each element
    foreach ($array as $name => $value) {
        if (is_array($value)) {
            // If value is an array, retrieve it's contents
            // flat
            $flatternElements = flattern_nested_array($value, $separator);

            foreach ($flatternElements as $f_name => $f_value) {
                // Add each subelement to the main array with
                // combined key
                $flatArray[$name . $separator . $f_name] = $f_value;
            }
        } else {
            $flatArray[$name] = $value;
        }
    }

    // Return flattern array
    return $flatArray;
}

function read_date($date, $day = true, $month = true, $year = true, $inline = false, $short_month = false) {
    $inlineMonths = array(
        0 => __('January {inline}', true),
        1 => __('February {inline}', true),
        2 => __('March {inline}', true),
        3 => __('April {inline}', true),
        4 => __('May {inline}', true),
        5 => __('June {inline}', true),
        6 => __('July {inline}', true),
        7 => __('August {inline}', true),
        8 => __('September {inline}', true),
        9 => __('October {inline}', true),
        10 => __('November {inline}', true),
        11 => __('December {inline}', true),
    );

    if ($short_month) {
        foreach ($inlineMonths as $index => $month) {
            $inlineMonths[$index] = mb_substr($inlineMonths[$index], 0, 3, 'UTF-8');
        }
    }

    $time = strtotime($date);

    $d = intval(date('d', $time));
    $m = intval(date('m', $time));
    $y = intval(date('Y', $time));

    $date_read = null;

    if ($day) {
        switch ($d) {
            case 1:
                $date_read = sprintf(__('1st of %s', true), $inlineMonths[$m - 1]);
                break;

            case 2:
                $date_read = sprintf(__('2nd of %s', true), $inlineMonths[$m - 1]);
                break;

            case 3:
                $date_read = sprintf(__('3rd of %s', true), $inlineMonths[$m - 1]);
                break;

            default:
                $date_read = sprintf(__('%sth of %s', true), $d, $inlineMonths[$m - 1]);
                break;
        }
    } else {
        if ($inline) {
            $date_read = $inlineMonths[$m - 1];
        } else {
            $date_read = ucfirst($inlineMonths[$m - 1]);
        }
    }

    if ($year) {
        $date_read = sprintf(__('%s of %s {month of year}', true), $date_read, $y);
    }

    return $date_read;
}

function sql_escape($var) {
    return MySQLModel::realEscapeString($var);
}

function list_to_assoc_array($list) {
    $assoc = array();

    foreach ($list as $el) {
        $assoc[$el] = $el;
    }

    return $assoc;
}

/**
 * Shortcut to htmlentities($text, ENT_COMPAT,
 * 'UTF-8'), handles correctly
 * the codification of UTF8 chars into HTML.
 * Otherwise, multibyte chars are broken.
 *
 * @param $text string UTF8 text to encode
 *
 * @return string HTML encoded text
 */
function utf8html($text) {
    return htmlentities($text, ENT_COMPAT, 'UTF-8');
}

function json_encode_safe($value) {
    return json_encode(utf8_verify_nested_array($value));
}

function utf8_verify_nested_array($array) {
    if (!is_array($array) && !is_object($array)) {
        if (mb_detect_encoding($array, 'UTF-8', true) === false) {
            $array = utf8_encode($array);
        }
    } else {
        foreach ($array as $key => $value) {
            $array[$key] = utf8_verify_nested_array($value);
        }
    }

    return $array;
}

/**
 * Checks if the array keys are sequential ints
 * starting with 0
 */
function array_is_list(&$array) {
    $keys = array_keys($array);

    return !sizeof(array_diff($keys, array_keys($keys)));
}

function starts_with($haystack, $needle) {
    $haystack = strval($haystack);
    $needle = strval($needle);

    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function ends_with($haystack, $needle) {
    $haystack = strval($haystack);
    $needle = strval($needle);

    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

/**
 * Create resource image from BMP manager
 *
 * @credits
 * http://es.php.net/manual/en/function.imagecreate.php#53879
 *             http://es.php.net/manual/en/function.imagecreate.php#87601
 *
 * @param $fileStreamname
 *
 * @return bool|resource image
 */
function imagecreatefrombmp($fileStreamname) {
    if (!$f1 = fopen($fileStreamname, 'rb')) {
        return false;
    }

    $fileStream = unpack('vfile_type/Vfile_size/Vreserved/Vbitmap_offset', fread($f1, 14));

    if ($fileStream['file_type'] != 19778) {
        return false;
    }

    $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel' . '/Vcompression/Vsize_bitmap/Vhoriz_resolution' . '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1, 40));
    $BMP['colors'] = pow(2, $BMP['bits_per_pixel']);

    if ($BMP['size_bitmap'] == 0) {
        $BMP['size_bitmap'] = $fileStream['file_size'] - $fileStream['bitmap_offset'];
    }

    $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel'] / 8;
    $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
    $BMP['decal'] = ($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
    $BMP['decal'] -= floor($BMP['width'] * $BMP['bytes_per_pixel'] / 4);
    $BMP['decal'] = 4 - (4 * $BMP['decal']);

    if ($BMP['decal'] == 4) {
        $BMP['decal'] = 0;
    }

    $PALETTE = array();

    if ($BMP['colors'] < 16777216 && $BMP['colors'] != 65536) {
        $PALETTE = unpack('V' . $BMP['colors'], fread($f1, $BMP['colors'] * 4));
    }

    $IMG = fread($f1, $BMP['size_bitmap']);
    $VIDE = chr(0);

    $res = imagecreatetruecolor($BMP['width'], $BMP['height']);
    $P = 0;
    $Y = $BMP['height'] - 1;

    while ($Y >= 0) {
        $X = 0;

        while ($X < $BMP['width']) {
            if ($BMP['bits_per_pixel'] == 24 || $BMP['bits_per_pixel'] == 32) {
                $COLOR = unpack('V', substr($IMG, $P, 3) . $VIDE);
            } elseif ($BMP['bits_per_pixel'] == 16) {
                $COLOR = unpack('v', substr($IMG, $P, 2));
                $blue = (($COLOR[1] & 0x001f) << 3) + 7;
                $green = (($COLOR[1] & 0x03e0) >> 2) + 7;
                $red = (($COLOR[1] & 0xfc00) >> 7) + 7;
                $COLOR[1] = $red * 65536 + $green * 256 + $blue;
            } elseif ($BMP['bits_per_pixel'] == 8) {
                $COLOR = unpack('n', $VIDE . substr($IMG, $P, 1));
                $COLOR[1] = $PALETTE[$COLOR[1] + 1];
            } elseif ($BMP['bits_per_pixel'] == 4) {
                $COLOR = unpack('n', $VIDE . substr($IMG, floor($P), 1));

                if (($P * 2) % 2 == 0) {
                    $COLOR[1] = ($COLOR[1] >> 4);
                } else {
                    $COLOR[1] = ($COLOR[1] & 0x0F);
                }

                $COLOR[1] = $PALETTE[$COLOR[1] + 1];
            } elseif ($BMP['bits_per_pixel'] == 1) {
                $COLOR = unpack('n', $VIDE . substr($IMG, floor($P), 1));

                if (($P * 8) % 8 == 0) {
                    $COLOR[1] = $COLOR[1] >> 7;
                } elseif (($P * 8) % 8 == 1) {
                    $COLOR[1] = ($COLOR[1] & 0x40) >> 6;
                } elseif (($P * 8) % 8 == 2) {
                    $COLOR[1] = ($COLOR[1] & 0x20) >> 5;
                } elseif (($P * 8) % 8 == 3) {
                    $COLOR[1] = ($COLOR[1] & 0x10) >> 4;
                } elseif (($P * 8) % 8 == 4) {
                    $COLOR[1] = ($COLOR[1] & 0x8) >> 3;
                } elseif (($P * 8) % 8 == 5) {
                    $COLOR[1] = ($COLOR[1] & 0x4) >> 2;
                } elseif (($P * 8) % 8 == 6) {
                    $COLOR[1] = ($COLOR[1] & 0x2) >> 1;
                } elseif (($P * 8) % 8 == 7) {
                    $COLOR[1] = ($COLOR[1] & 0x1);
                }

                $COLOR[1] = $PALETTE[$COLOR[1] + 1];
            } else {
                return false;
            }

            imagesetpixel($res, $X, $Y, $COLOR[1]);
            $X++;
            $P += $BMP['bytes_per_pixel'];
        }

        $Y--;
        $P += $BMP['decal'];
    }

    fclose($f1);

    return $res;
}

/**
 * Creates a random complex token with variable length
 *
 * @param $especial_chars bool whether or not to use special chars
 * @param $min_length int
 * @param $max_length int
 *
 * @return string
 */
function random_token($especial_chars = true, $min_length = 100, $max_length = 150) {
    $chars = 'abcdefghijklmnopqrstuvwxyz';
    $chars .= strtoupper($chars);
    $chars .= '0123456789';

    if ($especial_chars) {
        $chars .= '!$%&/()=?*[]{}+';
    }

    $chars = str_split($chars);

    $token = '';

    $token_length = rand($min_length, $max_length);
    while (strlen($token) < $token_length) {
        if (rand(0, 1) == time() % 2) {
            shuffle($chars);
        }

        $token .= $chars[array_rand($chars)];
    }

    return $token;
}

/**
 * Random wait (for security purposes)
 *
 * @param $up_to_seconds float max seconds
 *
 * @return void
 */
function random_sleep($up_to_seconds = 1.00) {
    usleep(rand(0, $up_to_seconds) * 1000000);
}

/**
 * Multibyte version of ucfirst
 *
 * @param $text
 * @param $encoding
 * @param $keep_rest_of_caps bool default false
 *
 * @return string
 */
function mb_ucfirst($text, $encoding = 'UTF-8', $keep_rest_of_caps = false) {
    // If the string is not empty
    if (strlen($text)) {
        if ($keep_rest_of_caps) {
            // Change the first letter to uppercase and keep the rest
            return mb_strtoupper(mb_substr($text, 0, 1, $encoding), $encoding) . mb_substr($text, 1, mb_strlen($text, $encoding) - 1, $encoding);
        } else {
            // Change the first letter to uppercase and the rest to lowercase
            return mb_strtoupper(mb_substr($text, 0, 1, $encoding), $encoding) . mb_strtolower(mb_substr($text, 1, mb_strlen($text, $encoding) - 1, $encoding), $encoding);
        }
    }

    // Return empty string in any other case
    return '';
}

function var_export_min(&$var, $return = false) {
    if (is_array($var) && array_is_list($var)) {
        $export = 'array(';

        foreach ($var as $value) {
            $export .= var_export_min($value, true) . ',';
        }

        $export .= ')';

        if (!$return) {
            echo $export;
        }
        if ($return) {
            return $export;
        }
    } else {
        return var_export($var, $return);
    }
}

function table2var($tablename) {
    return Inflector::camelcase(Inflector::singularize($tablename));
}

function table2class($tablename) {
    return Inflector::pascalcase(Inflector::singularize($tablename));
}

function class2table($tablename) {
    return Inflector::pluralize(Inflector::underscore($tablename));
}

function class2var($model_name) {
    return Inflector::camelcase(Inflector::underscore($model_name));
}

function id2class($column) {
    return Inflector::pascalcase(substr($column, 0, -3));
}

function id2pluralclass($column) {
    return Inflector::pascalcase(Inflector::pluralize(substr($column, 0, -3)));
}

function field2noun($column, $uppercase = false) {
    if (ends_with($column, '_id')) {
        $column = substr($column, 0, -3);
    }

    $noun = str_replace('_', ' ', $column);

    if ($uppercase) {
        $noun = ucfirst($noun);
    }

    return $noun;
}

function id2noun($column, $uppercase = false) {
    return field2noun(substr($column, 0, -3), $uppercase);
}

function class2noun($model_name, $uppercase = false) {
    $noun = str_replace('_', ' ', Inflector::underscore($model_name));

    if ($uppercase) {
        $noun = ucfirst($noun);
    }

    return $noun;
}

function table2noun($tablename, $uppercase = false) {
    $noun = str_replace('_', ' ', $tablename);

    if ($uppercase) {
        $noun = ucfirst($noun);
    }

    return $noun;
}

function file_put_contents_with_777($filename, $data, $flags = null, $context = null) {
    $result = file_put_contents($filename, $data, $flags, $context);

    try {
        if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
            @chmod($filename, 0777);
        }
    } catch (Exception $ex) {
    }

    return $result;
}

function dataattr($arr) {
    return utf8html(json_encode($arr));
}

function date2php($date) {
    if (is_array($date)) {
        $date_value = null;
        $time_value = null;

        if (array_key_exists('date', $date)) {
            $date_value = $date['date'];
        }

        if (array_key_exists('time', $date)) {
            $time_value = $date['time'];
        }

        $date = trim($date_value . ($time_value ? ' ' . $time_value : ''));
    }

    $success = true;
    $dateParts = explode(' ', str_replace('T', ' ', $date));
    $date_value = null;
    $time_value = null;

    if (sizeof($date) < 3) {
        $date_value = $dateParts[0];
        $time_value = (sizeof($dateParts) == 2 ? $dateParts[1] : null);

        if ($date_value) {
            if (strpos($date_value, '-') !== false) {
                // Do nothing
            } else {
                $dateDateParts = array_reverse(explode('/', $date_value));

                if (sizeof($dateDateParts) == 3) {
                    if (LANG == 'en') {
                        $dateDateParts = array($dateDateParts[0], $dateDateParts[2], $dateDateParts[1]);
                    }

                    $date_value = implode('-', $dateDateParts);
                } else {
                    $success = false;
                }
            }
        } else {
            $date_value = null;
        }

        if (!$time_value) {
            $time_value = null;
        }
    } else {
        $success = false;
    }

    if ($success) {
        $date = null;

        if ($date_value) {
            $date = $date_value;
        }

        if ($time_value) {
            if ($date) {
                $date .= ' ';
            }

            $date .= $time_value;
        }

        return $date;
    } else {
        return $date;
    }
}

function date2lang($date) {
    $success = true;
    $dateParts = explode(' ', str_replace('T', ' ', $date));
    $date_value = null;
    $time_value = null;

    if (sizeof($date) < 3) {
        $date_value = $dateParts[0];
        $time_value = (sizeof($dateParts) == 2 ? $dateParts[1] : null);

        if (strpos($date_value, '-') === false) {
            // Do nothing
        } else {
            $dateDateParts = array_reverse(explode('-', $date_value));

            if (sizeof($dateDateParts) == 3) {
                if (LANG == 'en') {
                    $dateDateParts = array($dateDateParts[1], $dateDateParts[0], $dateDateParts[2]);
                }

                $date_value = implode('/', $dateDateParts);
            } else {
                $success = false;
            }
        }
    } else {
        $success = false;
    }

    if ($success) {
        return $date_value . ($time_value ? ' ' . $time_value : '');
    } else {
        return $date;
    }
}

/**
 * @deprecated No longer used, wrongly named and buggy. Use 'format' property of the HTMLHelper for this effect.
 * Or check Controller.php::cleanUpPostData for example functions.
 */
function formateuros($number, $decimals = null) {
    Debug::log('Using global deprecated function formateuros.', 'deprecated');

    if (!$number) {
        return '';
    }

    return number_format($number, ($decimals ? $decimals : 0), ',', '.') . '€';
}

/**
 * @deprecated No longer used, wrongly named and buggy. Use 'format' property of the HTMLHelper for this effect.
 * Or check Controller.php::cleanUpPostData for example functions.
 */
function numberfromeuros($number) {
    Debug::log('Using global deprecated function numberfromeuros.', 'deprecated');

    if (!$number) {
        return '';
    }

    return str_replace(array('.', '€', ' '), '', $number);
}

function umerge($array1, $array2) {
    return array_unique(array_merge($array1, $array2));
}

function number_format_lang($number, $decimals) {
    if ($number === null || strlen($number) === 0) {
        return '';
    }

    if (LANG == 'en') {
        return number_format($number, $decimals, '.', ',');
    } else {
        return number_format($number, $decimals, ',', '.');
    }
}

function strip_tags_recursive(&$var, $allowable_tags = null) {
    if ($var !== null && !is_object($var)) {
        if (is_array($var)) {
            foreach ($var as &$val) {
                strip_tags_recursive($val, $allowable_tags);
            }
        } else {
            $var = strip_tags($var, $allowable_tags);
        }
    }
}

function array_intersect_split(&$needle, $haystack, $preserve_keys = false) {
    if (!is_array($needle) || !is_array($haystack)) {
        return false;
    }
    $new_arr = array();
    foreach ($needle as $key => $value) {
        if (($loc = array_search($value, $haystack)) !== false) {
            if (!$preserve_keys) {
                $new_arr[] = $value;
            } else {
                $new_arr[$key] = $value;
            }
            unset($needle[$key]);
        }
    }

    return $new_arr;
}

function minify_output($content) {
    $search = array(
        '/\>[^\S ]+/s',
        '/[^\S ]+\</s',
        '/(\s)+/s',
    );
    $replace = array(
        '>',
        '<',
        '\\1',
    );

    if (preg_match("/\<html/i", $content) == 1 && preg_match("/\<\/html\>/i", $content) == 1) {
        $content = preg_replace($search, $replace, $content);
        $content = str_replace(ROOT_URL, ROOT_RELATIVE_URL, $content);
    }

    return $content;
}

if (!function_exists('mb_lcfirst')) {
    /**
     * Multi-byte version of lcfirst().
     *
     * @param string $str
     * @param null|string $encoding
     *
     * @return string
     */
    function mb_lcfirst($str, $encoding = null) {
        $first = mb_strtolower(mb_substr($str, 0, 1, $encoding), $encoding);

        return $first . mb_substr($str, 1, null, $encoding);
    }
}

function clitrace($message) {
    if (GestyMVC::isCli()) {
        if (is_array($message) || is_object($message)) {
            $message = json_encode($message);
        }

        echo now() . ' - ' . $message . PHP_EOL;
    }
}

/**
 * @deprecated No longer used, wrongly named. Use 'involvedFilesDebug' instead.
 *
 * @param $blacklist array
 * @return array
 */
function involedFilesDebug($blacklist = array()) {
    return involvedFilesDebug($blacklist);
}

function involvedFilesDebug($blacklist = array()) {
    $trace = debug_backtrace();
    $trace = array_reverse($trace);
    $files = array();

    foreach ($trace as $file) {
        if (isset($file['file'])) {
            if (in_array(basename($file['file']), $blacklist)) {
                continue;
            }

            $files[] = basename($file['file']) . ':' . $file['line'];
        }
    }

    return $files;
}

function rrmdir($src, $delete_dir_itself = true) {
    $dir = opendir($src);

    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            $full = $src . '/' . $file;

            if (is_dir($full)) {
                rrmdir($full, true);
            } else {
                unlink($full);
            }
        }
    }

    closedir($dir);

    if ($delete_dir_itself) {
        rmdir($src);
    }
}