<?php

/**
 * Class ModuleNameModelTemplate
 *
 * Model class for table ModuleNameModelTemplates
 */
class OtpReverseProxyModel extends OtpMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'OtpReverseProxyModel';
}
