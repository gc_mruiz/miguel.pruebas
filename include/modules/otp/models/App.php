<?php

class App {
    private $apps = array();

    public function App() {
        $this->initialize();
    }

    public function initialize() {
        $this->apps = json_decode(file_get_contents(ROOT_PATH . 'content/.config/apps.json'), true);
    }

    public function getByCode($code) {
        if (isset($this->apps[$code])) {
            $app_ = $this->apps[$code];
            $app_['code'] = $code;

            return array('App' => $app_);
        }

        return null;
    }

    public function getAll() {
        $all = array();

        foreach ($this->apps as $code => $app) {
            $app_ = $app;
            $app_['code'] = $code;

            $all[$code] = array('App' => $app_);
        }

        return $all;
    }

    public function updateFields($code, $appData) {
        $this->apps[$code] = $appData;

        return $this->save();
    }

    public function addNew($appData) {
        $this->apps[$appData['code']] = $appData;

        return $this->save();
    }

    public function deleteByCode($code) {
        unset($this->apps[$code]);

        return $this->save();
    }

    public function save() {
        foreach ($this->apps as $code => $app) {
            foreach ($app as $field => $value) {
                if (!$value) {
                    $this->apps[$code][$field] = null;
                }

                if ($field = 'https') {
                    $this->apps[$code][$field] = !!$this->apps[$code][$field];
                }
            }
        }

        return !!file_put_contents(ROOT_PATH . 'content/.config/apps.json', json_encode($this->apps));
    }
}