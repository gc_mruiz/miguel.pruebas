<!doctype html>
<html class="no-js" lang="<?php echo LANG ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $langs = GestyMVC::config('langs'); foreach($langs as $lang): if($lang == LANG) continue; $url = data_var('alternate[' . $lang . ']'); if(!$url) { $url = Dispatcher::currentControllerParams(); $url['controller'] = Dispatcher::currentControllerName(); $url['action'] = Dispatcher::currentActionName(); $url['lang'] = $lang; }
        ?><link rel="alternate" hreflang="<?php echo $lang ?>" href="<?php echo Router::url($url) ?>" /><?php
    endforeach ?>

    <?php if(!isset($seo_title)) $seo_title = 'CRM - Haya Real Estate'; ?><title><?php echo utf8html($seo_title) ?></title>
    <?php if(isset($seo_description) && $seo_description): ?><meta name="description" content="<?php echo utf8html($seo_description) ?>" /><?php endif ?>
    <?php if(isset($seo_title) && $seo_title): ?><meta property="og:title" content="<?php echo utf8html($seo_title) ?>" /><?php endif ?>
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="<?php echo utf8html('Haya Real Estate') ?>" />
    <?php if(isset($seo_description) && $seo_description): ?><meta property="og:description" content="<?php echo utf8html($seo_description) ?>" /><?php endif ?>
    <?php if(isset($og_image) && $og_image): ?><meta property="og:image" content="<?php echo utf8html($og_image) ?>" /><?php endif ?>
    <?php if(isset($canonical_url) && $canonical_url): ?><link rel="canonical" href="<?php echo utf8html($canonical_url) ?>" /><?php endif ?>
    <link rel="stylesheet" href="<?php echo ROOT_URL ?>static/css/crm.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL ?>static/css/gesty-crm.css">
</head>
<body class="<?php if(isset($page)) echo $page ?> <?php if(isset($page_class)) echo $page_class ?>" data-view="haya-list-results">

<main>
    <?php echo $content_for_layout ?>
</main>

<script type="text/javascript">
    var LANG = <?php echo json_encode_safe(LANG) ?>;
    var ROOT_URL = <?php echo json_encode_safe(ROOT_URL) ?>;
    var LOCALED_ROOT_URL = <?php echo json_encode_safe(LOCALED_ROOT_URL) ?>;
    var JS_FOLDER = <?php echo json_encode_safe(JS_FOLDER) ?>;
    var GOOGLE_MAPS_KEY = <?php echo json_encode(GestyMVC::config('google_maps_key')) ?>;
</script>

<script src="<?php echo ROOT_URL ?>static/js/single-page/svg4everybody.custom.js"></script>
<script src="<?php echo ROOT_URL ?>static/js/crm.js"></script>
<script src="<?php echo ROOT_URL ?>static/js/gesty-crm.js"></script>
</body>
</html>