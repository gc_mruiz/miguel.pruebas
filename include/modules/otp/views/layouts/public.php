<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <?php if(!isset($seo_title)) $seo_title = 'Portal Haya'; ?><title><?php echo utf8html($seo_title) ?></title>
        <?php if(isset($seo_description) && $seo_description): ?><meta name="description" content="<?php echo utf8html($seo_description) ?>" /><?php endif ?>
        <?php if(isset($seo_title) && $seo_title): ?><meta property="og:title" content="<?php echo utf8html($seo_title) ?>" /><?php endif ?>
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="<?php echo utf8html('Portal Haya') ?>" />
        <?php if(isset($seo_description) && $seo_description): ?><meta property="og:description" content="<?php echo utf8html($seo_description) ?>" /><?php endif ?>
        <?php if(isset($og_image) && $og_image): ?><meta property="og:image" content="<?php echo utf8html($og_image) ?>" /><?php endif ?>
        <?php if(isset($canonical_url) && $canonical_url): ?><link rel="canonical" href="<?php echo utf8html($canonical_url) ?>" /><?php endif ?>

        <?php echo HtmlHelper::favicon('/favicon.ico') ?>
        <?php echo HtmlHelper::css('public.css') ?>
    </head>
    <body>
        <?php echo $content_for_layout ?>

        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/jquery-1.11.1.min.js') ?>

        <script type="text/javascript">
            var LANG = <?php echo json_encode_safe(LANG) ?>;
            var ROOT_URL = <?php echo json_encode_safe(ROOT_URL) ?>;
            var LOCALED_ROOT_URL = <?php echo json_encode_safe(LOCALED_ROOT_URL) ?>;
            var JS_FOLDER = <?php echo json_encode_safe(JS_FOLDER) ?>;
        </script>

        <?php echo HtmlHelper::js('Message.js') ?>

        <?php if(Session::get('flash[message]')): ?>
        <script type="text/javascript">
            $(document).ready(function() {
                alert(<?php echo json_encode_safe(Session::get('flash[message]')) ?>);
            });
        </script>
        <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>
    </body>
</html>
