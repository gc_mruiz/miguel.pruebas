<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?php if(!isset($seo_title)) $seo_title = 'CRM - Haya Real Estate'; ?><title><?php echo utf8html($seo_title) ?></title>
        <?php if(isset($seo_description) && $seo_description): ?><meta name="description" content="<?php echo utf8html($seo_description) ?>" /><?php endif ?>
        <?php if(isset($seo_title) && $seo_title): ?><meta property="og:title" content="<?php echo utf8html($seo_title) ?>" /><?php endif ?>
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="<?php echo utf8html('Haya Real Estate') ?>" />
        <?php if(isset($seo_description) && $seo_description): ?><meta property="og:description" content="<?php echo utf8html($seo_description) ?>" /><?php endif ?>
        <?php if(isset($og_image) && $og_image): ?><meta property="og:image" content="<?php echo utf8html($og_image) ?>" /><?php endif ?>
        <?php if(isset($canonical_url) && $canonical_url): ?><link rel="canonical" href="<?php echo utf8html($canonical_url) ?>" /><?php endif ?>

        <?php echo HtmlHelper::favicon('/favicon.ico') ?>
        <?php echo HtmlHelper::css('css.css') ?>
        <?php echo FormHelper::translationStyles() ?>

        <script type="text/javascript">
            var LANG = <?php echo json_encode_safe(LANG) ?>;
            var ROOT_URL = <?php echo json_encode_safe(ROOT_URL) ?>;
            var LOCALED_ROOT_URL = <?php echo json_encode_safe(LOCALED_ROOT_URL) ?>;
            var JS_FOLDER = <?php echo json_encode_safe(JS_FOLDER) ?>;
            var GOOGLE_MAPS_KEY = <?php echo json_encode(GestyMVC::config('google_maps_key')) ?>;
        </script>

        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/bootstrap.min.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/font-awesome/css/font-awesome.css', array('no_session' => false)) ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/plugins/morris/morris-0.4.3.min.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/js/plugins/gritter/jquery.gritter.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/animate.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/style.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/plugins/datapicker/datepicker3.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/plugins/summernote/summernote.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/plugins/summernote/summernote-bs3.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/plugins/dataTables/dataTables.bootstrap.css') ?>
        <?php echo HtmlHelper::css('/bootstrap-static/vendors/bootstrap/css/plugins/dataTables/dataTables.responsive.css') ?>
    </head>
    <body class="pace-done <?php if(!Session::get('layout[nav_deployed]')) echo 'mini-navbar' ?>">
        <div class="pace  pace-inactive">
            <div class="pace-progress" data-progress-text="100%" data-progress="99">
                <div class="pace-progress-inner"></div>
            </div>
            <div class="pace-activity"></div>
        </div>
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element">
                                <span> <img alt="image" class="img-circle" src="https://secure.gravatar.com/avatar/<?php echo md5(strtolower(trim(Authentication::get('user', 'email')))) ?>?s=60"> </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0)" aria-expanded="false"><span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo Authentication::get('user', 'email') ?></strong> </span> <span class="text-muted text-xs block"><?php echo $userLevels[Authentication::get('user', 'level')] ?><b class="caret"></b></span> </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li>
                                        <?php echo HtmlHelper::link(__('Change password', true), array('controller' => 'Users', 'action' => 'changePassword')) ?>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <?php echo HtmlHelper::link(__('Logout', true), array('controller' => 'Users', 'action' => 'logout')) ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                GC
                            </div>
                        </li>
                        <?php
                        echo '<li>' . HtmlHelper::link('<i class="fa fa-home"></i><span class="nav-label">' . __('Home', true) . '</span>', array('controller' => 'ItUsers', 'action' => 'index'), array('escape' => false)) . '</li>'; ?>
                        <?php echo '<li>' . HtmlHelper::link('<i class="fa fa-th"></i><span class="nav-label">' . __('Debug logs', true) . '</span>', array('controller' => 'DebugLogs', 'action' => 'adminIndex'), array('escape' => false)) . '</li>'; ?>
                        <!-- generated button goes here -->
                    </ul>
                </div>
            </nav>
            <div id="page-wrapper" class="gray-bg">
                <div class="row">
                    <nav class="navbar navbar-static-top white-bg" role="navigation">
                        <div class="navbar-header">
                            <a class="navbar-minimalize minimalize-styl-2 btn btn-deviaje" href="javascript:void(0)"><i class="fa fa-bars"></i> </a>
                        </div>
                        <ul class="nav navbar-top-links navbar-right">
                            <li></li>
                            <li>
                                <?php echo HtmlHelper::link('<i class="fa fa-sign-out"></i>' . __('Logout', true), array('controller' => 'Users', 'action' => 'logout'), array('escape' => false)) ?>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper wrapper-content">
                    <?php echo $content_for_layout ?>
                </div>
                <div class="footer">
                    <div>
                        Haya Real Estate
                    </div>
                </div>
            </div>
        </div>

        <!-- Mainly scripts -->
        <?php echo HtmlHelper::js('/bootstrap-static/vendors/bootstrap/js/jquery-1.11.1.min.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/vendors/bootstrap/js/jquery-ui-1.10.4.min.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/vendors/bootstrap/js/bootstrap.min.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/vendors/bootstrap/js/plugins/metisMenu/jquery.metisMenu.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/vendors/bootstrap/js/plugins/datapicker/bootstrap-datepicker.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/vendors/bootstrap/js/plugins/summernote/summernote.min.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/vendors/bootstrap/js/plugins/slimscroll/jquery.slimscroll.min.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/vendors/bootstrap/js/inspinia.js') ?>

        <?php echo HtmlHelper::js('/bootstrap-static/Message.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/Form.js') ?>
        <?php echo HtmlHelper::js('/bootstrap-static/GCBootstrap.js') ?>

        <?php if(Session::get('flash[message]')): ?>
        <script type="text/javascript">
            $(document).ready(function() {
                Message.showText(<?php echo json_encode_safe(Session::get('flash[message]')) ?>, <?php echo json_encode_safe(Session::get('flash[title]')) ?>);
            });
        </script>
        <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>
    </body>
</html>