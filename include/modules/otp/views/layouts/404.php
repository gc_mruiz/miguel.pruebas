<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo ((isset($seo_title) && $seo_title) ? utf8html($seo_title) : utf8html('GestyMVC')) ?></title>
        <meta name="description" content="<?php echo (isset($seo_description) && $seo_description ? utf8html($seo_description) : '') ?>" />
        <meta property="og:title" content="<?php echo (isset($seo_title) && $seo_title ? utf8html($seo_title) : utf8html('GestyMVC')) ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="<?php echo utf8html('GestyMVC') ?>" />
        <meta property="og:description" content="<?php echo (isset($seo_description) && $seo_description ? utf8html($seo_description) : '') ?>" />
        <meta property="og:image" content="<?php echo (isset($og_image) && $og_image ? utf8html($og_image) : ROOT_URL . 'static/img/logo.png') ?>" />

        <?php echo HtmlHelper::favicon('/favicon.ico') ?>
        <?php echo HtmlHelper::css('css.css') ?>

        <script type="text/javascript">
            var LANG = <?php echo json_encode_safe(LANG) ?>;
            var ROOT_URL = <?php echo json_encode_safe(ROOT_URL) ?>;
            var LOCALED_ROOT_URL = <?php echo json_encode_safe(LOCALED_ROOT_URL) ?>;
            var JS_FOLDER = <?php echo json_encode_safe(JS_FOLDER) ?>;
        </script>

        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/bootstrap.min.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/font-awesome/css/font-awesome.css', array('no_session' => false)) ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/plugins/morris/morris-0.4.3.min.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/js/plugins/gritter/jquery.gritter.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/animate.css') ?>
        <?php echo HtmlHelper::css('/static/vendors/bootstrap/css/style.css') ?>
    </head>

    <body class="gray-bg">
        <?php echo $content_for_layout ?>

        <!-- Mainly scripts -->
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/jquery-1.11.1.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/bootstrap.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/plugins/metisMenu/jquery.metisMenu.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/plugins/slimscroll/jquery.slimscroll.min.js') ?>
        <?php echo HtmlHelper::js('/static/vendors/bootstrap/js/inspinia.js') ?>

        <?php echo HtmlHelper::js('Message.js') ?>

        <?php if(Session::get('flash[message]')): ?>
        <script type="text/javascript">
            $(document).ready(function() {
                Message.showText(<?php echo json_encode_safe(Session::get('flash[message]')) ?>, <?php echo json_encode_safe(Session::get('flash[title]')) ?>);
            });
        </script>
        <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>
    </body>
</html>