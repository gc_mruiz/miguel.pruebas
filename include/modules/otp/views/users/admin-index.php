<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php __('Users') ?></h2>
        <ol class="breadcrumb">
            <li>
                <?php echo HtmlHelper::link(__('Home', true), array('action' => 'index')) ?>
            </li>
            <li class="active">
                <strong><?php __('Users') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <?php echo FormHelper::button(__('add new', true), array('div' => false, 'class' => 'pull-right m btn btn-success', 'href' => array('action' => 'adminAdd'))) ?>
        <?php echo FormHelper::button(__('excel', true), array('div' => false, 'class' => 'pull-right m btn btn-info', 'href' => array('action' => 'adminIndex', 'excel' => 1))) ?>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <?php if(!empty($users)): ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php echo HtmlHelper::flashMessage() ?>
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Id', true), 'id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Email', true), 'email') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Level', true), 'level') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Created', true), 'created') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Modified', true), 'modified') ?></th>
                            <th class="actions">&nbsp;</th>
                        </tr>
                        <?php
                        $altrow = false;
                        $count = 0; foreach($users as $user): ?>
                        <tr class="<?php if($altrow) echo 'alt' ?>">
                            <td><?php echo $user['User']['id'] ?>&nbsp;</td>
                            <td><?php echo $user['User']['email'] ?>&nbsp;</td>
                            <td><?php echo $userLevels[$user['User']['level']] ?>&nbsp;</td>
                            <td><?php echo ($user['User']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($user['User']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($user['User']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($user['User']['modified'])) : '') ?>&nbsp;</td>
                            <td>
                                <span class="btn-group">
                                    <?php echo FormHelper::button(__('edit', true), array('div' => false, 'href' => array('action' => 'adminEdit', $user['User']['id']), '+class' => 'btn btn-sm btn-primary')); ?>
                                    <?php if(Authentication::get('user', 'id') != $user['User']['id'])  echo FormHelper::button(__('delete', true), array('div' => false, 'href' => array('action' => 'adminDelete', $user['User']['id']), 'confirm' => sprintf(__('Are you sure you want to delete #%s?', true), $user['User']['id']), '+class' => 'btn btn-sm btn-danger')); ?>
                                </span>
                            </td>
                        </tr>
                        <?php $altrow = !$altrow; $count++; endforeach ?>
                    </table>
                    <?php echo PaginatorHelper::links($pagination) ?>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="ibox-content m-b-sm border-bottom">
        <?php if(Session::get('flash[message]')): ?>
        <div class="alert alert-info">
            <?php echo nl2br(utf8html(Session::get('flash[message]'))) ?>
        </div>
        <?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>

        <div class="text-center p-lg">
            <h2><?php __('There are yet no elements to display.'); ?>
        </div>
    </div>
    <?php endif ?>
</div>
<div class="form-actions">
    <div class="left">
        <?php echo FormHelper::button(__('< return', true), array('div' => false, 'href' => array('action' => 'index'), '+class' => 'btn btn-sm')); ?>
    </div>
</div>
