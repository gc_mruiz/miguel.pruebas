<div class="columns header-actions">
    <div class="row margin-top-small margin-bottom">
        <div class="columns">
            <h1>
                <span class="header-text"><?php echo Authentication::get('user', 'name') ?>, vamos a configurar la doble autenticación</span>
            </h1>
        </div>
    </div>
</div>
<div class="columns">
    <div class="row">
        <div class="columns medium-12 margin-bottom">
            <ol>
                <li>Por favor, instala en tu teléfono una aplicación de doble factor de autenticación. Te recomendamos Google Authenticator. Puedes obtenerla aquí para <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=es">Android</a> y <a href="https://itunes.apple.com/es/app/google-authenticator/id388497605?mt=8">iPhone</a>.</li>
                <li>Escanea con tu aplicación el QR que tienes a continuación.</li>
                <li>Indícanos en el campo de texto el código que marca la aplicación.</li>
                <li>Pulsa continuar.</li>
            </ol>
        </div>
    </div>
</div>
<div class="columns">
    <div class="row" data-equalizer data-equalize-on="medium">
        <div class="columns medium-4 margin-bottom">
            <div class="feedback placeholder-actions no-padding">
                <div class="placeholder-actions-content" data-equalizer-watch>
                    <p>
                        <img src="<?php echo $qr_url ?>" width="100%">
                    </p>
                    <p>
                        ¿No puedes escanear el código? Escribe manualmente la clave en tu app: <strong><?php echo Session::get('otp_seed_temp') ?></strong>
                    </p>
                </div>
            </div>
        </div>
        <div class="columns medium-4 margin-bottom end">
            <div class="feedback placeholder-actions no-padding">
                <?php if(isset($error)): ?>
                <div class="columns">
                    <div class="row columns margin-top">
                        <div class="callout success">
                            <div class="row columns text-center">
                                <p class="no-margin">
                                    Código incorrecto
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif ?>
                <div class="placeholder-actions-content" data-equalizer-watch>
                    <?php echo FormHelper::open() ?>
                    <div class="row">
                        <div class="columns">
                            <?php echo FormHelper::input('code', 'text', false, array('placeholder' => 'Código', 'required' => null, 'autocomplete' => 'off', 'label' => false, 'div' => false, 'error' => false)) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="columns">
                            <a href="javascript:void(0)" class="expanded button primary no-margin btn-submit-form">Continuar</a>
                        </div>
                    </div>
                    <?php echo FormHelper::close() ?>
                </div>
            </div>
        </div>
    </div>
</div>