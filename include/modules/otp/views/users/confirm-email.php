<div class="columns header-actions">
    <div class="row margin-top-small margin-bottom">
        <div class="columns">
            <h1>
                <span class="header-text">¡Bienvenido/a <?php echo Authentication::get('user', 'name') ?>!</span>
            </h1>
        </div>
    </div>
</div>
<div class="columns">
    <div class="row">
        <div class="columns medium-12 margin-bottom">
            <p>
                Antes de empezar, necesitamos que confirmes que tu dirección de correo electrónico es<br> <strong style="font-size: 1.5em"><?php echo Authentication::get('user', 'email') ?></strong>
            </p>
        </div>
    </div>
</div>
<div class="columns">
    <div class="row" data-equalizer data-equalize-on="medium">
        <div class="columns medium-4 margin-bottom">
            <div class="feedback placeholder-actions no-padding">
                <div class="placeholder-actions-content" data-equalizer-watch>
                    <p>Mi dirección de email es correcta</p>
                    <a href="<?php echo Router::url(array('controller' => 'Users', 'action' => 'confirmEmail', 1)) ?>" class="button">Continuar</a>
                </div>
            </div>
        </div>
        <div class="columns medium-4 margin-bottom end">
            <div class="feedback placeholder-actions no-padding">
                <div class="placeholder-actions-content" data-equalizer-watch>
                    <p>Mi dirección de email no es correcta</p>
                    <a style="background: #956" href="<?php echo Router::url(array('controller' => 'Users', 'action' => 'confirmEmail', 2)) ?>" class="button">Solicitar cambio</a>
                </div>
            </div>
        </div>
    </div>
</div>