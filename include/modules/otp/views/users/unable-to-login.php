<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div class="crm-login">
            <div>
                <h1 class="logo-name">HAYA</h1>
            </div>
            <p>No hemos podido iniciar sesión con tu usuario.</p>

            <?php echo FormHelper::open() ?>
            <?php echo HtmlHelper::flashMessage() ?>
            <?php echo HtmlHelper::link('<small>Forzar cierre de sesión en ADFS</small>', 'https://adfs.haya.es/adfs/ls/?wa=wsignout1.0', array('escape' => false)) ?><br>
            <?php echo HtmlHelper::link('<small>Volver a intentarlo</small>', array('action' => 'login'), array('escape' => false)) ?>
            <?php echo FormHelper::close() ?>
        </div>
        <p class="m-t"> <small>Haya Real Estate &copy; <?php echo date('Y') ?></small> </p>
    </div>
</div>