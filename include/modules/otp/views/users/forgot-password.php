<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">GC</h1>
        </div>
        <h3><?php __('Forgot password?') ?></h3>
        <p><?php __('Enter your registered email and we will send you an email with instructions to restore your password.') ?></p>

        <?php echo FormHelper::open() ?>
        <?php echo HtmlHelper::flashMessage() ?>
        <?php echo FormHelper::input('user[User][email]', 'email', false, array('placeholder' => __('Email', true), '+class' => 'single-line')) ?>
        <?php echo FormHelper::submit(__('Send email', true), array('div' => false, 'class' => 'btn btn-primary block full-width m-b')) ?>

        <?php echo HtmlHelper::link('<small>' . __('Back to login', true) . '</small>', array('action' => 'login'), array('escape' => false)) ?>

        <?php echo FormHelper::close() ?>

        <p class="m-t"> <small>Portal Haya &copy; <?php echo date('Y') ?></small> </p>
    </div>
</div>