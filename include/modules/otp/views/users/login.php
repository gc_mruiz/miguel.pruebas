<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">GC</h1>
        </div>
        <h3><?php echo sprintf(__('Welcome to %s', true), 'Portal Haya') ?></h3>
        <p><?php __('Please login to continue.') ?></p>

        <?php echo FormHelper::open() ?>
        <?php echo HtmlHelper::flashMessage() ?>
        <?php echo FormHelper::input('user[User][email]', 'email', false, array('placeholder' => __('Email', true), '+class' => 'single-line')) ?>
        <?php echo FormHelper::input('user[User][password]', 'password', false, array('placeholder' => __('Password', true), '+class' => 'single-line')) ?>
        <?php echo FormHelper::submit(__('Login', true), array('div' => false, 'class' => 'btn btn-primary block full-width m-b')) ?>
        <?php echo HtmlHelper::link('<small>' . __('Forgot password?', true) . '</small>', array('action' => 'forgotPassword'), array('escape' => false)) ?>
        <?php echo FormHelper::close() ?>

        <p class="m-t"> <small>Portal Haya &copy; <?php echo date('Y') ?></small> </p>
    </div>
</div>