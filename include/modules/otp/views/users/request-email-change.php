<div class="columns">
    <div class="row columns">
        <a href="<?php echo Router::url(array('action' => 'index')) ?>" class="go-back margin-bottom-small">
            <i class="icon icon-filled icon-arrow-left">
                <svg role="img">
                    <use xlink:href="<?php echo ROOT_URL ?>static/img/svg/sprite-crm.svg#arrow-left"/>
                </svg>
            </i>
            Volver
        </a>
    </div>
</div>

<div class="columns header-actions">
    <div class="row margin-top-small margin-bottom">
        <div class="columns medium-4">
            <h1>
                <span class="header-text">Portal Haya</span>
            </h1>
        </div>

    </div>
</div>
<?php if(isset($success)): ?>
<div class="columns">
	<div class="row columns margin-top">
		<div class="callout success">
			<div class="row columns text-center">
				<p class="no-margin">
					Solicitud enviada
				</p>
			</div>
		</div>
	</div>
</div>
<?php endif ?>
<div class="row columns" style="    overflow: hidden;">
    <?php echo FormHelper::open() ?>
	<div class="panel">
		<div class="row collapse panel-header">
			<div class="columns medium-10">
				<h2>Solicitud de cambio de dirección de correo electrónico</h2>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="columns">
					<?php echo FormHelper::input('user[User][email]', 'text', false, array('placeholder' => 'Ejemplo: info@empresa.com', 'required' => null, 'autocomplete' => 'off', 'label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Email'), 'div' => false, 'error' => false)) ?>
				</div>
			</div>
            <p>
                Tu solicitud se enviará al equipo técnico. Recibirás una respuesta lo antes posible.
            </p>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="columns medium-offset-6 medium-6">
					<div class="row">
						<div class="columns small-6">
							<a href="<?php echo Router::url(array('action' => 'index')) ?>" class="expanded button hollow neutral no-margin-bottom">Cancelar</a>
						</div>
						<div class="columns small-6">
							<a href="javascript:void(0)" class="expanded button primary no-margin btn-submit-form">Solicitar</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <?php echo FormHelper::close() ?>
</div>