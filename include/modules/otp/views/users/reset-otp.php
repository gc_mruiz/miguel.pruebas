<div class="columns">
    <div class="row columns">
        <a href="<?php echo Router::url(array('action' => 'index')) ?>" class="go-back margin-bottom-small">
            <i class="icon icon-filled icon-arrow-left">
                <svg role="img">
                    <use xlink:href="<?php echo ROOT_URL ?>static/img/svg/sprite-crm.svg#arrow-left"/>
                </svg>
            </i>
            Volver
        </a>
    </div>
</div>

<div class="columns header-actions">
    <div class="row margin-top-small margin-bottom">
        <div class="columns medium-4">
            <h1>
                <span class="header-text">Portal Haya</span>
            </h1>
        </div>

    </div>
</div>
<div class="row columns" style="    overflow: hidden;">
    <?php echo FormHelper::open() ?>
	<div class="panel">
		<div class="row collapse panel-header">
			<div class="columns medium-10">
				<h2>Restablecer la semilla de doble autenticación</h2>
			</div>
		</div>
		<div class="panel-body">
    		<?php echo FormHelper::input('user[User][reset]', 'hidden', 1) ?>
            <p>
                Si restableces tu semilla la aplicación móvil que hayas configurado dejará de proporcionar códigos válidos y deberás añadir una aplicación nueva.
            </p>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="columns medium-offset-6 medium-6">
					<div class="row">
						<div class="columns small-6">
							<a href="<?php echo Router::url(array('action' => 'index')) ?>" class="expanded button hollow neutral no-margin-bottom">Cancelar</a>
						</div>
						<div class="columns small-6">
							<a href="javascript:void(0)" class="expanded button primary no-margin btn-submit-form">Restablecer</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <?php echo FormHelper::close() ?>
</div>