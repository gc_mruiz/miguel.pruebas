<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div class="crm-login">
            <div>
                <h1 class="logo-name">HAYA</h1>
            </div>
            <p>Por favor indica el código de doble autenticación que aparece en tu app</p>

            <?php echo FormHelper::open() ?>
            <?php echo HtmlHelper::flashMessage() ?>
            <?php echo FormHelper::input('email', 'email', Authentication::get('user', 'email'), array('readonly' => null, 'placeholder' => 'Email', 'required' => null, 'autocomplete' => 'off', 'label' => false, 'div' => false, 'error' => false)) ?>
            <?php echo FormHelper::input('code', 'password', false, array('placeholder' => 'Código', 'required' => null, 'autocomplete' => 'off', 'label' => false, 'div' => false, 'error' => false)) ?>
            <?php echo FormHelper::submit('Continuar', array('div' => false, 'class' => 'button expanded')) ?>
            <?php echo HtmlHelper::link('<small>No tengo acceso a mi app, enviadme un código por email a ' . Authentication::get('user', 'email') . '</small>', array('action' => 'sendOtpToken'), array('escape' => false)) ?>
            <hr>
            <?php echo HtmlHelper::link('<small>cancelar inicio de sesión</small>', array('action' => 'logout'), array('escape' => false)) ?>
            <?php echo FormHelper::close() ?>
        </div>
        <p class="m-t"> <small>Haya Real Estate &copy; <?php echo date('Y') ?></small> </p>
    </div>
</div>