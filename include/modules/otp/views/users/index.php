<?php if(Session::get('flash[message]')): ?>
<div class="columns">
    <div class="row columns margin-top">
        <div class="callout success">
            <div class="row columns text-center">
                <p class="no-margin">
                    <?php echo Session::get('flash[message]') ?>
                </p>
            </div>
        </div>
    </div>
</div>
<?php Session::set('flash[message]', null); Session::set('flash[title]', null); endif ?>
<?php if(!$apps) $apps = array(); $menuGroups = array(); $cleanApps = array(); foreach($apps as $app): if(empty($app['App']['menu_group'])) $app['App']['menu_group'] = 'General'; $cleanApps[$app['App']['menu_group']][] = $app; endforeach; ?>
<?php foreach($cleanApps as $menu_group => $menuGroupApps): if(isset($group_name) && $group_name != $menu_group) continue; ?>
<div class="columns header-actions">
    <div class="row margin-top-small margin-bottom">
        <div class="columns small-6 medium-4">
            <h1>
                <span class="header-text"><?php echo $menu_group ?></span>
            </h1>
        </div>
    </div>
</div>
<div class="columns">
    <div class="row" data-equalizer data-equalize-on="medium">
        <?php $count = 0; if(!$menuGroupApps) $menuGroupApps = array(); foreach($menuGroupApps as $app): ?>
        <div class="columns medium-3 margin-bottom <?php if($count == sizeof($menuGroupApps) - 1) echo 'end' ?>">
            <div class="feedback placeholder-actions no-padding">
                <div class="placeholder-actions-content" data-equalizer-watch>
                    <?php if($app['App']['picture_url']): ?>
                    <p><img src="<?php echo $app['App']['picture_url'] ?>" style="width: 144px; height: 102px;"></p>
                    <?php else: ?>
                    <p>
                        <span style="display: inline-block">
                            <span style="display: table; width: 144px; height: 102px; background: #043D59; color: white; position: relative;">
                                <span style="display: table-cell; vertical-align: middle; text-align: center; padding: 5%"><?php echo $app['App']['name'] ?></span>
                            </span>
                        </span>
                    </p>
                    <?php endif ?>
                    <p style="min-height: 45px;"><?php echo $app['App']['description'] ?></p>
                    <a target="_blank" href="<?php echo Router::url(array('controller' => 'Users', 'action' => 'appForm', $app['App']['code'])) ?>" class="button">Acceder</a>
                </div>
            </div>
        </div>
        <?php $count++; endforeach ?>
    </div>
</div>
<?php endforeach ?>