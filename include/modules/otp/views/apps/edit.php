<div class="columns header-actions">
	<div class="row margin-top-small margin-bottom">
		<div class="columns medium-6">
			<h1>
				<span class="header-text">App</span>
			</h1>
		</div>
	</div>
</div>
<?php if(Session::get('flash[message]')): ?>
<div class="columns">
	<div class="row columns margin-top">
		<div class="callout success">
			<div class="row columns text-center">
				<p class="no-margin">
					<?php echo Session::get('flash[message]'); Session::set('flash', false) ?>
				</p>
			</div>
		</div>
	</div>
</div>
<?php endif ?>
<div class="row columns" style="overflow: hidden;">
	<?php echo FormHelper::open() ?>
	<?php echo FormHelper::input('app[App][code]', 'hidden', false) ?>
	<?php include('form-tab.php') ?>
	<?php echo FormHelper::close() ?>
</div>