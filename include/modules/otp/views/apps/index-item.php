<tr data-role="app-stock" data-app-stock-id="<?php echo $app['App']['code'] ?>">
    <td>
        <div class="card card-common">
            <div>
                <span class="truncate">
                    <a href="<?php echo Router::url(array('controller' => 'Apps', 'action' => 'edit', $app['App']['code'])) ?>" title="<?php echo $app['App']['menu_group'] ?>"><?php echo $app['App']['menu_group'] ?></a>
                </span>
            </div>
        </div>
    </td>
    <td>
        <div class="card card-common">
            <div>
                <span class="truncate">
                    <a href="<?php echo Router::url(array('controller' => 'Apps', 'action' => 'edit', $app['App']['code'])) ?>" title="<?php echo $app['App']['name'] ?>"><?php echo $app['App']['name'] ?></a>
                </span>
            </div>
        </div>
    </td>
    <td>
        <div class="card card-common">
            <div>
                <span class="truncate">
                    <a href="<?php echo Router::url(array('controller' => 'Apps', 'action' => 'edit', $app['App']['code'])) ?>" title="<?php echo $app['App']['description'] ?>"><?php echo $app['App']['description'] ?></a>
                </span>
            </div>
        </div>
    </td>
    <td class="text-center">
        <a href="javascript:void(0)" class="custom-dropdown button transparent dots-only float-right no-margin-bottom list-item-actions" data-toggle="RANDOMAJAXID-apps-stock-more-options-<?php echo $app['App']['code'] ?>">
            <span class="dots-button"></span>
        </a>
        <div class="dropdown-pane bottom" id="RANDOMAJAXID-apps-stock-more-options-<?php echo $app['App']['code'] ?>" data-dropdown data-auto-focus="false" data-h-offset="0" data-v-offset="0" data-close-on-click="true">
            <ul class="dropdown-pane-list">
                <li class="dropdown-pane-list-item">
                    <a href="<?php echo Router::url(array('controller' => 'Apps', 'action' => 'edit', $app['App']['code'])) ?>">
                        <i class="icon icon-edit">
                            <svg role="img">
                                <use xlink:href="<?php echo ROOT_URL ?>static/img/svg/sprite-crm.svg#edit"/>
                            </svg>
                        </i>
                        Editar
                    </a>
                </li>
                <li class="dropdown-pane-list-item">
                    <a data-confirm-text="¿Eliminar la página?" href="<?php echo Router::url(array('controller' => 'Apps', 'action' => 'delete', $app['App']['code'])) ?>">
                        <i class="icon icon-delete">
                            <svg role="img">
                                <use xlink:href="<?php echo ROOT_URL ?>static/img/svg/sprite-crm.svg#delete"/>
                            </svg>
                        </i>
                        Borrar
                    </a>
                </li>
            </ul>
        </div>
    </td>
</tr>