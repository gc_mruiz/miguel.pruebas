<div class="tabs-before" id="spanish">
    <div class="panel">
        <div class="row collapse panel-header">
            <div class="columns medium-10">
                <h2>Datos de la app</h2>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <?php if(isset($app)): ?>
                <div class="columns medium-12">
                    <?php echo FormHelper::input('app[App][name]', 'text', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Nombre'),'div' => false,'error' => false,)) ?>
                </div>
                <?php else: ?>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][code]', 'text', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Código (minúsculas sin espacios ni caracteres especialess)'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][name]', 'text', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Nombre'),'div' => false,'error' => false,)) ?>
                </div>
                <?php endif ?>
                <div class="columns medium-12">
                    <?php echo FormHelper::input('app[App][description]', 'textarea', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Descripción'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][domain]', 'text', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Dominio'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][ip]', 'text', false, array('autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'IP'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::select('app[App][https]', array(0 => 'No', 1 => 'Sí'), false, array('associative' => true, 'required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'HTTPS'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][subdomain]', 'text', false, array('autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Subdominio de proxy'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][startup_path]', 'text', false, array('autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Ruta de arranque'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][sha256_key]', 'text', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Clave SHA256'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][group]', 'text', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Grupo de LDAP'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][menu_group]', 'text', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Agrupación (menú)'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6">
                    <?php echo FormHelper::input('app[App][picture_url]', 'text', false, array('required' => null, 'autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'URL público del icono de la aplicación'),'div' => false,'error' => false,)) ?>
                </div>
                <div class="columns medium-6 end">
                    <?php echo FormHelper::select('app[App][ssl_version]', $listOfSslVersions, false, array('autocomplete' => 'off','label' => array('input' => 'sibling', 'side' => 'before', 'content' => 'Versión de SSL'),'div' => false,'error' => false, 'placeholder' => 'Por defecto',)) ?>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="columns medium-offset-6 medium-6">
                    <div class="row">
                        <div class="columns small-6">
                            <a href="<?php echo Router::url(array('action' => 'index')) ?>"
                               class="expanded button hollow neutral no-margin-bottom">Cancelar</a>
                        </div>
                        <div class="columns small-6">
                            <a href="javascript:void(0)" class="expanded button primary no-margin btn-submit-form">Guardar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>