<div>
	<div class="columns header-actions">
		<div class="row margin-top-small margin-bottom">
			<div class="columns medium-8">
				<h1>
					<span class="header-text">Apps</span>
				</h1>
			</div>
			<div class="columns medium-4 text-right">
				<a href="<?php echo Router::url(array('controller' => 'Apps', 'action' => 'add')) ?>" class="button primary has-icon">
					<span>
						<i class="icon icon-duplicate">
							<svg role="img">
								<use xlink:href="<?php echo ROOT_URL ?>static/img/svg/sprite-crm.svg#duplicate"/>
							</svg>
						</i>
						Nueva app
					</span>
				</a>
			</div>
		</div>
	</div>
    <?php include('index-result.php') ?>
</div>