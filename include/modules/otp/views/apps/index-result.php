<div>
    <div class="columns margin-top-large">
        <div class="row">
            <div class="columns medium-6">
                <h2 class="margin-top-small">Aplicaciones</span></h2>
            </div>
        </div>
    </div>
    <?php if(Session::get('flash[message]')): ?>
    <div class="columns">
        <div class="row columns margin-top">
            <div class="callout success">
                <div class="row columns text-center">
                    <p class="no-margin">
                        <?php echo Session::get('flash[message]'); Session::set('flash', false) ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>
    <div class="columns margin-top-small">
        <div class="row">
            <div class="columns margin-bottom">
                <table class="results hover" id="RANDOMAJAXID-apps-stock-result-table">
                    <caption class="loading loading-full">
                        <span class="loading-content">
                            <span class="loading-info">
                                <svg class="circular-spinner" viewBox="25 25 50 50">
                                  <circle class="path-spinner" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                                </svg>
                            </span>
                        </span>
                    </caption>
                    <thead>
                    <tr>
                        <th width="150">
                            Grupo
                        </th>
                        <th width="300">
                            Nombre
                        </th>
                        <th>
                            Descripción
                        </th>
                        <th width="50" class="text-center ">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php if(sizeof($apps)): ?>
                        <?php include('index-listed.php') ?>
                        <?php else: ?>
                        <?php include('index-result-empty.php') ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>