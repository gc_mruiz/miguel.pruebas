(function ($) {
    var AuthApp = function () {
        this.init();
    };

    AuthApp.prototype = {
        init: function () {
            AuthApp.i = this;
            AuthApp.i.initialization();
            AuthApp.i.generalListeners();
        },
        initialization: function () {
        },
        filterValue: function (value, type) {
            var filter = '';

            if (type == 'none') {
                return true;
            } else {
                value = $.trim(value);

                switch (type) {
                    case 'text' :
                        value = value.toUpperCase();
                        filter = /^[A-ZÁÉÍÓÚÑ \-]{2,150}$/;
                        break;

                    case 'email' :
                        value = value.toLowerCase();
                        filter = /^[a-z0-9._%\-]+@[a-z0-9.\-]+\.(?:[a-z]{2}|aero|asia|biz|pro|es|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|xxx)$/i;
                        break;

                    case 'password' :
                        filter = /^[A-Za-z0-9\-\_]{6,20}$/;
                        break;

                    case 'phone' :
                        filter = /^[0-9 ]{9,20}$/;
                        break;
                }

                return filter.test(value);
            }
        },
        generalListeners: function () {
            /**
             * Registration with Facebook
             */
            $(document).on('click', '#access-fb', function (event) {
                event.preventDefault();
                window.open(LOCALED_ROOT_URL + 'auth-access/access-fb/', 'Facebook registration Window', 'width=800, height=600');
            });

            /**
             * Registration with Twitter
             */
            $(document).on('click', '#access-tw', function (event) {
                event.preventDefault();
                window.open(LOCALED_ROOT_URL + 'auth-access/access-tw/', 'Twitter registration Window', 'width=800, height=600');
            });

            /**
             * Registration with Google Plus
             */
            $(document).on('click', '#access-go', function (event) {
                event.preventDefault();
                window.open(LOCALED_ROOT_URL + 'auth-access/access-go/', 'Google registration Window', 'width=800, height=600');
            });

            /**
             * Registration with LinkedIn
             */
            $(document).on('click', '#access-li', function (event) {
                event.preventDefault();
                window.open(LOCALED_ROOT_URL + 'auth-access/access-li/', 'LinkedIn registration Window', 'width=800, height=600');
            });
        }
    };

    $(document).ready(function () {
        window.authApp = new AuthApp();
    });
})(jQuery);