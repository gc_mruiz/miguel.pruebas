function Form() {
}

(function ($) {
    $.extend($.summernote.lang, {
        'es-ES': {
            font: {
                bold: 'Negrita',
                italic: 'Cursiva',
                underline: 'Subrayado',
                clear: 'Quitar estilo de fuente',
                height: 'Altura de línea',
                name: 'Fuente',
                strikethrough: 'Tachado',
                superscript: 'Superíndice',
                subscript: 'Subíndice',
                size: 'Tamaño de la fuente'
            },
            image: {
                image: 'Imagen',
                insert: 'Insertar imagen',
                resizeFull: 'Redimensionar a tamaño completo',
                resizeHalf: 'Redimensionar a la mitad',
                resizeQuarter: 'Redimensionar a un cuarto',
                floatLeft: 'Flotar a la izquierda',
                floatRight: 'Flotar a la derecha',
                floatNone: 'No flotar',
                shapeRounded: 'Forma: Redondeado',
                shapeCircle: 'Forma: Círculo',
                shapeThumbnail: 'Forma: Marco',
                shapeNone: 'Forma: Ninguna',
                dragImageHere: 'Arrastrar una imagen o texto aquí',
                dropImage: 'Suelta la imagen o texto',
                selectFromFiles: 'Seleccionar desde los archivos',
                maximumFileSize: 'Tamaño máximo del archivo',
                maximumFileSizeError: 'Has superado el tamaño máximo del archivo.',
                url: 'URL de la imagen',
                remove: 'Eliminar imagen'
            },
            video: {
                video: 'Vídeo',
                videoLink: 'Link del vídeo',
                insert: 'Insertar vídeo',
                url: '¿URL del vídeo?',
                providers: '(YouTube, Vimeo, Vine, Instagram, DailyMotion o Youku)'
            },
            link: {
                link: 'Link',
                insert: 'Insertar link',
                unlink: 'Quitar link',
                edit: 'Editar',
                textToDisplay: 'Texto para mostrar',
                url: '¿Hacia que URL lleva el link?',
                openInNewWindow: 'Abrir en una nueva ventana'
            },
            table: {
                table: 'Tabla'
            },
            hr: {
                insert: 'Insertar línea horizontal'
            },
            style: {
                style: 'Estilo',
                p: 'p',
                blockquote: 'Cita',
                pre: 'Código',
                h1: 'Título 1',
                h2: 'Título 2',
                h3: 'Título 3',
                h4: 'Título 4',
                h5: 'Título 5',
                h6: 'Título 6'
            },
            lists: {
                unordered: 'Lista desordenada',
                ordered: 'Lista ordenada'
            },
            options: {
                help: 'Ayuda',
                fullscreen: 'Pantalla completa',
                codeview: 'Ver código fuente'
            },
            paragraph: {
                paragraph: 'Párrafo',
                outdent: 'Menos tabulación',
                indent: 'Más tabulación',
                left: 'Alinear a la izquierda',
                center: 'Alinear al centro',
                right: 'Alinear a la derecha',
                justify: 'Justificar'
            },
            color: {
                recent: 'Último color',
                more: 'Más colores',
                background: 'Color de fondo',
                foreground: 'Color de fuente',
                transparent: 'Transparente',
                setTransparent: 'Establecer transparente',
                reset: 'Restaurar',
                resetToDefault: 'Restaurar por defecto'
            },
            shortcut: {
                shortcuts: 'Atajos de teclado',
                close: 'Cerrar',
                textFormatting: 'Formato de texto',
                action: 'Acción',
                paragraphFormatting: 'Formato de párrafo',
                documentStyle: 'Estilo de documento',
                extraKeys: 'Teclas adicionales'
            },
            help: {
                'insertParagraph': 'Insertar párrafo',
                'undo': 'Deshacer última acción',
                'redo': 'Rehacer última acción',
                'tab': 'Tabular',
                'untab': 'Eliminar tabulación',
                'bold': 'Establecer estilo negrita',
                'italic': 'Establecer estilo cursiva',
                'underline': 'Establecer estilo subrayado',
                'strikethrough': 'Establecer estilo tachado',
                'removeFormat': 'Limpiar estilo',
                'justifyLeft': 'Alinear a la izquierda',
                'justifyCenter': 'Alinear al centro',
                'justifyRight': 'Alinear a la derecha',
                'justifyFull': 'Justificar',
                'insertUnorderedList': 'Insertar lista desordenada',
                'insertOrderedList': 'Insertar lista ordenada',
                'outdent': 'Reducir tabulación del párrafo',
                'indent': 'Aumentar tabulación del párrafo',
                'formatPara': 'Cambiar estilo del bloque a párrafo (etiqueta P)',
                'formatH1': 'Cambiar estilo del bloque a H1',
                'formatH2': 'Cambiar estilo del bloque a H2',
                'formatH3': 'Cambiar estilo del bloque a H3',
                'formatH4': 'Cambiar estilo del bloque a H4',
                'formatH5': 'Cambiar estilo del bloque a H5',
                'formatH6': 'Cambiar estilo del bloque a H6',
                'insertHorizontalRule': 'Insertar línea horizontal',
                'linkDialog.show': 'Mostrar panel enlaces'
            },
            history: {
                undo: 'Deshacer',
                redo: 'Rehacer'
            },
            specialChar: {
                specialChar: 'CARACTERES ESPECIALES',
                select: 'Selecciona Caracteres especiales'
            }
        }
    });
})(jQuery);

Form.beautifyDateInputs = function () {
    // Retrieve all date inputs
    var dateInputs = $('div.input.date input');

    // For each of them, beautify
    dateInputs.each(function (index, input) {
        // Be sure to have the $ object
        input = $(input);

        // Check for class
        if (input.parents('.input.date').hasClass('date-beautified')) {
            return;
        }

        // Add class to avoid making this twice for the same input
        input.parent().addClass('date-beautified');
        var hiddenInput = $('<input type="hidden">').attr('name', input.attr('name')).attr('id', input.attr('id')).val(input.val());
        input.after(hiddenInput);
        input.remove();
        input = hiddenInput;

        // Hide original input and append new one
        input.after('<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value=""></div>');
        var $cloneInput = input.parent().find('.input-group.date input');
        $cloneInput[0].originalInput = input;

        // Set data format if not set
        if ($cloneInput.attr('data-format') === undefined || $cloneInput.attr('data-format') == '') {
            if (LANG == 'en') {
                $cloneInput.attr('data-format', 'm/d/Y');
            } else {
                $cloneInput.attr('data-format', 'd/m/Y');
            }
        }

        // Format time on cloned input
        $cloneInput.val(Form.formatDisplayDate(input.val()));

        // Init datetimepicker
        jQuery.datetimepicker.setLocale(LANG);
        $cloneInput.datetimepicker({
            weekStart: 1,
            timepicker: false,
            format: $cloneInput.data('format')
        }).on('change', function (ev) {
            var input = this;
            if (input.last_value === undefined || input.last_value != $(input).val()) {
                input.last_value = $(input).val();
                input.originalInput.val(Form.formatReverseDisplayDate(input.last_value, false));
            }
        });
    });
};

Form.beautifyDateTimeInputs = function () {
    // Retrieve all date inputs
    var dateInputs = $('div.input.datetime input');

    // For each of them, beautify
    dateInputs.each(function (index, input) {
        // Be sure to have the $ object
        input = $(input);

        // Check for class
        if (input.parents('.input.date').hasClass('date-beautified')) {
            return;
        }

        // Add class to avoid making this twice for the same input
        input.parent().addClass('date-beautified');
        var hiddenInput = $('<input type="hidden">').attr('name', input.attr('name')).attr('id', input.attr('id')).val(input.val());
        input.after(hiddenInput);
        input.remove();
        input = hiddenInput;

        // Hide original input and append new one
        input.after('<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value=""></div>');
        var $cloneInput = input.parent().find('.input-group.date input');
        $cloneInput[0].originalInput = input;

        // Set data format if not set
        if ($cloneInput.attr('data-format') === undefined || $cloneInput.attr('data-format') == '') {
            if (LANG == 'en') {
                $cloneInput.attr('data-format', 'm/d/Y H:i');
            } else {
                $cloneInput.attr('data-format', 'd/m/Y H:i');
            }
        }

        // Format time on cloned input
        $cloneInput.val(Form.formatDisplayDate(input.val(), true));

        // Init datetimepicker
        jQuery.datetimepicker.setLocale(LANG);
        $cloneInput.datetimepicker({
            weekStart: 1,
            format: $cloneInput.data('format')
        }).on('change', function (ev) {
            var input = this;
            if (input.last_value === undefined || input.last_value != $(input).val()) {
                input.last_value = $(input).val();
                input.originalInput.val(Form.formatReverseDisplayDate(input.last_value, true));
            }
        });
    });
};

Form.emptyDataInputs = function (anchor) {
    $(anchor).parent().find('input[type="text"], input[type="hidden"]').val('');
};

Form.formatDisplayDate = function (date_str, with_time) {
    // Language config
    var dateFormats = {
        'en': 'm/d/Y',
        'es': 'd/m/Y'
    };

    if (date_str.length == 0) {
        return '';
    }

    var year = date_str.substring(0, 4);
    var month = date_str.substring(5, 7);
    var day = date_str.substring(8, 10);

    var date_format = dateFormats[LANG];

    var new_formated_date = date_format.split('Y').join(year).split('m').join(month).split('d').join(day);

    if (with_time) {
        new_formated_date += date_str.substring(10).substring(0, 6);
    }

    return new_formated_date;
};

Form.formatReverseDisplayDate = function (date_str, with_time) {
    // Language config
    var dateFormats = {
        'en': {
            'm': [0, 2],
            'd': [3, 5],
            'Y': [6, 10]
        },
        'es': {
            'd': [0, 2],
            'm': [3, 5],
            'Y': [6, 10]
        }
    };

    if (date_str.length == 0) {
        return '';
    }

    var date_format = dateFormats[LANG];

    var year = date_str.substring(date_format['Y'][0], date_format['Y'][1]);
    var month = date_str.substring(date_format['m'][0], date_format['m'][1]);
    var day = date_str.substring(date_format['d'][0], date_format['d'][1]);

    var new_formated_date = year + '-' + month + '-' + day;

    if (with_time) {
        new_formated_date += date_str.substring(10);
    }

    return new_formated_date;
};

Form.formatStoreDate = function (date) {
    if (date == null) {
        return '';
    }

    // Extract date parts
    var year = date.getFullYear();
    var month = (date.getMonth() + 1 >= 10 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1));
    var day = (date.getDate() >= 10 ? date.getDate() : '0' + date.getDate());

    // Return MySQL formatted date
    return year + '-' + month + '-' + day;
};

Form.beautifyPictureSelector = function () {
    // Retrieve all picture inputs
    var pictureInputs = $('div.input.picture input');

    // For each of them, beautify
    pictureInputs.each(function (index, input) {
        // Be sure to have the $ object
        input = $(input);

        // Check for class
        if (input.parent().hasClass('picture-beautified')) {
            return;
        }

        // Add class to avoid making this twice for the same input
        input.parent().addClass('picture-beautified');

        // Set input to type=hidden
        input[0].type = 'hidden';

        // Check for picture mode property
        var picture_width = input.attr('data-picture-width');
        var picture_height = input.attr('data-picture-height');

        var scale_mode = input.attr('data-scale-mode');
        var width = input.attr('data-scale-width');
        var height = input.attr('data-scale-height');

        // Create an picture display the readonly value
        var displayPicture = $('<img>').addClass('display-picture')
        if (input.attr('data-picture-320_token')) {
            displayPicture.attr('src', LOCALED_ROOT_URL + 'pictures/thumb/320/' + input.attr('data-picture-id') + '/' + input.attr('data-picture-320_token'));
        } else {
            displayPicture.attr('src', input.attr('data-picture-src'));
        }

        var displayPictureContainer = null;

        // If the picture must be scaled, create its container
        if (!(scale_mode && width != null && width != "0" && height != null && height != "0")) {
            scale_mode = 'centered';
            width = 100;
            height = 100;
        }
        // Container
        displayPictureContainer = $('<div>').addClass('display-picture-container').css('width', parseInt(width)).css('height', parseInt(height)).append(displayPicture);

        // Append container
        input.after(displayPictureContainer);

        // Positionate original picture
        Form.positionatePicture(displayPicture, scale_mode, picture_width, picture_height);

        // Create a new input to change the value
        var changeInput = $('<input type="button">').addClass('change-picture').addClass('btn btn-primary btn-sm inline').val('Sel.').click(function () {
            // Retrieve both inputs
            var input = this.input;
            var displayPicture = this.displayPicture;

            // Open picture selection
            Form.selectPicture(function (picture) {
                if (picture != null) {
                    // Store values
                    displayPicture.attr('src', LOCALED_ROOT_URL + 'pictures/thumb/320/' + picture.Picture.id + '/' + picture.Picture['320_token']);
                    input.val(picture.Picture.id);

                    // Positionate
                    Form.positionatePicture(displayPicture, scale_mode, picture.Picture.width, picture.Picture.height);
                }
            });
        });

        var removeInput = $('<input type="button">').addClass('remove-picture').addClass('btn btn-danger btn-sm inline').val('X').click(function () {
            // Retrieve both inputs
            var input = this.input;
            var displayPicture = this.displayPicture;

            // Store values
            displayPicture.attr('src', ROOT_URL + 'img/dummy.png');
            input.val('');

            // Positionate
            Form.positionatePicture(displayPicture, scale_mode, width, height);
        });

        // Append container
        displayPictureContainer.append(changeInput);
        displayPictureContainer.append(removeInput);

        // Store for future access
        changeInput[0].input = input;
        changeInput[0].displayPicture = displayPicture;
        removeInput[0].input = input;
        removeInput[0].displayPicture = displayPicture;
    });
};

/**
 * Scales and positionates an picture inside its container cropped or centered.
 *
 * @param picture HTMLPicture the picture to positionate.
 * @param mode string position mode (centered/cropped)
 * @param picture_width integer HTMLPicture width (picture could not be yet loaded)
 * @param picture_height integer HTMLPicture height (picture could not be yet loaded)
 * @return void
 */
Form.positionatePicture = function (picture, mode, picture_width, picture_height) {
    // We need the $ object
    picture = $(picture);

    // Check if valid mode
    if (mode == 'centered' || mode == 'cropped') {
        // Retrieve container dimensions
        var containerWidth = picture.parent().width();
        var containerHeight = picture.parent().height();

        // Vars for new picture dimensions and position
        var newPictureWidth = null;
        var newPictureHeight = null;
        var newPictureMarginLeft = null;
        var newPictureMarginTop = null;

        // Calculate dimensions and position based on the mode and the container dimensions
        if ((mode == 'centered' && picture_height / picture_width * containerWidth < containerHeight) || (mode != 'centered' && picture_height / picture_width * containerWidth > containerHeight)) {
            // Picture will be scaled to fit the container width
            newPictureWidth = containerWidth;
            newPictureHeight = picture_height / picture_width * containerWidth;
            newPictureMarginLeft = 0;
            newPictureMarginTop = (containerHeight - newPictureHeight) / 2;
        } else {
            // Picture will be scaled to fit the container height
            newPictureWidth = picture_width / picture_height * containerHeight;
            newPictureHeight = containerHeight;
            newPictureMarginTop = 0;
            newPictureMarginLeft = (containerWidth - newPictureWidth) / 2;
        }

        // Apply dimensions an position
        picture.css('width', newPictureWidth);
        picture.css('height', newPictureHeight);
        picture.css('margin-left', newPictureMarginLeft);
        picture.css('margin-top', newPictureMarginTop);
    }
};

Form.onPictureSelected = null;

Form.selectPicture = function (callback) {
    Form.onPictureSelected = callback;

    var newWindow = window.open(LOCALED_ROOT_URL + 'pictures/admin-index/', 'picture_manager', 'width=690,height=600,scrollbars=yes');
};

Form.beautifHtmlInputs = function () {
    // Retrieve all html inputs
    var $htmlInputs = $('[data-validation-type="html"]').parent();

    // For each of them, beautify
    $htmlInputs.each(function () {
        // Check for class
        if ($(this).hasClass('html-beautified')) {
            return;
        }

        $(this).addClass('html-beautified');

        // Turn textarea into hidden input
        $textarea = $(this).find('textarea');
        $inputHidden = $('<input type="hidden">');
        $inputHidden.attr('name', $textarea.attr('name'));
        $inputHidden.attr('id', $textarea.attr('id'));
        $inputHidden.val($textarea.val());
        $textarea.after($inputHidden).remove();

        // Add <p> with contents
        $inputHidden.after('<p class="summernote">');
        $(this).children('p.summernote')[0].innerHTML = $inputHidden.val();

        $(this).children('p.summernote').summernote({
            lang: (LANG == 'es' ? 'es-ES' : 'en-US' ),
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']]
            ],
            onChange: function (contents, $editable) {
                $inputHidden.val(contents.html());
            }
        });

        var $summernote = $(this).children('p.summernote');

        if ($(this).parents('form').length > 0) {
            var form = $(this).parents('form').get(0);

            if (!form.summernotes) {
                form.summernotes = [];
            }

            form.summernotes.push({$summernote: $summernote, $inputHidden: $inputHidden});
        }
    });

    $htmlInputs.parents('form').submit(function () {
        var form = this;

        if (!form.summernotes) {
            form.summernotes = [];
        }

        for (var i = 0; i < form.summernotes.length; i++) {
            form.summernotes[i].$inputHidden.val(form.summernotes[i].$summernote.code());
        }
    });
};

Form.pictureSelected = function (w, imageData) {
    if (typeof (Form.onPictureSelected) != 'undefined' && Form.onPictureSelected != null) {
        Form.onPictureSelected(imageData);
        w.close();
    }
};

Form.sortTrs = function () {
    if ($('.sortable-table tr').length > 0) {
        $('.sortable-table tr').parent().sortable({
            start: function (e, ui) {
                ui.placeholder.height(ui.helper.outerHeight());
            },
            placeholder: 'reorder-placeholder',
            change: function (event, ui) {
                $('.form-actions').show();
                $('.pagination-container, .btn-move-up').hide();
            },
            items: 'tr:not(.not-sortable)'
        }).disableSelection();
    }
};

$(document).ready(function () {
    Form.beautifyDateInputs();
    Form.beautifyPictureSelector();
    Form.beautifyDateTimeInputs();
    Form.beautifHtmlInputs();
    Form.sortTrs();
});