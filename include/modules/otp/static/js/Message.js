function Message() {
}

Message.showText = function(text, header) {
	var modal = $('<div>').attr('class', 'modal inmodal').attr('tabindex', -1).attr('role', 'dialog').attr('aria-hidden', 'true');
	modal.append($('<div>').addClass('modal-dialog'));
	modal.children().append($('<div>').addClass('modal-content animated fadeIn'));
	var modalContent = modal.children().children();

	modalContent.append($('<div>').addClass('modal-header'));
	modal.find('div.modal-header').append($('<button type="button">').text('x'));
	modal.find('div.modal-header button').addClass('close').attr('data-dismiss', 'modal').attr('aria-hidden', 'true');

	modal.find('div.modal-header').append($('<h3>'));
	if (header == undefined || header.length == 0) {
		modal.find('div.modal-header h3').html('&nbsp;');
	} else {
		modal.find('div.modal-header h3').text(header);
	}

	modalContent.append($('<div>').addClass('modal-body'));
	modal.find('div.modal-body').append($('<p>').text(text));

	modalContent.append($('<div>').addClass('modal-footer'));
	modal.find('div.modal-footer').append($('<a>').attr('href', 'javascript:void(0)').addClass('btn').addClass('btn-primary').text('OK').attr('data-dismiss', 'modal'));

	modal.modal();
};
