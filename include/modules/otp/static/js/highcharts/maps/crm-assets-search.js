(function ($) {
    var self = {
        MODE_PROVINCE: 0,
        MODE_CITY: 1,
        MODE_ASSET: 2,
        $map: null,
        map: null,
        mode: null,
        active: false,
        filters: {},
        markers: [],
        fitBoundsTimeout: null,
        toggle: function (active) {
            if (active) {
                if (!self.active) {
                    self.active = true;
                    $('.asset-ajax-search:not(.reveal-body)').data('searchObj').search(true);

                    if (self.$map) {
                        $('#map-results').after(self.$map).remove();
                    }
                }
            } else if (!active) {
                if (self.active) {
                    self.active = false;
                    $('.asset-ajax-search:not(.reveal-body)').data('searchObj').search(true);
                    self.$map = $('#map-results');
                }
            }

            libs.xhr.postJSON(LOCALED_ROOT_URL + 'assets/set-map-status/', {active: (active ? 1 : 0)});
        },
        updateTimeout: null,
        update: function (filters, force) {
            self.filters = filters;
            self.filters.pagination_random_token = undefined;
            self.filters.per_page = undefined;
            self.filters.order_by = undefined;

            if (!self.active) {
                return;
            }

            clearTimeout(self.updateTimeout);

            if (!force) {
                self.updateTimeout = setTimeout(function () {
                    self.update(filters, true);
                }, 200);
            }

            if (self.map == null) {
                self.map = new google.maps.Map($('#map-results').get(0), {
                    zoom: 5,
                    center: {lat: 40.4343517135316, lng: -3.6289586000000327}
                });
            }

            self.setMode();
            self.setMinZoom();

            for (var iM = 0; iM < self.markers.length; iM++) {
                self.markers[iM].setMap(null);
            }
            self.markers = [];

            if (self.mode == self.MODE_ASSET) {
                libs.xhr.getJSON(LOCALED_ROOT_URL + 'assets/index/object/1/', self.filters, function (result) {
                    if (result.response.succeeded) {
                        if (result.response.assets.length > 0) {
                            var bounds = new google.maps.LatLngBounds();

                            for (var i = 0; i < result.response.assets.length; i++) {
                                (function (i) {
                                    var marker = new google.maps.Marker({
                                        map: self.map,
                                        position: {
                                            lat: parseFloat(result.response.assets[i].Asset.lat),
                                            lng: parseFloat(result.response.assets[i].Asset.lng)
                                        }
                                    });

                                    self.markers.push(marker);

                                    bounds.extend(marker.position);

                                    marker.addListener('click', function () {
                                        location.href = LOCALED_ROOT_URL + 'assets/view/' + result.response.assets[i].Asset.haya_asset_identifier;
                                    });
                                })(i);
                            }

                            clearTimeout(self.fitBoundsTimeout);
                            self.fitBoundsTimeout = setTimeout(function () {
                                self.map.fitBounds(bounds);

                                if (self.map.getZoom() > 15) {
                                    self.map.setZoom(15);
                                }
                            }, 200);
                        }
                    }
                });
            } else if (self.mode == self.MODE_CITY) {
                libs.xhr.getJSON(LOCALED_ROOT_URL + 'geo/cities-obj/', self.filters, function (result) {
                    if (result.response.succeeded) {
                        if (result.response.geoCities.length > 0) {
                            var bounds = new google.maps.LatLngBounds();

                            for (var i = 0; i < result.response.geoCities.length; i++) {
                                (function (i) {
                                    var marker = new google.maps.Marker({
                                        icon: generateIcon(result.response.geoCities[i].GeoCity.asset_count),
                                        map: self.map,
                                        position: {
                                            lat: parseFloat(result.response.geoCities[i].GeoCity.lat),
                                            lng: parseFloat(result.response.geoCities[i].GeoCity.lng)
                                        }
                                    });

                                    self.markers.push(marker);

                                    bounds.extend(marker.position);

                                    marker.addListener('click', function () {
                                        $('#GeoCityId').val(result.response.geoCities[i].GeoCity.id);
                                        $('#GeoCityId').change();
                                    });

                                    clearTimeout(self.fitBoundsTimeout);
                                    self.fitBoundsTimeout = setTimeout(function () {
                                        self.map.fitBounds(bounds);

                                        if (self.map.getZoom() > 11) {
                                            self.map.setZoom(11);
                                        }
                                    }, 200);
                                })(i);
                            }
                        }
                    }
                });
            } else if (self.mode == self.MODE_PROVINCE) {
                libs.xhr.getJSON(LOCALED_ROOT_URL + 'geo/provinces-obj/', self.filters, function (result) {
                    if (result.response.succeeded) {
                        if (result.response.geoProvinces.length > 0) {
                            var bounds = new google.maps.LatLngBounds();

                            for (var i = 0; i < result.response.geoProvinces.length; i++) {
                                (function (i) {
                                    var marker = new google.maps.Marker({
                                        icon: generateIcon(result.response.geoProvinces[i].GeoProvince.asset_count),
                                        map: self.map,
                                        position: {
                                            lat: parseFloat(result.response.geoProvinces[i].GeoProvince.lat),
                                            lng: parseFloat(result.response.geoProvinces[i].GeoProvince.lng)
                                        }
                                    });

                                    self.markers.push(marker);

                                    bounds.extend(marker.position);

                                    marker.addListener('click', function () {
                                        $('#GeoCityId').val('');
                                        $('#GeoProvinceId').val(result.response.geoProvinces[i].GeoProvince.id);
                                        $('#GeoProvinceId').change();
                                    });

                                    clearTimeout(self.fitBoundsTimeout);
                                    self.fitBoundsTimeout = setTimeout(function () {
                                        self.map.fitBounds(bounds);
                                    }, 200);
                                })(i);
                            }
                        }
                    }
                });
            }
        },
        setMode: function () {
            if (!self.filters.geo_province_id) {
                self.mode = self.MODE_PROVINCE;
            } else if (!self.filters.geo_city_id) {
                self.mode = self.MODE_CITY;
            } else {
                self.mode = self.MODE_ASSET;
            }
        },
        setMinZoom: function () {
            var min_zoom = 0;

            if (self.mode == self.MODE_ASSET) {
                min_zoom = 7;
            } else if (self.mode == self.MODE_CITY) {
                min_zoom = 5;
            } else if (self.mode == self.MODE_PROVINCE) {
                min_zoom = 0;
            }

            if (self.map.getZoom() < min_zoom) {
                self.map.setZoom(min_zoom);
            }
        }
    };

    window.AssetMapSearch = self;

    function generateIcon(number) {
        var fontSize = 60,
            size = 35,
            tx = 65,
            ty = 106;

        if (number >= 1000) {
            fontSize = 35;
            size = 55;
            tx = 45.3447266;
            ty = 99;
        } else if (number < 1000 && number >= 100) {
            fontSize = 45;
            size = 45;
            tx = 50;
            ty = 103;
        } else if (number < 100 & number >= 10) {
            tx = 49;
            ty = 104;
        }

        return 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent('<svg width="' + size + 'px" height="' + size + 'px" viewBox="0 0 172 172" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="marker"><path d="M86,148 C51.7583455,148 24,120.241654 24,86 C24,51.7583455 51.7583455,24 86,24 C120.241654,24 148,51.7583455 148,86 C148,120.241654 120.241654,148 86,148 Z M86,139 C115.271092,139 139,115.271092 139,86 C139,56.7289083 115.271092,33 86,33 C56.7289083,33 33,56.7289083 33,86 C33,115.271092 56.7289083,139 86,139 Z" id="Combined-Shape" fill-opacity="0.5" fill="#0A93D5" fill-rule="nonzero"></path><path d="M86,160 C45.1309285,160 12,126.869071 12,86 C12,45.1309285 45.1309285,12 86,12 C126.869071,12 160,45.1309285 160,86 C160,126.869071 126.869071,160 86,160 Z M86,150.967742 C121.880693,150.967742 150.967742,121.880693 150.967742,86 C150.967742,50.1193069 121.880693,21.0322581 86,21.0322581 C50.1193069,21.0322581 21.0322581,50.1193069 21.0322581,86 C21.0322581,121.880693 50.1193069,150.967742 86,150.967742 Z" id="Combined-Shape" fill-opacity="0.3" fill="#0A93D5" fill-rule="nonzero"></path><path d="M86,172 C38.5035115,172 0,133.496488 0,86 C0,38.5035115 38.5035115,0 86,0 C133.496488,0 172,38.5035115 172,86 C172,133.496488 133.496488,172 86,172 Z M86,163.258936 C128.668932,163.258936 163.258936,128.668932 163.258936,86 C163.258936,43.3310677 128.668932,8.7410636 86,8.7410636 C43.3310677,8.7410636 8.7410636,43.3310677 8.7410636,86 C8.7410636,128.668932 43.3310677,163.258936 86,163.258936 Z" id="Combined-Shape" fill-opacity="0.1" fill="#0A93D5" fill-rule="nonzero"></path><circle id="Oval" fill="#0A93D5" fill-rule="nonzero" cx="86" cy="86" r="50"></circle><text id="9999" font-family="Arial-BoldMT, Arial" font-size="' + fontSize + '" font-weight="bold" fill="#FFFFFF"><tspan x="' + tx + '" y="' + ty + '">' + number + '</tspan></text></g></g></svg>');
    }
})(jQuery);