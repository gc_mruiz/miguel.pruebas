if (typeof(assetLatLng) != 'undefined') {
    $(document).ready(function () {
        $('#assets-view-map').each(function () {
            // $('#assets-view-map').on('inview', function (event, isInView) {
            //     if (!isInView) {
            //         return;
            //     } else {
            //         $(this).unbind('inview');
            //     }

            var map = new google.maps.Map(this, {
                center: assetLatLng,
                scrollwheel: false,
                zoom: 14
            });

            var icon = {
                url: ROOT_URL + 'static/img/marker.svg',
                scaledSize: new google.maps.Size(26, 32)
            };

            var marker = new google.maps.Marker({
                map: map,
                icon: icon,
                position: assetLatLng,
                title: assetLatLng.address
            });

            var poiAliases = {
                161: 'bus',
                166: 'subway',
                167: 'train',
                168: 'trolley',
                20: 'childhood',
                21: 'elementary',
                19: 'high-school',
                22: 'university',
                31: 'ambulatory',
                33: 'hospital',
                105: 'supermarket'
            };

            var poiMarkers = {};

            $('[name*="poi[Poi]"]').change(function () {
                var $input = $(this),
                    poi_type_identifier = $input.attr('name').split(']').join('').split('[').slice(-1).pop();

                if (($input.is('[type=checkbox]:checked')) || ($input.is('[type=hidden]') && $input.val() == '1')) {
                    if (poiMarkers[poi_type_identifier] == undefined) {
                        loadPois(poi_type_identifier, function (pois) {
                            poiMarkers[poi_type_identifier] = [];

                            for (var i = 0; i < pois.length; i++) {
                                (function (i) {
                                    var infowindow;

                                    if (pois[i].nombre) {
                                        infowindow = new google.maps.InfoWindow({
                                            content: pois[i].nombre
                                        });
                                    }

                                    var icon = {
                                        url: ROOT_URL + 'static/svg/pin-' + poiAliases[poi_type_identifier] + '.svg',
                                        anchor: new google.maps.Point(17, 17),
                                        size: new google.maps.Size(35, 35)
                                    };

                                    var marker = new google.maps.Marker({
                                        map: map,
                                        icon: icon,
                                        position: {
                                            lat: parseFloat(pois[i].lat),
                                            lng: parseFloat(pois[i].lng)
                                        }
                                    });

                                    if (pois[i].nombre) {
                                        marker.infowindow = infowindow;
                                        marker.addListener('click', function () {
                                            hideAllInfoWindows(marker);
                                            infowindow.open(map, marker);
                                        });
                                    }

                                    poiMarkers[poi_type_identifier].push(marker);
                                })(i);
                            }
                        })
                        ;
                    } else {
                        togglePois(poi_type_identifier, true);
                    }
                } else {
                    togglePois(poi_type_identifier, false);
                }
            });

            var togglePois = function (poi_type_identifier, show) {
                if (poiMarkers[poi_type_identifier] !== undefined) {
                    for (var i = 0; i < poiMarkers[poi_type_identifier].length; i++) {
                        poiMarkers[poi_type_identifier][i].setMap((show ? map : null));
                    }
                }
            };

            var hideAllInfoWindows = function (but_this) {
                for (var poi_type_identifier in poiMarkers) {
                    for (var i = 0; i < poiMarkers[poi_type_identifier].length; i++) {
                        if (but_this !== poiMarkers[poi_type_identifier][i]) {
                            if (poiMarkers[poi_type_identifier][i].infowindow) {
                                poiMarkers[poi_type_identifier][i].infowindow.close();
                            }
                        }
                    }
                }
            };

            var loadedPois = {};
            var loadPois = function (poi_type_identifier, callback) {
                if (loadedPois[poi_type_identifier] === undefined) {
                    $.getJSON('https://api.tercerob.com/pois/dev/pois', {
                        lat: assetLatLng.lat,
                        lng: assetLatLng.lng,
                        tipo: poi_type_identifier,
                        source: 'haya'
                    }, function (pois) {
                        loadedPois[poi_type_identifier] = pois;
                        callback(loadedPois[poi_type_identifier]);
                    });
                } else {
                    callback(loadedPois[poi_type_identifier]);
                }
            };
        });
    });
}