$('#calendar').fullCalendar({
    editable: true,
    lang: 'es',
    buttonText: {
        //today: 'hoy',
        //month: 'mes',
        //week: 'semana',
        //day: 'día'
        month: 'mes'
    },
    header: {
        //center: 'month,agendaWeek,agendaDay'
        center: 'month'
    },

    views: {
        month: {
            titleFormat: 'DD / MM / YYYY'
        }
    },
    events: LOCALED_ROOT_URL + 'provider-events/index/',
    eventClick: function (calEvent, jsEvent, view) {
        libs.modal.show(null, 'provider-event-form', {provider_event_id: calEvent.id});
    }
});
