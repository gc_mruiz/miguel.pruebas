var XHR = {
    getJSON: function (url, data, success, error) {
        $.ajax({
            method: 'GET',
            dataType: 'json',
            url: url,
            data: data,
            success: function (result) {
                if (typeof (result) != undefined && typeof (result.fatal_error) != undefined && result.fatal_error) {
                    location.href = LOCALED_ROOT_URL + 'users/logout.php';
                } else {
                    if (success) {
                        success(result);
                    }
                }
            },
            error: error
        });
    },
    postJSON: function (url, data, success, error) {
        $.ajax({
            method: 'POST',
            dataType: 'json',
            url: url,
            data: data,
            success: function (result) {
                if (typeof (result) != undefined && typeof (result.fatal_error) != undefined && result.fatal_error) {
                    location.href = LOCALED_ROOT_URL + 'users/logout.php';
                } else {
                    if (success) {
                        success(result);
                    }
                }
            },
            error: error
        });
    },
    postMultipartJSON: function (url, data, success, error) {
        $.ajax({
            method: 'POST',
            dataType: 'json',
            url: url,
            data: data,
            success: function (result) {
                if (typeof (result) != undefined && typeof (result.fatal_error) != undefined && result.fatal_error) {
                    location.href = LOCALED_ROOT_URL + 'users/logout.php';
                } else {
                    if (success) {
                        success(result);
                    }
                }
            },
            error: error,
            processData: false,
            contentType: false
        });
    }
};