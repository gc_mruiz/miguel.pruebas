var Filters = {
    init: function () {
        $('.haya-filters-delete').click(function () {
            $(this).parents('form').find('input, textarea, select, .haya-dropdown-button-select').each(function () {
                if ($(this).is('.haya-dropdown-button-select')) {
                    $(this).data('obj').val('');
                } else {
                    $(this).val('');
                }
            });

            $(this).parents('form').submit();
        });
    }
}

$(document).ready(function () {
    Filters.init();

    var updateClientInfo = function ($el) {
        var $form = $el.parents('form');

        libs.xhr.getJSON(LOCALED_ROOT_URL + 'cajamar-crm/get-client-info/', {document: $form.find('[name*="[Client][document]"]').val()}, function (result) {
            if (result.response.succeeded) {
                function setValue($el, value) {
                    if ($el.data('ulObj')) {
                        $el.data('ulObj').val(value);
                    } else {
                        $el.val(value);
                    }
                }

                for (var field in result.response.clientInfo) {
                    if (field == 'responsible_provider_code') {
                        setValue($form.find('[name*="[Visit][client_' + field + ']"]'), result.response.clientInfo[field]);
                        setValue($form.find('[name*="[Offer][client_' + field + ']"]'), result.response.clientInfo[field]);
                    }

                    setValue($form.find('[name*="[Client][' + field + ']"]'), result.response.clientInfo[field]);
                }
            }
        });
    }

    $(document).on('click', '.haya-cajamar-search-document', function () {
        updateClientInfo($(this));
    });

    $(document).on('change', '[name="offer[Client][document]"]', function () {
        updateClientInfo($(this));
    });
});

var User = {
    on_login_callback: null,
    logged_in: false,
    init: function () {
        this.setListeners();
    },
    showLoginModal: function () {
        User.onLogin(null);
    },
    onLogin: function (callback, open_modal_for) {
        this.on_login_callback = callback;
        User.checkLogin(true, open_modal_for);
    },
    checkLogin: function (open_modal, open_modal_for) {
        XHR.getJSON(LOCALED_ROOT_URL + 'public-users/check-login/', null, function (result) {
            console.log(result.response);
            if (result.response.succeeded) {
                if (User.on_login_callback) {
                    User.on_login_callback();
                    User.on_login_callback = null;
                }
            } else {
                redirectToHome();
            }

            this.logged_in = result.response.succeeded;
        }, function (result) {
            console.log(result);
        });
    },
    favouriteToggle: function (haya_asset_identifier, add) {
        if (typeof add == 'undefined') {
            add = true;
        }

        this.onLogin(function () {
            XHR.getJSON(LOCALED_ROOT_URL + 'public-users/toggle-favourite-asset/' + haya_asset_identifier + '/', {
                add: !!add
            }, function (result) {
                if (result.response.succeeded) {
                    if (result.response.added) {
                        $('article[data-identifier="' + haya_asset_identifier + '"]').addClass('favourite');
                    } else {
                        $('article[data-identifier="' + haya_asset_identifier + '"]').removeClass('favourite');
                    }
                }
            }, function () {
            });
        }, 'favourite');
    },
    favouriteToggleDetail: function (haya_asset_identifier, add) {
        if (typeof add == 'undefined') {
            add = false;
        }

        this.onLogin(function () {
            XHR.getJSON(LOCALED_ROOT_URL + 'public-users/toggle-favourite-asset/' + haya_asset_identifier + '/', {
                add: !!add
            }, function (result) {
                if (result.response.succeeded) {
                    if (result.response.added) {
                        $('body').addClass('favourite');
                    } else {
                        $('body').removeClass('favourite');
                    }
                }
            }, function () {
            });
        }, 'favourite');
    },
    saveSearch: function (filters) {
        this.onLogin(function () {
            XHR.getJSON(LOCALED_ROOT_URL + 'public-users/save-search/', {filters: filters.filters}, function (result) {
                if (result.response.succeeded) {
                    libs.modal.show(null, 'saved-search-success', null);
                }
            }, function () {
            });
        }, 'save-search');
    },
    deleteSavedSearch: function (button) {
        if (confirm('¿Borrar la busqueda?')) {
            var id = $(button).closest('.haya-client-area-alert').data('id');
            XHR.getJSON(LOCALED_ROOT_URL + 'public-users/delete-saved-search/', {id: id}, function (result) {
                if (result.response.succeeded) {
                    $('.haya-client-area-alert[data-id="' + id + '"]').stop().slideUp(1000, function () {
                        $(this).remove()
                    });
                }
            }, function () {
            });
        }
    },
    setListeners: function () {
        $(document).on('click', '[data-saved-search]', function () {
            AssetSearch.search($(this).data('saved-search'));
        });

        $(document).on('click', '.haya-favourite', function (e) {
            e.stopPropagation();
            e.preventDefault();

            if ($(this).hasClass('list')) {
                var $article = $(this).closest('article');
                User.favouriteToggle($article.data('identifier'), !$article.hasClass('favourite'));
            } else {
                User.favouriteToggleDetail($('.haya-top-bar').data('identifier'), !$('body').hasClass('favourite'));
            }
        });

        $(document).on('click', '.delete-saved-search', function () {
            User.deleteSavedSearch(this);
        });

        $(document).on('click', '.save-search-action', function () {
            User.saveSearch($('.haya-results-of-search-wrapper').data('filters'));
        });
    }
};

window.UserCheckLogin = function () {
    User.checkLogin();
};

$(document).ready(function () {
    User.init();
});

var ApiSearch = {
    removeProvider: function () {
        $('#VisitVisitClientResponsibleProviderCode').val('');
        $('#VisitClientResponsibleProviderName').val('');
    },
    selectProvider: function (provider_code, name) {
        $('#VisitVisitClientResponsibleProviderCode').val(provider_code);
        $('#VisitClientResponsibleProviderName').val(name);
        $('.haya-modal-close').trigger('click');
        return false;
    },
    search: function(query, form) {
        if(form) {
            query = $(form).find('#Select-apiSearch').val();
        }

        libs.xhr.getJSON(LOCALED_ROOT_URL + 'cajamar-crm/search-api/', {query: query}, function(result) {
            if(result.response.succeeded) {
                $('#select-api-results').after(result.response.html).remove();
            }
        });
    }
};