$(document).ready(function () {
    var $body = $('body');

    if ($body.is('.mediator-diary')) {
        libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/moment-with-locales.js', function () {
            libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/fullcalendar.js', function () {
                if ($body.is('.mediator-diary')) {
                    libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/calendars/mediator-diary.js');
                }
            });
        });
    }
});