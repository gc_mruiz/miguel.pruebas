addons.geoCodeMaps = function () {
    $('[data-map-geocoder]').once('load-map').each(function () {
        var $map = $(this),
            geocoder = new google.maps.Geocoder(),
            center = new google.maps.LatLng(40.428528, -3.687576),
            mapOptions = {
                zoom: 8,
                center: center
            };

        map = new google.maps.Map($map.get(0), mapOptions);

        geocoder.geocode({
            address: $(this).data('map-geocoder')
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);

                if (typeof(add_assets_to_map_class) != 'undefined' && $map.is('.' + add_assets_to_map_class) && typeof(assetCoordinates) != 'undefined') {
                    var bounds = new google.maps.LatLngBounds();

                    for (var i = 0; i < assetCoordinates.length; i++) {
                        (function (i) {
                            var marker = new google.maps.Marker({
                                map: map,
                                position: {
                                    lat: parseFloat(assetCoordinates[i].Asset.lat),
                                    lng: parseFloat(assetCoordinates[i].Asset.lng)
                                }
                            });

                            bounds.extend(marker.position);

                            marker.addListener('click', function () {
                                location.href = LOCALED_ROOT_URL + 'assets/view/' + assetCoordinates[i].Asset.haya_asset_identifier;
                            });
                        })(i);
                    }

                    map.fitBounds(bounds);
                } else {
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                }
            }
        });
    });
};

addons.geoCodeMaps();