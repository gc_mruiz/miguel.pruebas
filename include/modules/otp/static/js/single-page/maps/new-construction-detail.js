var weAre = {lat: 40.4343517135316, lng: -3.6289586000000327};

var map = new google.maps.Map(document.getElementById('haya-map'), {
    center: weAre,
    scrollwheel: false,
    zoom: 14
});

var icon = {
    url: ROOT_URL + 'static/img/marker.svg',
    scaledSize: new google.maps.Size(26, 32)
};

var marker = new google.maps.Marker({
    map: map,
    icon: icon,
    position: weAre,
    title: '¡Hey!'
});