$(document).ready(function () {
    var $body = $('body');

    if ($body.is('.index, .territorial-management, .office-management, .area-management, .commercial-management')) {
        libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/highcharts.js', function () {
            if ($body.is('.index')) {
                libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/highcharts/dashboard.js');
            }
            if ($body.is('.territorial-management, .office-management, .area-management, .commercial-management')) {
                libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/highcharts/management.js');
            }
        });
    }
});