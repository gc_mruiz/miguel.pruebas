(function (H) {
    function deferAnimate(proceed, init) {
        var series = this,
            $renderTo = $(this.chart.container.parentNode);

        // Prevent pre-rendering without animation
        if (init) {
            series.group.hide();
        }

        // Prepare for animation
        if (init) {
            $renderTo.appear(); // initialize appear plugin
            proceed.call(this, init);

            // It is appeared, run animation
        } else if ($renderTo.is(':appeared')) {
            proceed.call(series);
            series.group.show('fast');

            // It is not appeared, halt animation until appear
        } else {
            $renderTo.on('appear', function () {
                if (!series.animated) {
                    proceed.call(series);
                    series.group.show();
                    series.animated = true;
                }
            });
        }
    }

    H.wrap(H.Series.prototype, 'animate', deferAnimate);
    H.wrap(H.seriesTypes.column.prototype, 'animate', deferAnimate);
    H.wrap(H.seriesTypes.pie.prototype, 'animate', deferAnimate);
}(Highcharts));

$(function () {
    Highcharts.setOptions({
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        }
    });

    $('.data-graphs#graph-mensual-visits').highcharts({
        chart: {
            type: 'column',
            backgroundColor: "transparent"
        },
        colors: [
            '#0A93D5',
            '#043D59',
            '#00BA39',
            '#a75bc8',
            '#F13462'
        ],
        credits: {
            enabled: false
        },
        title: {
            text: false
        },
        subtitle: {
            text: false
        },
        plotOptions: {
            series: {
                animation: {
                    duration: 900
                }
            },
            column: {
                stacking: 'normal'
            }
        },
        tooltip: {
            enabled: false
        },
        legend: {
            enabled: false,
            borderRadius: 0,
            borderColor: '#f5f5f5'
        },
        xAxis: {
            categories: highchartsData['graph-mensual-visits'].categories,
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif'
                }
            },
            lineColor: '#cacaca',
            lineWidth: 1,
            tickLength: 0
        },
        yAxis: {
            gridLineColor: '#e6e6e6',
            allowDecimals: false,
            title: {
                text: false
            },
            stackLabels: {
                enabled: true,
                align: 'center',
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif',
                    fontWeight: 500,
                    color: '#043D59'
                },
                formatter: function () {
                    return Highcharts.numberFormat(this.total, 0, ',') + "";
                }
            },
            labels: {
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif',
                    fontWeight: 400,
                    color: '#cacaca'
                }
            }
        },
        series: [{
            name: 'Visitas',
            data: highchartsData['graph-mensual-visits'].data,
            dataLabels: {
                formatter: function () {
                    // return this.point.y + '% ' + this.point.name;
                }
            }
        }]
    });

    $('.data-graphs#graph-mensual-offers').highcharts({
        chart: {
            type: 'line',
            backgroundColor: "transparent"
        },
        colors: [
            '#0A93D5',
            '#043D59',
            '#00BA39',
            '#a75bc8',
            '#F13462'
        ],
        credits: {
            enabled: false
        },
        title: {
            text: false
        },
        subtitle: {
            text: false
        },
        plotOptions: {
            series: {
                animation: {
                    duration: 900
                }
            },
            column: {
                stacking: 'normal'
            }
        },
        tooltip: {
            enabled: false
        },
        legend: {
            enabled: false,
            borderRadius: 0,
            borderColor: '#f5f5f5'
        },
        xAxis: {
            categories: highchartsData['graph-mensual-offers'].categories,
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif'
                }
            },
            lineColor: '#cacaca',
            lineWidth: 1,
            tickLength: 0
        },
        yAxis: {
            gridLineColor: '#e6e6e6',
            allowDecimals: false,
            title: {
                text: false
            },
            stackLabels: {
                enabled: true,
                align: 'center',
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif',
                    fontWeight: 500,
                    color: '#043D59'
                },
                formatter: function () {
                    return Highcharts.numberFormat(this.total, 0, ',') + "";
                }
            },
            labels: {
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif',
                    fontWeight: 400,
                    color: '#cacaca'
                }
            }
        },
        series: [{
            name: 'Ofertas',
            data: highchartsData['graph-mensual-offers'].data,
            dataLabels: {
                formatter: function () {
                    // return this.point.y + '% ' + this.point.name;
                }
            }
        }]
    });

    $('.data-graphs#graph-mensual-sales').highcharts({
        chart: {
            type: 'column',
            backgroundColor: "transparent"
        },
        colors: [
            '#0A93D5',
            '#043D59',
            '#00BA39',
            '#a75bc8',
            '#F13462'
        ],
        credits: {
            enabled: false
        },
        title: {
            text: false
        },
        subtitle: {
            text: false
        },
        plotOptions: {
            series: {
                animation: {
                    duration: 900
                }
            },
            column: {
                stacking: 'normal'
            }
        },
        tooltip: {
            enabled: false
        },
        legend: {
            enabled: false,
            borderRadius: 0,
            borderColor: '#f5f5f5'
        },
        xAxis: {
            categories: highchartsData['graph-mensual-sales'].categories,
            title: {
                text: null
            },
            labels: {
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif'
                }
            },
            lineColor: '#cacaca',
            lineWidth: 1,
            tickLength: 0
        },
        yAxis: {
            gridLineColor: '#e6e6e6',
            allowDecimals: false,
            title: {
                text: false
            },
            stackLabels: {
                enabled: true,
                align: 'center',
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif',
                    fontWeight: 500,
                    color: '#043D59'
                },
                formatter: function () {
                    return Highcharts.numberFormat(this.total, 0, ',') + "";
                }
            },
            labels: {
                style: {
                    fontSize: '14px',
                    fontFamily: 'Circular, Helvetica, sans-serif',
                    fontWeight: 400,
                    color: '#cacaca'
                }
            }
        },
        series: [{
            name: 'Ventas',
            data: highchartsData['graph-mensual-sales'].data,
            dataLabels: {
                formatter: function () {
                    // return this.point.y + '% ' + this.point.name;
                }
            }
        }]
    });
});