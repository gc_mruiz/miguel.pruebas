(function () {
    if (!String.prototype.endsWith) {
        String.prototype.endsWith = function (searchString, position) {
            var subjectString = this.toString();
            if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
                position = subjectString.length;
            }
            position -= searchString.length;
            var lastIndex = subjectString.indexOf(searchString, position);
            return lastIndex !== -1 && lastIndex === position;
        };
    }

    // Shortcuts
    var libs = window.libs;
    var backend = libs.backend;

    // Content URLs
    backend.addUrlHandler('content/modal', function (modal_name) {
        if (!modal_name.endsWith('/')) {
            modal_name += '/';
        }

        return LOCALED_ROOT_URL + 'public-modals/' + modal_name;
    });

    backend.addUrlHandler('content/lazy', function (name) {
        if (!name.endsWith('/')) {
            name += '/';
        }

        return LOCALED_ROOT_URL + 'lazy-content/' + name
    });

    // GEO URLs
    backend.addUrlHandler('geo/getCitiesByProvinceId', function (province_id) {
        return LOCALED_ROOT_URL + 'geo/get-cities-by-province-id/';
    });
    backend.addUrlHandler('geo/getSubcitiesByCityId', function (city_id) {
        return LOCALED_ROOT_URL + 'geo/get-subcities-by-city-id/';
    });
    backend.addUrlHandler('geo/getDistrictsByCityId', function (city_id) {
        return LOCALED_ROOT_URL + 'geo/get-districts-by-city-id/';
    });
    backend.addUrlHandler('asset/getAssetSubtypesByAssetTypeId', function (asset_type_id) {
        return LOCALED_ROOT_URL + 'public-assets/get-assets-subtypes-by-asset-type-id/';
    });
    backend.addUrlHandler('visit/getVisitStatusDetailsByVisitStatusId', function (visit_status_id) {
        return LOCALED_ROOT_URL + 'visits/get-visit-status-details-by-visit-status-id/';
    });
})();