/**
 * @author Federico Luis Lescano Carroll <flescano@gestycontrol.com> for Gestycontrol Consultores informáticos S.L.
 */
(function () {
    var lazy = {
        init: function () {
            this.setListeners();
        },
        setListeners: function () {
            $('[data-lazy-content]').once('set-lazy-content-listener').each(function () {
                $(this).on('inview', function (event, isInView) {
                    if (!isInView) {
                        return;
                    }

                    $(this).unbind('inview').once('load-lazy-content').each(function () {
                        lazy._load($(this));
                    });
                });
            });

            $('[data-lazy-content-refresh]').once('refresh-lazy-content').each(function () {
                var $refleshElement = $(this);

                $refleshElement.data('refreshLazyContent', function (force_load) {
                    var $deleteElements = $refleshElement.parent().find('[data-lazy-content-delete-on-refresh]'),
                        lazy_name = $refleshElement.data('lazy-content-refresh'),
                        lazy_params = $refleshElement.data('lazy-content-params'),
                        table_with_caption = $refleshElement.find('caption').length > 0,
                        panel_body_replace = $refleshElement.is('.panel'),
                        other_element = $refleshElement.hasClass('has-loading'),
                        $heightElement = ($refleshElement.data('lazy-content-fix-height-to') ? $($refleshElement.data('lazy-content-fix-height-to')) : null),
                        $html = null,
                        original_height = $refleshElement.get(0).offsetHeight;

                    $refleshElement.removeAttr('data-lazy-content-refresh');
                    $refleshElement.removeAttr('data-lazy-content');

                    switch (true) {
                        case table_with_caption:
                            $html = $('<div>');
                            $html.attr({
                                'data-lazy-content': lazy_name,
                                'data-lazy-content-replace': 'true'
                            });
                            $html.data('lazy-content-params', lazy_params);
                            $refleshElement.after($html).appendTo($html);
                            $refleshElement.find('caption').show();
                            break;

                        case other_element:
                            $refleshElement.prepend(
                                '<div class="loading '
                                + ($refleshElement.is('.is-pagination, .panel-body') ? 'loading-full' : 'loading-list')
                                + '"><span class="loading-content"><span class="loading-info">'
                                + ($refleshElement.is('.is-pagination, .result-name, .panel-body') ? '' : '<svg class="circular-spinner" viewBox="25 25 50 50"><circle class="path-spinner" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>')
                                + '</span></span></div>'
                            );

                            $html = $('<div>');
                            $html.attr({
                                'data-lazy-content': lazy_name,
                                'data-lazy-content-replace': 'true'
                            });
                            $html.data('lazy-content-params', lazy_params);
                            $refleshElement.after($html).appendTo($html);
                            break;

                        case panel_body_replace:
                            $html = $('<div class="panel"><div class="panel-body empty-loading"><div class="loading loading-full loading-panel"><span class="loading-content"><span class="loading-info"><svg class="circular-spinner" viewBox="25 25 50 50"><circle class="path-spinner" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></span></span></div></div></div>');
                            $html.attr({
                                'data-lazy-content': lazy_name,
                                'data-lazy-content-replace': 'true'
                            });
                            $html.data('lazy-content-params', lazy_params);
                            $html.css({height: $refleshElement.get(0).offsetHeight});
                            $refleshElement.after($html).remove();
                            break;
                    }

                    // Set height if required
                    if ($heightElement && $heightElement.length) {
                        var height = 0;

                        $heightElement.filter(':visible').each(function () {
                            if (this.offsetHeight > height) {
                                height = this.offsetHeight;
                            }
                        });

                        if (height > original_height) {
                            $html.css({height: height});
                        }
                    }

                    $deleteElements.remove();
                    libs.lazy.init();

                    if (force_load && $html != null) {
                        libs.lazy.forceLoad([$html.get(0)]);
                    }
                });
            });
        },
        forceLoad: function (names, $container) {
            var $elements = [];

            if (typeof(names) == 'object') {
                $elements = $($.unique($(names).get()));
            } else {
                if (!$container) {
                    $container = $('body');
                }

                if (typeof(names) == 'string') {
                    names = names.split(',');
                }

                for (var i = 0; i < names.length; i++) {
                    var name = names[i];

                    if (name) {
                        $container.find('[data-lazy-content="' + name + '"]').each(function () {
                            $elements.push(this);
                        });
                    }
                }

                $elements = $($.unique($elements));
            }

            $elements.each(function () {
                $(this).unbind('inview').once('load-lazy-content').each(function () {
                    lazy._load($(this));
                });
            });
        },
        refreshAll: function () {
            this.refresh($('[data-lazy-content-refresh]'), null, true);
        },
        refresh: function (names, $container, force_load) {
            var $elements = [];

            if (typeof(names) == 'object') {
                $elements = $($.unique($(names).get()));
            } else {
                if (!$container) {
                    $container = $('body');
                }

                if (typeof(names) == 'string') {
                    names = names.split(',');
                }

                for (var i = 0; i < names.length; i++) {
                    var name = names[i];

                    if (name) {
                        $container.find('[data-lazy-content-refresh="' + name + '"]').each(function () {
                            $elements.push(this);
                        });
                    }
                }

                $elements = $($.unique($elements));
            }

            $elements.each(function () {
                var refreshLazyContentFunction = $(this).data('refreshLazyContent');

                if (refreshLazyContentFunction) {
                    refreshLazyContentFunction(force_load);
                }
            });
        },
        _load: function ($container) {
            var lazy_name = $container.data('lazy-content'),
                lazy_params = $container.data('lazy-content-params'),
                lazy_replace = !!$container.data('lazy-content-replace');

            libs.backend.content.getLazy(lazy_name, lazy_params, function (result) {
                var $replacedElement = null;

                if (lazy_replace) {
                    $replacedElement = $container;
                } else {
                    $replacedElement = $container.children('.panel-body.empty-loading');
                }

                if (!result.response.html) {
                    result.response.html = '';
                }

                // Replace content
                var $html = $('<div>').html(result.response.html.split('RANDOMAJAXID').join(libs.randomizer.id())).children();

                $replacedElement.after($html).remove();

                // Callback (load)
                if (lazy.afterLoad !== undefined) {
                    lazy.afterLoad(lazy_name, lazy_params, lazy_replace, $html);
                }

                // Callback (visible)
                if (lazy.afterVisible) {
                    $html.on('inview', function (event, isInView) {
                        if (!isInView) {
                            return;
                        } else {
                            $html.unbind('inview');
                        }

                        lazy.afterVisible(lazy_name, lazy_params, lazy_replace, $html);
                    });
                }
            }, libs.lazy.showError, true);

            $container.removeAttr('data-lazy-content-params');
            $container.removeAttr('data-lazy-content');
        }
    };

    $(document).ready(function () {
        lazy.init();
    });

    // Initialize lib namespace
    if (window.libs === undefined) {
        window.libs = {};
    }

    // Add to libs
    window.libs.lazy = lazy;
})();