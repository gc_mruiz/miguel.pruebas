/**
 * http://stackoverflow.com/questions/7718935/load-scripts-asynchronously
 */
(function () {
    var randomizer = {
        id: function () {
            return this._time() + this._randomDecimalsToString();
        },
        _randomDecimalsToString: function () {
            return Math.random().toString(36).substring(7);
        },
        _time: function () {
            return Math.floor(new Date().getTime() / 1000)
        }
    };

    // Initialize lib namespace
    if (window.libs === undefined) {
        window.libs = {};
    }

    // Add to libs
    window.libs.randomizer = randomizer;
})();