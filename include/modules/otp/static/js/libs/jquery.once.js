/**
 * @author Federico Luis Lescano Carroll <flescano@gestycontrol.com> for Gestycontrol Consultores informáticos S.L.
 */
(function ($) {
    /**
     * Filters the list of elements keeping only the elements that were not already filtered with the same identifier
     *
     * @param identifier string name of the filter (unique)
     */
    $.fn.once = function (identifier) {
        if (!identifier) {
            throw 'Parameter identifier is required for Jquery.fn.once';
        }

        // Filter
        return this.filter(function () {
            // Add prefix to identifier avoid reserved words
            identifier = 'jquery.once.' + identifier;

            // Skip elements that already have the identifier
            if (this[identifier] != undefined) {
                return false;
            }

            // Add the identifier
            this[identifier] = true;

            // Keep elements that did not have the identifier set
            return true;
        });
    };
}(jQuery));