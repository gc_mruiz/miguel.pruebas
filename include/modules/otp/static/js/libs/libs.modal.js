/**
 * @author Federico Luis Lescano Carroll <flescano@gestycontrol.com> for Gestycontrol Consultores informáticos S.L.
 */
(function () {
    var modal = {
        init: function () {
            this.setListeners();
        },
        setListeners: function () {
            $(document).once('modal-open').on('click', '[data-modal-name]', function (e) {
                modal.show(this, $(this).data('modal-name'), $(this).data('modal-params'));
            });
        },
        show: function (target, modal_name, modal_params, loaded, visible) {
            $('body').addClass('has-loading').prepend('<div class="loading loading-full loading-full-page"><span class="loading-content"><span class="loading-info"></span></span></div>').css({height: $('body').get(0).scrollHeight});

            // Load extra information
            modal._beforeShow(target, modal_name, modal_params, function (show_modal, data) {
                if (show_modal) {
                    // Load modal from backend
                    window.libs.backend.content.getModal(modal_name, modal_params, data, function (result) {
                        if (!result.response.html) {
                            result.response.html = '';
                        }

                        // Parse HTML
                        var $html = $('<div>');
                        $html.append(result.response.html.split('RANDOMAJAXID').join(libs.randomizer.id()));
                        $html = $html.children();

                        // Add to body
                        $('body').append($html);

                        // Callback (load)
                        if (modal.afterLoad !== undefined) {
                            modal.afterLoad(modal_name, modal_params, data, $html);
                        }

                        if (loaded) {
                            loaded($html);
                        }

                        // Show modal
                        modal._show($html);

                        // Callback (visible)
                        if (modal.afterVisible || visible) {
                            $html.on('inview', function (event, isInView) {
                                if (!isInView) {
                                    return;
                                } else {
                                    $html.unbind('inview');
                                }

                                if (modal.afterVisible) {
                                    modal.afterVisible(modal_name, modal_params, data, $html);
                                }

                                if (visible) {
                                    visible($html);
                                }
                            });
                        }

                        if($('body.has-loading')) {
                            $('body').removeClass('has-loading').children('div.loading-full').remove();
                            $('body').attr('style', '');
                        }
                    }, libs.modal.showError, true);
                }
            });
        },
        _show: function ($html) {
            var popup = new Foundation.Reveal($html);
            popup.open();

            $html.on('closed.zf.reveal', function () {
                if (modal.afterClose) {
                    modal.afterClose();
                }
                
                $html.parent().remove();
            });

            modal._lastShownElement = $html;
        },
        _close: function ($html) {
            $html.foundation('close');
        },
        _lastShownElement: null,
        close: function () {
            modal._close(modal._lastShownElement);
        },
        showError: function (err) {
            if (!err) {
                err = 'Error inesperado.';
            }

            console.log(err);
        },
        _beforeShowHandlers: {},
        _beforeShow: function (target, modal_name, modal_params, callback) {
            if (modal._beforeShowHandlers[modal_name] === undefined) {
                callback(true, {});
            } else {
                modal._beforeShowHandlers[modal_name](target, modal_name, modal_params, callback);
            }
        },
        addBeforeShowHandler: function (modal_name, watcherFunction) {
            modal._beforeShowHandlers[modal_name] = watcherFunction;
        }
    };

    $(document).ready(function () {
        modal.setListeners();
    });

    // Initialize lib namespace
    if (window.libs === undefined) {
        window.libs = {};
    }

    // Add to libs
    window.libs.modal = modal;
})();