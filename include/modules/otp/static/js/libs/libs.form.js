/**
 * @author Federico Luis Lescano Carroll <flescano@gestycontrol.com> for Gestycontrol Consultores informáticos S.L.
 */
(function () {
    var form = {
        appendObjectToFormData: function (formData, varname, obj) {
            var varValue = this.objToVarValue(obj, varname);

            for (var key in varValue) {
                formData.append(key, varValue[key]);
            }
        },
        objToVarValue: function (obj, prefix) {
            var varValue = {};

            for (var key in obj) {
                var subVarValues = {};

                if (typeof obj[key] === 'object') {
                    subVarValues = this.objToVarValue(obj[key], '[' + key + ']');
                } else {
                    subVarValues['[' + key + ']'] = obj[key];
                }

                for (var sub in subVarValues) {
                    varValue[prefix + sub] = subVarValues[sub];
                }
            }

            return varValue;
        },
        /**
         * Retrieve data form contained inputs.
         * @param {DOMObject} $container
         */
        getData: function ($container, form_data) {
            $container = $($container);

            var formData = new FormData();
            var data = {};

            $container.find('input, textarea, select').each(function () {
                var name = $(this).attr('name');
                var add_value = false;
                var value = null;

                if (!name) {
                    return;
                }

                if (this.disabled || ($(this).is('input') && (this.type == 'checkbox' || this.type == 'radio') && !this.checked)) {
                    // ignore unchecked inputs and disabled elements
                } else {
                    add_value = true;
                    value = $(this).val();
                }

                if (add_value) {
                    if (form_data) {
                        if ($(this).is('[type=file]')) {
                            formData.append(name, this.files[0]);
                        } else {
                            formData.append(name, value);
                        }
                    } else {
                        data = form._setVarValue(data, name, value);
                    }
                }
            });

            if (form_data) {
                return formData;
            }

            return data;
        },
        /**
         * Recursive function to allow structured data retrieval
         * @param {Object} data
         * @param {Object} name
         * @param {Object} value
         */
        _setVarValue: function (data, name, value) {
            // Remove initial [
            if (name.substring(0, 1) == '[') {
                name = name.substring(1);
            }

            // Split name structure
            var nameParts = name.split('[');

            // Remove trailing ]
            if (nameParts[0].substring(nameParts[0].length - 1, nameParts[0].length) == ']') {
                nameParts[0] = nameParts[0].substring(0, nameParts[0].length - 1);
            }

            if (nameParts.length == 1 || (nameParts.length == 2 && (nameParts[nameParts.length - 1] == '' || nameParts[nameParts.length - 1] == ']'))) {
                // Check if it is and unindexed array
                if (name.length > 2 && name.substring(name.length - 2, name.length) == '[]') {
                    // Initialize array
                    if (data[nameParts[0]] == undefined) {
                        data[nameParts[0]] = [];
                    }

                    // Push value
                    data[nameParts[0]].push(value);
                } else {
                    // Single value, save
                    data[nameParts[0]] = value;
                }
            } else {
                // Initialize hashmap
                if (data[nameParts[0]] == undefined) {
                    data[nameParts[0]] = {};
                }

                // Alter structure
                data[nameParts[0]] = form._setVarValue(data[nameParts[0]], name.substring(nameParts[0].length + 1), value);
            }

            return data;
        }
    };

    // Initialize lib namespace
    if (window.libs === undefined) {
        window.libs = {};
    }

    // Add to libs
    window.libs.form = form;
})();