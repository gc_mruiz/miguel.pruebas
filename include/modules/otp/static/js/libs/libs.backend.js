/**
 * @author Federico Luis Lescano Carroll <flescano@gestycontrol.com> for Gestycontrol Consultores informáticos S.L.
 */
(function () {
    var backend = {
        _urlHandlers: {},
        addUrlHandler: function (type, handler) {
            backend._urlHandlers[type] = handler;
        },
        url: function (type, param1, param2, param3, param4, param5) {
            return backend._urlHandlers[type](param1, param2, param3, param4, param5);
        }
    };

    // Initialize lib namespace
    if (window.libs === undefined) {
        window.libs = {};
    }

    // Add to libs
    window.libs.backend = backend;
})();