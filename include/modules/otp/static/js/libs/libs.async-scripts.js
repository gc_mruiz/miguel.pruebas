/**
 * http://stackoverflow.com/questions/7718935/load-scripts-asynchronously
 */
(function () {
    var asyncScripts = {
        load: function (src, callback) {
            var s,
                r,
                t;
            r = false;
            s = document.createElement('script');
            s.type = 'text/javascript';
            s.src = src;
            s.onload = s.onreadystatechange = function () {
                if (!r && (!this.readyState || this.readyState == 'complete')) {
                    r = true;
                    if (callback) {
                        callback();
                    }
                }
            };
            t = document.getElementsByTagName('script');
            t = t[t.length - 1];
            t.parentNode.insertBefore(s, t);
        }
    };

    // Initialize lib namespace
    if (window.libs === undefined) {
        window.libs = {};
    }

    // Add to libs
    window.libs.asyncScripts = asyncScripts;
})();