/**
 * @author Federico Luis Lescano Carroll <flescano@gestycontrol.com> for Gestycontrol Consultores informáticos S.L.
 */
(function () {
    var xhr = {
        getJSON: function (url, data, success, error, call_error_on_succeeded_false) {
            // Dummy services
            if (xhr._dummyUrl(url, success, error, call_error_on_succeeded_false)) {
                return;
            }

            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: url,
                data: data,
                success: function (result) {
                    xhr._success(result, success, error, call_error_on_succeeded_false);
                },
                error: error
            });
        },
        postJSON: function (url, data, success, error, call_error_on_succeeded_false) {
            // Dummy services
            if (xhr._dummyUrl(url, success, error, call_error_on_succeeded_false)) {
                return;
            }

            $.ajax({
                method: 'POST',
                dataType: 'json',
                url: url,
                data: data,
                success: function (result) {
                    xhr._success(result, success, error, call_error_on_succeeded_false);
                },
                error: error
            });
        },
        postMultipartJSON: function (url, data, success, error, call_error_on_succeeded_false) {
            // Dummy services
            if (xhr._dummyUrl(url, success, error, call_error_on_succeeded_false)) {
                return;
            }

            $.ajax({
                method: 'POST',
                dataType: 'json',
                url: url,
                data: data,
                success: function (result) {
                    xhr._success(result, success, error, call_error_on_succeeded_false);
                },
                error: error,
                processData: false,
                contentType: false
            });
        },
        _success: function (result, success, error, call_error_on_succeeded_false) {
            if (typeof (result) !== undefined && typeof (result.fatal_error) != undefined && result.fatal_error) {
                location.reload();
            } else {
                if (call_error_on_succeeded_false && (!result || !result.response || !result.response.succeeded)) {
                    if (error) {
                        error((result ? result.error : null));
                    }

                    return;
                }

                if (success) {
                    success(result);
                }
            }
        },
        _dummyUrl: function (url, success, error, call_error_on_succeeded_false) {
            if (url.dummy) {
                if (url.file) {
                    $.get(url.file, function (content) {
                        if (url.format == 'html') {
                            xhr._success({
                                response: {html: content, succeeded: true},
                                error: null
                            }, success, error, call_error_on_succeeded_false);
                        } else {
                            xhr._success(content, success, error, call_error_on_succeeded_false);
                        }
                    });
                } else if (url.result) {
                    setTimeout(function () {
                        success(url.result);
                    }, 100);
                }
            }

            return url.dummy;
        }
    };

    // Initialize lib namespace
    if (window.libs === undefined) {
        window.libs = {};
    }

    // Add to libs
    window.libs.xhr = xhr;
})();