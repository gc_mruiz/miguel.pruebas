/**
 * @author Federico Luis Lescano Carroll <flescano@gestycontrol.com> for Gestycontrol Consultores informáticos S.L.
 */
(function ($) {
    $.fn.disableByReason = function (identifier) {
        return this.toggleDisableByReason(identifier, true);
    };

    $.fn.enableByReason = function (identifier) {
        return this.toggleDisableByReason(identifier, false);
    };

    $.fn.toggleDisableByReason = function (identifier, disabled) {
        if (!identifier) {
            throw 'Parameter identifier is required for Jquery.fn.disableByReason';
        }

        this.each(function () {
            var $el = $(this);

            if(!this.disabledReasons) {
                this.disabledReasons = [];
            }

            if(this.disabledReasons.length == 0 && $(this).is('[disabled]')) {
                this.disabledReasons.push('html');
            }

            var index = this.disabledReasons.indexOf(identifier);

            if(disabled) {
                if(index == -1) {
                    this.disabledReasons.push(identifier);
                }
            } else {
                if (index > -1) {
                    this.disabledReasons.splice(index, 1);
                }
            }

            if(this.disabledReasons.length == 0) {
                $el.removeAttr('disabled');
            } else {
                $el.attr('disabled', 'disabled');
            }
        });

        return this;
    };
}(jQuery));