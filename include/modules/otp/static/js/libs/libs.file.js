/**
 * @author Federico Luis Lescano Carroll <flescano@gestycontrol.com> for Gestycontrol Consultores informáticos S.L.
 */
(function () {
    var file = {
        forceDownload: function (url, basename) {
            if (basename == null) {
                basename = url.split('/');
                basename = basename[basename.length - 1];
            }

            var $a = $('<a>');
            $a.attr('href', url);
            $a.attr('target', '_blank');
            $a.attr('download', basename);
            $a.appendTo('body');

            setTimeout(function() {
                $a.get(0).click();

                setTimeout(function() {
                    $a.remove();
                }, 100);
            }, 100);
        },
        upload: function (formData, url, progressChanged, success, error, call_error_on_succeeded_false) {
            var xhr = new XMLHttpRequest();

            if (xhr.upload) {
                xhr.upload.addEventListener('progress', function (e) {
                    var percentage = parseInt((e.loaded / e.total * 100));

                    if (progressChanged) {
                        progressChanged(percentage);
                    }
                }, false);

                xhr.onreadystatechange = function (e) {
                    if (xhr.readyState == 4) {
                        if (xhr.status == 200) {
                            if (progressChanged) {
                                progressChanged(100);
                            }

                            if (success) {
                                var result = JSON.parse(xhr.responseText);

                                if (call_error_on_succeeded_false && (!result || !result.response || !result.response.succeeded)) {
                                    error((result ? result.error : null));
                                } else {
                                    success(result);
                                }
                            }
                        } else {
                            if (error) {
                                error();
                            }
                        }
                    }
                };

                xhr.open('POST', url, true);
                xhr.send(formData);
            } else {
                libs.modal.showError('Tu navegador no soporta subida de archivos asíncrona.');
                return null;
            }

            return xhr;
        }
    };

    // Initialize lib namespace
    if (window.libs === undefined) {
        window.libs = {};
    }

    // Add to libs
    window.libs.file = file;
})();