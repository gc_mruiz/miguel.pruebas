$(document).ready(function () {
    var $body = $('body');

    if ($body.is('.assets-index, .new-construction, .new-construction-detail, .assets-search') || $('[data-map-geocoder]').length > 0) {
        libs.asyncScripts.load('https://maps.googleapis.com/maps/api/js?key=' + GOOGLE_MAPS_KEY, function () {
            if ($body.is('.assets-index')) {
                libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/maps/assets-index.js');
            } else if ($body.is('.new-construction')) {
                libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/maps/new-construction.js');
            } else if ($body.is('.new-construction-detail')) {
                libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/maps/new-construction-detail.js');
            } else if ($body.is('.assets-search')) {
                libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/maps/crm-assets-search.js');
            }

            if ($('[data-map-geocoder]').length > 0) {
                libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/maps/geocoder.js');
            }
        });
    } else if ($body.is('.assets-view')) {
        if ($body.is('.assets-view')) {
            libs.asyncScripts.load(ROOT_URL + 'static/js/single-page/maps/assets-view.js');
        }
    }
});