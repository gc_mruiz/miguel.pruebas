(function () {
    if (!String.prototype.endsWith) {
        String.prototype.endsWith = function (searchString, position) {
            var subjectString = this.toString();
            if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
                position = subjectString.length;
            }
            position -= searchString.length;
            var lastIndex = subjectString.indexOf(searchString, position);
            return lastIndex !== -1 && lastIndex === position;
        };
    }

    // Shortcuts
    var libs = window.libs;
    var backend = libs.backend;

    // Content URLs
    backend.addUrlHandler('content/modal', function (modal_name) {
        if (!modal_name.endsWith('/')) {
            modal_name += '/';
        }

        return LOCALED_ROOT_URL + 'modals/' + modal_name;
    });

    backend.addUrlHandler('content/lazy', function (name) {
        if (!name.endsWith('/')) {
            name += '/';
        }

        return LOCALED_ROOT_URL + 'lazy-content/' + name
    });

    // Client
    backend.addUrlHandler('client/file/lopd-empty', function () {
        return {
            dummy: true,
            result: {
                response: {
                    succeeded: true,
                    url: ROOT_URL + 'static/_backend/example.pdf'
                },
                error: null
            }
        };
    });

    // File
    backend.addUrlHandler('file/upload', function () {
        return LOCALED_ROOT_URL + 'files/upload/';
    });
    backend.addUrlHandler('client-file/delete', function () {
        return LOCALED_ROOT_URL + 'files/delete-client-file/';
    });
    backend.addUrlHandler('file/delete', function () {
        return LOCALED_ROOT_URL + 'files/delete/';
    });
    backend.addUrlHandler('file/deletePicture', function () {
        return LOCALED_ROOT_URL + 'files/delete-picture/';
    });
    backend.addUrlHandler('file/deleteAssetPicture', function () {
        return LOCALED_ROOT_URL + 'files/delete-asset-picture/';
    });

    // GEO URLs
    backend.addUrlHandler('geo/getCitiesByProvinceId', function (province_id) {
        return LOCALED_ROOT_URL + 'geo/get-cities-by-province-id/';
    });
    backend.addUrlHandler('geo/getSubcitiesByCityId', function (city_id) {
        return LOCALED_ROOT_URL + 'geo/get-subcities-by-city-id/';
    });
    backend.addUrlHandler('geo/getDistrictsByCityId', function (city_id) {
        return LOCALED_ROOT_URL + 'geo/get-districts-by-city-id/';
    });
    backend.addUrlHandler('asset/getAssetSubtypesByAssetTypeId', function (asset_type_id) {
        return LOCALED_ROOT_URL + 'assets/get-assets-subtypes-by-asset-type-id/';
    });
    backend.addUrlHandler('visit/getVisitStatusDetailsByVisitStatusId', function (visit_status_id) {
        return LOCALED_ROOT_URL + 'visits/get-visit-status-details-by-visit-status-id/';
    });

    backend.addUrlHandler('alert/mark-as-read', function () {
        return LOCALED_ROOT_URL + 'alerts/mark-as-read/';
    });
    backend.addUrlHandler('providerConversation/mark-as-read', function () {
        return LOCALED_ROOT_URL + 'provider-conversations/mark-as-read/';
    });
})();