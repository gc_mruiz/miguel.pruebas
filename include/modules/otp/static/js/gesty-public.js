var AssetSearch = {
    asset_type_id: null,
    asset_selling_mode_id: null,
    geo_province_id: null,
    postal_code: null,
    haya_asset_identifier: null,
    $form: null,
    init: function () {
        this.$form = $('.haya-main-search-wrapper');

        $(document).on('operationChanged', function (e, val) {
            AssetSearch.asset_selling_mode_id = val;
        });
        $(document).on('typeChanged', function (e, val) {
            AssetSearch.asset_type_id = val;
        });
        $(document).on('locationChanged', function (e, val) {
            AssetSearch.geo_province_id = val;
        });

        this.$form.find('ul[data-role="select"]').each(function () {
            var $li = $(this).find('.haya-selected');

            if ($li.length == 0) {
                $li = $(this).children().eq(0);
                $li.addClass('haya-selected');
            }

            var event = $(this).data('event');

            if (event && event != 'languageChanged') {
                $(document).trigger(event, [$li.data('value')]);
            }
        });

        $('.btn-search-assets-by-id').click(function () {
            var searchInfo = {};
            searchInfo.haya_asset_identifier = AssetSearch.$form.find('[name="haya_asset_identifier"]').val();

            AssetSearch.search(searchInfo);
        });

        $('.btn-search-assets-by-postal-code').click(function () {
            var searchInfo = {};
            searchInfo.asset_type_id = AssetSearch.asset_type_id;
            searchInfo.asset_selling_mode_id = AssetSearch.$form.find('[name="operation"]:checked').val();
            searchInfo.postal_code = AssetSearch.$form.find('[name="postal_code"]').val();

            AssetSearch.search(searchInfo);
        });

        $('.btn-search-assets-by-guided').click(function () {
            var searchInfo = {};
            searchInfo.asset_type_id = AssetSearch.asset_type_id;
            searchInfo.asset_selling_mode_id = AssetSearch.asset_selling_mode_id;
            searchInfo.geo_province_id = AssetSearch.geo_province_id;

            AssetSearch.search(searchInfo);
        });
    },
    getUrl: function (searchInfo, onSuccess, onError) {
        libs.xhr.getJSON(LOCALED_ROOT_URL + 'public-assets/get-url/' + (searchInfo.haya_asset_identifier != undefined ? '?t=' + (new Date()).getTime() : ''), searchInfo, function (result) {
            if (result.response.succeeded) {
                onSuccess(result.response.url);
            }
        }, onError);
    },
    search: function (searchInfo) {
        this.getUrl(searchInfo, function (url) {
            location.href = url;
            //PostLinks.redirect(url, searchInfo);
        });
    }
};

var Cookies = {
    init: function () {
        $('.container-cookies-popup .we-call-you-pop-up .close-pop-up').click(function () {
            Cookies.hideWeCallYouLater();
            $(this).closest('.we-call-you-pop-up').remove();
        });
        $('.container-cookies-popup .cookies-advice .cookies-advice-button').click(function () {
            Cookies.hideCookiesAdvice();
            $(this).closest('.cookies-advice').remove();
        });

        if (!localStorage.getItem('hide-we-call-you-pop-up')) {
            $('.container-cookies-popup .we-call-you-pop-up').removeAttr('style');
        } else {
            $('.container-cookies-popup .we-call-you-pop-up').remove();
        }

        if (!localStorage.getItem('hide-cookie-advice')) {
            $('.container-cookies-popup .cookies-advice').removeAttr('style');
        } else {
            $('.container-cookies-popup .cookies-advice').remove();
        }
    },
    hideWeCallYouLater: function () {
        XHR.getJSON(LOCALED_ROOT_URL + 'public-index/hide-we-call-you-later', {}, null, null);
        localStorage.setItem('hide-we-call-you-pop-up', '1');
    },
    hideCookiesAdvice: function () {
        XHR.getJSON(LOCALED_ROOT_URL + 'public-index/hide-cookies-advice', {}, null, null);
        localStorage.setItem('hide-cookie-advice', '1');
    }
};

var searches = {
    init: function () {
        searches.asset.init();
        searches.topFilter.init();
    },
    asset: {
        init: function () {
            $('.asset-ajax-search').once('asset-ajax-search').each(function () {
                new searches.asset._obj($(this));
            });
            $('.asset-ajax-search').each(function () {
                $(this).data('searchObj').setListeners();
            });
        },
        _obj: function ($container) {
            var self = this;
            $container.data('searchObj', this);
            self.$container = $container;
            self.first_auto_search = true;
            self.lastFilterOptions = $('.haya-results-of-search, .promotion-active-type-haya').data('lazy-content-params');
            self.pending_request = false;

            self.searchHasChanged = function (newParams, oldParams) {
                if (oldParams === undefined) {
                    oldParams = self.lastFilterOptions;
                }

                for (var key in newParams) {
                    if (key === 'pagination_random_token' || key === 'params' || key === 'filters') {
                        continue;
                    }

                    if (newParams[key] == '') {
                        newParams[key] = undefined;
                    }

                    if (oldParams[key] == '') {
                        oldParams[key] = undefined;
                    }

                    if (oldParams[key] != newParams[key]) {
                        // console.log('key: ' + key + ' old_value: ' + oldParams[key] + ' new_value: ' + newParams[key]);
                        return true;
                    }
                }

                return false;
            };

            this.setListeners = function () {
                self.$btnFilter = self.$container.find('.btn-apply-filters');
                self.$topFormBtns = self.$container.find('.btn-filter');
                self.$btnClearFilter = self.$container.find('.btn-clear-filters');

                self.$orderBy = self.$container.find('[name="order_by"]');
                self.$elementsPerPage = self.$container.find('[name="results_per_page"]');
                self.$sidebarFilters = self.$container.find('.sidebar-filters').find('input, textarea, select');
                self.$topFormFilters = self.$topFormBtns.parents('form').find('input, textarea, select');
                self.$resultContainer = self.$container.find('[data-lazy-content-refresh="asset/index-result"], [data-lazy-content-refresh="asset/index-result-modal"], [data-lazy-content="asset/index-result"], [data-lazy-content="asset/index-result-modal"]');
                self.$resultName = self.$container.find('[data-lazy-content-refresh="asset/index-result-name"], [data-lazy-content-refresh="asset/index-result-name-modal"], [data-lazy-content="asset/index-result-name"], [data-lazy-content="asset/index-result-name-modal"]');

                self.$orderBy.once('asset-searchObj-listeners').change(function () {
                    self.search();
                });

                self.$elementsPerPage.once('asset-searchObj-listeners').change(function () {
                    self.search();
                });

                self.$topFormFilters.once('asset-searchObj-listeners').change(function () {
                    self.search();
                });

                self.$sidebarFilters.once('asset-searchObj-listeners').change(function () {
                    self.search();
                });

                self.$btnFilter.once('asset-searchObj-listeners').click(function (e) {
                    self.search(true);
                });

                self.$topFormBtns.parents('form').once('asset-searchObj-listeners').submit(function (e) {
                    e.preventDefault();
                    $(this).find('.btn-filter').click();
                });
                self.$sidebarFilters.parents('form').once('asset-searchObj-listeners').submit(function (e) {
                    e.preventDefault();
                    self.search(true);
                });

                self.$topFormBtns.once('asset-searchObj-listeners').click(function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    var nonMirroredInputs = [];
                    var $visibleTopFormFiltersWithMirror = self.$topFormFilters.filter('[data-mirrored-select]:visible');

                    self.$sidebarFilters.each(function () {
                        var $input = $(this);
                        var mirror = $input.data('mirrored-select');

                        if (!mirror || $visibleTopFormFiltersWithMirror.filter('[data-mirrored-select="' + mirror + '"]').length == 0) {
                            nonMirroredInputs.push($input.get(0));
                        }
                    });

                    components.clearInputs($(nonMirroredInputs), false);

                    self.search();
                });

                self.$btnClearFilter.once('asset-searchObj-listeners').click(function () {
                    components.clearInputs(self.$sidebarFilters, false);
                    components.clearInputs(self.$topFormFilters, false);

                    self.search();
                });
            };

            this.setListeners();

            self.$container.data('filters', self.$resultContainer.data('lazy-content-params'));

            self.timeout = null;
            self.search = function (force, mode_has_changed) {
                clearTimeout(self.timeout);

                if (!force || self.pending_request) {
                    self.timeout = setTimeout(function () {
                        self.search(true);
                    }, 500);
                    return;
                }

                self.$resultContainer = $container.find('[data-lazy-content-refresh="public/asset/index-result"], [data-lazy-content-refresh*="public/promotion/index-result/"]');
                self.$pagination = $container.find('[data-lazy-content-refresh="public/asset/index-pagination"], [data-lazy-content-refresh*="public/promotion/index-pagination/"]');
                self.$resultName = $container.find('[data-lazy-content-refresh="public/asset/index-result-name"], [data-lazy-content-refresh*="public/promotion/index-result-name/"]');
                self.$breadcrumbs = $container.find('[data-lazy-content-refresh="public/asset/index-result-breadcrumb"], [data-lazy-content-refresh*="public/promotion/index-result-breadcrumb/"]');
                self.$locationDetail = $container.find('[data-lazy-content-refresh="public/asset/index-location-detail"], [data-lazy-content-refresh*="public/promotion/index-location-detail/"]');

                var old_params = self.$resultContainer.data('lazy-content-params');

                var params = {
                    pagination_random_token: self.$resultContainer.data('lazy-content-params').pagination_random_token,
                    params: self.$resultContainer.data('lazy-content-params').params,
                    filters: self.$resultContainer.data('lazy-content-params').filters
                };

                self.$topFormFilters.each(function () {
                    var $input = $(this);

                    if (!$input.get(0).disabled && $input.val() && $input.val().length > 0) {
                        if ($input.is('[type="checkbox"], [type="radio"]')) {
                            if ($input.is(':checked')) {
                                params[$input.attr('name')] = $input.val();
                            } else {
                                params[$input.attr('name')] = undefined;
                            }
                        } else if ($input.is('.haya-input-checkbox-value') && $input.val().toString() === '0') {
                            params[$input.attr('name')] = undefined;
                        } else {
                            params[$input.attr('name')] = $input.val();
                        }
                    } else {
                        params[$input.attr('name')] = undefined;
                    }
                });

                self.$sidebarFilters.each(function () {
                    var $input = $(this),
                        id = $input.attr('id'),
                        $pseudoSelect = $('[data-for="' + id + '"]');

                    if ($pseudoSelect.length > 0 && !$pseudoSelect.is(':visible') && $pseudoSelect.parents('.haya-filters').is(':visible')) {
                        return;
                    }

                    if (!$input.get(0).disabled && $input.val() && $input.val().length > 0) {
                        if ($input.is('[type="checkbox"], [type="radio"]')) {
                            if ($input.is(':checked')) {
                                params[$input.attr('name')] = $input.val();
                            } else {
                                params[$input.attr('name')] = undefined;
                            }
                        } else if ($input.is('.haya-input-checkbox-value') && $input.val().toString() === '0') {
                            params[$input.attr('name')] = undefined;
                        } else {
                            params[$input.attr('name')] = $input.val();
                        }
                    } else {
                        params[$input.attr('name')] = undefined;
                    }
                });

                // Set order & elements per page
                params.per_page = self.$elementsPerPage.val();
                params.order_by = self.$orderBy.val();

                // Remove page
                params.p = undefined;

                var params_no_pagination = jQuery.extend({}, params);
                params_no_pagination.pagination_random_token = undefined;
                params_no_pagination.per_page = undefined;
                params_no_pagination.order_by = undefined;

                if (!self.searchHasChanged(params) && !mode_has_changed) {
                    return;
                }

                self.pending_request = true;
                self.lastFilterOptions = params;

                var map_mode = false;

                if (typeof(window.AssetMapSearch) != 'undefined') {
                    window.AssetMapSearch.update(params_no_pagination);

                    if (window.AssetMapSearch.active) {
                        map_mode = true;
                    }
                }

                $('.autoupdate-on-search').data('filters', {filters: JSON.stringify(params_no_pagination)});

                // Update ajax params and refresh
                self.$resultContainer.data('lazy-content-params', params);
                self.$pagination.data('lazy-content-params', params);
                self.$resultName.data('lazy-content-params', params);
                self.$breadcrumbs.data('lazy-content-params', params);
                self.$locationDetail.data('lazy-content-params', params);

                if (!map_mode) {
                    libs.lazy.refresh([self.$resultContainer, self.$pagination, self.$resultName, self.$breadcrumbs, self.$locationDetail], self.$container, true);
                } else {
                    libs.lazy.refresh([self.$resultName, self.$breadcrumbs, self.$locationDetail], self.$container, true);
                }

                self.$container.data('filters', params);

                var params_with_page_number = jQuery.extend({}, params_no_pagination);
                params_with_page_number.p = old_params.p;

                AssetSearch.getUrl(params_with_page_number, function (url) {
                    self.pending_request = false;

                    if (typeof(is_promotion) == 'undefined') {
                        if (location.toString() != url) {
                            history.pushState(params_with_page_number, '', url);
                        }
                    }
                }, function () {
                    self.pending_request = false;
                });
            };
        }
    },
    topFilter: {
        init: function () {
            $('.top-filter-ajax-search').once('top-filter-ajax-search').each(function () {
                new searches.topFilter._obj($(this));
            });
            $('.top-filter-ajax-search').each(function () {
                $(this).data('searchObj').setListeners();
            });
        },
        _obj: function ($container) {
            var self = this;
            $container.data('searchObj', this);
            self.$container = $container;

            this.setListeners = function () {
                self.$topFormBtns = self.$container.find('.btn-filter');
                self.$btnClearFilter = self.$container.find('.btn-clear-filters');

                self.$elementsPerPage = self.$container.find('[name="results_per_page"]');
                self.$topFormFilters = self.$topFormBtns.parents('form').find('input, textarea, select');

                self.$elementsPerPage.once('searchObj-setListeners').change(function () {
                    self.search();
                });

                self.$topFormFilters.once('searchObj-setListeners').change(function () {
                    self.search();
                });

                self.$topFormBtns.parents('form').once('searchObj-setListeners').submit(function (e) {
                    e.preventDefault();
                    $(this).find('.btn-filter').click();
                });

                self.$topFormBtns.once('searchObj-setListeners').click(function (e) {
                    e.stopPropagation();
                    e.preventDefault();

                    self.search();
                });

                self.$btnClearFilter.once('searchObj-setListeners').click(function () {
                    components.clearInputs(self.$topFormFilters, false);

                    self.search();
                });

                self.$container.find('a[data-order_by]').once('searchObj-setListeners').click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();

                    self.$resultContainer = self.$container.find('[data-lazy-content-refresh], [data-lazy-content-refresh], [data-lazy-content*="index-result"], [data-lazy-content*="index-result-modal"]');
                    var params = self.$resultContainer.data('lazy-content-params');
                    params.order_by = $(this).data('order_by');
                    self.search();
                });
            };

            this.setListeners();

            self.timeout = null;
            self.search = function (force) {
                clearTimeout(self.timeout);

                if (!force) {
                    self.timeout = setTimeout(function () {
                        self.search(true);
                    }, 500);
                    return;
                }

                self.$resultContainer = self.$container.find('[data-lazy-content-refresh*="index-result"], [data-lazy-content-refresh*="index-result-modal"], [data-lazy-content*="index-result"], [data-lazy-content*="index-result-modal"]');
                self.$pagination = self.$container.find('[data-lazy-content-refresh*="index-pagination-inner"], [data-lazy-content-refresh*="index-pagination-inner-modal"], [data-lazy-content*="index-pagination-inner"], [data-lazy-content*="index-pagination-inner-modal"]');

                var params = {
                    pagination_random_token: self.$resultContainer.data('lazy-content-params').pagination_random_token,
                    params: self.$resultContainer.data('lazy-content-params').params,
                    order_by: self.$resultContainer.data('lazy-content-params').order_by,
                    filters: self.$resultContainer.data('lazy-content-params').filters
                };

                self.$topFormFilters.each(function () {
                    var $input = $(this);

                    if (!$input.get(0).disabled && $input.val() && $input.val().length > 0) {
                        if ($input.is('[type="checkbox"], [type="radio"]')) {
                            if ($input.is(':checked')) {
                                params[$input.attr('name')] = $input.val();
                            } else {
                                params[$input.attr('name')] = undefined;
                            }
                        } else {
                            params[$input.attr('name')] = $input.val();
                        }
                    } else {
                        params[$input.attr('name')] = undefined;
                    }
                });

                // Set order & elements per page
                params.per_page = self.$elementsPerPage.val();

                // Remove page
                params.p = undefined;

                // Update ajax params and refresh
                self.$resultContainer.data('lazy-content-params', params);
                self.$pagination.data('lazy-content-params', params);
                libs.lazy.refresh([self.$resultContainer, self.$pagination], self.$container, true);
            };
        }
    }
};

var PostLinks = {
    init: function () {
        $('a[data-method="post"]').once('post-redirect').click(function (e) {
            e.stopPropagation();
            e.preventDefault();

            PostLinks.redirect($(this).attr('href'), $(this).data('post-data'));
        });
    },
    redirect: function (url, post) {
        var $form = $('<form>').appendTo('body');
        $form.attr('action', url);
        $form.attr('method', 'post');

        for (var field in post) {
            var $input = $('<input type="hidden">');
            $input.attr('name', field);
            $input.val(post[field]);
            $form.append($input);
        }

        setTimeout(function () {
            $form.submit();
        }, 50);
    }
};

var addons = {
    init: function () {
        addons.pagination();
    },
    pagination: function () {
        addons.pagination.setListeners = function ($pagination, $table) {
            $pagination.find('[data-page-number]').once('pagination-page-number').click(function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (!$(this).hasClass('disabed')) {
                    addons.pagination.setPage($pagination, $table, $(this).data('page-number'));
                }
            });

            $pagination.find('[name="results_per_page"]').once('pagination-results-per-page').change(function () {
                if ($(this).parents('.filtered-pagination').length === 0) {
                    $pagination.data('lazy-content-params', params);
                    $('#' + $pagination.data('lazy-content-for')).data('lazy-content-params', params);

                    libs.lazy.refresh($pagination, null, true);
                    libs.lazy.refresh($('#' + $pagination.data('lazy-content-for')), null, true);
                }
            });
        };

        addons.pagination.updateLinks = function ($pagination, $table) {
            var current_page = $pagination.data('current-page'),
                pages = $pagination.data('pagination-pages'),
                pagesToShow = ['prev', 1, current_page - 1, current_page, current_page + 1, pages, 'next'],
                buttons = [],
                last_page_added = 0,
                html = [];

            for (var i = 0; i < pagesToShow.length; i++) {
                var page = pagesToShow[i];

                if (page != 'prev' && page != 'next' && (page > pages || page < 1)) {
                    continue;
                }

                if ($.inArray(page, buttons) == -1) {
                    if (page != 'prev' && page != 'next') {
                        if (page - last_page_added > 1) {
                            buttons.push('..');
                        }

                        last_page_added = page;
                    }

                    buttons.push(page);
                }
            }


            for (var i = 0; i < buttons.length; i++) {
                var label = null,
                    area_label = null,
                    page_number = buttons[i],
                    btn_class = null;

                if (page_number == '..') {
                    html.push('<li>...</li>');
                } else if (page_number == 'prev') {
                    html.push('<li><a data-page-number="' + page_number + '" href="javascript:void(0)" class="haya-button haya-has-icon haya-button-light"><span><i class="haya-icon haya-icon-arrow-left-slim"><svg role="img" preserveAspectRatio="xMinYMin meet"><use xlink:href="' + ROOT_URL + 'static/img/svg/sprite.svg#arrow-left-slim"/></svg></i><strong>' + (LANG == 'es' ? 'Anterior' : 'Previous') + '</strong></span></a></li>');
                } else if (page_number == 'next') {
                    html.push('<li><a data-page-number="' + page_number + '" href="javascript:void(0)" class="haya-button haya-has-icon-right haya-button-light"><span><i class="haya-icon haya-icon-arrow-right-slim"><svg role="img" preserveAspectRatio="xMinYMin meet"><use xlink:href="' + ROOT_URL + 'static/img/svg/sprite.svg#arrow-right-slim"/></svg></i><strong>' + (LANG == 'es' ? 'Siguiente' : 'Next') + '</strong></span></a></li>');
                } else {
                    if (current_page == page_number) {
                        html.push('<li><a data-page-number="' + page_number + '" class="haya-current-page" href="javascript:void(0)">' + page_number + '</a></li>');
                    } else {
                        html.push('<li><a data-page-number="' + page_number + '" href="javascript:void(0)">' + page_number + '</a></li>');
                    }
                }
            }

            $pagination.find('ul').get(0).innerHTML = html.join('');
            addons.pagination.setListeners($pagination, $table);
        };

        addons.pagination.setPage = function ($pagination, $table, page_number, do_not_update_lazy) {
            var current_page = $pagination.data('current-page'),
                number_of_pages = $pagination.data('pagination-pages');

            if (page_number == 'prev') {
                if (current_page > 1) {
                    current_page--;
                } else {
                    return;
                }
            } else if (page_number == 'next') {
                if (current_page < number_of_pages) {
                    current_page++;
                } else {
                    return;
                }
            } else {
                if (page_number < 1 || page_number > number_of_pages) {
                    return;
                }

                current_page = page_number;
            }
            var params = $pagination.data('lazy-content-params');
            params.p = current_page;

            if ($pagination.data('pagination-redirect')) {
                params.pagination_random_token = undefined;
                params = jQuery.extend({}, params);

                location.href = location.protocol + '//' + location.host + location.pathname + '?' + jQuery.param(params);
                return;
            }

            $pagination.data('current-page', current_page);
            addons.pagination.updateLinks($pagination, $table);

            if (!do_not_update_lazy) {
                $pagination.data('lazy-content-params', params);

                var updated_page_on_url = false;

                $('#' + $pagination.data('lazy-content-for')).each(function () {
                    $(this).data('lazy-content-params', params);
                    libs.lazy.refresh($(this), null, true);

                    if ($(this).offset().top < $(window).scrollTop()) {
                        $(window).scrollTop($(this).offset().top - 30);
                    }

                    if (!updated_page_on_url) {
                        updated_page_on_url = true;

                        if ($(this).is('.haya-results-of-search')) {
                            var new_url = location.toString().split('?')[0];
                            if (current_page > 1) {
                                new_url += '?p=' + current_page;
                            }

                            if (location.toString() != new_url) {
                                history.pushState($pagination.data('lazy-content-params'), '', new_url);
                            }
                        }
                    }
                });
            }
        };

        $('[data-pagination-lazy-content]').once('initialize-pagination').each(function () {
            var $pagination = $(this);
            var $table = $('#' + $pagination.data('lazy-content-for'));

            var params = $pagination.data('lazy-content-params');
            if (!params || params.push !== undefined) {
                params = {};
            }
            $pagination.data('lazy-content-params', params);

            addons.pagination.updateLinks($pagination, $table);
        });
    }
};
var DataGtm = {
    init: function () {
        $('a[data-gtm]').on('click', function () {
            // $(this).attr('target', '_blank');
            // var data = $(this).data('gtm');
            // console.log(data);
            if (typeof(dataLayer) != 'undefined') dataLayer.push(data);
        });
    },
    tagAtPercentage: function () {
        var win_top = $(window).scrollTop();

        var doc_height = $(document).height();
        var win_height = $(window).height();

        var scrolled_percentage = parseInt(win_top / (doc_height - win_height) * 100);

        if (scrolled_percentage == 20 || scrolled_percentage == 40 || scrolled_percentage == 60 || scrolled_percentage == 80 || scrolled_percentage == 100) {
            if (typeof(dataLayer) != 'undefined') dataLayer.push({
                'eAction': 'Scroll ' + scrolled_percentage,
                'eLabel': 'Scroll ' + scrolled_percentage,
                'event': 'Scroll'
            });
        }
    }
};


var Visitor = {
    on_login_callback: null,
    logged_in: false,
    init: function () {
        this.setListeners();
    },
    showLoginModal: function () {
        Visitor.onLogin(null);
    },
    onLogin: function (callback, open_modal_for) {
        this.on_login_callback = callback;
        Visitor.checkLogin(true, open_modal_for);
    },
    checkLogin: function (open_modal, open_modal_for) {
        XHR.getJSON(LOCALED_ROOT_URL + 'public-visitors/check-login/', null, function (result) {
            if (result.response.succeeded) {
                if (!this.logged_in) {
                    Visitor.updateMenuAndFavourites();
                }

                if (Visitor.on_login_callback) {
                    Visitor.on_login_callback();
                    Visitor.on_login_callback = null;
                }
            } else {
                if (this.logged_in) {
                    Visitor.updateMenuAndFavourites();
                }

                if (open_modal) {
                    libs.modal.show(null, 'login', {open_modal_for: open_modal_for});
                }
            }

            this.logged_in = result.response.succeeded;
        }, function () {
        });
    },
    updateMenuAndFavourites: function () {
        XHR.getJSON(LOCALED_ROOT_URL + 'public-visitors/update-menu-and-favourites/', null, function (result) {
            if (result.response.succeeded) {
                $('#main-header').replaceWith(result.response.html);

                if (result.response.listOfVisitorAssetFavouriteIds) {

                }
            }
        }, function () {
        });
    },
    favouriteToggle: function (haya_asset_identifier, add) {
        if (typeof add == 'undefined') {
            add = true;
        }

        this.onLogin(function () {
            XHR.getJSON(LOCALED_ROOT_URL + 'public-visitors/toggle-favourite-asset/' + haya_asset_identifier + '/', {
                add: !!add
            }, function (result) {
                if (result.response.succeeded) {
                    if (result.response.added) {
                        $('article[data-identifier="' + haya_asset_identifier + '"]').addClass('favourite');
                    } else {
                        $('article[data-identifier="' + haya_asset_identifier + '"]').removeClass('favourite');
                    }
                }
            }, function () {
            });
        }, 'favourite');
    },
    favouriteToggleDetail: function (haya_asset_identifier, add) {
        if (typeof add == 'undefined') {
            add = false;
        }

        this.onLogin(function () {
            XHR.getJSON(LOCALED_ROOT_URL + 'public-visitors/toggle-favourite-asset/' + haya_asset_identifier + '/', {
                add: !!add
            }, function (result) {
                if (result.response.succeeded) {
                    if (result.response.added) {
                        $('body').addClass('favourite');
                    } else {
                        $('body').removeClass('favourite');
                    }
                }
            }, function () {
            });
        }, 'favourite');
    },
    saveSearch: function (filters) {
        this.onLogin(function () {
            XHR.getJSON(LOCALED_ROOT_URL + 'public-visitors/save-search/', {filters: filters}, function (result) {
                if (result.response.succeeded) {
                    libs.modal.show(null, 'saved-search-success', null);
                }
            }, function () {
            });
        }, 'save-search');
    },
    deleteSavedSearch: function (button) {
        if (confirm('¿Borrar la busqueda?')) {
            var id = $(button).closest('.haya-client-area-alert').data('id');
            XHR.getJSON(LOCALED_ROOT_URL + 'public-visitors/delete-saved-search/', {id: id}, function (result) {
                if (result.response.succeeded) {
                    $('.haya-client-area-alert[data-id="' + id + '"]').stop().slideUp(1000, function () {
                        $(this).remove()
                    });
                }
            }, function () {
            });
        }
    },
    setListeners: function () {
        $(document).on('click', '[data-saved-search]', function () {
            AssetSearch.search($(this).data('saved-search'));
        });

        $(document).on('click', '.haya-favourite', function (e) {
            e.stopPropagation();
            e.preventDefault();

            if ($(this).hasClass('list')) {
                var $article = $(this).closest('article');
                Visitor.favouriteToggle($article.data('identifier'), !$article.hasClass('favourite'));
            } else {
                Visitor.favouriteToggleDetail($('.haya-top-bar').data('identifier'), !$('body').hasClass('favourite'));
            }
        });

        $(document).on('click', '.delete-saved-search', function () {
            Visitor.deleteSavedSearch(this);
        });

        $(document).on('click', '.save-search-action', function () {
            var post_params = {};
            for (var i = 0; i < $('div.haya-filters-body input').length; i++) {
                var name = $('div.haya-filters-body input')[i].name;
                var value = $('div.haya-filters-body input')[i].value;
                post_params[name] = value;
            }
            Visitor.saveSearch(JSON.stringify(post_params));
        });
    }
};

window.VisitorCheckLogin = function () {
    Visitor.checkLogin();
};

window.redirectToHome = function () {
    location.href = LOCALED_ROOT_URL;
};

$(document).ready(function () {
    AssetSearch.init();
    searches.init();
    Cookies.init();
    addons.init();
    components.init();
    DataGtm.init();
    Visitor.init();
});

$(window).on('scroll', function () {
    DataGtm.tagAtPercentage();
});


$(document).on('languageChanged', function (e, data) {
    if (data != LANG) {
        location.href = $('link[rel=alternate][hreflang="' + data + '"]').attr('href');
    }
});