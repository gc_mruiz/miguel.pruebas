(function () {
    // Shortcuts
    var libs = window.libs;
    var backend = libs.backend;
    var xhr = libs.xhr;

    // Content functions
    backend.content = {
        getModal: function (name, params, data, success, error, call_error_on_succeeded_false) {
            xhr.postJSON(backend.url('content/modal', name), {
                params: params,
                data: data
            }, success, error, call_error_on_succeeded_false);
        },
        getLazy: function (name, params, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('content/lazy', name), params, success, error, call_error_on_succeeded_false);
        }
    };

    // Client functions
    backend.client = {
        file: {
            getLopdEmptyUrl: function (client_id, success, error, call_error_on_succeeded_false) {
                xhr.getJSON(backend.url('client/file/lopd-empty', client_id), {
                    client_id: client_id
                }, success, error, call_error_on_succeeded_false);
            },
            delete: function (client_file_id, success, error, call_error_on_succeeded_false) {
                xhr.getJSON(backend.url('client-file/delete', client_file_id), {
                    client_file_id: client_file_id
                }, success, error, call_error_on_succeeded_false);
            }
        }
    };

    backend.sendModalForm = function ($form, success) {
        backend.sendForm($form, success, function (err) {
            var errHtml = '<div class="reveal-errors"><div class="row collapse"><div class="columns"><p>';
            errHtml += (err ? err : 'Error inesperado.');
            errHtml += '<i class="icon icon-warning"><svg role="img"><use xlink:href="' + ROOT_URL + 'static/img/svg/sprite-crm.svg#warning"/></svg></i></div></div></div>';

            $form.find('.reveal-errors').remove();
            $form.find('.reveal-header').after(errHtml);
            $form.parents('.reveal-overlay').scrollTop(0)
        }, true);
    };

    backend.sendForm = function ($form, success, error, call_error_on_succeeded_false) {
        var url = $form.attr('action');
        if (!url || url == 'javascript:void(0)') {
            url = {
                dummy: true,
                result: {
                    response: {
                        succeeded: true
                    },
                    error: null
                }
            };
        }

        xhr.postMultipartJSON(url, libs.form.getData($form, true), function (result) {
            $form.find('.form-error').remove();

            if (!result || !result.response || !result.response.succeeded) {
                if (result.errorsByField) {
                    var $formInputs = $form.find('select, input, textarea');

                    for (var model in result.errorsByField) {
                        for (var field in result.errorsByField[model]) {
                            var errorFields = $formInputs.filter('[name*="[' + model + '][' + field + ']"], [data-name*="[' + model + '][' + field + ']"]').toArray();
                            $formInputs.filter('[name*="[' + field + ']"]').each(function () {
                                if($.inArray(this, errorFields) == -1) {
                                    if ($(this).attr('name').split('[' + model + '][Translation]').length > 1) {
                                        errorFields.push(this);
                                    }
                                }
                            });
                            $formInputs.filter('[data-name*="[' + field + ']"]').each(function () {
                                if($.inArray(this, errorFields) == -1) {
                                    if ($(this).attr('data-name').split('[' + model + '][Translation]').length > 1) {
                                        errorFields.push(this);
                                    }
                                }
                            });

                            jQuery.each(errorFields, function () {
                                var $afterElement = $(this);

                                if ($afterElement.data('select2')) {
                                    $afterElement = $afterElement.data('select2').$container;
                                }

                                if ($afterElement.parent().is('.input-group')) {
                                    $afterElement = $afterElement.parent();
                                }
                                if ($afterElement.is('.rich-editor')) {
                                    $afterElement = $afterElement.parent();
                                }

                                $afterElement.after('<span class="form-error is-visible">' + result.errorsByField[model][field] + '</span>');
                            });
                        }
                    }
                }

                if (call_error_on_succeeded_false) {
                    if (error) {
                        error(result.error);
                    }
                }
            } else {
                success(result);
            }
        }, error, false);
    };

    // File functions
    backend.file = {
        delete: function (file_id, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('file/delete', file_id), {
                file_id: file_id
            }, success, error, call_error_on_succeeded_false);
        },
        deletePicture: function (picture_id, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('file/deletePicture', picture_id), {
                picture_id: picture_id
            }, success, error, call_error_on_succeeded_false);
        },
        deleteAssetPicture: function (asset_picture_id, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('file/deleteAssetPicture', asset_picture_id), {
                asset_picture_id: asset_picture_id
            }, success, error, call_error_on_succeeded_false);
        }
    };

    // Geo functions
    backend.asset = {
        getAssetSubtypesByAssetTypeId: function (asset_type_id, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('asset/getAssetSubtypesByAssetTypeId', asset_type_id), {
                asset_type_id: asset_type_id
            }, success, error, call_error_on_succeeded_false);
        }
    };

    // Geo functions
    backend.visit = {
        getVisitStatusDetailsByVisitStatusId: function (visit_status_id, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('visit/getVisitStatusDetailsByVisitStatusId', visit_status_id), {
                visit_status_id: visit_status_id
            }, success, error, call_error_on_succeeded_false);
        }
    };

    // Geo functions
    backend.geo = {
        getCitiesByProvinceId: function (province_id, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('geo/getCitiesByProvinceId', province_id), {
                province_id: province_id
            }, success, error, call_error_on_succeeded_false);
        },
        getSubcitiesByCityId: function (city_id, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('geo/getSubcitiesByCityId', city_id), {
                city_id: city_id
            }, success, error, call_error_on_succeeded_false);
        },
        getDistrictsByCityId: function (city_id, success, error, call_error_on_succeeded_false) {
            xhr.getJSON(backend.url('geo/getDistrictsByCityId', city_id), {
                city_id: city_id
            }, success, error, call_error_on_succeeded_false);
        }
    };

    backend.alert = {
        markAsRead: function (alert_id) {
            xhr.getJSON(backend.url('alert/mark-as-read'), {alert_id: alert_id});
        }
    };

    backend.providerConversation = {
        markAsRead: function (provider_conversation_id) {
            xhr.getJSON(backend.url('providerConversation/mark-as-read'), {provider_conversation_id: provider_conversation_id});
        }
    };
})();