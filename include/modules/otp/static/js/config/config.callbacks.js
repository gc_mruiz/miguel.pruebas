(function () {
    // Shortcuts
    var libs = window.libs;
    var modal = libs.modal;
    var lazy = libs.lazy;

    modal.afterLoad = function (modal_name, modal_params, data, $html) {
        components.init();
        modalSelection.setListeners($html);
        addons.pagination();
        searches.init();
        lazy.init();
    };

    modal.afterVisible = function (modal_name, modal_params, data, $html) {
        $html.children().foundation();

        libs.modal.init();
        actions.init();
        addons.init();
        modalSelection.init();
    };

    modal.afterClose = function () {
        lazy.refreshAll();
    };

    lazy.afterLoad = function (lazy_name, lazy_params, lazy_replace, $html) {
        components.init();
        modalSelection.setListeners($html);
        addons.pagination();
        searches.init();
        lazy.init();
    };

    lazy.afterVisible = function (lazy_name, lazy_params, lazy_replace, $html) {
        $html.children().foundation();

        libs.modal.init();
        actions.init();
        addons.init();
        modalSelection.init();
    };
})();