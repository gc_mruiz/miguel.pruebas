(function () {
    // Shortcuts
    var libs = window.libs;
    var backend = libs.backend;

    // Content URLs
    backend.addUrlHandler('content/modal', function (modal_name) {
        return {
            dummy: true,
            file: '/modals/' + modal_name + '.html',
            format: 'html'
        }
    });

    backend.addUrlHandler('content/lazy', function (name) {
        return {
            dummy: true,
            file: '/lazy-content/' + name + '.html',
            format: 'html'
        }
    });

    // Client
    backend.addUrlHandler('client/file/lopd-empty', function () {
        return {
            dummy: true,
            result: {
                response: {
                    succeeded: true,
                    url: '/static/_backend/example.pdf'
                },
                error: null
            }
        };
    });

    // File
    backend.addUrlHandler('file/upload', function () {
        $.support.cors = true;
        return 'http://www.proyectosrealizados.com/haya/file-upload.php';
    });
    backend.addUrlHandler('file/delete', function () {
        return {
            dummy: true,
            result: {
                response: {
                    succeeded: true
                },
                error: null
            }
        };
    });
    backend.addUrlHandler('client-file/delete', function () {
        return {
            dummy: true,
            result: {
                response: {
                    succeeded: true
                },
                error: null
            }
        };
    });

    // GEO URLs
    backend.addUrlHandler('geo/getCitiesByProvinceId', function (province_id) {
        var file;

        if (province_id == 1 || province_id == 2) {
            file = '/static/_backend/geo/cities-' + province_id + '.json';
        } else {
            file = '/static/_backend/geo/cities-other.json';
        }

        return {
            dummy: true,
            file: file,
            format: 'json'
        }
    });
    backend.addUrlHandler('geo/getSubcitiesByCityId', function (city_id) {
        var file;

        if (city_id == '1001' || city_id == '1002') {
            file = '/static/_backend/geo/subcities-' + city_id + '.json';
        } else {
            file = '/static/_backend/geo/subcities-other.json';
        }

        return {
            dummy: true,
            file: file,
            format: 'json'
        }
    });
    backend.addUrlHandler('geo/getDistrictsByCityId', function (city_id) {
        return {
            dummy: true,
            result: {
                response: {
                    succeeded: true,
                    keys: [],
                    values: {}
                },
                error: null
            }
        };
    });
    backend.addUrlHandler('asset/getAssetSubtypesByAssetTypeId', function (asset_type_id) {
        if (asset_type_id == 2) {
            return {
                dummy: true,
                result: {
                    response: {
                        succeeded: true,
                        keys: [1, 2],
                        values: {1: 'Piso', 2: 'Chalet'}
                    },
                    error: null
                }
            };
        } else {
            return {
                dummy: true,
                result: {
                    response: {
                        succeeded: true,
                        keys: [],
                        values: {}
                    },
                    error: null
                }
            };
        }
    });
    backend.addUrlHandler('visit/getVisitStatusDetailsByVisitStatusId', function (visit_status_id) {
        return {
            dummy: true,
            result: {
                response: {
                    succeeded: true,
                    keys: [],
                    values: {}
                },
                error: null
            }
        };
    });

    backend.addUrlHandler('alert/mark-as-read', function () {
        return {
            dummy: true,
            result: {
                response: {
                    succeeded: true
                },
                error: null
            }
        };
    });

    backend.addUrlHandler('providerConversation/mark-as-read', function () {
        return {
            dummy: true,
            result: {
                response: {
                    succeeded: true
                },
                error: null
            }
        };
    });
})();