var PostLinks = {
    init: function () {
        $('a[data-method="post"]').once('post-redirect').click(function (e) {
            e.stopPropagation();
            e.preventDefault();

            PostLinks.redirect($(this).attr('href'), $(this).data('post-data'));
        });
    },
    redirect: function (url, post) {
        var $form = $('<form>').appendTo('body');
        $form.attr('action', url);
        $form.attr('method', 'post');

        for (var field in post) {
            var $input = $('<input type="hidden">');
            $input.attr('name', field);
            $input.val(post[field]);
            $form.append($input);
        }

        setTimeout(function () {
            $form.submit();
        }, 50);
    }
};

$(document).ready(function () {
    $('[data-asset-report-form]').each(function () {
        var $form = $(this),
            $assetTypeSelect = $form.find('[name="assetReport[AssetReport][asset_type_id]"]'),
            original_asset_type_id = $assetTypeSelect.val();

        $assetTypeSelect.change(function () {
            if ($(this).val() != original_asset_type_id) {
                $form.find('[name="do_not_save"]').val(1);
                $form.submit();
            }
        });

        $(document).on('responsiveTabsActivate', function (e, tab) {
            $form.find('[name="selected_tab"]').val(tab.selector.substring(1));
        });
    });

    PostLinks.init();
});

var AssetFilter = {
    modal: function (params, onSelect) {
        libs.modal.show(null, 'asset-select-from-list', params, function ($html) {
            $html.find('[data-confirm-selection]').click(function () {
                var assetIds = [];

                $html.find('[name*="selectedAssetIds["]').each(function () {
                    if (this.checked) {
                        assetIds.push($(this).attr('name').split('[')[1].split(']')[0]);
                    }
                });

                libs.modal.close();
                onSelect(assetIds);
            });
        });
    }
};

libs.modal.addBeforeShowHandler('asset-report-missing-data', function (target, modal_name, modal_params, callback) {
    callback(true, libs.form.getData($('[data-asset-report-form]')));
});

var noAdBlockModals = [
    'brochure-select-page-design',
    'brochure-page-add',
    'catalog-asset-add',
    'print-advertisement-form',
    'print-advertisement-add-page'
];

for (var i = 0; i < noAdBlockModals.length; i++) {
    libs.modal.addBeforeShowHandler(noAdBlockModals[i], function (target, modal_name, modal_params, callback) {
        if (!ad_block_enabled) {
            callback(true, {});
        } else {
            callback(false, {});
            libs.modal.show(target, 'client-disabled');
        }
    });
}

function createAdvertisementFromFavourites(client_id) {
    AssetFilter.modal({favourites_of_client_id: client_id}, function (assetIds) {
        libs.modal.show(null, 'print-advertisement-form', {'type': 'catalog', 'assetIds': assetIds});
    });
}

function createAdvertisementFromSearch(filters) {
    AssetFilter.modal(filters, function (assetIds) {
        libs.modal.show(null, 'print-advertisement-form', {'type': 'catalog', 'assetIds': assetIds});
    });
}

function exportToExcelFromSearch() {
    var $savedSearch = $('[data-modal-name="saved-search-form"].autoupdate-on-search').data('modal-params');
    var filters = JSON.parse($savedSearch.filters);

    if (!filters.geo_province_id) {
        libs.modal.show(null, 'error', {error_message: 'Debe seleccionar la provincia antes de descargar.'});
    } else {
        document.location.href = '/assets/index-result/excel/?' + jQuery.param(filters);
    }
}


function exportClientsToExcelFromSearch() {
    var filters = $('.top-filter-ajax-search').data('searchObj').filters;

    document.location.href = '/clients/index-result/excel/?' + jQuery.param(filters);
}

function exportWorkToExcelFromSearch() {
    var filters = $('.top-filter-ajax-search').data('searchObj').filters;

    document.location.href = '/works/index-result/excel/?' + jQuery.param(filters);
}

function exportNotificationToExcelFromSearch() {
    var filters = $('.top-filter-ajax-search').data('searchObj').filters;

    document.location.href = '/notifications/index-result/excel/?' + jQuery.param(filters);
}

function exportAssetReportsToExcelFromSearch() {
    var filters = $('.top-filter-ajax-search').data('searchObj').filters;

    document.location.href = '/asset-reports/index-result/excel/?' + jQuery.param(filters);
}

function exportVisitsToExcelFromSearch() {
    var filters = $('.top-filter-ajax-search').data('searchObj').filters;

    document.location.href = '/visits/index-result/excel/?' + jQuery.param(filters);
}

function exportOffersToExcelFromSearch() {
    var filters = $('.top-filter-ajax-search').data('searchObj').filters;

    document.location.href = '/offers/index-result/excel/?' + jQuery.param(filters);
}

var ad_block_enabled = false;
var testAd = document.createElement('div');
testAd.innerHTML = '&nbsp;';
testAd.className = 'adsbox';
testAd.style.position = 'absolute';
testAd.style.top = '-100px';
document.body.appendChild(testAd);
window.setTimeout(function () {
    if (testAd.offsetHeight === 0) {
        ad_block_enabled = true;
    }

    if (testAd.remove != undefined) {
        testAd.remove();
    }
}, 100);

$(document).ready(function () {
    $('[name="cmsHomeBlock[CmsHomeBlock][home_mode]"]').on('change', function () {
        var $value = $(this).val();
        var width = '218';
        var heigth = '150';

        switch ($value) {
            case '2':
                var width = '366';
                var heigth = '178';

                break;
            case '3':
                var width = '265';
                var heigth = '164';

                break;
        }

        $('.dynamic-image-help-text').html('La imagen debe tener ' + width + 'px de ancho por ' + heigth + 'px de alto');
    }).change();
});

$(document).on('click', '[data-show-locate-on-map]', function () {
    var $row = $('#asset-report-map');

    if ($row.is(':visible')) {
        return;
    }

    $row.show().addClass('asset-report-map-expanded');

    libs.asyncScripts.load('https://maps.googleapis.com/maps/api/js?key=' + GOOGLE_MAPS_KEY, function () {
        var marker, $map, geocoder, center, mapOptions, map, $acceptButton, $query, $searchButton;

        $map = $('#asset-report-map-inner');

        function searchOnMap() {
            geocoder = new google.maps.Geocoder();
            center = new google.maps.LatLng(40.428528, -3.687576);
            mapOptions = {
                zoom: 14,
                center: center
            };
            map = new google.maps.Map($map.get(0), mapOptions);

            $query = $('[name="map_search_query"]');
            $searchButton = $('[name="map_search_search"]');
            $acceptButton = $('[name="map_search_save"]');
            marker = null;

            $acceptButton.click(function () {
                $('[name*="[lat]"]').val(marker.getPosition().lat());
                $('[name*="[lng]"]').val(marker.getPosition().lng());
                $row.hide();
            });

            $searchButton.click(function () {
                codeAddress($query.val());
            });

            var displayTextPars = [];
            $('[name*="[address_street_name]"]').each(function () {
                if ($(this).val()) {
                    displayTextPars.push($(this).val());
                }
            });
            $('[name*="[address_street_number]"]').each(function () {
                if ($(this).val()) {
                    displayTextPars.push($(this).val());
                }
            });
            $('[name*="[geo_city_id]"]').each(function () {
                if ($(this).val()) {
                    displayTextPars.push($(this).find('option:selected').text());
                }
            });
            $('[name*="[geo_province_id]"]').each(function () {
                if ($(this).val()) {
                    displayTextPars.push($(this).find('option:selected').text());
                }
            });

            if (displayTextPars.length > 0) {
                $query.val(displayTextPars.join(' '));

                setTimeout(function () {
                    $searchButton.click();
                }, 500);
            }
        }

        function codeAddress(address) {
            if (marker != null) {
                marker.setMap(null);
            }

            geocoder.geocode({
                address: address
            }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);

                    marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location,
                        draggable: true
                    });
                }
            });
        }

        searchOnMap();
    });
});

$(document).ready(function () {
    $('.excel-upload-progress').each(function () {
        checkUploadExcelProgress($('.excel-upload-progress').data('id'))
    });
});

function checkUploadExcelProgress(cms_offer_id) {
    $.getJSON(LOCALED_ROOT_URL + 'cms-offers/check-excel-upload-progress/' + cms_offer_id + '/')
        .done(function (result) {
            if (result.response.succeeded) {
                if (result.response.progress < 100) {
                    $('.excel-upload-progress')
                        .attr('aria-valuenow', result.response.progress)
                        .attr('aria-valuetext', result.response.progress + '%')
                        .find('.progress-meter').css('width', result.response.progress + '%')
                        .find('.progress-meter-text').html(result.response.progress + '%');
                    var timeoutID = window.setTimeout(checkUploadExcelProgress, 500, cms_offer_id);
                } else {
                    window.location.reload();
                }
            }
        });
}


var DisclaimerRules = {
    timer: undefined,
    resetOperation: function (obj) {
        var $selectField = $(obj);
        var $selectOperation = $("#selectRuleOperation");
        $('#rules-form').empty();
        if ($selectField.val() == "") {
            $selectOperation.disableByReason('selectRuleOperation');
        }
        else {
            $selectOperation.enableByReason('selectRuleOperation');
        }
        $selectOperation.val("");
        $selectOperation.change();
        var $operationsArray = DisclaimerRules.getOperationsByRule($selectField.val());
    },
    getOperationsByRule: function (rule) {
        $.getJSON('/cms-disclaimer-rules/get-operations-by-rule/', {rule: rule}).done(function (data) {
            var $selectOperation = $("#selectRuleOperation");
            $selectOperation.find('option:not([value=""])').remove();
            $.map(data.response.operations, function (value, index) {
                //operations[index]=value;
                $selectOperation.append($('<option>').attr({
                    value: index,
                }).text(value));
            });
        });
    },
    checkboxClick: function (checkbox) {
        var selectedValues = [];
        if ($("#selectValueField").val().length > 0) {
            selectedValues = $("#selectValueField").val().split(',');
        }
        var value = $(checkbox).val();
        var index = selectedValues.indexOf(value);
        if ($(checkbox).prop('checked') && index < 0) {
            selectedValues.push(value);
        }
        else if (!$(checkbox).prop('checked') && index >= 0) {
            selectedValues.splice(index, 1);
        }
        else return;
        $("#selectValueField").val(selectedValues.join(','));
    },
    searchOptions: function (rule, operation, value, filter, checkeds) {
        clearTimeout(this.timer);
        this.timer = setTimeout(function () {
            DisclaimerRules.getForm(rule, operation, value, filter, checkeds);
        }, 500);
    },
    getForm: function (rule, operation, value, filter, checkeds) {
        $.ajax({
            url: '/cms-disclaimer-rules/get-form-rule/',
            data: {rule: rule, operation: operation, value: value, filter: filter, checkeds: checkeds},
            method: 'POST',
            dataType: 'json',
        }).done(function (data) {
            if (data.response.succeeded) {
                $('#rules-form').after(data.response.html).remove();
            }
            else {
                $('#rules-form').after('').remove();
            }
            //$("#selectFilter").focus();
            $("#selectFilter").val($("#selectFilter").val());
        });
    }
};