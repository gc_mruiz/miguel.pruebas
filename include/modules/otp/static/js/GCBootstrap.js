var GCBootstrap = {
	init : function() {
		this.setListeners();
	},
	setListeners : function() {
		var self = this;

		$('.navbar-minimalize').click(function() {
			self.storeMenuStatus();
		});

		$('.language-selector').change(function() {
			$(this).parents('form').attr('data-lang', $(this).val());
		});
		$('.language-selector').change();
	},
	storeMenuStatus : function() {
		$.getJSON(LOCALED_ROOT_URL + 'layout/store-menu-status/', {
			deployed : ($('body').hasClass('mini-navbar') ? 0 : 1)
		});
	}
}

$(document).ready(function() {
	GCBootstrap.init();
});
