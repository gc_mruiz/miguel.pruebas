<?php

class ADFS {
    private static $config = array();

    public static function initialize($config = null) {
        if ($config == null) {
            $config = GestyMVC::config('adfs');
        }

        self::$config = $config;
    }

    public static function authorizeUrl($redirect_to) {
        return self::$config['base_url'] . 'adfs/oauth2/authorize?' . http_build_query(array(
                                                                                           'response_type' => 'code',
                                                                                           'client_id' => self::$config['client_id'],
                                                                                           'resource' => self::$config['resource'],
                                                                                           'redirect_uri' => Router::url($redirect_to),
                                                                                       ));
    }

    public static function getToken($code, $redirect_to) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, self::$config['base_url'] . 'adfs/oauth2/token');
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
                                                                  'grant_type' => 'authorization_code',
                                                                  'client_id' => self::$config['client_id'],
                                                                  'client_secret' => self::$config['client_secret'],
                                                                  'resource' => self::$config['resource'],
                                                                  'redirect_uri' => Router::url($redirect_to),
                                                                  'code' => $code,
                                                              )));

        $result = curl_exec($ch);

        // Close curl
        curl_close($ch);

        if ($result !== false) {
            $result = json_decode($result, true);

            return $result;
        }

        return false;
    }

    public static function loadData($user) {
        if (starts_with($user, 'CISA.0\\')) {
            $user = substr($user, strlen('CISA.0\\'));
        }

        $master_user = 'Srv.loginotp';
        $password = 'Cala2244';
        $basedn = 'dc=corp,dc=int';

        $ad = ldap_connect("ldap://corp.int") or die('Could not connect to LDAP server.');
        ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
        ldap_bind($ad, "{$master_user}@corp.int", $password) or die('Could not bind to AD.');

        $userdn = self::getDN($ad, $user, $basedn);

        /* @var App $App */
        $App = MySQLModel::getInstance('App');
        $apps = $App->getAll();
        $userApps = array();

        foreach ($apps as $code => $app) {
            if ($app['App']['group']) {
                if (self::checkGroupEx($ad, $userdn, 'CN=' . $app['App']['group'] . ',OU=Servicios,OU=Grupos,OU=Usuarios,DC=corp,DC=int') || self::checkGroupEx($ad, $userdn, 'CN=' . $app['App']['group'] . ',CN=Users,DC=corp,DC=int')) {
                    $userApps[$code] = $app;
                }
            }
        }

        $ldap_base_dn = $userdn;
        $search_filter = "(&(objectCategory=person))";
        $result = ldap_search($ad, $ldap_base_dn, $search_filter);

        Authentication::set('user', 'otp_seed', false);
        Session::set('otp_seed_temp', false);

        if (false !== $result) {
            $entries = ldap_get_entries($ad, $result);

            if (!empty($entries[0]['mail'][0])) {
                Authentication::set('user', 'email', $entries[0]['mail'][0]);
            }
            if (!empty($entries[0]['displayname'][0])) {
                Authentication::set('user', 'name', $entries[0]['displayname'][0]);
            }
            if (!Authentication::get('user', 'name') && !empty($entries[0]['name'][0])) {
                Authentication::set('user', 'name', $entries[0]['name'][0]);
            }
            if (!empty($entries[0]['semillaotp'][0])) {
                Authentication::set('user', 'otp_seed', $entries[0]['semillaotp'][0]);
            }
        }

        ldap_unbind($ad);

        Authentication::set('user', 'apps', $userApps);

        if (isset($userApps['admin'])) {
            Authentication::set('user', 'level', 1);
        } else {
            Authentication::set('user', 'level', 0);
        }
    }

    /*
    * This function searchs in LDAP tree ($ad -LDAP link identifier)
    * entry specified by samaccountname and returns its DN or epmty
    * string on failure.
    */
    static function getDN($ad, $samaccountname, $basedn) {
        $attributes = array('dn');

        $result = ldap_search($ad, $basedn, "(samaccountname={$samaccountname})", $attributes);
        if ($result === false) {
            return '';
        }

        $entries = ldap_get_entries($ad, $result);

        if ($entries['count'] > 0) {
            return $entries[0]['dn'];
        } else {
            return '';
        }
    }

    /*
    * This static function retrieves and returns CN from given DN
    */
    static function getCN($dn) {
        preg_match('/[^,]*/', $dn, $matchs, PREG_OFFSET_CAPTURE, 3);

        return $matchs[0][0];
    }

    /*
    * This static function checks group membership of the user, searching only
    * in specified group (not recursively).
    */
    static function checkGroup($ad, $userdn, $groupdn) {
        $attributes = array('members');
        $result = ldap_read($ad, $userdn, "(memberof={$groupdn})", $attributes);

        if ($result === false) {
            return false;
        };
        $entries = ldap_get_entries($ad, $result);

        return ($entries['count'] > 0);
    }

    /*
    * This static function checks group membership of the user, searching
    * in specified group and groups which is its members (recursively).
    */
    static function checkGroupEx($ad, $userdn, $groupdn) {
        $attributes = array('memberof');
        $result = ldap_read($ad, $userdn, '(objectclass=*)', $attributes);

        if ($result === false) {
            return false;
        };

        $entries = ldap_get_entries($ad, $result);

        if ($entries['count'] <= 0) {
            return false;
        };

        if (empty($entries[0]['memberof'])) {
            return false;
        } else {
            for ($i = 0; $i < $entries[0]['memberof']['count']; $i++) {
                if ($entries[0]['memberof'][$i] == $groupdn) {
                    return true;
                } elseif (self::checkGroupEx($ad, $entries[0]['memberof'][$i], $groupdn)) {
                    return true;
                };
            };
        };

        return false;
    }

    public static function saveOtp($seed) {
        $user = Authentication::get('user', 'unique_name');

        if (starts_with($user, 'CISA.0\\')) {
            $user = substr($user, strlen('CISA.0\\'));
        }

        $master_user = 'Srv.loginotp';
        $password = 'Cala2244';
        $basedn = 'dc=corp,dc=int';

        $ad = ldap_connect("ldap://corp.int") or die('Could not connect to LDAP server.');
        ldap_set_option($ad, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ad, LDAP_OPT_REFERRALS, 0);
        ldap_bind($ad, "{$master_user}@corp.int", $password) or die('Could not bind to AD.');

        $userdn = self::getDN($ad, $user, $basedn);

        ldap_modify($ad, $userdn, array('semillaOTP' => $seed));
        ldap_unbind($ad);
    }
}