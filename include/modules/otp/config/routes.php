<?php
/**
 * Route definitions.
 * Attention: parsing is made in the opposite order as listed here.
 */

$apps = json_decode(file_get_contents(ROOT_PATH . 'content/.config/apps.json'), true);

$found_specific_host = false;

foreach ($apps as $code => $app) {
    if (!empty($app['subdomain']) && $app['subdomain'] == $_SERVER['HTTP_HOST']) {
        // Default
        Router::addRoute(':controller/:action/*', array(
            'module' => 'otp',
            'controller' => 'OtpReverseProxyController',
            'action' => 'app',
            $code,
            'force_ending_slash' => false,
        ));

        Router::addRoute('', array(
            'module' => 'otp',
            'controller' => 'OtpReverseProxyController',
            'action' => 'app',
            $code,
            'force_ending_slash' => false,
        ));

        $found_specific_host = true;
        break;
    }
}

if (!$found_specific_host) {
    if($_SERVER['HTTP_HOST'] != GestyMVC::config('main_app_domain')) {
        header('Location: ' . GestyMVC::config('main_app_base_url'));
        exit();
    }

    // Default
    Router::addRoute(':controller/:action/*', array());

    // Home
    Router::addRoute('', array(
        'module' => 'otp',
        'controller' => 'Users',
        'action' => 'index',
    ));

    Router::addRoute('app/*', array(
        'controller' => 'OtpReverseProxy',
        'action' => 'app',
        'force_ending_slash' => false,
    ));
}