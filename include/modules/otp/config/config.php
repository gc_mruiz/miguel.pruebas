<?php

/** Module configuration */
GestyMVC::setConfig('otp', array());

// Set up main domains.
if (isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], ':81') !== false) {
    GestyMVC::setConfig('domain_for_cookies', '.haya.localhost');
    GestyMVC::setConfig('main_app_domain', 'otp.haya.localhost:81');
    GestyMVC::setConfig('main_app_base_url', 'http://' . GestyMVC::config('main_app_domain') . '/');
    GestyMVC::setConfig('highAvailabilityIps', array());

    GestyMVC::setConfig('mail', array(
        'mail_function' => false,
        'host' => 'smtp.gmail.com',
        'user' => 'web@gestycontrol.com',
        'password' => 'N0R3plYW3b',
        'port' => 465,
        'ssl' => true,
        'timeout' => 30,
        'email' => 'web@gestycontrol.com',
        'display' => 'Portal Haya',
    ));
} else {
    GestyMVC::setConfig('domain_for_cookies', '.haya.es');
    GestyMVC::setConfig('main_app_domain', 'login.haya.es');
    GestyMVC::setConfig('main_app_base_url', 'https://' . GestyMVC::config('main_app_domain') . '/');
    GestyMVC::setConfig('highAvailabilityIps', array('10.10.14.59', '10.10.25.194'));

    GestyMVC::setConfig('mail', array(
        'mail_function' => false,
        'host' => 'noreply.haya.es',
        'user' => '',
        'password' => '',
        'port' => 25,
        'ssl' => false,
        'SMTPAutoTLS' => false,
        'timeout' => 30,
        'email' => 'noreply@haya.es',
        'display' => 'Haya Real Estate',
    ));
}