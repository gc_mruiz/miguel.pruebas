<?php

/**
 * Auth module specific model class.
 * Current module models inherit this class instead of the original one. All specific modifications to the model class
 * must be done to this one instead.
 */
class OtpMySQLModel extends AppMySQLModel {
}
