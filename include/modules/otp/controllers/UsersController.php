<?php

/**
 * Class UsersController.
 *
 * Handles an user related HTTP request.
 */
class UsersController extends OtpController {
    /**
     * Controller name
     */
    public $name = 'Users';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
        'adminAdd' => 3,
        'adminDelete' => 3,
        'adminEdit' => 3,
        'adminIndex' => 3,
        'login' => -1,
        'forgotPassword' => -1,
        'reset' => -1,
    );

    protected function beforeFilter() {
        Authentication::set('user', array(
            'id' => time(),
            'level' => 1,
            'adfs_origin' => 'Altamira',
            'unique_name' => 'CISA2.0/gc.flescano',
            'default_adfs_user' => 'flescano@gestycontrol.com',
            'default_adfs_password' => 'Somos984',
            'otp_seed' => 1,
            'otp_checked' => 1,
            'apps' => array(
                'hayaotp' => array(
                    'App' => json_decode('{
                        "code": "hayaotp",
                        "name": "hayaotp",
                        "description": "Otp publicada de HRE",
                        "domain": "login.haya.es",
                        "adfs_domain": "adfs.haya.es",
                        "default_adfs": true,
                        "ip": null,
                        "https": true,
                        "subdomain": null,
                        "startup_path": null,
                        "sha256_key": "Hams18127!???18273gasfgkdagi1yula",
                        "group": "servicio_login",
                        "menu_group": "Test ADFS",
                        "picture_url": null,
                        "ssl_version": null
                      }', true),
                ),
            ),
        ));

        if (Authentication::get('user', 'unique_name') && (starts_with($_SERVER['REMOTE_ADDR'], '10.10.') || in_array($_SERVER['REMOTE_ADDR'], array(
                    '213.164.164.164',
                    '62.15.161.103',
                )))) {
            if (!Authentication::get('user', 'otp_seed')) {
                Authentication::set('user', 'otp_seed', 1);
            }

            Authentication::set('user', 'otp_checked', 1);
        }

        if (Authentication::get('user', 'unique_name') && !Authentication::get('user', 'otp_seed') && !in_array($this->action, array(
                'login',
                'logout',
                'setOtp',
                'confirmEmail',
                'requestEmailChange',
            ))) {
            $this->redirect(array('action' => 'setOtp'));
        }

        if (Authentication::get('user', 'otp_seed') && !Authentication::get('user', 'otp_checked') && !in_array($this->action, array(
                'checkOtp',
                'sendOtpToken',
                'logout',
            ))) {
            $this->redirect(array('action' => 'checkOtp'));
        }

        return parent::beforeFilter();
    }

    public function reloadData() {
        ADFS::loadData(Authentication::get('user', 'unique_name'));
        $this->redirect(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/');
    }

    /**
     * Handles user change password request. Data is served via $_POST['user']['User']
     */
    public function changePassword() {
        $this->redirect(Authentication::get('user[pwd_url]') . '?' . http_build_query(array(
                                                                                          'redirect_uri' => Router::url(array(
                                                                                                                            'controller' => 'Users',
                                                                                                                            'action' => 'login',
                                                                                                                            '?' => array('return' => get_var('return')),
                                                                                                                        )),
                                                                                      )));
    }

    public function confirmEmail($status = 0) {
        $this->set('apps', array());
        $this->layout = 'cms';

        Session::set('email_confirmed', $status);

        if ($status == 2) {
            $this->redirect(array('action' => 'requestEmailChange'));
        } elseif ($status == 1) {
            $this->redirect(array('action' => 'setOtp'));
        }
    }

    public function setOtp() {
        $this->layout = 'cms';
        $this->set('apps', array());

        if (Authentication::get('user', 'otp_seed')) {
            $this->redirect(array('action' => 'index'));
        }

        if (!Session::get('email_confirmed')) {
            $this->redirect(array('action' => 'confirmEmail'));
        }

        include_once(INCLUDE_PATH . 'modules/otp/vendors/GoogleAuthenticator.php');

        $secret = Session::get('otp_seed_temp');
        $ga = new PHPGangsta_GoogleAuthenticator();

        if ($secret && post_var('code')) {
            $check_result = $ga->verifyCode($secret, post_var('code'), 2);

            if ($check_result) {
                ADFS::saveOtp($secret);
                Authentication::set('user', 'otp_seed', $secret);
                Authentication::set('user', 'otp_checked', true);
                Session::set('flash[message]', '¡Completado! Ya puedes continuar hacia las apps');
                $this->redirect(array('action' => 'index'));
            } else {
                $error = true;
                $this->set('error', $error);
            }
        }

        if (!$secret) {
            $secret = $ga->createSecret();
            Session::set('otp_seed_temp', $secret);
        }

        $qr_url = $ga->getQRCodeGoogleUrl('Portal Haya', $secret);
        $this->set('qr_url', $qr_url);
    }

    public function checkOtp() {
        // Set vars for layout
        $this->set(array(
                       'page' => 'login',
                       'page_class' => 'only-login-page',
                   ));

        $this->layout = 'login';
        $this->set('apps', array());

        include_once(INCLUDE_PATH . 'vendors/GoogleAuthenticator.php');

        $secret = Authentication::get('user', 'otp_seed');
        $ga = new PHPGangsta_GoogleAuthenticator();

        if ($secret && post_var('code')) {
            $check_result = $ga->verifyCode($secret, post_var('code'), 5);

            if ($check_result) {
                @syslog(LOG_INFO, 'OTP_SUCCESS ' . Authentication::get('user', 'unique_name') . ' completed double factor authentication');
                Authentication::set('user', 'otp_checked', true);
                $this->redirect(array('action' => 'index'));
            } else {
                Session::set('flash[message]', 'Código incorrecto');
                @syslog(LOG_WARNING, 'OTP_FAILED ' . Authentication::get('user', 'unique_name') . ' failed double factor authentication');
                $error = true;
                $this->set('error', $error);
            }
        }
    }

    public function sendOtpToken() {
        @syslog(LOG_WARNING, 'OTP_EMAIL ' . Authentication::get('user', 'unique_name') . ' requested code by email');
        include_once(INCLUDE_PATH . 'vendors/GoogleAuthenticator.php');

        $secret = Authentication::get('user', 'otp_seed');
        $ga = new PHPGangsta_GoogleAuthenticator();
        $code = $ga->getCode($secret);

        $success = false;

        if ($code) {
            if (Email::send('Código de acceso a portal Haya', 'Utiliza el siguiente código para acceder al portal: ' . $code, Authentication::get('user', 'email'))) {
                $success = true;
            }
        }

        if ($success) {
            Session::set('flash[message]', 'Código enviado por email');
        } else {
            Session::set('flash[message]', 'No se pudo enviar el código. Contacte con sistemas Haya.');
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function index($group_name = null) {
        $this->layout = 'cms';
        $apps = Authentication::get('user', 'apps');

        $this->set('apps', $apps);
        $this->set('group_name', $group_name);
    }

    /**
     * Handles user login request. Data is served via $_POST['user']['User']
     */
    public function login() {
        // Set layout
        $this->layout = 'login';

        // If user is already logged in, redirect
        if (Authentication::get('user', 'id')) {
            if (get_var('return') && strpos(get_var('return'), 'users/login/') === false) {
                // If return parameter exists and does not point to the login page, redirect to it
                $this->redirect(ROOT_URL . get_var('return'));
            } else {
                // If not, redirect to the user's home page
                $this->redirect(array('action' => 'index'));
            }
        } else {
            // Remove all cookies from previous session
            $this->removeCookies();

            if (!get_var('code')) {
                $this->redirect(ADFS::authorizeUrl(array(
                                                       'controller' => 'Users',
                                                       'action' => 'login',
                                                       '?' => array('return' => get_var('return')),
                                                   )));
            } else {
                $code = get_var('code');
                $result = ADFS::getToken($code, array(
                    'controller' => 'Users',
                    'action' => 'login',
                    '?' => array('return' => get_var('return')),
                ));

                if ($result) {
                    Authentication::set('adfs', $result);

                    include_once(ROOT_PATH . '/vendor/firebase/php-jwt/src/JWT.php');

                    $userData = json_decode(json_encode(\Firebase\JWT\JWT::decode($result['id_token'], GestyMVC::config('adfs[client_secret'), array(
                        'RS256',
                        'HS256',
                    ))), true);
                    $userData['id'] = time();
                    $userData['level'] = 0;
                    $userData['email'] = $userData['upn'];
                    Authentication::set('user', $userData);

                    ADFS::loadData(Authentication::get('user', 'unique_name'));
                    @syslog(LOG_INFO, 'ADFS_LOGIN ' . Authentication::get('user', 'unique_name'));
                }
            }
        }

        $this->redirect(array('action' => 'index'));
    }

    /**
     * Handles user logout request.
     */
    public function logout() {
        // Destroys session
        @syslog(LOG_INFO, 'LOGOUT ' . Authentication::get('user', 'unique_name'));
        Session::destroy();

        // Remove all cookies from previous session
        $this->removeCookies();

        // Redirect to the user's home page
        $this->redirect('https://adfs.haya.es/adfs/ls/?wa=wsignout1.0');
    }

    public function requestEmailChange() {
        // Set layout
        $this->layout = 'cms';

        $apps = Authentication::get('user', 'apps');

        $this->set('apps', $apps);

        if (post_var('user[User]')) {
            $succeeded = filter_var(post_var('user[User][email]'), FILTER_VALIDATE_EMAIL) !== false;

            if ($succeeded) {
                Email::send('Solicitud de cambio de correo electrónico', 'El usuario ' . Authentication::get('user', 'unique_name') . ' solicita que se cambie su correo electrónico de ' . Authentication::get('user', 'email') . ' a ' . post_var('user[User][email]') . '.', 'sistemas@haya.es');

                $this->set('success', true);
            } else {
                $error_message = __('Unable to save changes.', true);
                $error_message .= ' ' . __('Please try again.', true);

                $succeeded = false;

                $this->set('error_message', $error_message);
                $this->set('succeeded', $succeeded);
                $this->set('errorsByField', array('User' => array('email' => __('Email is not valid.', true))));
            }
        }
    }

    public function resetOtp() {
        // Set layout
        $this->layout = 'cms';

        $apps = Authentication::get('user', 'apps');

        $this->set('apps', $apps);

        if (post_var('user[User][reset]')) {
            ADFS::saveOtp('0');
            ADFS::loadData(Authentication::get('user', 'unique_name'));

            $this->redirect(array('action' => 'index'));
        }
    }

    public function appForm($code) {
        $apps = Authentication::get('user', 'apps');

        if (!isset($apps[$code])) {
            @syslog(LOG_WARNING, 'APP_ACCESS_DENIED ' . Authentication::get('user', 'unique_name') . ' tried to open ' . $code);
            $this->forbidden(false);
        }

        if ($code == 'admin') {
            @syslog(LOG_INFO, 'APP_ACCESS ' . Authentication::get('user', 'unique_name') . ' opened ' . $code);
            $this->redirect(array('controller' => 'Apps', 'action' => 'index'));
        }

        $url = Router::url(array('controller' => 'ReverseProxy', 'action' => 'app', $code));

        $app = $apps[$code];

        if ($app['App']['subdomain']) {
            $url = (Router::isHTTPS() ? 'https://' : 'http://') . $app['App']['subdomain'];
        }

        if (!ends_with($url, '/')) {
            $url .= '/';
        }

        $base_url = $url;

        @syslog(LOG_INFO, 'APP_ACCESS ' . Authentication::get('user', 'unique_name') . ' opened ' . $code);
        $this->redirect($base_url . '?persistentsession=' . session_id() . '|' . hash('sha256', $app['App']['sha256_key'] . '|' . session_id() . '|' . date('Ymd')));
    }

    /**
     * Removes user cookies from server
     *
     * @param null $cookie_path string
     */
    protected function removeCookies($cookie_path = null) {
        if (is_null($cookie_path)) {
            $cookie_path = ROOT_PATH . 'content/.temp/cookie-' . md5(Authentication::get('user', 'unique_name')) . '_';
        }

        @array_map('unlink', glob($cookie_path . '*'));
    }
}