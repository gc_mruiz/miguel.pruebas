<?php

/**
 * Class AppsController.
 *
 * Handles an app related HTTP request.
 */
class AppsController extends OtpController {
    /**
     * Controller name
     */
    public $name = 'Apps';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 1,
        'updateJson' => -1,
    );

    /**
     * @see Controller::$layout
     */
    protected $layout = 'cms';

    /**
     * Handles app listing.
     */
    public function index() {
        // Load App model
        /* @var App $App */
        $App = MySQLModel::getInstance('App');

        // Initialize pagination
        $apps = $App->getAll();
        $this->set('apps', $apps);
    }

    public function edit($code) {
        /* @var App $App */
        $App = MySQLModel::getInstance('App');
        $App->cache = false;
        $app = $App->getByCode($code);

        if (!$app) {
            $this->notFound();
        }

        $this->set('app', $app);

        if (post_var('app[App]')) {
            $succeeded = false;
            $error_message = null;
            $errorsByField = null;

            /* @var App $App */
            $App = MySQLModel::getInstance('App');
            $appData = post_var('app[App]');

            if ($App->updateFields($code, $appData)) {
                Session::set('flash[message]', 'Datos guardados.');
                $this->redirect(array('action' => 'index'));
            } else {
                $error_message = __('Unable to save changes.', true);
                $error_message .= ' ' . __('Please try again.', true);
            }

            $this->set('succeeded', $succeeded);
            $this->set('error_message', $error_message);
            $this->set('errorsByField', $errorsByField);
        }

        $this->set('listOfSslVersions', array(
            CURL_SSLVERSION_TLSv1 => 'CURL_SSLVERSION_TLSv1',
            CURL_SSLVERSION_SSLv2 => 'CURL_SSLVERSION_SSLv2',
            CURL_SSLVERSION_SSLv3 => 'CURL_SSLVERSION_SSLv3',
            CURL_SSLVERSION_TLSv1_0 => 'CURL_SSLVERSION_TLSv1_0',
            CURL_SSLVERSION_TLSv1_1 => 'CURL_SSLVERSION_TLSv1_1',
            CURL_SSLVERSION_TLSv1_2 => 'CURL_SSLVERSION_TLSv1_2',
        ));
    }

    public function delete($code) {
        /* @var App $App */
        $App = MySQLModel::getInstance('App');
        $App->deleteByCode($code);
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function add() {
        if (post_var('app[App]')) {
            $succeeded = false;
            $error_message = null;
            $errorsByField = null;

            /* @var App $App */
            $App = MySQLModel::getInstance('App');
            $appData = post_var('app[App]');

            if ($App->addNew($appData)) {
                Session::set('flash[message]', 'Datos guardados.');
                $this->redirect(array('action' => 'edit', $appData['code']));
            } else {
                $error_message = __('Unable to save changes.', true);
                $error_message .= ' ' . __('Please try again.', true);
            }

            $this->set('succeeded', $succeeded);
            $this->set('error_message', $error_message);
            $this->set('errorsByField', $errorsByField);
        }

        $this->set('listOfSslVersions', array(
            CURL_SSLVERSION_TLSv1 => 'CURL_SSLVERSION_TLSv1',
            CURL_SSLVERSION_SSLv2 => 'CURL_SSLVERSION_SSLv2',
            CURL_SSLVERSION_SSLv3 => 'CURL_SSLVERSION_SSLv3',
            CURL_SSLVERSION_TLSv1_0 => 'CURL_SSLVERSION_TLSv1_0',
            CURL_SSLVERSION_TLSv1_1 => 'CURL_SSLVERSION_TLSv1_1',
            CURL_SSLVERSION_TLSv1_2 => 'CURL_SSLVERSION_TLSv1_2',
        ));
    }

    public function updateJson() {
        $new_json = $this->originalPost['new_json'];
        $signature = $this->originalPost['signature'];

        if ($signature == md5($new_json . GestyMVC::config('update_json_signature_key')) && in_array($_SERVER['REMOTE_ADDR'], GestyMVC::config('highAvailabilityIps'))) {
            file_put_contents(ROOT_PATH . 'content/.config/apps.json', $new_json);
            echo 'OK';
            exit();
        }

        echo 'KO';
        exit();
    }
}