<?php

/**
 * Class ModuleNameControllerTemplateController.
 *
 * Handles a reverse proxy related HTTP request.
 */
class OtpReverseProxyController extends OtpController {
    /**
     * Controller name
     */
    public $name = 'OtpReverseProxy';

    public $view = false;
    public $layout = false;

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => -1,
    );

    protected $postValidation = array('app' => false, 'saveSession' => false);

    public function app($code = null) {
        set_time_limit(0);

        /* @var App $App */
        $App = MySQLModel::getInstance('App');
        $app = $App->getByCode($code);

        if (!$app) {
            // die(403 . ' ' . __LINE__);
            $this->forbidden(false);
        }

        if (get_var('persistentsession')) {
            $session_id = substr(get_var('persistentsession'), 0, strpos(get_var('persistentsession'), '|'));

            if (get_var('persistentsession') == $session_id . '|' . hash('sha256', $app['App']['sha256_key'] . '|' . $session_id . '|' . date('Ymd'))) {
                session_abort();
                session_id($session_id);
                session_start();

                Session::init(false);
            }
        }

        if (!Authentication::get('user', 'unique_name')) {
            @syslog(LOG_WARNING, 'APP_ACCESS_DENIED ' . $_SERVER['REMOTE_ADDR'] . ' tried to open ' . $code . ' without being authenticated.');
            //die(403 . ' ' . __LINE__);
            $this->forbidden(false);
        }

        $apps = Authentication::get('user', 'apps');

        if (!isset($apps[$code])) {
            @syslog(LOG_WARNING, 'APP_ACCESS_DENIED ' . Authentication::get('user', 'unique_name') . ' tried to open ' . $code);
            //die(403 . ' ' . __LINE__);
            $this->forbidden(false);
        }

        if ($code == 'admin') {
            //die(403 . ' ' . __LINE__);
            $this->forbidden(false);
        }

        $app = $apps[$code];
        $first_login_url = false;

        if (get_var('persistentsession')) {
            $url = Router::url(array('controller' => 'ReverseProxy', 'action' => 'app', $code));

            if ($app['App']['subdomain']) {
                $url = (Router::isHTTPS() ? 'https://' : 'http://') . $app['App']['subdomain'];
            }

            if (!ends_with($url, '/')) {
                $url .= '/';
            }

            if (!empty($app['App']['startup_path'])) {
                if (starts_with($app['App']['startup_path'], '/')) {
                    $app['App']['startup_path'] = substr($app['App']['startup_path'], 1);
                }

                $url .= $app['App']['startup_path'];
            }

            $_POST['access_token'] = Authentication::get('adfs', 'access_token');
            $_POST['token_type'] = Authentication::get('adfs', 'token_type');
            $_POST['expires_in'] = Authentication::get('adfs', 'expires_in');
            $_POST['resource'] = Authentication::get('adfs', 'resource');
            $_POST['refresh_token'] = Authentication::get('adfs', 'refresh_token');
            $_POST['refresh_token_expires_in'] = Authentication::get('adfs', 'refresh_token_expires_in');
            $_POST['id_token'] = Authentication::get('adfs', 'id_token');
            $_POST['user_id'] = Authentication::get('user', 'unique_name');

            if (starts_with($_POST['user_id'], 'CISA.0\a') && is_numeric(substr($_POST['user_id'], strlen('CISA.0\a')))) {
                $_POST['user_id'] = strtoupper($_POST['user_id']);
            }

            $_POST['email'] = Authentication::get('user', 'email');
            $_POST['signature'] = hash('sha256', $app['App']['sha256_key'] . '|' . $_POST['user_id'] . '|' . date('Ymd'));
            $_POST['email_signature'] = hash('sha256', $app['App']['sha256_key'] . '|' . $_POST['email'] . '|' . date('Ymd'));

            //if($_POST['user_id'] == 'CISA.0\A159263') die(json_encode($_POST));

            $first_login_url = $url;
            $_SERVER['REQUEST_METHOD'] = 'POST';
        }

        ini_set('memory_limit', '256M');

        $cookie_path = ROOT_PATH . 'content/.temp/cookie-' . md5(Authentication::get('user', 'unique_name')) . '_' . Session::get('authorized[' . $code . ']') . '-' . md5(Session::get('authorized[' . $code . ']') . '8917230917');

        /* config settings */
        $app_base_url = ($app['App']['https'] ? 'https://' : 'http://') . $app['App']['domain'];

        // Work out cookie domain
        $cookie_domain = $app_base_url;
        $proxy_domain = (Router::isHTTPS() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
        $app_root_url = substr(Router::url(array('action' => 'app', $code)), 0, -1);
        $proxy_url = $proxy_domain . $_SERVER['REQUEST_URI'];

        if (ends_with($app_root_url, '/')) {
            $app_root_url = substr($app_root_url, 0, -1);
        }

        if ($first_login_url) {
            $proxy_url = $first_login_url;
        }

        $proxy_relative_url = substr($proxy_url, strlen($app_root_url));

        $app_url = $app_base_url . $proxy_relative_url;

        if (!empty($app['App']['ip'])) {
            $app_url_for_curl = ($app['App']['https'] ? 'https://' : 'http://') . $app['App']['ip'] . $proxy_relative_url;
        } else {
            $app_url_for_curl = $app_url;
        }

        $replaces = array(
            $app_base_url,
            substr(json_encode($app_base_url), 1, -1),
        );

        $replacesBy = array(
            $app_root_url,
            substr(json_encode($app_root_url), 1, -1),
        );

        if (!empty($app['App']['additionalReplaces'])) {
            foreach ($app['App']['additionalReplaces'] as $find => $replace) {
                $replaces[] = $find;
                $replacesBy[] = str_replace('%BASE%', $app_root_url, $replace);
            }
        }

        // Open the cURL session
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $app_url_for_curl);
        curl_setopt($ch, CURLOPT_HEADER, 1);

        $headers = array();

        if (!empty($app['App']['ip'])) {
            $headers[] = 'Host: ' . $app['App']['domain'];
        }

        if (!empty($_SERVER['CONTENT_TYPE'])) {
            $headers[] = 'Content-Type: ' . $_SERVER['CONTENT_TYPE'];
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $diskFiles = array();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            curl_setopt($ch, CURLOPT_POST, true);

            if (!empty($_FILES)) {
                $originalPostFields = array_keys($_POST);
                $arrInputs = flattern_nested_array($_POST, '][');

                foreach ($originalPostFields as $original_post_field) {
                    foreach ($arrInputs as $key => $value) {
                        if (starts_with($key, $original_post_field . ']')) {
                            $arrInputs[$original_post_field . substr($key, strlen($original_post_field) + 1) . ']'] = $value;
                            unset($arrInputs[$key]);
                        }
                    }
                }

                foreach ($_FILES as $index => $files) {
                    foreach ($files as $file) {
                        if ($file['error'] === UPLOAD_ERR_OK) {
                            $arrInputs[$index] = new CurlFile($file['tmp_name'], $file['type'], $file['name']);
                        }
                    }
                }

                @curl_setopt($ch, CURLOPT_POSTFIELDS, $arrInputs);
            } elseif (!empty($_SERVER['CONTENT_TYPE']) && $_SERVER['CONTENT_TYPE'] == 'application/json') {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getPostedJson(false));
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
            }
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);

        if (isset($app['App']['ssl_version'])) {
            curl_setopt($ch, CURLOPT_SSLVERSION, $app['App']['ssl_version']);
        }

        // Handle other cookies
        foreach ($_COOKIE as $k => $v) {
            if (is_array($v)) {
                $v = serialize($v);
            }

            curl_setopt($ch, CURLOPT_COOKIE, "$k=$v; domain=.$cookie_domain ; path=/");
        }

        // Send the request and store the result in an array
        $response = curl_exec($ch);

        // Check that a connection was made
        if (curl_error($ch)) {
            // If it wasn't...
            print curl_error($ch);
        } else {
            $response = str_replace("HTTP/1.1 100 Continue\r\n\r\n", "", $response);

            $ar = explode("\r\n\r\n", $response, 2);

            $header = $ar[0];
            $body = $ar[1];

            $headers = explode(chr(10), $header);

            foreach ($headers as $k => $v) {
                if (!preg_match("/^Transfer-Encoding/", $v)) {
                    $v = str_replace($replaces, $replacesBy, $v);
                    $new_header = trim($v);

                    if (starts_with($new_header, 'Location: https://' . $app['App']['adfs_domain'])) {
                        $new_url = $this->otpRedirect($app, $new_header);

                        if ($new_url) {
                            $new_url = str_replace($replaces, $replacesBy, $new_url);

                            $this->redirect($new_url);
                        }
                    }

                    header($new_header);
                }
            }

            $body = str_replace($replaces, $replacesBy, $body);
            print $body;
        }

        foreach ($diskFiles as $disk_file) {
            @unlink($disk_file);
        }

        curl_close($ch);
        exit();
    }

    private function otpRedirect($app = null, $redirect_header = null) {
        if (is_null($app)) {
            return false;
        }

        if (is_null($redirect_header)) {
            return false;
        }

        error_reporting(E_ERROR | E_PARSE);

        $cookie_path = ROOT_PATH . 'content/.temp/cookie-' . md5(Authentication::get('user', 'unique_name')) . '_adfs_' . Session::get('authorized[' . $app['App']['sha256_key'] . ']') . '-' . md5(Session::get('authorized[' . $app['App']['sha256_key'] . ']') . '8917230917');

        /* config settings */
        $app_base_url = ($app['App']['https'] ? 'https://' : 'http://') . $app['App']['domain'];

        /**
         * Get login form from url
         */

        $url = substr(trim($redirect_header), 10);

        $headers = array(
            'Content-Type: text/html',
        );

        // Open the cURL session
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);

        $response = curl_exec($ch);

        // Check that a connection was made
        if (curl_error($ch)) {
            gtrace('cURL Error: ' . curl_error($ch));

            return false;
        }

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        curl_close($ch);

        $response_headers = substr($response, 0, $header_size);
        $response_body = substr($response, $header_size);

        gtrace(explode(PHP_EOL, trim($response_headers)));

        // New dom object
        $dom = new DomDocument;

        //load the html into the object
        $dom->loadHTML($response_body);

        //discard white space
        $dom->preserveWhiteSpace = false;

        // Form action and all input tags as a list
        $form_action = '';
        $postData = array();

        $form = $dom->getElementsByTagName('form');

        for ($i = 0; $i < $form->length; $i++) {
            if (is_object($form->item($i))) {
                $actionObject = $form->item($i)->attributes->getNamedItem('action');

                if (is_object($actionObject)) {
                    $form_action = $actionObject->value;
                }
            }
        }

        // All input tags as a list
        $inputTags = $dom->getElementsByTagName('input');

        // Get all rows from the table
        for ($i = 0; $i < $inputTags->length; $i++) {
            if (is_object($inputTags->item($i))) {
                $nameObject = $inputTags->item($i)->attributes->getNamedItem('name');

                if (is_object($nameObject) && !isset($postData[$nameObject->value]) || empty($postData[$nameObject->value])) {
                    $postData[$nameObject->value] = '';

                    $valueObject = $inputTags->item($i)->attributes->getNamedItem('value');
                    if (isset($valueObject->value) && !empty($valueObject->value)) {
                        $postData[$nameObject->value] = $valueObject->value;
                    }

                    // If input is empty, we try to fill it in with user data
                    if (empty($postData[$nameObject->value])) {
                        if (strpos(strtolower($nameObject->value), 'name') !== false) {
                            $postData[$nameObject->value] = Authentication::get('user', 'default_adfs_user');
                        } else if (strpos(strtolower($nameObject->value), 'password') !== false) {
                            $postData[$nameObject->value] = Authentication::get('user', 'default_adfs_password');
                        }
                    }
                }
            }
        }

        // Open the cURL session
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $form_action);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

        // Send the request and store the result in an array
        $response = curl_exec($ch);

        // Check that a connection was made
        if (curl_error($ch)) {
            gtrace('cURL Error: ' . curl_error($ch), 1);

            return false;
        }

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        curl_close($ch);

        $response_headers = substr($response, 0, $header_size);
        $response_body = substr($response, $header_size);

        gtrace(explode(PHP_EOL, trim($response_headers)), 1);

        $headers = explode(PHP_EOL, trim($response_headers));

        foreach ($headers as $header) {
            $header = trim($header);

            if (starts_with($header, 'Set-Cookie')) {
                curl_setopt($ch, CURLOPT_COOKIE, "$k=$v; domain=.$cookie_domain ; path=/");
            }


            if (starts_with($header, 'Location: ')) {
                // Open the cURL session
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, substr($header, 10));
                curl_setopt($ch, CURLOPT_HEADER, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_path);
                curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie_path);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);

                // Send the request and store the result in an array
                $response = curl_exec($ch);

                // Check that a connection was made
                if (curl_error($ch)) {
                    gtrace('cURL Error: ' . curl_error($ch), 1);

                    return false;
                }
                $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
                curl_close($ch);

                $response_headers = substr($response, 0, $header_size);
                $response_body = substr($response, $header_size);

                $headers = explode(PHP_EOL, trim($response_headers));

                gtrace($headers, null);
                gtrace($response_body, 1);

                break;
            }
        }

        exit;
    }
}
