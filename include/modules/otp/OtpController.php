<?php

/**
 * Module specific controller class.
 * Current module controllers inherit this class instead of the original one. All specific modifications to the controller
 * class must be done to this one instead.
 */
class OtpController extends AppController {
    /**
     * Before filter callback. Called before the action is called.
     * Checks access.
     *
     * @return bool if not true stops execution
     */
    protected function beforeFilter() {
        GestyMVC::addLibrary('ADFS');
        ADFS::initialize();

        $apps = Authentication::get('user', 'apps');
        $this->set('apps', $apps);

        return parent::beforeFilter();
    }
}