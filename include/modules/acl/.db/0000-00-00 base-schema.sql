ALTER TABLE `users`
	DROP COLUMN `level`;

CREATE TABLE `user_groups` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL,
	`description` VARCHAR(255) NULL DEFAULT NULL,
	`access_level` TINYINT(4) NOT NULL,
	`created` DATETIME NOT NULL,
	`modified` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8mb4_general_ci' ENGINE=InnoDB;

INSERT INTO `user_groups` (`id`, `name`, `description`, `access_level`, `created`, `modified`) VALUES
	(1, 'Root', 'God level', 3, '2018-03-09 11:10:34', '2018-03-09 11:10:37'),
	(2, 'Admin', 'Adminstrator level', 2, '2018-03-09 11:11:35', '2018-03-09 11:11:35'),
	(3, 'User', 'User level', 1, '2018-03-09 11:12:42', '2018-03-09 11:12:44');

CREATE TABLE `user_group_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_group_users_users` (`user_id`),
  KEY `FK_user_group_users_user_groups` (`user_group_id`),
  CONSTRAINT `FK_user_group_users_user_groups` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`),
  CONSTRAINT `FK_user_group_users_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
)
COLLATE='utf8mb4_general_ci' ENGINE=InnoDB;

INSERT INTO `user_group_users` (`id`, `user_id`, `user_group_id`, `created`, `modified`) VALUES
	(1, 1, 1, '2018-03-09 11:21:18', '2018-03-09 11:21:18');

