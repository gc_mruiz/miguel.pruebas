<?php

/**
 * Class User
 *
 * Model class for table users
 */
class User extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'User';

    /**
     * @see MySQLModel::$displayFields
     * @var array
     */
    public $displayFields = array('email');

    // Valid order by expressions to be provided by
    // user
    public $validOrderByExpressions = array(
        'id',
        'email',
        'level',
        'created',
        'modified',
    );

    /**
     * Returns user groups
     *
     * @param null $user_id
     * @return array|null
     */
    function getUserGroups($user_id = null) {
        if (is_null($user_id)) {
            $user_id = Authentication::get('user', 'id');
        }

        if ($user_id) {
            /* @var UserGroupUser $UserGroupUser */
            $UserGroupUser = MySQLModel::getInstance('UserGroupUser');
            $UserGroupUser->recursive = 0;
            $UserGroupUser->belongsTo = array(
                'UserGroup' => array(
                    'model' => 'UserGroup',
                    'foreign_key' => 'user_group_id',
                    'options' => array(
                        'fields' => array(
                            'id',
                            'name',
                            'description',
                            'access_level',
                        ),
                    ),
                ),
            );

            if ($userGroupUsers = $UserGroupUser->getAll(array('where' => array('user_id' => $user_id)))) {
                $user_accces_level = 0;
                foreach ($userGroupUsers as $key => $value) {
                    $user_accces_level = ($user_accces_level < $value['UserGroup']['access_level']) ? $value['UserGroup']['access_level'] : $user_accces_level;
                    unset($value['UserGroupUser']);
                    $userGroupUsers[$key] = $value['UserGroup'];
                }

                $userGroupUsers['access_level'] = $user_accces_level;

                return $userGroupUsers;
            } else {
                return null;
            }
        }

        return null;
    }

    /**
     * Initializes model
     */
    function initialize() {
        // Call parent
        parent::initialize();

        // Order by email ASC
        $this->order = $this->full_table_name . '.`email` ASC';
    }

    /**
     * Before validate callback. Does some data
     * generation:
     *
     * - digest password
     *
     * @param $element array original element
     * @param $values array new values
     * @param $options array
     *
     * @return boolean continue with validation
     */
    function beforeValidate($element, &$values, $options) {
        if (isset($values['email'])) {
            // Email will always be in lowercase
            $values['email'] = mb_strtolower($values['email'], 'UTF-8');

            // Check for duplicates
            $duplicatedConditions = array();
            $duplicatedConditions['email LIKE'] = $values['email'];

            // If user is being edit, remove itself from duplicate query
            if ($element) {
                $duplicatedConditions['id <>'] = $element[$this->name]['id'];
            }

            if ($this->count(array('where' => $duplicatedConditions))) {
                $this->errors[] = $this->errorsByField['email'] = __('There is already an user with that email address.', true);
                $this->errorCodesByField['email'] = 'unique';

                return false;
            }
        }

        // If password is set, digest it and check for
        // repeat
        if (isset($values['password'])) {
            // Check password security policy
            if (!$this->validPassword($values['password'])) {
                // Break
                return false;
            }

            // Validate password with the user history
            if (GestyMVC::config('password_history_days') && $element) {
                if ($this->passwordIsRepeated($element[$this->name]['id'], $values['password'])) {
                    // Add error
                    $this->errors[] = sprintf(__('You are not allowed to change your password to one you have used less than %s days ago.', true), GestyMVC::config('password_history_days'));

                    // Break
                    return false;
                }
            }

            // If repeat password is set and it does not match,
            // throw error
            if (isset($values['repeat_password'])) {
                if ($values['repeat_password'] != $values['password']) {
                    // Add error
                    $this->errors[] = __('Passwords do not match.', true);

                    // Break
                    return false;
                }

                // Unset repeat password
                unset($values['repeat_password']);
            }

            // Generate password salt
            $values['salt'] = random_token(true, 6, 10);

            // Clear it and fill password_digest instead
            $values['password_digest'] = $this->digestPassword($values['salt'], $values['password']);
            unset($values['password']);

            // Clear reset password token
            $values['reset_password_token'] = null;
            $values['last_password_change'] = now();
            $values['failed_attempts'] = 0;
        }

        // Update last reset request if token changed
        if (array_key_exists('reset_password_token', $values)) {
            $values['last_reset_request'] = ($values['reset_password_token'] ? now() : null);
        }

        // Delegate interruption on main class
        return parent::beforeValidate($element, $values, $options);
    }

    /**
     * Compares the user password to it's history to check for duplicates.
     *
     * @param $user_id
     * @param $password
     *
     * @return bool repeated
     */
    function passwordIsRepeated($user_id, $password) {
        // Initialize password history model
        /* @var AppMySQLModel $UserPasswordHistory */
        $UserPasswordHistory = MySQLModel::getInstance('UserPasswordHistory');

        // Retrieve user password history up to configured date
        $userPasswordHistories = $UserPasswordHistory->getAllByUserId($user_id, array(
            'fields' => array(
                'salt',
                'password_digest',
            ),
            'recursive' => -1,
            'where' => array('TIMESTAMPDIFF(DAY, NOW(), `created`) <' => GestyMVC::config('password_history_days')),
        ));

        // Check each password
        foreach ($userPasswordHistories as $userPasswordHistory) {
            if ($this->digestPassword($userPasswordHistory['UserPasswordHistory']['salt'], $password) == $userPasswordHistory['UserPasswordHistory']['password_digest']) {
                // Repeated
                return true;
            }
        }

        // By default, not repeated
        return false;
    }

    /**
     * Validates password security
     *
     * @param $password
     *
     * @return bool
     */
    function validPassword($password) {
        // Check password length
        if (!$password || mb_strlen($password, 'UTF-8') < GestyMVC::config('password_min_length')) {
            $this->errors[] = $this->errorsByField['password'] = __('The password does not meet the minimum security specifications.', true) . ' ' . sprintf(__('It must have at least %s characters.', true), GestyMVC::config('password_min_length'));
            $this->errorCodesByField['password'] = 'policy';

            return false;
        }

        // Check password casing
        if (GestyMVC::config('password_requires_both_cases') && ($password == mb_strtolower($password, 'UTF-8') || $password == mb_strtoupper($password, 'UTF-8'))) {
            $this->errors[] = $this->errorsByField['password'] = __('The password does not meet the minimum security specifications.', true) . ' ' . __('It requires both cases characters.', true);
            $this->errorCodesByField['password'] = 'policy';

            return false;
        }

        // If password does not require numbers or letters, set as if their where found
        $has_numbers = !GestyMVC::config('password_requires_numbers');
        $has_no_numerics = !GestyMVC::config('password_requires_letters');

        if (!$has_numbers || !$has_no_numerics) {
            $chars = str_split($password);

            foreach ($chars as $char) {
                if (!is_numeric($char)) {
                    $has_no_numerics = true;
                } else {
                    $has_numbers = true;
                }
            }

            if (!$has_numbers || !$has_no_numerics) {
                $this->errorsByField['password'] = __('The password does not meet the minimum security specifications.', true);
                if (!$has_numbers) {
                    $this->errorsByField['password'] .= ' ' . __('It requires at least one number.', true);
                }
                if (!$has_no_numerics) {
                    $this->errorsByField['password'] .= ' ' . __('It requires at least one no numeric character.', true);
                }

                $this->errors[] = $this->errorsByField['password'];
                $this->errorCodesByField['password'] = 'policy';

                return false;
            }
        }

        return true;
    }

    /**
     * Digests password
     *
     * @param $salt string user random salt
     * @param $password string password undigested
     *
     * @return string password digested
     */
    private function digestPassword($salt, $password) {
        // Add hash token and digest with md5
        return md5($salt . GestyMVC::config('user_digest_token') . $password);
    }

    /**
     * Checks users credentials
     *
     * @param $email string users' email
     * @param $password string users' password
     * undigested
     *
     * @return boolean success
     */
    function checkCredentials($email, $password) {
        // Reset errors
        $this->errors = array();

        // Avoid abuse
        random_sleep(1);

        // If not all parameters were set, skip
        if (!($email || $password)) {
            // Set error message
            $this->errors[] = __('Invalid username or password.', true);

            return false;
        }

        // Retrieve user
        $user = $this->getByEmail($email, array(
            'fields' => array(
                'id',
                'salt',
                'password_digest',
                'failed_attempts',
                'banned',
                'last_password_change',
            ),
            'recursive' => -1,
        ));

        // Check if user exists and password matches
        if (!$user || $user[$this->name]['password_digest'] != $this->digestPassword($user[$this->name]['salt'], $password)) {
            // Set error message
            $this->errors[] = __('Invalid username or password.', true);

            // Update failed attemps
            if ($user) {
                // Log failed attempt
                $this->updateFields($user[$this->name]['id'], array('failed_attempts' => intval($user[$this->name]['failed_attempts']) + 1), array('reset_errors' => false));
            }

            return false;
        }

        // Check if user is banned
        if ($user[$this->name]['banned']) {
            // Set error message
            $this->errors[] = __('The access to the site has been disabled for this user. Please contact the system administrator.', true);

            return false;
        }

        // Check if exceeded the number of failed attempts
        if ($user[$this->name]['failed_attempts'] && $user[$this->name]['failed_attempts'] >= GestyMVC::config('max_login_failed_attempts')) {
            // Set error message
            $this->errors[] = __('The maximun number of failed login attempts has been exceeded. Login has been disabled. Please, restore your password to enter the site.', true);

            return false;
        }

        // Log login
        $this->updateFields($user[$this->name]['id'], array(
            'last_login' => now(),
            'failed_attempts' => 0,
            'reset_password_token' => null,
        ), array('reset_errors' => false));

        /* @var AppMySQLModel $UserLogin */
        $UserLogin = MySQLModel::getInstance('UserLogin');
        $UserLogin->addNew(array(
                               'user_id' => $user[$this->name]['id'],
                               'ip' => (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null),
                               'user_agent' => ($_SERVER['HTTP_USER_AGENT'] ? $_SERVER['HTTP_USER_AGENT'] : null),
                           ));

        // Check if the password expired
        if (GestyMVC::config('password_valid_for_minutes') && (!$user[$this->name]['last_password_change'] || time() - strtotime($user[$this->name]['last_password_change']) >= GestyMVC::config('password_valid_for_minutes') * 60)) {
            // Set error message
            $this->errors[] = __('Your password has expired. Login has been disabled. Please, restore your password to enter the site.', true);

            return false;
        }

        // Success
        return true;
    }

    /**
     * Changes user password
     *
     * @param $email string user's email
     * @param $old_password string current user's
     * password
     * @param $new_password string new user's password
     *
     * @return boolean success
     */
    function changePassword($email, $old_password, $new_password) {
        // Avoid abuse
        random_sleep(1);

        // If not all parameters were set, skip
        if (!($email || $old_password || $new_password)) {
            return false;
        }

        // Check user's credentials
        if ($this->checkCredentials($email, $old_password)) {
            // Avoid errors crashing app
            try {
                // Retrieve full user
                $user = $this->getByEmail($email, array(
                    'fields' => array(
                        'id',
                    ),
                    'recursive' => -1,
                ));

                // If user exists
                if ($user) {
                    // Update password and token
                    return $this->updateFields($user[$this->name]['id'], array(
                        'password' => $new_password,
                    ));
                }
            } catch (Exception $e) {
            }
        }

        // There was an error
        return false;
    }

    /**
     * Checks if provided email and
     * reset_password_token matches an user
     *
     * @param $email string user's email
     * @param $reset_password_token string token
     *
     * @return boolean result
     */
    function checkToken($email, $reset_password_token) {
        // Reset errors
        $this->errors = array();

        // Avoid abuse
        random_sleep(1);

        // If not all parameters were provided, skip
        if (!($email || $reset_password_token)) {
            return false;
        }

        // Retrieve user
        $user = $this->getByEmail($email, array(
            'where' => array('reset_password_token <>' => null),
            'fields' => array(
                'reset_password_token',
                'last_reset_request',
            ),
            'recursive' => -1,
        ));

        // Check if user exists and token matches
        if (!$user || $user[$this->name]['reset_password_token'] != $reset_password_token) {
            $this->errors[] = __('Invalid token.', true);

            return false;
        }

        // Check token expiration
        if (GestyMVC::config('password_reset_validity_minutes') && time() - strtotime($user[$this->name]['last_reset_request']) > GestyMVC::config('password_reset_validity_minutes') * 60) {
            $this->errors[] = __('Invalid token.', true);

            return false;
        }

        // Success
        return true;
    }

    /**
     * Resets user password
     *
     * @param $email string user email
     * @param $reset_password_token string user reset
     * password token
     * @param $new_password string new password
     * (undigested)
     *
     * @return boolean result
     */
    function resetPassword($email, $reset_password_token, $new_password) {
        // Avoid abuse
        random_sleep(1);

        // If not all values were set, skip
        if (!($email || $reset_password_token || $new_password)) {
            return false;
        }

        // Check token
        if ($this->checkToken($email, $reset_password_token)) {
            // Avoid error crashing page
            try {
                // Retrieve full user
                $user = $this->getByEmail($email);

                // If user exists
                if ($user) {
                    // Update password and token
                    return $this->updateFields($user[$this->name]['id'], array(
                        'password' => $new_password,
                    ));
                }
            } catch (Exception $e) {
            }
        }

        // Error
        return false;
    }

    /**
     * Generates restore password token and sends an
     * email to user to restore it.
     *
     * @param $email string user's email
     *
     * @return boolean success
     */
    function sendPasswordResetLink($email) {
        // Retrieve user by email
        $user = $this->getByEmail($email, array(
            'fields' => array(
                'id',
                'last_reset_request',
                'email',
                'reset_password_token',
            ),
            'recursive' => -1,
        ));

        // If user does not exist, skip
        if (!$user || ($user[$this->name]['last_reset_request'] && time() - strtotime($user[$this->name]['last_reset_request']) < GestyMVC::config('password_reset_min_interval_minutes') * 60)) {
            // Avoid abuse
            random_sleep(5);

            return false;
        }

        // Generate new token
        $reset_password_token = random_token(false);

        // Update token on user
        $this->updateFields($user[$this->name]['id'], array('reset_password_token' => $reset_password_token));

        // Prevent attacker for distinguishing that an email was sent
        random_sleep(0.1);

        // Send email to user
        return Email::sendTemplate(__('Restore password', true), $email, array(), 'restore-password', array(
            'email' => $email,
            'reset_password_token' => $reset_password_token,
        ));
    }

    /**
     * Before delete callback. Allows to disable deleting of the element
     *
     * @param $element array deleted element
     * @param $options array options
     *
     * @return bool false to abort, true to continue
     */
    function beforeDelete($element, $options) {
        // Check if there is more than one user registered
        $there_is_one_or_less = $this->count(array('where' => array('banned' => null))) < 2;

        // Disable delete of last non-banned user
        if ($there_is_one_or_less) {
            $this->errors[] = __('You cannot delete the last non-banned user.', true);

            return false;
        }

        // By default, return true
        return true;
    }

    /**
     * After save callback.
     *
     * @param $created bool if element was just created
     * @param $element array final element
     * @param $values array updated values
     * @param $originalElement array|null element as it was before
     * @param $options array options
     *
     * @return void
     */
    function afterSave($created, $element, $values, $originalElement, $options) {
        // Run default behavior
        parent::afterSave($created, $element, $values, $originalElement, $options);

        // Check if password was just changed
        if (isset($values['password_digest'])) {
            // Log password
            /* @var AppMySQLModel $UserPasswordHistory */
            $UserPasswordHistory = MySQLModel::getInstance('UserPasswordHistory');
            $UserPasswordHistory->addNew(array(
                                             'user_id' => $element[$this->name]['id'],
                                             'password_digest' => $element[$this->name]['password_digest'],
                                             'salt' => $element[$this->name]['salt'],
                                         ));
        }
    }

}
