<?php

/**
 * Class ResourceAcces
 *
 * Model class for table resource_access
 *
 */
class ResourceAcces extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'ResourceAcces';

    /**
     * Default display fields for the model. When more than one, they are concatenated with an space.
     *
     * @var array
     */
    public $displayFields = array('id',);

    /**
     * Valid order by expressions. List of expression that can be used to order. Expression can be a field or a complex
     * expression. Do not include direction of order.
     *
     * @var array
     */
    public $validOrderByExpressions = array(
        'id',
        'resource_id',
        'user_id',
        'user_id_access',
        'user_group_id',
        'user_group_access',
        'created',
        'created_by_user_id',
        'modified',
        'modified_by_user_id',
    );

    /**
     * Belongs To (child->father) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'foreign_key' => 'foreign_key_id',
     * 'options' => array()) By convention ModelName and RelationshipName are equal unless they cannot be. By
     * convention foreign_key will be the id of the related model on the current table. Options are translated to the
     * other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $belongsTo = array(
        'Resource' => array(
            'model' => 'Resource',
            'foreign_key' => 'resource_id',
            'options' => array(),
        ),
        'User' => array(
            'model' => 'User',
            'foreign_key' => 'user_id',
            'options' => array(),
        ),
        'UserGroup' => array(
            'model' => 'UserGroup',
            'foreign_key' => 'user_group_id',
            'options' => array(),
        ),
    );

    /**
     * All mysql tables require created and modified fields. If this is set to true, the table also requires
     * created_by_user_id and modified_by_user_id and both area populated automatically.
     *
     * @var bool
     */
    protected $user_timestamp_fields = true;

    /**
     * Validation array for fields.
     * Structure: array('field_name' => array('validation_method' => 'Validation error message.')
     * Validation methods available:
     *  - notempty: if present, value cannot be empty
     *  - required: value must be present on creation. An empty value validates.
     *  - email: value must have an email format
     *  - numeric: value must be numeric
     *  - int: value must be the representation of an integer
     *
     * @var array
     */
    public $validation = array(
        'resource_id' => array(
            'id' => 'Resource must be a valid id value.',
            'required' => 'Resource is required.',
            'notempty' => 'Resource cannot be left empty.',
        ),
        'user_id' => array(
            'id' => 'User must be a valid id value.',
        ),
        'user_id_access' => array(
            'tinyint' => 'User id access must be a valid tinyint value.',
        ),
        'user_group_id' => array(
            'id' => 'User group must be a valid id value.',
        ),
        'user_group_access' => array(
            'tinyint' => 'User group access must be a valid tinyint value.',
        ),
        'created_by_user_id' => array(
            'id' => 'Created by user must be a valid id value.',
        ),
        'modified_by_user_id' => array(
            'id' => 'Modified by user must be a valid id value.',
        ),
    );

}
