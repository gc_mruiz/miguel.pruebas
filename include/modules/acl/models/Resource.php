<?php

/**
 * Class Resource
 *
 * Model class for table resources
 *
 */
class Resource extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'Resource';

    /**
     * Default display fields for the model. When more than one, they are concatenated with an space.
     *
     * @var array
     */
    public $displayFields = array('name',);

    /**
     * Valid order by expressions. List of expression that can be used to order. Expression can be a field or a complex
     * expression. Do not include direction of order.
     *
     * @var array
     */
    public $validOrderByExpressions = array(
        'id',
        'name',
        'is_parent',
        'resource_parent_id',
        'resource_type_identifier',
        'created',
        'created_by_user_id',
        'modified',
        'modified_by_user_id',
    );

    /**
     * Belongs To (child->father) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'foreign_key' => 'foreign_key_id',
     * 'options' => array()) By convention ModelName and RelationshipName are equal unless they cannot be. By
     * convention foreign_key will be the id of the related model on the current table. Options are translated to the
     * other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $belongsTo = array(
        'ResourceParent' => array(
            'model' => 'Resource',
            'foreign_key' => 'resource_parent_id',
            'options' => array(),
        ),
    );

    /**
     * Has Many (father->children) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'foreign_key' => 'foreign_key_id',
     * 'options' => array())
     * By convention ModelName and RelationshipName are equal unless they cannot be.
     * By convention foreign_key will be the id of the current model on the related table Options are translated to the
     * other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $hasMany = array(
        'ResourceAcces' => array(
            'model' => 'ResourceAcces',
            'foreign_key' => 'resource_id',
            'options' => array(),
        ),
    );

    /**
     * All mysql tables require created and modified fields. If this is set to true, the table also requires
     * created_by_user_id and modified_by_user_id and both area populated automatically.
     *
     * @var bool
     */
    protected $user_timestamp_fields = true;

    /**
     * Validation array for fields.
     * Structure: array('field_name' => array('validation_method' => 'Validation error message.')
     * Validation methods available:
     *  - notempty: if present, value cannot be empty
     *  - required: value must be present on creation. An empty value validates.
     *  - email: value must have an email format
     *  - numeric: value must be numeric
     *  - int: value must be the representation of an integer
     *
     * @var array
     */
    public $validation = array(
        'name' => array(
            'required' => 'Name is required.',
            'notempty' => 'Name cannot be left empty.',
        ),
        'is_parent' => array(
            'tinyint' => 'Is parent must be a valid tinyint value.',
        ),
        'resource_parent_id' => array(
            'id' => 'Resource parent must be a valid id value.',
        ),
        'resource_type_identifier' => array(
            'int' => 'Resource type identifier must be a valid int value.',
            'required' => 'Resource type identifier is required.',
            'notempty' => 'Resource type identifier cannot be left empty.',
        ),
        'created_by_user_id' => array(
            'id' => 'Created by user must be a valid id value.',
        ),
        'modified_by_user_id' => array(
            'id' => 'Modified by user must be a valid id value.',
        ),
    );

}
