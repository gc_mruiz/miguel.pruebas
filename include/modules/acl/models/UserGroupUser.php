<?php

/**
 * Class UserGroupUser
 *
 * Model class for table user_group_users
 *
 */
class UserGroupUser extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'UserGroupUser';

    /**
     * Default display fields for the model. When more than one, they are concatenated with an space.
     *
     * @var array
     */
    public $displayFields = array('id',);

    /**
     * Valid order by expressions. List of expression that can be used to order. Expression can be a field or a complex
     * expression. Do not include direction of order.
     *
     * @var array
     */
    public $validOrderByExpressions = array('id', 'user_id', 'user_group_id', 'created', 'modified',);

    /**
     * Belongs To (child->father) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'foreign_key' => 'foreign_key_id',
     * 'options' => array()) By convention ModelName and RelationshipName are equal unless they cannot be. By
     * convention foreign_key will be the id of the related model on the current table. Options are translated to the
     * other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'model' => 'User',
            'foreign_key' => 'user_id',
            'options' => array(),
        ),
        'UserGroup' => array(
            'model' => 'UserGroup',
            'foreign_key' => 'user_group_id',
            'options' => array(),
        ),
    );

    /**
     * Validation array for fields.
     * Structure: array('field_name' => array('validation_method' => 'Validation error message.')
     * Validation methods available:
     *  - notempty: if present, value cannot be empty
     *  - required: value must be present on creation. An empty value validates.
     *  - email: value must have an email format
     *  - numeric: value must be numeric
     *  - int: value must be the representation of an integer
     *
     * @var array
     */
    public $validation = array(
        'user_id' => array(
            'id' => 'User must be a valid id value.',
            'required' => 'User is required.',
            'notempty' => 'User cannot be left empty.',
        ),
        'user_group_id' => array(
            'id' => 'User group must be a valid id value.',
            'required' => 'User group is required.',
            'notempty' => 'User group cannot be left empty.',
        ),
    );

}
