<?php

/**
 * Class UserGroup
 *
 * Model class for table user_groups
 *
 */
class UserGroup extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'UserGroup';

    /**
     * Default display fields for the model. When more than one, they are concatenated with an space.
     *
     * @var array
     */
    public $displayFields = array('name',);

    /**
     * Valid order by expressions. List of expression that can be used to order. Expression can be a field or a complex
     * expression. Do not include direction of order.
     *
     * @var array
     */
    public $validOrderByExpressions = array('id', 'name', 'description', 'access_level', 'created', 'modified',);

    /**
     * Has Many (father->children) relationships with other models.
     * Structure: array('RelationshipName' => array('model' => 'ModelName', 'foreign_key' => 'foreign_key_id',
     * 'options' => array())
     * By convention ModelName and RelationshipName are equal unless they cannot be.
     * By convention foreign_key will be the id of the current model on the related table Options are translated to the
     * other model get call. Where conditions will be added to the foreign_key restraints
     *
     * @var array
     */
    public $hasMany = array(
        'UserGroupUser' => array(
            'model' => 'UserGroupUser',
            'foreign_key' => 'user_group_id',
            'options' => array(),
        ),
    );

    /**
     * Validation array for fields.
     * Structure: array('field_name' => array('validation_method' => 'Validation error message.')
     * Validation methods available:
     *  - notempty: if present, value cannot be empty
     *  - required: value must be present on creation. An empty value validates.
     *  - email: value must have an email format
     *  - numeric: value must be numeric
     *  - int: value must be the representation of an integer
     *
     * @var array
     */
    public $validation = array(
        'name' => array(
            'required' => 'Name is required.',
            'notempty' => 'Name cannot be left empty.',
        ),
        'access_level' => array(
            'tinyint' => 'Access level must be a valid tinyint value.',
            'required' => 'Access level is required.',
            'notempty' => 'Access level cannot be left empty.',
        ),
    );

}
