<?php

/**
 * Class ResourceType
 *
 * Model class for table resource_types
 *
 */
class ResourceType extends AppMySQLModel {
    /**
     * Model name
     *
     * @var string
     */
    public $name = 'ResourceType';

    /**
     * Default display fields for the model. When more than one, they are concatenated with an space.
     *
     * @var array
     */
    public $displayFields = array('name',);

    /**
     * Valid order by expressions. List of expression that can be used to order. Expression can be a field or a complex
     * expression. Do not include direction of order.
     *
     * @var array
     */
    public $validOrderByExpressions = array(
        'id',
        'name',
        'created',
        'created_by_user_id',
        'modified',
        'modified_by_user_id',
    );

    /**
     * All mysql tables require created and modified fields. If this is set to true, the table also requires
     * created_by_user_id and modified_by_user_id and both area populated automatically.
     *
     * @var bool
     */
    protected $user_timestamp_fields = true;

    /**
     * Validation array for fields.
     * Structure: array('field_name' => array('validation_method' => 'Validation error message.')
     * Validation methods available:
     *  - notempty: if present, value cannot be empty
     *  - required: value must be present on creation. An empty value validates.
     *  - email: value must have an email format
     *  - numeric: value must be numeric
     *  - int: value must be the representation of an integer
     *
     * @var array
     */
    public $validation = array(
        'name' => array(
            'required' => 'Name is required.',
            'notempty' => 'Name cannot be left empty.',
        ),
        'created_by_user_id' => array(
            'id' => 'Created by user must be a valid id value.',
        ),
        'modified_by_user_id' => array(
            'id' => 'Modified by user must be a valid id value.',
        ),
    );

}
