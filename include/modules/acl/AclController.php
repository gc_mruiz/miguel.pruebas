<?php

/**
 * Module specific controller class.
 * Current module controllers inherit this class instead of the original one. All specific modifications to the controller
 * class must be done to this one instead.
 */
class AclController extends AppController {
}