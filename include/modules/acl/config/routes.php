<?php
/**
 * Route definitions.
 * Attention: parsing is made in the opposite order as listed here.
 */

// Register
Router::addRoute('acl/', array(
    'module' => 'acl',
    'controller' => 'AclAccessController',
    'action' => 'index',
));