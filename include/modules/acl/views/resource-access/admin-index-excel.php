<table>
    <tr>
        <th><?php __('Id') ?></th>
        <th><?php __('Resource') ?></th>
        <th><?php __('User') ?></th>
        <th><?php __('User id access') ?></th>
        <th><?php __('User group') ?></th>
        <th><?php __('User group access') ?></th>
        <th><?php __('Created') ?></th>
        <th><?php __('Modified') ?></th>
            </tr>
    <?php foreach($resourceAccess as $resourceAcces): ?>
    <tr>
        <td><?php echo $resourceAcces['ResourceAcces']['id'] ?>&nbsp;</td>
                            <td><?php if($resourceAcces['ResourceAcces']['resource_id']) echo $resourceAcces['Resource']['name'] ?>&nbsp;</td>
                            <td><?php if($resourceAcces['ResourceAcces']['user_id']) echo $resourceAcces['User']['id'] ?>&nbsp;</td>
                            <td><?php echo ($resourceAcces['ResourceAcces']['user_id_access'] ? __('Yes', true) : __('No', true)) ?>&nbsp;</td>
                            <td><?php if($resourceAcces['ResourceAcces']['user_group_id']) echo $resourceAcces['UserGroup']['name'] ?>&nbsp;</td>
                            <td><?php echo ($resourceAcces['ResourceAcces']['user_group_access'] ? __('Yes', true) : __('No', true)) ?>&nbsp;</td>
                            <td><?php echo ($resourceAcces['ResourceAcces']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($resourceAcces['ResourceAcces']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($resourceAcces['ResourceAcces']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($resourceAcces['ResourceAcces']['modified'])) : '') ?>&nbsp;</td>
                                </tr>
    <?php endforeach ?>
</table>