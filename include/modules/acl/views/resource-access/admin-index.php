<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
    <h2><?php __('Resource access') ?></h2>
    <ol class="breadcrumb">
            <li>
                <?php echo HtmlHelper::link(__('Home', true), array('controller' => 'Users', 'action' => 'index')) ?>
            </li>
            <li class="active">
                <strong><?php __('Resource access') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <?php echo FormHelper::button(__('add new', true), array('div' => false, 'class' => 'pull-right m btn btn-success', 'href' => array('action' => 'adminAdd'))) ?>
        <?php echo FormHelper::button(__('excel', true), array('div' => false, 'class' => 'pull-right m btn btn-info', 'href' => array('action' => 'adminIndex', 'excel' => 1))) ?>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<?php if(!empty($resourceAccess)): ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php echo HtmlHelper::flashMessage() ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered ">
                        <tr class="not-sortable">
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Id', true), 'id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Resource', true), 'resource_id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('User', true), 'user_id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('User id access', true), 'user_id_access') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('User group', true), 'user_group_id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('User group access', true), 'user_group_access') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Created', true), 'created') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Modified', true), 'modified') ?></th>
                            <th class="actions">&nbsp;</th>
                        </tr>
                        <?php
                        $altrow = false;
                        $count = 0; foreach($resourceAccess as $resourceAcces): ?>
                        <tr class="<?php if($altrow) echo 'alt' ?>">
                            <td><?php echo $resourceAcces['ResourceAcces']['id'] ?>&nbsp;</td>
                            <td><?php if($resourceAcces['ResourceAcces']['resource_id']) echo $resourceAcces['Resource']['name'] ?>&nbsp;</td>
                            <td><?php if($resourceAcces['ResourceAcces']['user_id']) echo $resourceAcces['User']['id'] ?>&nbsp;</td>
                            <td><?php echo ($resourceAcces['ResourceAcces']['user_id_access'] ? __('Yes', true) : __('No', true)) ?>&nbsp;</td>
                            <td><?php if($resourceAcces['ResourceAcces']['user_group_id']) echo $resourceAcces['UserGroup']['name'] ?>&nbsp;</td>
                            <td><?php echo ($resourceAcces['ResourceAcces']['user_group_access'] ? __('Yes', true) : __('No', true)) ?>&nbsp;</td>
                            <td><?php echo ($resourceAcces['ResourceAcces']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($resourceAcces['ResourceAcces']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($resourceAcces['ResourceAcces']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($resourceAcces['ResourceAcces']['modified'])) : '') ?>&nbsp;</td>
                            <td>
                                <span class="btn-group">
                                    <?php echo FormHelper::button(__('edit', true), array('div' => false, 'href' => array('action' => 'adminEdit', $resourceAcces['ResourceAcces']['id']), '+class' => 'btn btn-sm btn-primary')); ?>
                                    <?php echo FormHelper::button(__('delete', true), array('div' => false, 'href' => array('action' => 'adminDelete', $resourceAcces['ResourceAcces']['id']), 'confirm' => sprintf(__('Are you sure you want to delete #%s?', true), $resourceAcces['ResourceAcces']['id']), '+class' => 'btn btn-sm btn-danger')); ?>
                                </span>
                            </td>
                        </tr>
                        <?php $altrow = !$altrow; $count++; endforeach ?>
                    </table>
                    </div>
                    <?php echo PaginatorHelper::links($pagination) ?>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="ibox-content m-b-sm border-bottom">
        <?php echo HtmlHelper::flashMessage() ?>
        <div class="text-center p-lg">
            <h2><?php __('There are yet no elements to display.'); ?>
        </div>
    </div>
    <?php endif ?>
</div>
<div class="form-actions">
    <div class="left">
        <?php echo FormHelper::button(__('< return', true), array('div' => false, 'href' => array('controller' => 'Users', 'action' => 'index'), '+class' => 'btn btn-sm')); ?>
    </div>
</div>