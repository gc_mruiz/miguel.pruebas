<table>
    <tr>
        <th><?php __('Id') ?></th>
        <th><?php __('Name') ?></th>
        <th><?php __('Created') ?></th>
        <th><?php __('Modified') ?></th>
            </tr>
    <?php foreach($resourceTypes as $resourceType): ?>
    <tr>
        <td><?php echo $resourceType['ResourceType']['id'] ?>&nbsp;</td>
                            <td><?php echo $resourceType['ResourceType']['name'] ?>&nbsp;</td>
                            <td><?php echo ($resourceType['ResourceType']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($resourceType['ResourceType']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($resourceType['ResourceType']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($resourceType['ResourceType']['modified'])) : '') ?>&nbsp;</td>
                                </tr>
    <?php endforeach ?>
</table>