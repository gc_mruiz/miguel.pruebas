<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
    <h2><?php echo sprintf(__('Edit %s', true), __('user group', true)) ?></h2>
    <ol class="breadcrumb">
            <li>
                <?php echo HtmlHelper::link(__('Home', true), array('controller' => 'Users', 'action' => 'index')) ?>
            </li>
            <li>
                <?php echo HtmlHelper::link(__('User groups', true), array('action' => 'adminIndex')) ?>
            </li>
            <li class="active">
                <strong><?php __('Edit') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php echo HtmlHelper::flashMessage() ?>
                    <?php echo FormHelper::open() ?>
                    <?php echo FormHelper::input('userGroup[UserGroup][id]', 'hidden', false) ?>
                    <?php echo FormHelper::input('userGroup[UserGroup][modified_by_user_id]', 'hidden', Authentication::get('user', 'id')) ?>
                    <?php echo FormHelper::input('userGroup[UserGroup][name]', 'text', false, array('required' => null, 'maxlength' => 50, 'label' => __('Name', true))) ?>
                    <?php echo FormHelper::input('userGroup[UserGroup][description]', 'text', false, array('maxlength' => 255, 'label' => __('Description', true))) ?>
                    <?php echo FormHelper::select('userGroup[UserGroup][access_level]', array(1, 2, 3), false, array('required' => null, 'label' => __('Access level', true))) ?>
                    <div>
                        <div class="pull-right">
                            <?php echo FormHelper::button(__('cancel', true), array('div' => false, 'href' => array('action' => 'adminIndex'), '+class' => 'btn btn-sm btn-white m-t-n-xs')); ?>
                            <?php echo FormHelper::submit(__('save', true), array('div' => false, 'class' => 'btn btn-sm btn-primary m-t-n-xs')) ?>
                        </div>
                        <span class="clearfix"></span>
                    </div>
                    <?php echo FormHelper::close() ?>
                </div>
            </div>
        </div>
    </div>
</div>
