<table>
    <tr>
        <th><?php __('Id') ?></th>
        <th><?php __('Name') ?></th>
        <th><?php __('Description') ?></th>
        <th><?php __('Access level') ?></th>
        <th><?php __('Created') ?></th>
        <th><?php __('Modified') ?></th>
            </tr>
    <?php foreach($userGroups as $userGroup): ?>
    <tr>
        <td><?php echo $userGroup['UserGroup']['id'] ?>&nbsp;</td>
                            <td><?php echo $userGroup['UserGroup']['name'] ?>&nbsp;</td>
                            <td><?php echo $userGroup['UserGroup']['description'] ?>&nbsp;</td>
                            <td><?php echo ($userGroup['UserGroup']['access_level'] ? __('Yes', true) : __('No', true)) ?>&nbsp;</td>
                            <td><?php echo ($userGroup['UserGroup']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($userGroup['UserGroup']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($userGroup['UserGroup']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($userGroup['UserGroup']['modified'])) : '') ?>&nbsp;</td>
                                </tr>
    <?php endforeach ?>
</table>