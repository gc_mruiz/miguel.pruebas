<table>
    <tr>
        <th><?php __('Id') ?></th>
        <th><?php __('Name') ?></th>
        <th><?php __('Is parent') ?></th>
        <th><?php __('Resource parent') ?></th>
        <th><?php __('Resource type identifier') ?></th>
        <th><?php __('Created') ?></th>
        <th><?php __('Modified') ?></th>
            </tr>
    <?php foreach($resources as $resource): ?>
    <tr>
        <td><?php echo $resource['Resource']['id'] ?>&nbsp;</td>
                            <td><?php echo $resource['Resource']['name'] ?>&nbsp;</td>
                            <td><?php echo ($resource['Resource']['is_parent'] ? __('Yes', true) : __('No', true)) ?>&nbsp;</td>
                            <td><?php if($resource['Resource']['resource_parent_id']) echo $resource['ResourceParent']['id'] ?>&nbsp;</td>
                            <td><?php echo $resource['Resource']['resource_type_identifier'] ?>&nbsp;</td>
                            <td><?php echo ($resource['Resource']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($resource['Resource']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($resource['Resource']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($resource['Resource']['modified'])) : '') ?>&nbsp;</td>
                                </tr>
    <?php endforeach ?>
</table>