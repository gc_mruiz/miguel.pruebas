<table>
    <tr>
        <th><?php __('Id') ?></th>
        <th><?php __('Email') ?></th>
        <th><?php __('Level') ?></th>
        <th><?php __('Created') ?></th>
        <th><?php __('Modified') ?></th>
    </tr>
    <?php foreach($users as $user): ?>
    <tr>
        <td><?php echo $user['User']['id'] ?></td>
        <td><?php echo $user['User']['email'] ?></td>
        <td><?php echo $userLevels[$user['User']['level']] ?></td>
        <td><?php echo ($user['User']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($user['User']['created'])) : '') ?></td>
        <td><?php echo ($user['User']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($user['User']['modified'])) : '') ?></td>
    </tr>
    <?php endforeach ?>
</table>