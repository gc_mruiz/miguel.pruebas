<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php __('Change password') ?></h2>
        <ol class="breadcrumb">
            <li>
                <?php echo HtmlHelper::link(__('Home', true), array('action' => 'index')) ?>
            </li>
            <li>
                <a><?php __('User') ?></a>
            </li>
            <li class="active">
                <strong><?php __('Change password') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php echo HtmlHelper::flashMessage() ?>
                    <?php echo FormHelper::open() ?>
                    <?php echo FormHelper::input('user[User][old_password]', 'password', false, array('label' => __('Current password', true), 'required' => 'required')) ?>
                    <?php echo FormHelper::input('user[User][new_password]', 'password', false, array('label' => __('New password', true), 'required' => 'required')) ?>
                    <?php echo FormHelper::input('user[User][check_new_password]', 'password', false, array('label' => __('Repeat password', true), 'required' => 'required')) ?>
                    <div>
                        <div class="pull-right">
                            <?php echo FormHelper::button(__('cancel', true), array('div' => false, 'href' => array('action' => 'index'), '+class' => 'btn btn-sm btn-white m-t-n-xs')); ?>
                            <?php echo FormHelper::submit(__('change', true), array('div' => false, 'class' => 'btn btn-sm btn-primary m-t-n-xs')) ?>
                        </div>
                        <span class="clearfix"></span>
                    </div>
                    <?php echo FormHelper::close() ?>
                </div>
            </div>
        </div>
    </div>
</div>