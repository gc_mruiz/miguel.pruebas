<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">GC</h1>
        </div>
        <h3><?php __('Restore password') ?></h3>

        <?php echo FormHelper::open() ?>
        <?php echo HtmlHelper::flashMessage() ?>
        <?php echo FormHelper::input('user[User][email]', 'email', false, array('placeholder' => __('Email', true), '+class' => 'single-line', 'disabled' => 'disabled')) ?>
        <?php echo FormHelper::input('user[User][new_password]', 'password', false, array('placeholder' => __('New password', true), '+class' => 'single-line')) ?>
        <?php echo FormHelper::input('user[User][check_new_password]', 'password', false, array('placeholder' => __('Repeat password', true), '+class' => 'single-line')) ?>
        <?php echo FormHelper::submit(__('Change', true), array('div' => false, 'class' => 'btn btn-primary block full-width m-b')) ?>
        <?php echo HtmlHelper::link('<small>' . __('Back to login', true) . '</small>', array('action' => 'login'), array('escape' => false)) ?>
        <?php echo FormHelper::close() ?>

        <p class="m-t"> <small>Sareb OTP &copy; <?php echo date('Y') ?></small> </p>
    </div>
</div>