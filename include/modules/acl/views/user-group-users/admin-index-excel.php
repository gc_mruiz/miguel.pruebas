<table>
    <tr>
        <th><?php __('Id') ?></th>
        <th><?php __('User') ?></th>
        <th><?php __('User group') ?></th>
        <th><?php __('Created') ?></th>
        <th><?php __('Modified') ?></th>
            </tr>
    <?php foreach($userGroupUsers as $userGroupUser): ?>
    <tr>
        <td><?php echo $userGroupUser['UserGroupUser']['id'] ?>&nbsp;</td>
                            <td><?php if($userGroupUser['UserGroupUser']['user_id']) echo $userGroupUser['User']['id'] ?>&nbsp;</td>
                            <td><?php if($userGroupUser['UserGroupUser']['user_group_id']) echo $userGroupUser['UserGroup']['name'] ?>&nbsp;</td>
                            <td><?php echo ($userGroupUser['UserGroupUser']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($userGroupUser['UserGroupUser']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($userGroupUser['UserGroupUser']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($userGroupUser['UserGroupUser']['modified'])) : '') ?>&nbsp;</td>
                                </tr>
    <?php endforeach ?>
</table>