<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
    <h2><?php __('User group users') ?></h2>
    <ol class="breadcrumb">
            <li>
                <?php echo HtmlHelper::link(__('Home', true), array('controller' => 'Users', 'action' => 'index')) ?>
            </li>
            <li class="active">
                <strong><?php __('User group users') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <?php echo FormHelper::button(__('add new', true), array('div' => false, 'class' => 'pull-right m btn btn-success', 'href' => array('action' => 'adminAdd'))) ?>
        <?php echo FormHelper::button(__('excel', true), array('div' => false, 'class' => 'pull-right m btn btn-info', 'href' => array('action' => 'adminIndex', 'excel' => 1))) ?>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
<?php if(!empty($userGroupUsers)): ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php echo HtmlHelper::flashMessage() ?>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered ">
                        <tr class="not-sortable">
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Id', true), 'id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('User', true), 'user_id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('User group', true), 'user_group_id') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Created', true), 'created') ?></th>
                            <th><?php echo PaginatorHelper::reorder($pagination, __('Modified', true), 'modified') ?></th>
                            <th class="actions">&nbsp;</th>
                        </tr>
                        <?php
                        $altrow = false;
                        $count = 0; foreach($userGroupUsers as $userGroupUser): ?>
                        <tr class="<?php if($altrow) echo 'alt' ?>">
                            <td><?php echo $userGroupUser['UserGroupUser']['id'] ?>&nbsp;</td>
                            <td><?php if($userGroupUser['UserGroupUser']['user_id']) echo $userGroupUser['User']['id'] ?>&nbsp;</td>
                            <td><?php if($userGroupUser['UserGroupUser']['user_group_id']) echo $userGroupUser['UserGroup']['name'] ?>&nbsp;</td>
                            <td><?php echo ($userGroupUser['UserGroupUser']['created'] ? date(__('m/d/Y H:i:s', true), strtotime($userGroupUser['UserGroupUser']['created'])) : '') ?>&nbsp;</td>
                            <td><?php echo ($userGroupUser['UserGroupUser']['modified'] ? date(__('m/d/Y H:i:s', true), strtotime($userGroupUser['UserGroupUser']['modified'])) : '') ?>&nbsp;</td>
                            <td>
                                <span class="btn-group">
                                    <?php echo FormHelper::button(__('edit', true), array('div' => false, 'href' => array('action' => 'adminEdit', $userGroupUser['UserGroupUser']['id']), '+class' => 'btn btn-sm btn-primary')); ?>
                                    <?php echo FormHelper::button(__('delete', true), array('div' => false, 'href' => array('action' => 'adminDelete', $userGroupUser['UserGroupUser']['id']), 'confirm' => sprintf(__('Are you sure you want to delete #%s?', true), $userGroupUser['UserGroupUser']['id']), '+class' => 'btn btn-sm btn-danger')); ?>
                                </span>
                            </td>
                        </tr>
                        <?php $altrow = !$altrow; $count++; endforeach ?>
                    </table>
                    </div>
                    <?php echo PaginatorHelper::links($pagination) ?>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>
    <div class="ibox-content m-b-sm border-bottom">
        <?php echo HtmlHelper::flashMessage() ?>
        <div class="text-center p-lg">
            <h2><?php __('There are yet no elements to display.'); ?>
        </div>
    </div>
    <?php endif ?>
</div>
<div class="form-actions">
    <div class="left">
        <?php echo FormHelper::button(__('< return', true), array('div' => false, 'href' => array('controller' => 'Users', 'action' => 'index'), '+class' => 'btn btn-sm')); ?>
    </div>
</div>