<?php

/**
 * Class UserGroupUsersController.
 *
 * Handles a user group user related HTTP request.
 */
class UserGroupUsersController extends AppController {
    /**
     * Controller name
     */
    public $name = 'UserGroupUsers';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles userGroupUser listing.
     */
    public function adminIndex() {
        // Load UserGroupUser model
        /* @var UserGroupUser $UserGroupUser */
        $UserGroupUser = MySQLModel::getInstance('UserGroupUser');
        $UserGroupUser->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $UserGroupUser;

        // If excel download was requested, switch view
        if (get_var('excel')) {
            // Change layout and view
            $this->layout = 'excel';
            $this->view = 'admin-index-excel';

            // Set excel file name
            $this->set('filename_for_layout', 'userGroupUsers.xls');
            $this->pagination['elements_per_page'] = 100000;

            // Ignore pages
            unset($_GET['p']);
        }

        // Retrieve elements
        $userGroupUsers = $this->paginate();

        // Set view vars
        $this->set(compact('userGroupUsers'));
    }

    /**
     * Handles userGroupUser add request. Data is served via $_POST['userGroupUser']['UserGroupUser'].
     */
    public function adminAdd() {
        // Load UserGroupUser model
        /* @var UserGroupUser $UserGroupUser */
        $UserGroupUser = MySQLModel::getInstance('UserGroupUser');

        // If post data was provided
        if (post_var('userGroupUser[UserGroupUser]')) {
            // Try to add userGroupUser
            $userGroupUserData = post_var('userGroupUser[UserGroupUser]');
            $userGroupUser = $UserGroupUser->addNew($userGroupUserData);

            // Set flash title
            Session::set('flash[title]', __('User group user', true));

            // Check result
            if ($userGroupUser) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($UserGroupUser->errors) {
                    // Add all retrieved messages translated
                    foreach ($UserGroupUser->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Load belongs to models

        /* @var User $User */
        $User = MySQLModel::getInstance('User');

        /* @var UserGroup $UserGroup */
        $UserGroup = MySQLModel::getInstance('UserGroup');

        // Load belongs to lists
        $this->set('listOfUsers', $User->getList());
        $this->set('listOfUserGroups', $UserGroup->getList());
    }

    /**
     * Handles userGroupUser edit request. Data is served via $_POST['userGroupUser']['UserGroupUser']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load UserGroupUser model
        /* @var UserGroupUser $UserGroupUser */
        $UserGroupUser = MySQLModel::getInstance('UserGroupUser');

        // If post data was provided
        if (post_var('userGroupUser[UserGroupUser]')) {
            // Try to update userGroupUser

            $userGroupUserData = post_var('userGroupUser[UserGroupUser]');
            $userGroupUser = $UserGroupUser->updateFields($id, $userGroupUserData);

            // Set flash title
            Session::set('flash[title]', __('User group user', true));

            // Check result
            if ($userGroupUser) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($UserGroupUser->errors) {
                    // Add all retrieved messages translated
                    foreach ($UserGroupUser->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load userGroupUser
        $userGroupUser = $UserGroupUser->getById($id);

        // Check if there was any result
        if (empty($userGroupUser)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('userGroupUser'));

        // Load belongs to models

        /* @var User $User */
        $User = MySQLModel::getInstance('User');

        /* @var UserGroup $UserGroup */
        $UserGroup = MySQLModel::getInstance('UserGroup');

        // Load belongs to lists
        $this->set('listOfUsers', $User->getList());
        $this->set('listOfUserGroups', $UserGroup->getList());
    }

    /**
     * Handles userGroupUser delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load UserGroupUser model
        /* @var UserGroupUser $UserGroupUser */
        $UserGroupUser = MySQLModel::getInstance('UserGroupUser');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load userGroupUser
        $userGroupUser = $UserGroupUser->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($userGroupUser)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('User group user', true));

        // Try to delete
        if ($UserGroupUser->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($UserGroupUser->errors) {
                // Add all retrieved messages translated
                foreach ($UserGroupUser->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }

}