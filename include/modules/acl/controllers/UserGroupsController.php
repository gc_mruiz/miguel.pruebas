<?php

/**
 * Class UserGroupsController.
 *
 * Handles a user group related HTTP request.
 */
class UserGroupsController extends AppController {
    /**
     * Controller name
     */
    public $name = 'UserGroups';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles userGroup listing.
     */
    public function adminIndex() {
        // Load UserGroup model
        /* @var UserGroup $UserGroup */
        $UserGroup = MySQLModel::getInstance('UserGroup');
        $UserGroup->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $UserGroup;

        // If excel download was requested, switch view
        if (get_var('excel')) {
            // Change layout and view
            $this->layout = 'excel';
            $this->view = 'admin-index-excel';

            // Set excel file name
            $this->set('filename_for_layout', 'userGroups.xls');
            $this->pagination['elements_per_page'] = 100000;

            // Ignore pages
            unset($_GET['p']);
        }

        // Retrieve elements
        $userGroups = $this->paginate();

        // Set view vars
        $this->set(compact('userGroups'));
    }

    /**
     * Handles userGroup add request. Data is served via $_POST['userGroup']['UserGroup'].
     */
    public function adminAdd() {
        // Load UserGroup model
        /* @var UserGroup $UserGroup */
        $UserGroup = MySQLModel::getInstance('UserGroup');

        // If post data was provided
        if (post_var('userGroup[UserGroup]')) {
            // Try to add userGroup
            $userGroupData = post_var('userGroup[UserGroup]');
            $userGroup = $UserGroup->addNew($userGroupData);

            // Set flash title
            Session::set('flash[title]', __('User group', true));

            // Check result
            if ($userGroup) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($UserGroup->errors) {
                    // Add all retrieved messages translated
                    foreach ($UserGroup->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }
    }

    /**
     * Handles userGroup edit request. Data is served via $_POST['userGroup']['UserGroup']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load UserGroup model
        /* @var UserGroup $UserGroup */
        $UserGroup = MySQLModel::getInstance('UserGroup');

        // If post data was provided
        if (post_var('userGroup[UserGroup]')) {
            // Try to update userGroup

            $userGroupData = post_var('userGroup[UserGroup]');
            $userGroup = $UserGroup->updateFields($id, $userGroupData);

            // Set flash title
            Session::set('flash[title]', __('User group', true));

            // Check result
            if ($userGroup) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($UserGroup->errors) {
                    // Add all retrieved messages translated
                    foreach ($UserGroup->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load userGroup
        $userGroup = $UserGroup->getById($id);

        // Check if there was any result
        if (empty($userGroup)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('userGroup'));
    }

    /**
     * Handles userGroup delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load UserGroup model
        /* @var UserGroup $UserGroup */
        $UserGroup = MySQLModel::getInstance('UserGroup');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load userGroup
        $userGroup = $UserGroup->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($userGroup)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('User group', true));

        // Try to delete
        if ($UserGroup->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($UserGroup->errors) {
                // Add all retrieved messages translated
                foreach ($UserGroup->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }

}