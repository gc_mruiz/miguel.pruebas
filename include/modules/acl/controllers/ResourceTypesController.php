<?php

/**
 * Class ResourceTypesController.
 *
 * Handles a resource type related HTTP request.
 */
class ResourceTypesController extends AppController {
    /**
     * Controller name
     */
    public $name = 'ResourceTypes';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles resourceType listing.
     */
    public function adminIndex() {
        // Load ResourceType model
        /* @var ResourceType $ResourceType */
        $ResourceType = MySQLModel::getInstance('ResourceType');
        $ResourceType->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $ResourceType;

        // If excel download was requested, switch view
        if (get_var('excel')) {
            // Change layout and view
            $this->layout = 'excel';
            $this->view = 'admin-index-excel';

            // Set excel file name
            $this->set('filename_for_layout', 'resourceTypes.xls');
            $this->pagination['elements_per_page'] = 100000;

            // Ignore pages
            unset($_GET['p']);
        }

        // Retrieve elements
        $resourceTypes = $this->paginate();

        // Set view vars
        $this->set(compact('resourceTypes'));
    }

    /**
     * Handles resourceType add request. Data is served via $_POST['resourceType']['ResourceType'].
     */
    public function adminAdd() {
        // Load ResourceType model
        /* @var ResourceType $ResourceType */
        $ResourceType = MySQLModel::getInstance('ResourceType');

        // If post data was provided
        if (post_var('resourceType[ResourceType]')) {
            // Try to add resourceType
            $resourceTypeData = post_var('resourceType[ResourceType]');
            $resourceType = $ResourceType->addNew($resourceTypeData);

            // Set flash title
            Session::set('flash[title]', __('Resource type', true));

            // Check result
            if ($resourceType) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($ResourceType->errors) {
                    // Add all retrieved messages translated
                    foreach ($ResourceType->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }
    }

    /**
     * Handles resourceType edit request. Data is served via $_POST['resourceType']['ResourceType']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load ResourceType model
        /* @var ResourceType $ResourceType */
        $ResourceType = MySQLModel::getInstance('ResourceType');

        // If post data was provided
        if (post_var('resourceType[ResourceType]')) {
            // Try to update resourceType

            $resourceTypeData = post_var('resourceType[ResourceType]');
            $resourceType = $ResourceType->updateFields($id, $resourceTypeData);

            // Set flash title
            Session::set('flash[title]', __('Resource type', true));

            // Check result
            if ($resourceType) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($ResourceType->errors) {
                    // Add all retrieved messages translated
                    foreach ($ResourceType->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load resourceType
        $resourceType = $ResourceType->getById($id);

        // Check if there was any result
        if (empty($resourceType)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('resourceType'));
    }

    /**
     * Handles resourceType delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load ResourceType model
        /* @var ResourceType $ResourceType */
        $ResourceType = MySQLModel::getInstance('ResourceType');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load resourceType
        $resourceType = $ResourceType->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($resourceType)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('Resource type', true));

        // Try to delete
        if ($ResourceType->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($ResourceType->errors) {
                // Add all retrieved messages translated
                foreach ($ResourceType->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }

}