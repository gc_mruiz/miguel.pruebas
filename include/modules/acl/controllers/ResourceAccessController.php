<?php

/**
 * Class ResourceAccessController.
 *
 * Handles a resource acces related HTTP request.
 */
class ResourceAccessController extends AppController {
    /**
     * Controller name
     */
    public $name = 'ResourceAccess';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles resourceAcces listing.
     */
    public function adminIndex() {
        // Load ResourceAcces model
        /* @var ResourceAcces $ResourceAcces */
        $ResourceAcces = MySQLModel::getInstance('ResourceAcces');
        $ResourceAcces->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $ResourceAcces;

        // If excel download was requested, switch view
        if (get_var('excel')) {
            // Change layout and view
            $this->layout = 'excel';
            $this->view = 'admin-index-excel';

            // Set excel file name
            $this->set('filename_for_layout', 'resourceAccess.xls');
            $this->pagination['elements_per_page'] = 100000;

            // Ignore pages
            unset($_GET['p']);
        }

        // Retrieve elements
        $resourceAccess = $this->paginate();

        // Set view vars
        $this->set(compact('resourceAccess'));
    }

    /**
     * Handles resourceAcces add request. Data is served via $_POST['resourceAcces']['ResourceAcces'].
     */
    public function adminAdd() {
        // Load ResourceAcces model
        /* @var ResourceAcces $ResourceAcces */
        $ResourceAcces = MySQLModel::getInstance('ResourceAcces');

        // If post data was provided
        if (post_var('resourceAcces[ResourceAcces]')) {
            // Try to add resourceAcces
            $resourceAccesData = post_var('resourceAcces[ResourceAcces]');
            $resourceAcces = $ResourceAcces->addNew($resourceAccesData);

            // Set flash title
            Session::set('flash[title]', __('Resource acces', true));

            // Check result
            if ($resourceAcces) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($ResourceAcces->errors) {
                    // Add all retrieved messages translated
                    foreach ($ResourceAcces->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Load belongs to models

        /* @var Resource $Resource */
        $Resource = MySQLModel::getInstance('Resource');

        /* @var User $User */
        $User = MySQLModel::getInstance('User');

        /* @var UserGroup $UserGroup */
        $UserGroup = MySQLModel::getInstance('UserGroup');

        // Load belongs to lists
        $this->set('listOfResources', $Resource->getList());
        $this->set('listOfUsers', $User->getList());
        $this->set('listOfUserGroups', $UserGroup->getList());
    }

    /**
     * Handles resourceAcces edit request. Data is served via $_POST['resourceAcces']['ResourceAcces']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load ResourceAcces model
        /* @var ResourceAcces $ResourceAcces */
        $ResourceAcces = MySQLModel::getInstance('ResourceAcces');

        // If post data was provided
        if (post_var('resourceAcces[ResourceAcces]')) {
            // Try to update resourceAcces

            $resourceAccesData = post_var('resourceAcces[ResourceAcces]');
            $resourceAcces = $ResourceAcces->updateFields($id, $resourceAccesData);

            // Set flash title
            Session::set('flash[title]', __('Resource acces', true));

            // Check result
            if ($resourceAcces) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($ResourceAcces->errors) {
                    // Add all retrieved messages translated
                    foreach ($ResourceAcces->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load resourceAcces
        $resourceAcces = $ResourceAcces->getById($id);

        // Check if there was any result
        if (empty($resourceAcces)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('resourceAcces'));

        // Load belongs to models

        /* @var Resource $Resource */
        $Resource = MySQLModel::getInstance('Resource');

        /* @var User $User */
        $User = MySQLModel::getInstance('User');

        /* @var UserGroup $UserGroup */
        $UserGroup = MySQLModel::getInstance('UserGroup');

        // Load belongs to lists
        $this->set('listOfResources', $Resource->getList());
        $this->set('listOfUsers', $User->getList());
        $this->set('listOfUserGroups', $UserGroup->getList());
    }

    /**
     * Handles resourceAcces delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load ResourceAcces model
        /* @var ResourceAcces $ResourceAcces */
        $ResourceAcces = MySQLModel::getInstance('ResourceAcces');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load resourceAcces
        $resourceAcces = $ResourceAcces->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($resourceAcces)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('Resource acces', true));

        // Try to delete
        if ($ResourceAcces->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($ResourceAcces->errors) {
                // Add all retrieved messages translated
                foreach ($ResourceAcces->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }

}