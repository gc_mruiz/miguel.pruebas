<?php

/**
 * Class ResourcesController.
 *
 * Handles a resource related HTTP request.
 */
class ResourcesController extends AppController {
    /**
     * Controller name
     */
    public $name = 'Resources';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
    );

    /**
     * Handles resource listing.
     */
    public function adminIndex() {
        // Load Resource model
        /* @var Resource $Resource */
        $Resource = MySQLModel::getInstance('Resource');
        $Resource->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $Resource;

        // If excel download was requested, switch view
        if (get_var('excel')) {
            // Change layout and view
            $this->layout = 'excel';
            $this->view = 'admin-index-excel';

            // Set excel file name
            $this->set('filename_for_layout', 'resources.xls');
            $this->pagination['elements_per_page'] = 100000;

            // Ignore pages
            unset($_GET['p']);
        }

        // Retrieve elements
        $resources = $this->paginate();

        // Set view vars
        $this->set(compact('resources'));
    }

    /**
     * Handles resource add request. Data is served via $_POST['resource']['Resource'].
     */
    public function adminAdd() {
        // Load Resource model
        /* @var Resource $Resource */
        $Resource = MySQLModel::getInstance('Resource');

        // If post data was provided
        if (post_var('resource[Resource]')) {
            // Try to add resource
            $resourceData = post_var('resource[Resource]');
            $resource = $Resource->addNew($resourceData);

            // Set flash title
            Session::set('flash[title]', __('Resource', true));

            // Check result
            if ($resource) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($Resource->errors) {
                    // Add all retrieved messages translated
                    foreach ($Resource->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Load belongs to lists
        $this->set('listOfResourceParents', $Resource->getList('id', null, array('where' => array('is_parent' => 1))));

        /* @var ResourceType $ResourceType */
        $ResourceType = MySQLModel::getInstance('ResourceType');
        
        $this->set('listOfResourceTypes', $ResourceType->getList());
    }

    /**
     * Handles resource edit request. Data is served via $_POST['resource']['Resource']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load Resource model
        /* @var Resource $Resource */
        $Resource = MySQLModel::getInstance('Resource');

        // If post data was provided
        if (post_var('resource[Resource]')) {
            // Try to update resource

            $resourceData = post_var('resource[Resource]');
            $resource = $Resource->updateFields($id, $resourceData);

            // Set flash title
            Session::set('flash[title]', __('Resource', true));

            // Check result
            if ($resource) {
                // Success, set flash message and redirect
                Session::set('flash[message]', __('Changes saved.', true));

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($Resource->errors) {
                    // Add all retrieved messages translated
                    foreach ($Resource->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load resource
        $resource = $Resource->getById($id);

        // Check if there was any result
        if (empty($resource)) {
            $this->notFound();
        }

        // Set view vars
        $this->set(compact('resource'));

        // Load belongs to lists
        $this->set('listOfResourceParents', $Resource->getList('id', null, array('where' => array('is_parent' => 1, 'id !=' => $id))));
    }

    /**
     * Handles resource delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load Resource model
        /* @var Resource $Resource */
        $Resource = MySQLModel::getInstance('Resource');

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load resource
        $resource = $Resource->getById($id, array('recursive' => -1, 'fields' => array('id')));

        // Check if there was any result
        if (empty($resource)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('Resource', true));

        // Try to delete
        if ($Resource->deleteById($id)) {
            // Success, set flash message
            Session::set('flash[message]', __('Deleted.', true));
        } else {
            // Set flash message
            $flash_message = __('Unable to delete.', true);

            // Check for errors
            if ($Resource->errors) {
                // Add all retrieved messages translated
                foreach ($Resource->errors as $error) {
                    $flash_message .= ' ' . $error;
                }
            }

            // Add retry message
            $flash_message .= ' ' . __('Please try again.', true);
            Session::set('flash[message]', $flash_message);
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }

}