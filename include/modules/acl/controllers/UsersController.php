<?php

/**
 * Class UsersController.
 *
 * Handles an user related HTTP request.
 */
class UsersController extends AppController {
    /**
     * Controller name
     */
    public $name = 'Users';

    /**
     * @see Controller::$actionAccessLevel
     */
    protected $actionAccessLevel = array(
        '*' => 0,
        'adminAdd' => 3,
        'adminDelete' => 3,
        'adminEdit' => 3,
        'adminIndex' => 3,
        'login' => -1,
        'forgotPassword' => -1,
        'reset' => -1,
    );

    /**
     * Handles user listing.
     */
    public function adminIndex() {
        // Load User model
        $User = MySQLModel::getInstance('User');
        $User->recursive = 0;

        // Pagination config
        $this->pagination['model'] =& $User;
        $this->pagination['where'] = array('level <' => Authentication::get('user', 'level'));

        // If excel download was requested, switch view
        if (get_var('excel')) {
            // Change layout and view
            $this->layout = 'excel';
            $this->view = 'admin-index-excel';

            // Set excel file name
            $this->set('filename_for_layout', 'users.xls');
            $this->pagination['elements_per_page'] = 100000;

            // Ignore pages
            unset($_GET['p']);
        }

        // Retrieve elements
        $users = $this->paginate();

        // Set view vars
        $this->set(compact('users'));
    }

    /**
     * Handles user add request. Data is served via $_POST['user']['User'].
     */
    public function adminAdd() {
        // Load User model
        $User = MySQLModel::getInstance('User');

        // If post data was provided
        if (post_var('user[User]')) {
            $userData = post_var('user[User]');

            // You cannot create an user greater nor equal
            if (array_key_exists('level', $userData) && intval($userData['level']) >= Authentication::get('user', 'level')) {
                $userData['level'] = Authentication::get('user', 'level') - 1;
            }

            $userData['created_by_user_id'] = Authentication::get('user', 'id');

            // Try to add user
            $user = $User->addNew($userData);

            // Check result
            if ($user) {
                // Add user groups
                /* @var UserGroupUser $UserGroupUser */
                $UserGroupUser = MySQLModel::getInstance('UserGroupUser');
                $UserGroupUser->recursive = -1;

                $userGroupUsers = $this->originalPost['userGroup']['UserGroup']['id'];
                $success = true;

                foreach ($userGroupUsers as $user_group_user) {
                    if (is_numeric($user_group_user)) {
                        if (!$UserGroupUser->addNew(array(
                                                        'user_id' => $user['User']['id'],
                                                        'user_group_id' => $user_group_user,
                                                    ))) {
                            $success = false;
                        }
                    }
                }

                // Success, set flash message and redirect
                Session::set('flash[title]', __('User', true));

                if ($success) {
                    Session::set('flash[message]', __('Changes saved.', true));
                } else {
                    Session::set('flash[message]', __('Changes saved but could not be added to some user groups.', true));
                }

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                Session::set('flash[title]', __('User', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($User->errors) {
                    // Add all retrieved messages translated
                    foreach ($User->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        /* @var UserGroup $UserGroups */
        $UserGroups = MySQLModel::getInstance('UserGroup');
        $UserGroups->recursive = -1;

        $userGroups = $UserGroups->getList('id', null, array('where' => array('access_level <=' => Authentication::get('user', 'level'))));

        // Set view vars
        $this->set(compact('userGroups'));
    }

    /**
     * Handles user edit request. Data is served via $_POST['user']['User']
     *
     * @param $id int
     */
    public function adminEdit($id = null) {
        // Load User model
        $User = MySQLModel::getInstance('User');
        $User->recursive = 0;

        // Set user conditions (other than the id)
        $userConditions = array('level <' => Authentication::get('user', 'level'));

        // If post data was provided
        if (post_var('user[User]')) {
            $userData = post_var('user[User]');

            // You cannot upgrade an user greater nor equal
            if (array_key_exists('level', $userData) && intval($userData['level']) >= Authentication::get('user', 'level')) {
                $userData['level'] = Authentication::get('user', 'level') - 1;
            }

            // If user is editing himself, session need to be updated
            $editing_himself = (Authentication::get('user', 'id') == $id);

            // If password was sent empty, remove it
            if (array_key_exists('password', $userData) && !$userData['password']) {
                unset($userData['password']);
                unset($userData['repeat_password']);
            }

            $userData['modified_by_user_id'] = Authentication::get('user', 'id');

            // Try to update user
            $user = $User->updateFields($id, $userData);

            // Check result
            if ($user) {
                if ($editing_himself) {
                    // Update session
                    Authentication::set('user', 'email', $user['User']['email']);
                }
                // Add user groups
                /* @var UserGroupUser $UserGroupUser */
                $UserGroupUser = MySQLModel::getInstance('UserGroupUser');
                $UserGroupUser->recursive = -1;

                // First we remove all user's user groups.
                $UserGroupUser->deleteAll(array('where' => array('user_id' => $user['User']['id'])));

                $userGroupUsers = $this->originalPost['userGroup']['UserGroup']['id'];
                $success = true;

                foreach ($userGroupUsers as $user_group_user) {
                    if (is_numeric($user_group_user)) {
                        if (!$UserGroupUser->addNew(array(
                                                        'user_id' => $user['User']['id'],
                                                        'user_group_id' => $user_group_user,
                                                        'created_by_user_id' => Authentication::get('user', 'id'),
                                                    ))) {
                            $success = false;
                        }
                    }
                }

                // Success, set flash message and redirect
                Session::set('flash[title]', __('User', true));

                if ($success) {
                    Session::set('flash[message]', __('Changes saved.', true));
                } else {
                    Session::set('flash[message]', __('Changes saved but could not be added to some user groups.', true));
                }

                // Redirect
                $this->redirect(array('action' => 'adminIndex'));
            } else {
                // Set flash message
                Session::set('flash[title]', __('User', true));
                $flash_message = __('Unable to save changes.', true);

                // Check for errors
                if ($User->errors) {
                    // Add all retrieved messages translated
                    foreach ($User->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        }

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Load user
        $user = $User->getById($id, array('where' => $userConditions));

        // Check if there was any result
        if (empty($user)) {
            $this->notFound();
        }

        /* @var UserGroup $UserGroups */
        $UserGroups = MySQLModel::getInstance('UserGroup');
        $UserGroups->recursive = -1;

        $userGroups = $UserGroups->getList('id', null, array('where' => array('access_level <=' => Authentication::get('user', 'level'))));

        // Set view vars
        $this->set(compact('user', 'userGroups'));
    }

    /**
     * Handles user delete request.
     *
     * @param $id int
     */
    public function adminDelete($id = null) {
        // Load User model
        $User = MySQLModel::getInstance('User');
        $User->recursive = 0;

        // Check if and id was provided
        if (!$id) {
            $this->notFound();
        }

        // Set user conditions (other than the id)
        $userConditions = array('level <' => Authentication::get('user', 'level'));

        // Load user
        $user = $User->getById($id, array(
            'where' => $userConditions,
            'recursive' => -1,
            'fields' => array('id', 'email'),
        ));

        // Check if there was any result
        if (empty($user)) {
            $this->notFound();
        }

        // Set flash title
        Session::set('flash[title]', __('User', true));

        if (Authentication::get('user', 'email') != $user['User']['email']) {
            // Try to delete
            if ($User->deleteById($id)) {
                // Success, set flash message
                Session::set('flash[message]', __('Deleted.', true));
            } else {
                // Set flash message
                $flash_message = __('Unable to delete.', true);

                // Check for errors
                if ($User->errors) {
                    // Add all retrieved messages translated
                    foreach ($User->errors as $error) {
                        $flash_message .= ' ' . $error;
                    }
                }

                // Add retry message
                $flash_message .= ' ' . __('Please try again.', true);
                Session::set('flash[message]', $flash_message);
            }
        } else {
            // Error, set flash
            Session::set('flash[message]', __('You cannot delete yourself.', true));
        }

        // Redirect to index
        if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER']) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->redirect(array('action' => 'adminIndex'));
        }
    }

    /**
     * Handles user change password request. Data is served via $_POST['user']['User']
     */
    public function changePassword() {
        // If post data was provided
        if (post_var('user[User]')) {
            // Set flash title
            Session::set('flash[title]', __('Password change', true));

            // Initialize user model
            $User = MySQLModel::getInstance('User');

            // Check if password repeat has been fulfilled
            if (post_var('user[User][new_password]') == post_var('user[User][check_new_password]')) {
                // Attempt to change password
                if ($User->changePassword(Authentication::get('user', 'email'), post_var('user[User][old_password]'), post_var('user[User][new_password]'))) {
                    // Set success flash
                    Session::set('flash[message]', __('Password changed.', true));

                    // Redirect
                    $this->redirect(array('action' => 'index'));
                } else {
                    // Set flash message
                    $flash_message = __('Could not change password.', true);

                    // Check for errors
                    if ($User->errors) {
                        // Add all retrieved messages translated
                        foreach ($User->errors as $error) {
                            $flash_message .= ' ' . $error;
                        }
                    }

                    // Add retry message
                    $flash_message .= ' ' . __('Please try again.', true);
                    Session::set('flash[message]', $flash_message);
                }
            } else {
                Session::set('flash[message]', __('Passwords do not match.', true));
            }
        }
    }

    /**
     * User home page.
     */
    public function index() {
        gtrace(Authentication::get('user'));
    }

    /**
     * Handles user login request. Data is served via $_POST['user']['User']
     */
    public function login() {
        // Set layout
        $this->layout = 'login';

        // If user is already logged in, redirect
        if (Authentication::get('user', 'id')) {
            if (get_var('return') && strpos(get_var('return'), 'users/login/') === false) {
                // If return parameter exists and does not point to the login page, redirect to it
                $this->redirect(ROOT_URL . get_var('return'));
            } else {
                // If not, redirect to the user's home page
                $this->redirect(array('action' => 'index'));
            }
        }

        // If post data was sent
        if (post_var('user[User]')) {
            // Initialize User model
            $User = MySQLModel::getInstance('User');

            // Check provided credentials
            if ($User->checkCredentials(post_var('user[User][email]'), post_var('user[User][password]'))) {
                // Retrieve logged user data
                $user = $User->getByEmail(post_var('user[User][email]'), array(
                    'recursive' => -1,
                ));

                $sessionUserData = array(
                    'User' => array(
                        'id' => $user['User']['id'],
                        'email' => $user['User']['email'],
                    ),
                );

                $userGroups = $User->getUserGroups($user['User']['id']);

                $sessionUserData['User']['level'] = $userGroups['access_level'];

                unset($userGroups['access_level']);
                $sessionUserData['User']['UserGroup'] = $userGroups;

                // Store data in session
                Authentication::set('user', $sessionUserData['User']);

                // Redirect
                if (get_var('return') && strpos(get_var('return'), 'users/login/') === false) {
                    // If return parameter exists and does not point to the login page, redirect to it
                    $this->redirect(ROOT_URL . get_var('return'));
                } else {
                    // If not, redirect to the user's home page
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                // Set flash message
                Session::set('flash[title]', __('Restricted area', true));
                $flash_message = '';

                // Check for errors
                if ($User->errors) {
                    // Add all retrieved messages translated
                    foreach ($User->errors as $error) {
                        $flash_message .= ($flash_message ? ' ' : '') . $error;
                    }
                }

                // Set flash
                Session::set('flash[message]', $flash_message);
            }
        }
    }

    /**
     * Handles user logout request.
     */
    public function logout() {
        // Destroys session
        Session::destroy();

        // Redirect to the user's home page
        $this->redirect(array('action' => 'index'));
    }

    /**
     * Handles user restore password request. Data is served via $_POST['user']['User']
     */
    public function forgotPassword() {
        // Set layout
        $this->layout = 'login';

        // If post data was provided
        if (post_var('user[User]')) {
            // Attempt to send a restoration email to the user
            $User = MySQLModel::getInstance('User');
            $User->sendPasswordResetLink(post_var('user[User][email]'));

            // For security reasons we do not disclose if the user is really registered.
            Session::set('flash', array(
                'title' => __('Forgotten password', true),
                'message' => __('If the user is registered on the site, he or she will receive and email with a link to reset the account password.', true),
            ));
        }
    }

    /**
     * Handles user reset password request. Data is served via $_POST['user']['User']
     *
     * @param $email string
     * @param $reset_password_token string
     */
    public function reset($email = null, $reset_password_token = null) {
        // Set layout
        $this->layout = 'login';

        // If user is already logged in, redirect
        if (Authentication::get('user', 'id')) {
            if (get_var('return') && strpos(get_var('return'), 'users/login/') === false) {
                // If return parameter exists and does not point to the login page, redirect to it
                $this->redirect(ROOT_URL . get_var('return'));
            } else {
                // If not, redirect to the user's home page
                $this->redirect(array('action' => 'index'));
            }
        }

        // Initialize model User
        $User = MySQLModel::getInstance('User');

        // On the first load, vars will be passed by $_GET
        if ($email && $reset_password_token) {
            // Check provided token
            if (!$User->checkToken($email, $reset_password_token)) {
                // Token was not valid, generate error message
                $flash_message = '';

                // Check for errors
                if ($User->errors) {
                    // Add all retrieved messages translated
                    foreach ($User->errors as $error) {
                        $flash_message .= ($flash_message ? ' ' : '') . $error;
                    }
                }

                // Set flash
                Session::set('flash[message]', $flash_message);
                $this->redirect(array('action' => 'login'));
            }

            // Set $data['user'] for the view
            $this->set('user', array('User' => array('email' => $email)));
        } else {
            // Invalid request, redirect to the home page
            $this->redirect(array('action' => 'index'));
        }

        // If post data was sent
        if (post_var('user[User]')) {
            // Set flash title
            Session::set('flash[title]', __('Reset password', true));

            // Check password repetition
            if (post_var('user[User][new_password]') == post_var('user[User][check_new_password]')) {
                // Attempt to restore password
                if ($User->resetPassword($email, $reset_password_token, post_var('user[User][new_password]'))) {
                    // Success, set flahs
                    Session::set('flash[message]', __('Password changed.', true));

                    // Redirect to login
                    $this->redirect(array('action' => 'login'));
                } else {
                    // Set flash message
                    $flash_message = __('Could not change password.', true);

                    // Check for errors
                    if ($User->errors) {
                        // Add all retrieved messages translated
                        foreach ($User->errors as $error) {
                            $flash_message .= ' ' . $error;
                        }
                    }

                    // Add retry message
                    $flash_message .= ' ' . __('Please try again.', true);
                    Session::set('flash[message]', $flash_message);
                }
            } else {
                // Password were not repeated correctly
                Session::set('flash[message]', __('Passwords do not match.', true));
            }
        }
    }
}