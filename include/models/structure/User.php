<?php return array (
  'id' => 
  array (
    'COLUMN_NAME' => 'id',
    'ORDINAL_POSITION' => '1',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'NO',
    'DATA_TYPE' => 'int',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => '10',
    'NUMERIC_SCALE' => '0',
    'DATETIME_PRECISION' => NULL,
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => 'PRI',
    'EXTRA' => 'auto_increment',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'email' => 
  array (
    'COLUMN_NAME' => 'email',
    'ORDINAL_POSITION' => '2',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'NO',
    'DATA_TYPE' => 'varchar',
    'CHARACTER_MAXIMUM_LENGTH' => '150',
    'CHARACTER_OCTET_LENGTH' => '600',
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => NULL,
    'CHARACTER_SET_NAME' => 'utf8mb4',
    'COLLATION_NAME' => 'utf8mb4_general_ci',
    'COLUMN_KEY' => 'UNI',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'password_digest' => 
  array (
    'COLUMN_NAME' => 'password_digest',
    'ORDINAL_POSITION' => '3',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'NO',
    'DATA_TYPE' => 'varchar',
    'CHARACTER_MAXIMUM_LENGTH' => '32',
    'CHARACTER_OCTET_LENGTH' => '128',
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => NULL,
    'CHARACTER_SET_NAME' => 'utf8mb4',
    'COLLATION_NAME' => 'utf8mb4_general_ci',
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'salt' => 
  array (
    'COLUMN_NAME' => 'salt',
    'ORDINAL_POSITION' => '4',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'NO',
    'DATA_TYPE' => 'varchar',
    'CHARACTER_MAXIMUM_LENGTH' => '10',
    'CHARACTER_OCTET_LENGTH' => '40',
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => NULL,
    'CHARACTER_SET_NAME' => 'utf8mb4',
    'COLLATION_NAME' => 'utf8mb4_general_ci',
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'reset_password_token' => 
  array (
    'COLUMN_NAME' => 'reset_password_token',
    'ORDINAL_POSITION' => '5',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'YES',
    'DATA_TYPE' => 'varchar',
    'CHARACTER_MAXIMUM_LENGTH' => '255',
    'CHARACTER_OCTET_LENGTH' => '1020',
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => NULL,
    'CHARACTER_SET_NAME' => 'utf8mb4',
    'COLLATION_NAME' => 'utf8mb4_general_ci',
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'failed_attempts' => 
  array (
    'COLUMN_NAME' => 'failed_attempts',
    'ORDINAL_POSITION' => '6',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'YES',
    'DATA_TYPE' => 'int',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => '10',
    'NUMERIC_SCALE' => '0',
    'DATETIME_PRECISION' => NULL,
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'banned' => 
  array (
    'COLUMN_NAME' => 'banned',
    'ORDINAL_POSITION' => '7',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'YES',
    'DATA_TYPE' => 'datetime',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => '0',
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'banned_reason' => 
  array (
    'COLUMN_NAME' => 'banned_reason',
    'ORDINAL_POSITION' => '8',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'YES',
    'DATA_TYPE' => 'varchar',
    'CHARACTER_MAXIMUM_LENGTH' => '255',
    'CHARACTER_OCTET_LENGTH' => '1020',
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => NULL,
    'CHARACTER_SET_NAME' => 'utf8mb4',
    'COLLATION_NAME' => 'utf8mb4_general_ci',
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'last_login' => 
  array (
    'COLUMN_NAME' => 'last_login',
    'ORDINAL_POSITION' => '9',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'YES',
    'DATA_TYPE' => 'datetime',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => '0',
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'last_password_change' => 
  array (
    'COLUMN_NAME' => 'last_password_change',
    'ORDINAL_POSITION' => '10',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'YES',
    'DATA_TYPE' => 'datetime',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => '0',
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'last_reset_request' => 
  array (
    'COLUMN_NAME' => 'last_reset_request',
    'ORDINAL_POSITION' => '11',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'YES',
    'DATA_TYPE' => 'datetime',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => '0',
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'modified_by_user_id' => 
  array (
    'COLUMN_NAME' => 'modified_by_user_id',
    'ORDINAL_POSITION' => '12',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'YES',
    'DATA_TYPE' => 'int',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => '10',
    'NUMERIC_SCALE' => '0',
    'DATETIME_PRECISION' => NULL,
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'created' => 
  array (
    'COLUMN_NAME' => 'created',
    'ORDINAL_POSITION' => '13',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'NO',
    'DATA_TYPE' => 'datetime',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => '0',
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
  'modified' => 
  array (
    'COLUMN_NAME' => 'modified',
    'ORDINAL_POSITION' => '14',
    'COLUMN_DEFAULT' => NULL,
    'IS_NULLABLE' => 'NO',
    'DATA_TYPE' => 'datetime',
    'CHARACTER_MAXIMUM_LENGTH' => NULL,
    'CHARACTER_OCTET_LENGTH' => NULL,
    'NUMERIC_PRECISION' => NULL,
    'NUMERIC_SCALE' => NULL,
    'DATETIME_PRECISION' => '0',
    'CHARACTER_SET_NAME' => NULL,
    'COLLATION_NAME' => NULL,
    'COLUMN_KEY' => '',
    'EXTRA' => '',
    'COLUMN_COMMENT' => '',
    'GENERATION_EXPRESSION' => '',
  ),
);