<?php

// Load main class and run
include_once(__DIR__ . '/include/core/GestyMVC.php');
GestyMVC::initialize();

// Initialize session
Session::init(false);

// Avoid unauthenticated access
if (Authentication::get('user', 'level') < 3) {
    header('Location: ./');
    exit();
}

// Set vars as if user had already logged in
if (!isset($_GET['server']) && !isset($_GET['file'])) {
    $dbConfig = GestyMVC::dbConfig();

    $_POST['auth'] = array();
    $_POST['auth']['driver'] = 'server';
    $_POST['auth']['server'] = $dbConfig['host'];
    $_POST['auth']['username'] = $dbConfig['user'];
    $_POST['auth']['password'] = $dbConfig['password'];
    $_POST['auth']['db'] = $dbConfig['schema'];
}

// Inclued adminer library
include_once(ROOT_PATH . 'include/vendors/adminer-4.2.1-en.php');

// Adminer switches the id, we need to reload it
Session::reloadId();